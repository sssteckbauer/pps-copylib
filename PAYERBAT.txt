      *---------------------------------------------------------*
      *  COPY MEMBER PAYERBAT. BATCH SEGMENT FOR THE PAYERROR   *
      *  FILE. KEY = GGGBBB0000000 FOR HEADER, GGGBBB9999999    *
      *  FOR BATCH TRAILER. USE COPY MEMBER PAYERREC FOR THE    *
      *  HEADER PORTION OF THIS RECORD.                         *
      *---------------------------------------------------------*
           05  ER-BAT-SEGMENT      REDEFINES ER-SEGMENT-DATA.
               10  ER-BAT-TYPE              PIC X(01).
               10  ER-BAT-ORG               PIC X(03).
               10  ER-BAT-NUM-TRANS-SETS    PIC 9(03).
               10  ER-BAT-NUM-TRANS         PIC 9(03).
               10  ER-BAT-NUM-MSGS          PIC 9(04).
               10  ER-BAT-SEQUENCE          PIC X(05).

               10  FILLER                   PIC X(217).
