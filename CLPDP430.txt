000010*==========================================================%      DS795
000020*=    COPY MEMBER: CLPDP430                               =%      DS795
000030*=    CHANGE # DS795        PROJ. REQUEST: LE/370         =%      DS795
000040*=                                         CONVERSION     =%      DS795
000050*=    NAME: R. SWIDERSKI    MODIFICATION DATE: 09/01/93   =%      DS795
000060*=                                                        =%      DS795
000070*=    DESCRIPTION:                                        =%      DS795
000080*=     REPLACED TIME 'ACCEPT' STATEMENT WITH              =%      DS795
000090*=     LE/370 VERSION OF CPPDTIME                         =%      DS795
000091*==========================================================%      DS795
000100*                                                                 CLPDP430
000200*      CONTROL REPORT PPP430CR                                    CLPDP430
000300*                                                                 CLPDP430
000400*                                                                 CLPDP430
000500 PRINT-CONTROL-REPORT SECTION.                                    CLPDP430
000600                                                                  CLPDP430
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP430
000800                                                                  CLPDP430
000900     MOVE 'PPP430CR'             TO CR-HL1-RPT.                   CLPDP430
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP430
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP430
001200                                                                  CLPDP430
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP430
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP430
001500                                                                  CLPDP430
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP430
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP430
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP430
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP430
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP430
002100                                                                  CLPDP430
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP430
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP430
002400                                                                  CLPDP430
002500     MOVE NO-CONTROL-CARD        TO CR-DL5-CTL-CARD.              CLPDP430
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP430
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP430
002800                                                                  CLPDP430
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP430
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP430
003100                                                                  CLPDP430
003200*    ACCEPT TIME-WORK-AREA FROM TIME.                             DS795
003300     COPY CPPDTIME.                                               DS795
003310     MOVE PRE-EDIT-TIME          TO TIME-WORK-AREA.               DS795
003400     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP430
003500     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP430
003600     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP430
003700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP430
003800                                                                  CLPDP430
003900     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP430
004000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP430
004100*                                                                 CLPDP430
004200*  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP430
004300*                                                                 CLPDP430
004400*  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP430
004500*  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP430
004600*                                                                 CLPDP430
004700*                                                                 CLPDP430
004800     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP430
004900     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP430
005000                                                                  CLPDP430
005100     CLOSE CONTROLREPORT.                                         CLPDP430
