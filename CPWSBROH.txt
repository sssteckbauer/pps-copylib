000100**************************************************************/   36330704
000200*  COPYMEMBER: CPWSBROH                                      */   36330704
000300*  RELEASE: ____0704____  SERVICE REQUEST(S): ____3633____   */   36330704
000400*  NAME:    ___B.I.T.___  CREATION DATE:      __10/02/92__   */   36330704
000500*  DESCRIPTION:                                              */   36330704
000600*    DCLGEN FOR THE PPPVBROH VIEW                            */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900******************************************************************CPWSBROH
001000* COBOL DECLARATION FOR TABLE PAYCXD.PPPVBROH_BROH               *CPWSBROH
001100******************************************************************CPWSBROH
001200*01  DCLPPPVBROH-BROH.                                            CPWSBROH
001300     10 BRO-CBUC             PIC X(2).                            CPWSBROH
001400     10 BRO-REP              PIC X(1).                            CPWSBROH
001500     10 BRO-SHC              PIC X(1).                            CPWSBROH
001600     10 BRO-DUC              PIC X(1).                            CPWSBROH
001700     10 BRO-PLAN-CODE        PIC X(2).                            CPWSBROH
001800     10 BRO-COVERAGE-CODE    PIC X(3).                            CPWSBROH
001900     10 SYSTEM-ENTRY-DATE    PIC X(10).                           CPWSBROH
002000     10 SYSTEM-ENTRY-TIME    PIC X(8).                            CPWSBROH
002100     10 BRO-RATE             PIC S999V99 USAGE COMP-3.            CPWSBROH
002200     10 BRO-UC-SINGLE        PIC S999V99 USAGE COMP-3.            CPWSBROH
002300     10 BRO-UC-TWO-PARTY     PIC S999V99 USAGE COMP-3.            CPWSBROH
002400     10 BRO-UC-FAMILY        PIC S999V99 USAGE COMP-3.            CPWSBROH
002500     10 BRO-UC-LAST-ACTION   PIC X(1).                            CPWSBROH
002600     10 BRO-UC-LAST-ACT-DT   PIC X(10).                           CPWSBROH
002700     10 BRO-EFFECTIVE-DATE   PIC X(10).                           CPWSBROH
002800     10 BRO-LAST-ACTION      PIC X(1).                            CPWSBROH
002900     10 BRO-LAST-ACTION-DT   PIC X(10).                           CPWSBROH
003000******************************************************************CPWSBROH
003100* THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 17      *CPWSBROH
003200******************************************************************CPWSBROH
