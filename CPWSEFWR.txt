000100**************************************************************/   36060628
000200*  COPYMEMBER: CPWSEFWR                                      */   36060628
000300*  RELEASE # ____0628____ SERVICE REQUEST NO(S)___3606_______*/   36060628
000400*  NAME ___G. STEINITZ___ CREATION DATE:    ____01/10/92_____*/   36060628
000500*  DESCRIPTION                                               */   36060628
000600*   - WORK STUDY ARRAY FOR FNW TABLE OF PPPEDB               */   36060628
000700**************************************************************/   36060628
000800*    COPYID=CPWSEFWR                                              CPWSEFWR
000900******************************************************************CPWSEFWR
001000*    COPY MEMBER USED TO STORE THE WORK STUDY LIMITS ARRAY TABLE *CPWSEFWR
001100*    MAXIMUM ENTRIES OF TABLE IS  STORED IN PAYROLL CONSTANT     *CPWSEFWR
001200*    (IDC) MEMBER.                                               *CPWSEFWR
001300******************************************************************CPWSEFWR
001400*COPYID=CPWSEFWR                                                  CPWSEFWR
001500*                                                                 CPWSEFWR
001600*01  WORK-STUDY-ARRAY.                                            CPWSEFWR
001700     05  EFWR-WORK-STUDY                             OCCURS 99.   CPWSEFWR
001800         10  EFWR-OCCURRENCE-KEY.                                 CPWSEFWR
001900             15  EFWR-FISCAL-YEAR    PIC S9(04)          COMP.    CPWSEFWR
002000             15  EFWR-WORK-STUDY-PGM PIC  X(01).                  CPWSEFWR
002100             15  EFWR-DEPT-CODE      PIC  X(06).                  CPWSEFWR
002200         10  FILLER.                                              CPWSEFWR
002300             15  EFWR-LIMIT          PIC S9(05)          COMP-3.  CPWSEFWR
002400             15  EFWR-AS-OF-DATE     PIC  X(10).                  CPWSEFWR
002500             15  EFWR-PRIOR-LIMIT    PIC S9(05)          COMP-3.  CPWSEFWR
002600             15  EFWR-PRIOR-DATE     PIC  X(10).                  CPWSEFWR
002700             15  EFWR-FYTD-GROSS     PIC S9(05)V9(02)    COMP-3.  CPWSEFWR
002800             15  EFWR-ADC-CODE       PIC  X(01).                  CPWSEFWR
002900******************************************************************CPWSEFWR
003000      SKIP1                                                       CPWSEFWR
