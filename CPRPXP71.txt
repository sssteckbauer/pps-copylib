000100**************************************************************/   30930413
000200*  COPYMEMBER: CPRPXP71                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100*    COPYID=CPRPXP71                                              CPRPXP71
000200******************************************************************CPRPXP71
000300*    THESE TITLES AND HEADINGS ARE USED FOR THE                   CPRPXP71
000400*    -PAYROLL INPUT RECONCILIATION- REPORT (PAYR061 AND PAYR071). CPRPXP71
000500******************************************************************CPRPXP71
000600*                                                                 CPRPXP71
000700*01  XP71-RPT-TITLE.                                              30930413
000800     03  XP71-STND-RPT-TTL   PIC X(32) VALUE                      CPRPXP71
000900         '--PAYROLL INPUT RECONCILIATION--'.                      CPRPXP71
001000 01  XP71-RPT-HD1.                                                CPRPXP71
001100     03  FILLER              PIC X(01) VALUE SPACE.               CPRPXP71
001200     03  FILLER              PIC X(25) VALUE SPACE.               CPRPXP71
001300     03  FILLER              PIC X(11) VALUE 'TRANSACTION'.       CPRPXP71
001400     03  FILLER              PIC X(30) VALUE SPACE.               CPRPXP71
001500     03  FILLER              PIC X(06) VALUE 'RECORD'.            CPRPXP71
001600     03  FILLER              PIC X(60) VALUE SPACE.               CPRPXP71
001700 01  XP71-RPT-HD2.                                                CPRPXP71
001800     03  FILLER              PIC X(01) VALUE SPACE.               CPRPXP71
001900     03  FILLER              PIC X(24) VALUE SPACE.               CPRPXP71
002000     03  FILLER              PIC X(13) VALUE 'CODE     TYPE'.     CPRPXP71
002100     03  FILLER              PIC X(29) VALUE SPACE.               CPRPXP71
002200     03  FILLER              PIC X(05) VALUE 'COUNT'.             CPRPXP71
002300     03  FILLER              PIC X(08) VALUE SPACE.               CPRPXP71
002400     03  FILLER              PIC X(07) VALUE 'DOLLARS'.           CPRPXP71
002500     03  FILLER              PIC X(13) VALUE SPACE.               CPRPXP71
002600     03  XP71-HEAD-HOURS      PIC X(05) VALUE 'HOURS'.
002700     03  FILLER              PIC X(28) VALUE SPACE.               CPRPXP71
