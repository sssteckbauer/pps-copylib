000000**************************************************************/   30871465
000001*  COPYMEMBER: CPCTCRAI                                      */   30871465
000002*  RELEASE: ___1465______ SERVICE REQUEST(S): _____3087____  */   30871465
000003*  NAME:_____SRS_________ CREATION DATE:      ___02/11/03__  */   30871465
000004*  DESCRIPTION:                                              */   30871465
000005*  - NEW COPY MEMBER FOR COSTING-NON-DESIG FUNDS TABLE INPUT */   30871465
000007**************************************************************/   30871465
000008*    COPYID=CPCTCRAI                                              CPCTCRAI
010000*01  COSTING-NON-DESIG-TABLE-INPUT.                               CPCTCRAI
010100     05 CRAI-ACTION                      PIC XX.                  CPCTCRAI
010200     05 CRAI-FUND-GROUP-CD               PIC XXXX.                CPCTCRAI
010400     05 CRAI-FAU                         PIC X(30).               CPCTCRAI
