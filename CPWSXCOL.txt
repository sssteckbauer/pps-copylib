000100**************************************************************/   30930413
000200*  COPY MODULE:  CPWSXCOL                                    */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000300*    COPYID=CPWSXCOL                                              CPWSXCOL
000200*01  XCOL1-ERR-HDR.                                               30930413
000300     03  XCOL1-DESC1             PICTURE X(6)  VALUE ' BATCH'.    CPWSXCOL
000400     03  XCOL1-DESC2             PICTURE X(10) VALUE '   BATCH  '.CPWSXCOL
000500     03  FILLER                  PICTURE X(18) VALUE              CPWSXCOL
000600           '    SOC. SEC.     '.                                  CPWSXCOL
000700     03  XCOL1-DESC3             PICTURE X(5)  VALUE 'CARD '.     CPWSXCOL
000800     03  FILLER                  PICTURE X(25) VALUE '     ERROR  CPWSXCOL
000900-          '      MESSAGE'.                                       CPWSXCOL
001000     03  FILLER                  PICTURE X(69) VALUE SPACES.      CPWSXCOL
001100     SKIP1                                                        CPWSXCOL
001200 01  XCOL2-ERR-HDR.                                               CPWSXCOL
001300     03  XCOL2-DESC1             PICTURE X(6)  VALUE ' TYPE '.    CPWSXCOL
001400     03  XCOL2-DESC2             PICTURE X(10) VALUE '   NUMBER '.CPWSXCOL
001500     03  FILLER                  PICTURE X(18) VALUE              CPWSXCOL
001600           '     NUMBER       '.                                  CPWSXCOL
001700     03  XCOL2-DESC3             PICTURE X(5)  VALUE 'TYPE '.     CPWSXCOL
001800     03  FILLER                  PICTURE X(47) VALUE '    SEVERITYCPWSXCOL
001900-          '      NUMBER     ERROR MESSAGE TEXT'.                 CPWSXCOL
002000     03  FILLER                  PICTURE X(47) VALUE SPACES.      CPWSXCOL
002100     SKIP1                                                        CPWSXCOL
002200 01  XCOL3-UNDERSCORES.                                           CPWSXCOL
002300     03  XCOL3-DESC1             PICTURE X(6)  VALUE ' _____'.    CPWSXCOL
002400     03  XCOL3-DESC2             PICTURE X(10) VALUE '   ______ '.CPWSXCOL
002500     03  FILLER                  PICTURE X(18) VALUE              CPWSXCOL
002600           '     ______       '.                                  CPWSXCOL
002700     03  XCOL3-DESC3             PICTURE X(5)  VALUE '____ '.     CPWSXCOL
002800     03  FILLER                  PICTURE X(48) VALUE '    ________CPWSXCOL
002900-          '      ______     __________________'.                 CPWSXCOL
003000     03  FILLER                  PICTURE X(47) VALUE SPACES.      CPWSXCOL
003100     SKIP1                                                        CPWSXCOL
003200 01  XCOL-MSSG-LINE              VALUE SPACES.                    CPWSXCOL
003300     03  FILLER                  PICTURE X(3).                    CPWSXCOL
003400     03  XCOL-BATCH-TYPE         PICTURE X.                       CPWSXCOL
003500     03  FILLER                  PICTURE X(6).                    CPWSXCOL
003600     03  XCOL-BATCH-NO           PICTURE X(3).                    CPWSXCOL
003700     03  FILLER                  PICTURE X(6).                    CPWSXCOL
003800     03  XCOL-SS-NO              PICTURE X(3)BXXBX(4).            CPWSXCOL
003900     03  FILLER REDEFINES XCOL-SS-NO.                             CPWSXCOL
004000         05  FILLER              PICTURE X(3).                    CPWSXCOL
004100         05  XCOL-DASH1          PICTURE X.                       CPWSXCOL
004200         05  FILLER              PICTURE XX.                      CPWSXCOL
004300         05  XCOL-DASH2          PICTURE X.                       CPWSXCOL
004400     03  FILLER                  PICTURE X(5).                    CPWSXCOL
004500     03  XCOL-TRANS-TYPE         PICTURE XX.                      CPWSXCOL
004600     03  FILLER                  PICTURE X(6).                    CPWSXCOL
004700     03  XCOL-ERR-SEV            PICTURE X(13).                   CPWSXCOL
004800     03  FILLER                  PICTURE X.                       CPWSXCOL
004900     03  XCOL-MSSG-NO            PICTURE XXBXXX.                  CPWSXCOL
005000     03  FILLER  REDEFINES XCOL-MSSG-NO.                          CPWSXCOL
005100         05  FILLER              PICTURE XX.                      CPWSXCOL
005200         05  XCOL-DASH-3         PICTURE X.                       CPWSXCOL
005300     03  FILLER                  PICTURE X(5).                    CPWSXCOL
005400     03  XCOL-MSSG-TEXT          PICTURE X(65).                   CPWSXCOL
005500     SKIP1                                                        CPWSXCOL
