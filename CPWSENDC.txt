000001**************************************************************/   36490863
000002*  COPYMEMBER: CPWSENDC                                      */   36490863
000003*  RELEASE: ___0863______ SERVICE REQUEST(S): _____3649____  */   36490863
000004*  NAME:_______DDM_______ MODIFICATION DATE:  __01/27/94____ */   36490863
000005*  DESCRIPTION: NEW NOTIFICATION DISTRIBUTION CLASSIFICATION */   36490863
000006*    ARRAY FOR PAN IMPLEMENTATION.                           */   36490863
000007******************************************************************36490863
000910*    COPYID=CPWSENDC                                              CPWSENDC
001000******************************************************************CPWSENDC
001001*   COPY MEMBER USED TO STORE THE NOTIF DIST CLASS ARRAY.        *CPWSENDC
001002*   MAXIMUM ENTRIES OF TABLE IS STORED IN PAYROLL CONSTANT       *CPWSENDC
001003*   (IDC) MEMBER.                                                *CPWSENDC
001004******************************************************************CPWSENDC
001100*01  NOTIF-ARRAY.                                                 CPWSENDC
001200     05  NDC-ARRAY                     OCCURS 100.                CPWSENDC
001210         10  NDC-OCCURRENCE-KEY.                                  CPWSENDC
001300             15  NOTIF-CLASS           PIC  X(08).                CPWSENDC
002207******************************************************************CPWSENDC
