000100*                                                                 CLPDP789
000200*      CONTROL REPORT PPP789CR                                    CLPDP789
000300*                                                                 CLPDP789
000400*                                                                 CLPDP789
000500 PRINT-CONTROL-REPORT SECTION.                                    CLPDP789
000600                                                                  CLPDP789
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP789
000800                                                                  CLPDP789
000900     MOVE 'PPP789CR'             TO CR-HL1-RPT.                   CLPDP789
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP789
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP789
001200                                                                  CLPDP789
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP789
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP789
001500                                                                  CLPDP789
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP789
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP789
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP789
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP789
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP789
002100                                                                  CLPDP789
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP789
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP789
002400                                                                  CLPDP789
002500     MOVE NO-CONTROL-CARD        TO CR-DL5-CTL-CARD.              CLPDP789
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP789
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP789
002800                                                                  CLPDP789
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP789
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP789
003100                                                                  CLPDP789
003200     ACCEPT TIME-WORK-AREA FROM TIME.                             CLPDP789
003300*    MOVE TIME-OF-DAY            TO TIME-WORK-AREA.               CLPDP789
003400     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP789
003500     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP789
003600     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP789
003700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP789
003800                                                                  CLPDP789
003900     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP789
004000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP789
004100*                                                                 CLPDP789
004200*  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP789
004300*                                                                 CLPDP789
004400*  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP789
004500*  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP789
004600*                                                                 CLPDP789
004700     MOVE 'UPTE EMPLOYEES'        TO CR-DL9-FILE.                 CLPDP789
004800     MOVE 'DELETED'              TO CR-DL9-ACTION.                CLPDP789
004900     MOVE WS-TOT-TPR-COUNT       TO CR-DL9-VALUE.                 CLPDP789
005000     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP789
005100     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP789
005200*                                                                 CLPDP789
005300*                                                                 CLPDP789
005400*                                                                 CLPDP789
005500     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP789
005600     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP789
005700                                                                  CLPDP789
005800     CLOSE CONTROLREPORT.                                         CLPDP789
