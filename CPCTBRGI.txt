000000**************************************************************/   48711432
000001*  COPYMEMBER: CPCTBRGI                                      */   48711432
000002*  RELEASE: ___1432______ SERVICE REQUEST(S): ____14871____  */   48711432
000003*  NAME:_____S.ISAACS____ CREATION DATE:      ___08/29/02__  */   48711432
000004*  DESCRIPTION:                                              */   48711432
000005*  - ADDED FIELD FOR REDUCED DEDUCTION AMOUNT                */   48711432
000007**************************************************************/   48711432
000000**************************************************************/   30871413
000001*  COPYMEMBER: CPCTBRGI                                      */   30871413
000002*  RELEASE: ___1413______ SERVICE REQUEST(S): _____3087____  */   30871413
000003*  NAME:_____SRS_________ CREATION DATE:      ___05/21/02__  */   30871413
000004*  DESCRIPTION:                                              */   30871413
000005*  - NEW COPY MEMBER FOR BENEFITS RATES GTN TABLE INPUT      */   30871413
000007**************************************************************/   30871413
000008*    COPYID=CPCTBRGI                                              CPCTBRGI
010000*01  BRG-GTN-TABLE-INPUT.                                         CPCTBRGI
010100     05 BRGI-CBUC                        PIC XX.                  CPCTBRGI
010200     05 BRGI-REP                         PIC X.                   CPCTBRGI
010300        88  BRGI-VALID-REP               VALUE 'C' 'U'.           CPCTBRGI
010400     05 BRGI-SHC                         PIC X.                   CPCTBRGI
010500        88  BRGI-VALID-SHC               VALUE 'A' THRU 'Z'       CPCTBRGI
010600                                               '0' THRU '9'       CPCTBRGI
010700                                                SPACE.            CPCTBRGI
010800     05 BRGI-DUC                         PIC X.                   CPCTBRGI
010900        88  BRGI-VALID-DUC               VALUE 'A' THRU 'Z'       CPCTBRGI
011000                                               '0' THRU '9'       CPCTBRGI
011100                                                SPACE.            CPCTBRGI
011200     05 BRGI-GTN-NUMBER                  PIC XXX.                 CPCTBRGI
011300     05 BRGI-RATE-AMOUNT-X.                                       48711432
011310        10 BRGI-RATE-AMOUNT           PIC S9(5)V9(4).             48711432
011350**** 05 BRGI-AMOUNTS.                                             48711432
011400****    10 BRGI-RATE-AMOUNT-X.                                    48711432
011401****       15 BRGI-RATE-AMOUNT           PIC S9(5)V9(4).          48711432
011402     05 BRGI-AMOUNTS.                                             48711432
011400        10 BRGI-MONTHLY-CAP-X.                                    CPCTBRGI
011400           15 BRGI-MONTHLY-CAP           PIC S9(5)V9(4).          CPCTBRGI
011400        10 BRGI-MONTHLY-CAP2-X.                                   CPCTBRGI
011400           15 BRGI-MONTHLY-CAP2          PIC S9(5)V9(4).          CPCTBRGI
011400        10 BRGI-CAP-GROSS-X.                                      CPCTBRGI
011400           15 BRGI-CAP-GROSS             PIC S9(5)V9(4).          CPCTBRGI
011409     05 BRGI-THRESHOLD-AMOUNTS.                                   48711432
011400        10 BRGI-THRESHOLD-PCT-X.                                  CPCTBRGI
011400           15 BRGI-THRESHOLD-PCT         PIC S9(5)V9(4).          CPCTBRGI
011400        10 BRGI-DEDUCTION-PCT-X.                                  CPCTBRGI
011400           15 BRGI-DEDUCTION-PCT         PIC S9(5)V9(4).          CPCTBRGI
011430        10 BRGI-RED-DED-AMT-X.                                    48711432
011440           15 BRGI-RED-DED-AMT           PIC S9(5)V9(4).          48711432
