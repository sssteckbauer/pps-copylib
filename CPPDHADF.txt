000800*==========================================================%      UCSD0102
000200*=    PROGRAM:  CPPDHADF                                  =%      UCSD0102
000300*=    CHANGE   #UCSD0102    PROJ. REQUEST: RELEASE 1138   =%      UCSD0102
000400*=    NAME: J.E. MCCLANE    MODIFICATION DATE: 09/26/97   =%      UCSD0102
000500*=                                                        =%      UCSD0102
000600*=    DESCRIPTION:                                        =%      UCSD0102
000700*=    CHANGES MADE TO ACCOMMODATE THE LOCAL COA.          =%      UCSD0102
000900*=                                                        =%      UCSD0102
008000*==========================================================%      UCSD0102
000100**************************************************************/   32021138
000200*  COPYMEMBER: CPPDHADF                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_PHIL THOMPSON___ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*  BASE FAU RELATED PROCEDURE CODE FOR ONLINE FUNCTION HADF. */   32021138
000700*  THIS CODE ONLY WORKS WITH THE BASE PROGRAM PPWHADF AND    */   32021138
000800*  BASE MAP PPHADF0.                                         */   32021138
000900**************************************************************/   32021138
001000 CPPD-INIT-SCREEN     SECTION.                                    CPPDHADF
001100**************************************************************/   CPPDHADF
001200*****MOVE UCCOMMON-CLR-FLD-LABEL TO L-FAUC.                       UCSD0102
001200*****MOVE UCCOMMON-ATR-FLD-LABEL TO L-FAUA.                       UCSD0102
001200*****MOVE UCCOMMON-HLT-FLD-LABEL TO L-FAUH.                       UCSD0102
001200     MOVE UCCOMMON-CLR-NORM-DISPL TO                              CPPDHADF
001300*****     LOCATION-EDB2043C     (WS-SUB)                          UCSD0102
001400*****     ACCOUNT-EDB2043C      (WS-SUB)                          UCSD0102
001500*****     COST-CENTER-EDB2043C  (WS-SUB)                          UCSD0102
001600*****     FUND-EDB2043C         (WS-SUB)                          UCSD0102
001700*****     PROJ-CODE-EDB2043C    (WS-SUB)                          UCSD0102
001800*****     SUB-ACCOUNT-EDB2043C  (WS-SUB)                          UCSD0102
001800          IFIS-INDEX-DE2043C    (WS-SUB)                          UCSD0102
001800          IFIS-FUND-DE2043C     (WS-SUB)                          UCSD0102
001800          SUB-ACCOUNT-DE2043C   (WS-SUB)                          UCSD0102
R01800*****     FAU-TITLEC            (WS-SUB).                         UCSD0102
001900     MOVE UCCOMMON-ATR-BOLD-DISPL TO                              CPPDHADF
002000*****     LOCATION-EDB2043A     (WS-SUB)                          UCSD0102
002100*****     ACCOUNT-EDB2043A      (WS-SUB)                          UCSD0102
002200*****     COST-CENTER-EDB2043A  (WS-SUB)                          UCSD0102
002300*****     FUND-EDB2043A         (WS-SUB)                          UCSD0102
002400*****     PROJ-CODE-EDB2043A    (WS-SUB)                          UCSD0102
002500*****     SUB-ACCOUNT-EDB2043A  (WS-SUB)                          UCSD0102
002500          IFIS-INDEX-DE2043A    (WS-SUB)                          UCSD0102
002500          IFIS-FUND-DE2043A     (WS-SUB)                          UCSD0102
002500          SUB-ACCOUNT-DE2043A   (WS-SUB)                          UCSD0102
001800*****     FAU-TITLEA            (WS-SUB).                         UCSD0102
001800*****     FAU-TITLEA            (WS-SUB).                         UCSD0102
002600     MOVE UCCOMMON-HLT-NORM-DISPL TO                              CPPDHADF
002700*****     LOCATION-EDB2043H     (WS-SUB)                          UCSD0102
002800*****     ACCOUNT-EDB2043H      (WS-SUB)                          UCSD0102
002900*****     COST-CENTER-EDB2043H  (WS-SUB)                          UCSD0102
003000*****     FUND-EDB2043H         (WS-SUB)                          UCSD0102
003100*****     PROJ-CODE-EDB2043H    (WS-SUB)                          UCSD0102
003200*****     SUB-ACCOUNT-EDB2043H  (WS-SUB)                          UCSD0102
003200          IFIS-INDEX-DE2043H    (WS-SUB)                          UCSD0102
003200          IFIS-FUND-DE2043H     (WS-SUB)                          UCSD0102
003200          SUB-ACCOUNT-DE2043H   (WS-SUB).                         UCSD0102
001800*****     FAU-TITLEH            (WS-SUB).                         UCSD0102
003300**************************************************************/   CPPDHADF
003400 CPPD-FAU-TRANSLATION SECTION.                                    CPPDHADF
003500**************************************************************/   CPPDHADF
003600*****MOVE F007-TITLE TO FAU-TITLEO (WS-SUB).                      UCSD0102
003600     MOVE F007-TITLE TO INDEX-NAME-AFPO (WS-SUB).                 UCSD0102
003300**************************************************************/   CPPDHADF
003400 CPPD-FILL-DIST-LINES SECTION.                                    CPPDHADF
003500**************************************************************/   CPPDHADF
003600     MOVE FULL-ACCT-UNIT  OF WS-PPPVZDST TO                       CPPDHADF
003700          XFAU-FAU-UNFORMATTED                                    CPPDHADF
003710*****MOVE XFAU-FAU-LOCATION OF XFAU-FAU-UNFORMATTED TO            UCSD0102
003720*****     LOCATION-EDB2043O    (WS-SUB).                          UCSD0102
003730*****MOVE XFAU-FAU-ACCOUNT OF XFAU-FAU-UNFORMATTED TO             UCSD0102
003900*****     ACCOUNT-EDB2043O     (WS-SUB).                          UCSD0102
004000*****MOVE XFAU-FAU-COST-CENTER OF XFAU-FAU-UNFORMATTED TO         UCSD0102
004100*****     COST-CENTER-EDB2043O (WS-SUB).                          UCSD0102
004200     MOVE XFAU-FAU-INDEX OF XFAU-FAU-UNFORMATTED TO               CPPDHADF
004300          IFIS-INDEX-DE2043O    (WS-SUB).                         CPPDHADF
004200     MOVE XFAU-FAU-FUND OF XFAU-FAU-UNFORMATTED TO                CPPDHADF
004300          IFIS-FUND-DE2043O    (WS-SUB).                          UCSD0102
004300*****     FUND-EDB2043O        (WS-SUB).                          CPPDHADF
004400*****MOVE XFAU-FAU-PROJECT-CODE OF XFAU-FAU-UNFORMATTED TO        UCSD0102
004500*****     PROJ-CODE-EDB2043O   (WS-SUB).                          UCSD0102
004600     MOVE XFAU-FAU-ACCOUNT-SUB OF XFAU-FAU-UNFORMATTED TO         CPPDHADF
004700          SUB-ACCOUNT-DE2043O  (WS-SUB).                          UCSD0102
004700*****     SUB-ACCOUNT-EDB2043O (WS-SUB).                          UCSD0102
