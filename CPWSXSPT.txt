000100*************************************************************/    73820210
000200*  COPYMEMBER:  CPWSXSPT                                    */    73820210
000300*  RELEASE:  0210                 SERVICE REQUEST:  7382    */    73820210
000400*  NAME:     G. STEINITZ    MODIFICATION DATE:  05/29/86    */    73820210
000500*  DESCRIPTION                                              */    73820210
000600*     THIS IS THE RECORD DESCRIPTION FOR SPECIAL PERFOR-    */    73820210
000700*     MANCE AWARD TRANSACTIONS.                             */    73820210
000800*************************************************************/    73820210
000900*01  XSPT-RECORD.                                                 CPWSXCTS
001000     05  XSPT-EMPLOYEE-ID            PIC  X(09).                  CPWSXCTS
001100     05  XSPT-TITLE-CODE.                                         CPWSXCTS
001200         10  XSPT-TITLE-CODE-9       PIC  9(04).                  CPWSXCTS
001300     05  XSPT-LOCATION               PIC  X(01).                  CPWSXCTS
001400     05  XSPT-ACCOUNT                PIC  X(06).                  CPWSXCTS
001500     05  XSPT-FUND                   PIC  X(05).                  CPWSXCTS
001600     05  FILLER                      PIC  X(01).                  CPWSXCTS
001700     05  XSPT-SUB                    PIC  X(01).                  CPWSXCTS
001800     05  XSPT-AMOUNT-X.                                           CPWSXCTS
001900         10  XSPT-AMOUNT             PIC  9(05)V9(02).            CPWSXCTS
002000     05  XSPT-EARNINGS-END-DATE.                                  CPWSXCTS
002100         10  XSPT-EARNINGS-END-MO    PIC  9(02).                  CPWSXCTS
002200         10  XSPT-EARNINGS-END-DA    PIC  9(02).                  CPWSXCTS
002300         10  XSPT-EARNINGS-END-YR    PIC  X(02).                  CPWSXCTS
002400     05  FILLER                      PIC  X(40).                  CPWSXCTS
