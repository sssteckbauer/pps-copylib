000000**************************************************************/   52101313
000001*  COPYMEMBER: CPLNKFIC                                      */   52101313
000002*  RELEASE: ___1313______ SERVICE REQUEST(S): ____15210____  */   52101313
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___11/15/00__  */   52101313
000004*  DESCRIPTION:                                              */   52101313
000005*  - ADDED PASSED-IN FIELD THAT SIGNALS REQUIRED HOURS       */   52101313
000006*    TOWARD BENEFITS ELIGIBILITY HAS BEEN ACHIEVED.          */   52101313
000005*  - ADDED RETURNED FIELD THAT SIGNALS UCRP ENROLLMENT       */   52101313
000006*    OCCURRED.                                               */   52101313
000007**************************************************************/   52101313
000000**************************************************************/   42601109
000001*  COPYMEMBER: CPLNKFIC                                      */   42601109
000002*  RELEASE: ___1109______ SERVICE REQUEST(S): ____14260____  */   42601109
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___01/30/97__  */   42601109
000004*  DESCRIPTION:                                              */   42601109
000005*  - ADD PASSED AND RETURNED FIELDS USED IN TRIGGERING       */   42601109
000006*    PENDING REHIRE LOGIC IN RET/FICA DERIVATION.            */   42601109
000007**************************************************************/   42601109
000100**************************************************************/   17900995
000200*  COPYMEMBER: CPLNKFIC                                      */   17900995
000300*  RELEASE: ___0995______ SERVICE REQUEST(S): ____11790____  */   17900995
000400*  NAME:______M. SANO____ MODIFICATION DATE:  ___06/05/95__  */   17900995
000500*  DESCRIPTION:                                              */   17900995
000600*   ADD CODES FOR DERIVATION OF UNEMPLOYMENT INSURANCE       */   17900995
000700*   COVERAGE CODE                                            */   17900995
000800**************************************************************/   17900995
000100**************************************************************/   17810964
000200*  COPYMEMBER: CPLNKFIC                                      */   17810964
000300*  RELEASE: ___0964______ SERVICE REQUEST(S): ____11781____  */   17810964
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___11/16/94__  */   17810964
000500*  DESCRIPTION:                                              */   17810964
000600*    LINKAGE BETWEEN CALLING PROGRAMS AND PPFICRET WHICH     */   17810964
000700*    DERIVES THE RETIREMENT AND FICA CODES                   */   17810964
000800**************************************************************/   17810964
000900*COPYID=CPLNKFIC                                                  CPLNKFIC
001000******************************************************************CPLNKFIC
001100*                        C P L N K F I C                         *CPLNKFIC
001200******************************************************************CPLNKFIC
001300*                                                                *CPLNKFIC
001400*01  PPFICRET-INTERFACE.                                          CPLNKFIC
001500*                                                                 CPLNKFIC
001600******************************************************************CPLNKFIC
001700*      THE FOLLOWING FLAGS AND FIELDS ARE PASSED BY CALLER       *CPLNKFIC
001800******************************************************************CPLNKFIC
001900*                                                                 CPLNKFIC
002000     05  PPFICRET-CALL-FLAG          PIC X(01).                   CPLNKFIC
002100         88  PPFICRET-INITIALIZE-CALL            VALUE 'I'.       CPLNKFIC
002200         88  PPFICRET-DERIVE-CALL                VALUE 'D'.       CPLNKFIC
002300*                                                                 CPLNKFIC
002400     05  PPFICRET-AS-OF-DATE.                                     CPLNKFIC
002500         10  PPFICRET-AS-OF-YY       PIC 99.                      CPLNKFIC
002600         10  PPFICRET-AS-OF-MM       PIC 99.                      CPLNKFIC
003401*                                                                 42601109
003402     05  PPFICRET-REQUESTOR          PIC X(02).                   42601109
003403         88  PPFICRET-REQUESTED-BY-08            VALUE '08'.      42601109
003404         88  PPFICRET-REQUESTED-BY-12            VALUE '12'.      42601109
003405         88  PPFICRET-REQUESTED-BY-13            VALUE '13'.      42601109
003406         88  PPFICRET-REQUESTED-BY-OL            VALUE 'OL'.      42601109
005200     05  PPFICRET-HRS-BEN-ELIG-SW     PIC X.                      52101313
005300         88  PPFICRET-HRS-BEN-ELIG-NOT-ACHV      VALUE  SPACE.    52101313
005300         88  PPFICRET-HRS-BEN-ELIG-ACHIEVED      VALUE  'Y'.      52101313
002700*                                                                 CPLNKFIC
002800*                                                                 CPLNKFIC
002900******************************************************************CPLNKFIC
003000*      THE FOLLOWING FLAGS AND FIELDS ARE RETURNED TO CALLER     *CPLNKFIC
003100******************************************************************CPLNKFIC
003200*                                                                 CPLNKFIC
003300     05  PPFICRET-RETURN-STATUS-FLAG PIC 99.                      CPLNKFIC
003400         88  PPFICRET-NORMAL-STATUS              VALUE  ZERO.     CPLNKFIC
003500         88  PPFICRET-ERROR-STATUS               VALUES 01 THRU   CPLNKFIC
003600                                                        99.       CPLNKFIC
003700         88  PPFICRET-IOCTL-ERROR                VALUES 01 THRU   CPLNKFIC
003800                                                        30.       CPLNKFIC
003900         88  PPPBEIL-INVALID-CALL                VALUE  31.       CPLNKFIC
004000         88  PPFICRET-INVALID-CTL-STATUS         VALUE  32.       CPLNKFIC
004100         88  PPFICRET-TITLE-TABLE-OVERFLOW       VALUE  33.       CPLNKFIC
004200         88  PPFICRET-APPT-TABLE-OVERFLOW        VALUE  34.       CPLNKFIC
004300*                                                                 CPLNKFIC
004400     05  PPFICRET-CODE-DERIVATIONS    PIC 99.                     CPLNKFIC
004500         88  PPFICRET-NOT-DERIVED                VALUE  ZERO.     CPLNKFIC
004600         88  PPFICRET-DERIVED                    VALUE 01 THRU    CPLNKFIC
005500                                                       04.        42601109
005510*****                                                          CD 52101313
004800         88  PPFICRET-FICA-DERIVED               VALUE  01.       CPLNKFIC
004900         88  PPFICRET-RET-DERIVED                VALUE  02.       CPLNKFIC
005900         88  PPFICRET-UI-DERIVED                 VALUE  03.       17900995
005910         88  PPFICRET-DER-IND-DERIVED            VALUE  04.       42601109
005200     05  PPFICRET-BEN-ELIG-DERIVE-SW      PIC X(01).              52101313
005300         88  PPFICRET-BEN-ELIG-U-NOT-DERIVD      VALUE  SPACE.    52101313
005300         88  PPFICRET-BEN-ELIG-U-DERIVED         VALUE  'Y'.      52101313
005000     05  PPFICRET-CODES.                                          CPLNKFIC
005100         10  PPFICRET-FICA-CODE           PIC X(01).              CPLNKFIC
005200         10  PPFICRET-RETIREMENT-CODE     PIC X(01).              CPLNKFIC
005300         10  PPFICRET-APPT-PERCNT-IND     PIC X(01).              CPLNKFIC
006400         10  PPFICRET-UI-CODE             PIC X(01).              17900995
006410         10  PPFICRET-RET-FICA-DERIVE     PIC X(01).              42601109
005400*                                                                *CPLNKFIC
005500***********> END OF CPLNKFIC <************************************CPLNKFIC
