000100*==========================================================%      UCSD9999
000200*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD9999
000300*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD9999     =%      UCSD9999
000400*==========================================================%      UCSD9999
000060*==========================================================%      UCSD0150
000070*=    COPY MEMBER: CPWSRCTS                               =%      UCSD0150
000080*=    CHANGE # UCSD0150     PROJ. REQUEST: PAYROLL OFFICE =%      UCSD0150
000090*=    NAME: G CHIU          MODIFICATION DATE: 08/21/01   =%      UCSD0150
000091*=                                                        =%      UCSD0150
000092*=    DESCRIPTION:                                        =%      UCSD0150
000093*=    ADD DATA ELEMENT RCTS-SAMEDAYCK.  IF SET TO 'Y'     =%      UCSD0150
000093*=    THIS FIELD INDICATES THAT A RUSH CHECK IS TO BE     =%      UCSD0150
 00093*=    ISSUED   THE SAME DAY INSTEAD OF OVERNIGHT BATCH    =%      UCSD0150
 00093*=    DISBURSEMENT PROCESS.                               =%      UCSD0150
000097*==========================================================%      UCSD0150
000500*==========================================================%      UCSD0102
000600*=    COPY MEMBER: CPWSRCTS                               =%      UCSD0102
000700*=    CHANGE # UCSD0102     PROJ. REQUEST: REL1138        =%      UCSD0102
000800*=    NAME: G. CHIU         MODIFICATION DATE: 9/02/97    =%      UCSD0102
000900*=                                                        =%      UCSD0102
001000*=    DESCRIPTION                                         =%      UCSD0102
001100*=    REMOVED THE FOLLOWING REDEFINED DATA FIELDS:        =%      UCSD0102
001300*=             RCTS-ERN-IFIS-INDEX                        =%      UCSD0102
001400*=             RCTS-ERN-IFIS-FUND                         =%      UCSD0102
001500*=             RCTS-ERN-SUB-ACCT                          =%      UCSD0102
001500*=             FILLER                                     =%      UCSD0102
001800*==========================================================%      UCSD0102
000500*==========================================================%      UCSD0079
000600*=    COPY MEMBER: CPWSRCTS                               =%      UCSD0079
000700*=    CHANGE UCSD0079       PROJ. REQUEST: PPR303         =%      UCSD0079
000800*=    NAME: RON BLOCK       MODIFICATION DATE: 9/18/95    =%      UCSD0079
000900*=                                                        =%      UCSD0079
001000*=    DESCRIPTION                                         =%      UCSD0079
001100*=    ADD DATA ELEMENT RCTS-PREVIEW.  IF SET TO 'Y'       =%      UCSD0079
001200*=    THIS FIELD WILL INDICATE THAT A PRELIMINARY RESULT  =%      UCSD0079
001300*=    IS TO BE DISPLAYED BUT THE RUSH CHECK REQUEST WILL  =%      UCSD0079
001400*=    NOT BE STORED ON THE ABEYANCE DATABASE.             =%      UCSD0079
001800*==========================================================%      UCSD0079
000500*==========================================================%      UCSD0058
000600*=    COPY MEMBER: CPWSRCTS                               =%      UCSD0058
000700*=    CHANGE # DT-058       PROJ. REQUEST: COST CENTER    =%      UCSD0058
000800*=    NAME: RON BLOCK       MODIFICATION DATE: 9/13/94    =%      UCSD0058
000900*=                                                        =%      UCSD0058
001000*=    DESCRIPTION                                         =%      UCSD0058
001100*=    NEW DATA ELEMENTS HAVE BEEN ADDED TO SUPPORT COST   =%      UCSD0058
001200*=    CENTER PROJECT (REPLACING RCTS-ERN-LAFS):           =%      UCSD0058
001300*=             RCTS-ERN-IFIS-INDEX                        =%      UCSD0058
001400*=             RCTS-ERN-IFIS-FUND                         =%      UCSD0058
001500*=             RCTS-ERN-SUB-ACCT                          =%      UCSD0058
001800*==========================================================%      UCSD0058
000000**************************************************************/   48681411
000001*  COPYMEMBER: CPWSRCTS                                      */   48681411
000002*  RELEASE: ___1411______ SERVICE REQUEST(S): ____14868____  */   48681411
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___04/22/02__  */   48681411
000004*  DESCRIPTION:                                              */   48681411
000005*  - ADDED TRANS PROCESS DATE, CHECK AVAILABLE DATE, EMAIL   */   48681411
000006*    ADDRESS, AND USER COMMENTS.                             */   48681411
000005*  - ADDED ERROR FLAGS FOR EMAIL ADDRESS AND AVAILABLE CHECK.*/   48681411
000007**************************************************************/   48681411
000100**************************************************************/   74611376
000200*  COPYMEMBER: CPWSRCTS                                      */   74611376
000300*  RELEASE: ___1376______ SERVICE REQUEST(S): ____17461____  */   74611376
000400*  NAME:___STEINITZ______ MODIFICATION DATE:  ___10/31/01__  */   74611376
000500*  DESCRIPTION:                                              */   74611376
000600*  - ADDED APPOINTMENT SUB-LOCATION.                         */   74611376
000700**************************************************************/   74611376
000100**************************************************************/   EFIX1371
000200*  COPYMEMBER: CPWSRCTS                                      */   EFIX1371
000300*  RELEASE: ___1371______ SERVICE REQUEST(S): _____EFIX____  */   EFIX1371
000400*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/20/01__  */   EFIX1371
000500*  DESCRIPTION:                                              */   EFIX1371
000600*  - INCREASED NUMBER OF EARNING DISTRIBUTIONS TO 96.        */   EFIX1371
000700**************************************************************/   EFIX1371
000010**************************************************************/   32021138
000020*  COPYMEMBER: CPWSRCTS                                      */   32021138
000030*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000040*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000050*  DESCRIPTION:                                              */   32021138
000060*  - MODIFIED FOR FULL ACCOUNTING UNIT                       */   32021138
000070**************************************************************/   32021138
000100**************************************************************/   EFIX1078
000200*  COPYMEMBER: CPWSRCTS                                      */   EFIX1078
000300*  RELEASE: ___1078______ SERVICE REQUEST(S): _____EFIX____  */   EFIX1078
000400*  NAME:__SRS____________ MODIFICATION DATE:  ___07/17/96__  */   EFIX1078
000500*  DESCRIPTION: ERROR REPORT 1415:                           */   EFIX1078
000600*   - ADD VALID STUDENT STATUS CODE VALUES                   */   EFIX1078
000700*   - ADD EARNINGS LEVEL LEAVE ACCRUAL                       */   EFIX1078
000800**************************************************************/   EFIX1078
000000**************************************************************/   28521025
000001*  COPYMEMBER: CPWSRCTS                                      */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___09/06/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    ADDED CENTURY FOR FIELDS RCTS-BIRTH-DT, RCTS-END-DATE,  */   28521025
000007*    RCTS-BEGIN-DATE, AND RCTS-CHECK-DATE.                   */   28521025
000008**************************************************************/   28521025
000100**************************************************************/   36430911
000200*  COPY MODULE:  CPWSRCTS.                                   */   36430911
000300*  RELEASE __0911__           SERVICE REQUEST __3643__       */   36430911
000400*  NAME __SRS__               CREATION DATE ____06/30/94_____*/   36430911
000500*  DESCRIPTION                                               */   36430911
000600*   - REPLACEMENT FOR AIOOE ARCHITECTURE                     */   36430911
000700*   - OLRC INPUT WORK AREA                                   */   36430911
000800**************************************************************/   36430911
000800     SKIP1                                                        CPWSRCTS
001000*01  CPWSRCTS.                                                    CPWSRCTS
001000     05  RCTS-TRANS-ID                PIC X(4).                   CPWSRCTS
001010     05  RCTS-ID                      PIC X(9).                   CPWSRCTS
001100     05  RCTS-NAME                    PIC X(26).                  CPWSRCTS
001110     05  RCTS-NAMESUFFIX              PIC X(04).                  CPWSRCTS
001200     05  RCTS-EMP-STATUS              PIC X(1).                   CPWSRCTS
001201     05  RCTS-PRI-PAY-SCHED           PIC X(02).                  CPWSRCTS
001202     05  RCTS-HOME-DEPT               PIC X(06).                  CPWSRCTS
002010     05  RCTS-BEGIN-DATE-CCYYMMDD     PIC X(08).                  28521025
002011     05  RCTS-BEGIN-DT-CCYYMMDD-9                                 28521025
002012                              REDEFINES RCTS-BEGIN-DATE-CCYYMMDD  28521025
002013                                      PIC 9(08).                  28521025
002014     05  FILLER               REDEFINES RCTS-BEGIN-DATE-CCYYMMDD. 28521025
002016         07  RCTS-BEGIN-DT-CC         PIC 9(02).                  28521025
002017         07  RCTS-BEGIN-DT-YYMMDD     PIC 9(06).                  28521025
002018     05  RCTS-END-DATE-CCYYMMDD       PIC X(08).                  28521025
002019     05  RCTS-END-DT-CCYYMMDD-9                                   28521025
002020                              REDEFINES RCTS-END-DATE-CCYYMMDD    28521025
002021                                      PIC 9(08).                  28521025
002022     05  FILLER               REDEFINES RCTS-END-DATE-CCYYMMDD.   28521025
002024         07  RCTS-END-DT-CC           PIC 9(02).                  28521025
002025         07  RCTS-END-DT-YYMMDD       PIC 9(06).                  28521025
002026     05  RCTS-CHECK-DATE-CCYYMMDD     PIC X(08).                  28521025
002019     05  RCTS-CHECK-DT-CCYYMMDD-9                                 28521025
002020                              REDEFINES RCTS-CHECK-DATE-CCYYMMDD  28521025
002021                                      PIC 9(08).                  28521025
002030     05  FILLER               REDEFINES RCTS-CHECK-DATE-CCYYMMDD. 28521025
002050         07  RCTS-CHECK-DT-CC         PIC 9(02).                  28521025
002060         07  RCTS-CHECK-DT-YYMMDD     PIC 9(06).                  28521025
001206     05  RCTS-QUARTER-CODE            PIC X(01).                  CPWSRCTS
001207     05  RCTS-SSN                     PIC X(09).                  CPWSRCTS
001208     05  RCTS-CITIZEN-CODE            PIC X(1).                   CPWSRCTS
001209     05  RCTS-PRIOR-EMP-ID            PIC X(09).                  CPWSRCTS
001210     05  RCTS-UI-ELIG-CODE            PIC X(1).                   CPWSRCTS
001211     05  RCTS-LAST-ACTION-DATE        PIC X(10).                  CPWSRCTS
001212     05  RCTS-HIRE-DATE               PIC X(10).                  CPWSRCTS
001213     05  RCTS-SEPARATE-DATE           PIC X(10).                  CPWSRCTS
001214     05  RCTS-LAST-PAY-DATE           PIC X(10).                  CPWSRCTS
001300     05  RCTS-EMP-REL-CODE            PIC X(1).                   CPWSRCTS
001301     05  RCTS-EMP-CBUC                PIC X(2).                   CPWSRCTS
001302     05  RCTS-EMP-REL-UNIT            PIC X(2).                   CPWSRCTS
001303     05  RCTS-EMP-SPEC-HAND           PIC X(1).                   CPWSRCTS
001304     05  RCTS-EMP-REP-CODE            PIC X(1).                   CPWSRCTS
001305     05  RCTS-DIST-UNIT-CODE          PIC X(1).                   CPWSRCTS
001306     05  RCTS-EXEC-BEN-ELIG           PIC 9(1).                   CPWSRCTS
001307     05  RCTS-MIN-RCD-FLAG            PIC X(1).                   CPWSRCTS
001400         88 NOT-MIN-RCD               VALUE '1'.                  CPWSRCTS
001500     05  RCTS-INPUT-DATA.                                         CPWSRCTS
001600         10  RCTS-SUPPRESS-PCT        PIC X(1).                   CPWSRCTS
001610         10  RCTS-SUPPRESS-PCT-ERR    PIC X(1).                   CPWSRCTS
001700         10  RCTS-SUPPRESS-SUS        PIC X(1).                   CPWSRCTS
001710         10  RCTS-SUPPRESS-SUS-ERR    PIC X(1).                   CPWSRCTS
001800         10  RCTS-CURR-PRIOR-YR       PIC X(1).                   CPWSRCTS
001801         10  RCTS-CURR-PRIOR-YR-ERR   PIC X(1).                   CPWSRCTS
008210         10  RCTS-LIAB-FAU            PIC X(30).                  32021138
008300         10  RCTS-LIAB-FAU-ERR        PIC X(1).                   32021138
001804         10  RCTS-RETR-CODE           PIC X(1).                   CPWSRCTS
004900             88 RCTS-VALID-RETR-CODE VALUE ' ' 'B' 'C' 'F' 'N'    CPWSRCTS
005000                       'O' 'P' 'S' 'U' 'H' '1' '2' '3' '4' '5'.   CPWSRCTS
004201         10  RCTS-RETR-CODE-ERR       PIC X(1).                   CPWSRCTS
004202         10  RCTS-FICA-CODE           PIC X(1).                   CPWSRCTS
005300             88 RCTS-VALID-FICA-CODE VALUE ' ' 'E' 'M' 'N'.       CPWSRCTS
004204         10  RCTS-FICA-CODE-ERR       PIC X(1).                   CPWSRCTS
004205         10  RCTS-TIP-IND             PIC X(1).                   CPWSRCTS
005600             88 RCTS-VALID-TIP-IND   VALUE ' ' 'Y' 'N' 'H'.       CPWSRCTS
004207         10  RCTS-TIP-IND-ERR         PIC X(1).                   CPWSRCTS
004208         10  RCTS-STUD-STAT           PIC X(1).                   CPWSRCTS
009600             88 RCTS-VALID-STUD-STAT VALUE ' ' '1' THRU '7'.      EFIX1078
004210         10  RCTS-STUD-STAT-ERR       PIC X(1).                   CPWSRCTS
004220         10  RCTS-LVE-ACC             PIC X(1).                   CPWSRCTS
006200             88 RCTS-VALID-LVE-ACC   VALUE ' ' 'A' THRU 'N'.      CPWSRCTS
004222         10  RCTS-LVE-ACC-ERR         PIC X(1).                   CPWSRCTS
004223         10  RCTS-OASDI-YTD-GRS-X     PIC X(9).                   CPWSRCTS
004224         10  RCTS-OASDI-YTD-GRS-N     REDEFINES                   CPWSRCTS
004225             RCTS-OASDI-YTD-GRS-X     PIC 9(7)V99.                CPWSRCTS
004226         10  RCTS-OASDI-YTD-GRS-ERR   PIC X(1).                   CPWSRCTS
004227         10  RCTS-MEDIC-YTD-GRS-X     PIC X(9).                   CPWSRCTS
004228         10  RCTS-MEDIC-YTD-GRS-N     REDEFINES                   CPWSRCTS
004229             RCTS-MEDIC-YTD-GRS-X     PIC 9(7)V99.                CPWSRCTS
004230         10  RCTS-MEDIC-YTD-GRS-ERR   PIC X(1).                   CPWSRCTS
004231         10  RCTS-OASDI-YTD-DED-X     PIC X(9).                   CPWSRCTS
004232         10  RCTS-OASDI-YTD-DED-N     REDEFINES                   CPWSRCTS
004233             RCTS-OASDI-YTD-DED-X     PIC 9(7)V99.                CPWSRCTS
004234         10  RCTS-OASDI-YTD-DED-ERR   PIC X(1).                   CPWSRCTS
004235         10  RCTS-MEDIC-YTD-DED-X     PIC X(9).                   CPWSRCTS
004236         10  RCTS-MEDIC-YTD-DED-N     REDEFINES                   CPWSRCTS
004237             RCTS-MEDIC-YTD-DED-X     PIC 9(7)V99.                CPWSRCTS
004238         10  RCTS-MEDIC-YTD-DED-ERR   PIC X(1).                   CPWSRCTS
004239         10  RCTS-FED-MARITAL         PIC X(1).                   CPWSRCTS
008100             88 RCTS-VALID-FED-MARITAL    VALUE ' ' 'S' 'M'.      CPWSRCTS
007201         10  RCTS-FED-MARITAL-ERR     PIC X(1).                   CPWSRCTS
007202         10  RCTS-FED-EXEMPT-X        PIC X(3).                   CPWSRCTS
007203         10  RCTS-FED-EXEMPT-N        REDEFINES                   CPWSRCTS
007204             RCTS-FED-EXEMPT-X        PIC 9(3).                   CPWSRCTS
008600             88 RCTS-VALID-FED-EXEMPT     VALUE 000 THRU 150      CPWSRCTS
008700                                                     998 999.     CPWSRCTS
007701         10  RCTS-FED-EXEMPT-ERR      PIC X(1).                   CPWSRCTS
007702         10  RCTS-STATE-MARITAL       PIC X(1).                   CPWSRCTS
009000             88 RCTS-VALID-STATE-MARITAL  VALUE ' ' 'S' 'M' 'H'.  CPWSRCTS
008101         10  RCTS-STATE-MARITAL-ERR   PIC X(1).                   CPWSRCTS
008102         10  RCTS-STATE-EXEMPT-X      PIC X(3).                   CPWSRCTS
008103         10  RCTS-STATE-EXEMPT-N      REDEFINES                   CPWSRCTS
008104             RCTS-STATE-EXEMPT-X      PIC 9(3).                   CPWSRCTS
009500             88 RCTS-VALID-STATE-EXEMPT   VALUE 000 THRU 150      CPWSRCTS
009600                                                     997 998.     CPWSRCTS
008701         10  RCTS-STATE-EXEMPT-ERR    PIC X(1).                   CPWSRCTS
008702         10  RCTS-STATE-ITEMIZED-X    PIC X(3).                   CPWSRCTS
008703         10  RCTS-STATE-ITEMIZED-N    REDEFINES                   CPWSRCTS
008704             RCTS-STATE-ITEMIZED-X    PIC 9(3).                   CPWSRCTS
008705         10  RCTS-STATE-ITEMIZED-ERR  PIC X(1).                   CPWSRCTS
013810         10  RCTS-FINAL-PAY-IND       PIC X(01).                  48681411
013820         10  RCTS-FINAL-PAY-ERR       PIC X(01).                  48681411
013830         10  RCTS-NOTIF-EMAIL-ADDRESS     PIC X(73).              48681411
013820         10  RCTS-NOTIF-EMAIL-ADDR-ERR    PIC X(01).              48681411
013840         10  RCTS-TRANS-DATE-MMDDCCYY     PIC X(08).              48681411
013850         10  RCTS-CHK-AVAIL-DATE-MMDDYY   PIC X(06).              48681411
013850         10  RCTS-CHK-AVAIL-DATE-ERR      PIC X(01).              48681411
013860         10  RCTS-COMMENTS-LINE-1         PIC X(72).              48681411
013870         10  RCTS-COMMENTS-LINE-2         PIC X(72).              48681411
013880         10  RCTS-COMMENTS-LINE-3         PIC X(72).              48681411
008706         10  RCTS-VENDOR-DATA-1       PIC X(45).                  CPWSRCTS
008707         10  RCTS-VENDOR-DATA-2       PIC X(45).                  CPWSRCTS
010610         10  RCTS-BIRTH-DT-CCYY       PIC X(4).                   28521025
010620         10  RCTS-BIRTH-DT9-CCYY REDEFINES RCTS-BIRTH-DT-CCYY     28521025
010630                                      PIC 9(4).                   28521025
008711         10  RCTS-BIRTH-DT-ERR        PIC X(1).                   CPWSRCTS
008712         10  RCTS-HLTH-PLAN           PIC X(2).                   CPWSRCTS
008713         10  RCTS-HLTH-PLAN-ERR       PIC X(1).                   CPWSRCTS
008714         10  RCTS-HLTH-COVG-CODE      PIC X(3).                   CPWSRCTS
008715         10  RCTS-HLTH-COVG-CODE-ERR  PIC X(1).                   CPWSRCTS
008716         10  RCTS-DENT-PLAN           PIC X(2).                   CPWSRCTS
008717         10  RCTS-DENT-PLAN-ERR       PIC X(1).                   CPWSRCTS
008718         10  RCTS-DENT-COVG-CODE      PIC X(3).                   CPWSRCTS
008719         10  RCTS-DENT-COVG-CODE-ERR  PIC X(1).                   CPWSRCTS
008720         10  RCTS-OPTI-PLAN           PIC X(2).                   CPWSRCTS
008721         10  RCTS-OPTI-PLAN-ERR       PIC X(1).                   CPWSRCTS
008722         10  RCTS-OPTI-COVG-CODE      PIC X(3).                   CPWSRCTS
008723         10  RCTS-OPTI-COVG-CODE-ERR  PIC X(1).                   CPWSRCTS
008724         10  RCTS-LEGL-PLAN           PIC X(2).                   CPWSRCTS
008725         10  RCTS-LEGL-PLAN-ERR       PIC X(1).                   CPWSRCTS
008726         10  RCTS-LEGL-COVG-CODE      PIC X(3).                   CPWSRCTS
008727         10  RCTS-LEGL-COVG-CODE-ERR  PIC X(1).                   CPWSRCTS
008728         10  RCTS-LIFE-PLAN           PIC X(1).                   CPWSRCTS
008729         10  RCTS-LIFE-PLAN-ERR       PIC X(1).                   CPWSRCTS
008730         10  RCTS-LIFE-SAL            PIC X(4).                   CPWSRCTS
008731         10  RCTS-LIFE-SAL-ERR        PIC X(1).                   CPWSRCTS
008732         10  RCTS-DEP-LIFE            PIC X(1).                   CPWSRCTS
008733         10  RCTS-DEP-LIFE-ERR        PIC X(1).                   CPWSRCTS
008734         10  RCTS-ADD-COVG-CODE       PIC X(1).                   CPWSRCTS
008735         10  RCTS-ADD-COVG-CODE-ERR   PIC X(1).                   CPWSRCTS
008736         10  RCTS-ADD-PRIN-SUM        PIC X(3).                   CPWSRCTS
008737         10  RCTS-ADD-PRIN-SUM-ERR    PIC X(1).                   CPWSRCTS
008738         10  RCTS-EMPL-DIS-WAIT       PIC X(3).                   CPWSRCTS
008739         10  RCTS-EMPL-DIS-WAIT-ERR   PIC X(1).                   CPWSRCTS
008740         10  RCTS-EMPL-DIS-SAL        PIC X(5).                   CPWSRCTS
008741         10  RCTS-EMPL-DIS-SAL-ERR    PIC X(1).                   CPWSRCTS
008742         10  RCTS-UCRS-AMT            PIC X(7).                   CPWSRCTS
008742         10  RCTS-UCRS-AMT-N          REDEFINES                   CPWSRCTS
008742             RCTS-UCRS-AMT            PIC 9(5)V99.                CPWSRCTS
008743         10  RCTS-UCRS-AMT-ERR        PIC X(1).                   CPWSRCTS
008744         10  RCTS-UCRS-PCT            PIC X(3).                   CPWSRCTS
008745         10  RCTS-UCRS-PCT-ERR        PIC X(1).                   CPWSRCTS
008746         10  RCTS-DEPCARE-AMT         PIC X(7).                   CPWSRCTS
008742         10  RCTS-DEPCARE-AMT-N       REDEFINES                   CPWSRCTS
008742             RCTS-DEPCARE-AMT         PIC 9(5)V99.                CPWSRCTS
008747         10  RCTS-DEPCARE-AMT-ERR     PIC X(1).                   CPWSRCTS
008748         10  RCTS-CHECK-AMT-X         PIC X(9).                   CPWSRCTS
008749         10  RCTS-CHECK-AMT-N         REDEFINES                   CPWSRCTS
008750             RCTS-CHECK-AMT-X         PIC 9(7)V99.                CPWSRCTS
008751         10  RCTS-CHECK-AMT-ERR       PIC X(1).                   CPWSRCTS
008752         10  RCTS-PRINT-ADVICE        PIC X(1).                   CPWSRCTS
008753         10  RCTS-PRINT-ADVICE-ERR    PIC X(1).                   CPWSRCTS
015400         10  RCTS-PREVIEW             PIC X(1).                   UCSD0079
015400         10  RCTS-PREVIEW-ERR         PIC X(1).                   UCSD0079
015400         10  RCTS-SAMEDAYCK           PIC X(1).                   UCSD0150
015400         10  RCTS-SAMEDAYCK-ERR       PIC X(1).                   UCSD0150
015400         10  RCTS-FILL1               PIC X(34).                  UCSD0150
015400*****    10  RCTS-FILL1               PIC X(38).                  UCSD0079
015400*****    10  RCTS-FILL1               PIC X(36).                  UCSD0150
008755         10  RCTS-ADVICE.                                         CPWSRCTS
008756             15  RCTS-ADVICE-MSG      OCCURS 4 TIMES              CPWSRCTS
008757                                      PIC X(60).                  CPWSRCTS
008758         10  RCTS-ERNS.                                           CPWSRCTS
021000*****                                                          CD 48681411
021100             15  RCTS-ERN-DATA                 OCCURS 96 TIMES.   EFIX1371
008760                 20  RCTS-ERN-TRAN        PIC X(02).              CPWSRCTS
008761                     88 RCTS-VALID-TRAN   VALUE 'LX' 'RX' 'AP'    CPWSRCTS
008762                                                'FT' 'ST' 'RA'.   CPWSRCTS
008763                 20  RCTS-ERN-PAY-END     PIC X(06).              CPWSRCTS
008764                 20  RCTS-ERN-CYCLE       PIC X(01).              CPWSRCTS
008765                 20  RCTS-ERN-DIST        PIC XX.                 CPWSRCTS
008766                 20  RCTS-ERN-DIST-N      REDEFINES               CPWSRCTS
008767                     RCTS-ERN-DIST        PIC 99.                 CPWSRCTS
008768                 20  RCTS-ERN-TITLE       PIC X(04).              CPWSRCTS
016900                 20  RCTS-ERN-FAU         PIC X(30).              32021138
008770                 20  RCTS-ERN-RATE-AMT-X  PIC X(7).               CPWSRCTS
008771                 20  RCTS-ERN-RATE-AMT-S  REDEFINES               CPWSRCTS
008772                     RCTS-ERN-RATE-AMT-X  PIC 9(5)V99.            CPWSRCTS
008773                 20  RCTS-ERN-RATE-AMT-R  REDEFINES               CPWSRCTS
008774                     RCTS-ERN-RATE-AMT-X  PIC 9(3)V9(4).          CPWSRCTS
008775                 20  RCTS-ERN-SIGN        PIC X.                  CPWSRCTS
008776                 20  RCTS-ERN-DOS         PIC X(03).              CPWSRCTS
008777                 20  RCTS-ERN-HOURS-X     PIC X(5).               CPWSRCTS
008778                 20  RCTS-ERN-HOURS-H     REDEFINES               CPWSRCTS
008779                     RCTS-ERN-HOURS-X     PIC 9(3)V99.            CPWSRCTS
008780                 20  RCTS-ERN-HOURS-P     REDEFINES               CPWSRCTS
008781                     RCTS-ERN-HOURS-X     PIC 9V9(4).             CPWSRCTS
008782                 20  RCTS-ERN-HR-PCT      PIC X.                  CPWSRCTS
008783                 20  RCTS-ERN-RAI         PIC X.                  CPWSRCTS
008784                 20  RCTS-ERN-WSP         PIC X.                  CPWSRCTS
022300                 20  RCTS-ERN-LVE-ACC     PIC X.                  EFIX1078
008785                 20  RCTS-ERN-ERR         PIC X.                  CPWSRCTS
022401*****                                                          CD 48681411
008785                 20  RCTS-ERN-SUB-LOCATION PIC X(02).             74611376
008785                 20  RCTS-FILL2           PIC X(06).              74611376
008786         10  RCTS-RF.                                             CPWSRCTS
022600*****                                                          CD 48681411
024300             15  RCTS-RF-DATA                  OCCURS 96 TIMES.   EFIX1371
008788                 20  RCTS-RF-GTN          PIC X(3).               CPWSRCTS
008789                 20  RCTS-RF-AMT-X        PIC X(7).               CPWSRCTS
008790                 20  RCTS-RF-AMT-N   REDEFINES                    CPWSRCTS
008791                     RCTS-RF-AMT-X        PIC 9(5)V99.            CPWSRCTS
008792                 20  RCTS-RF-QTD          PIC X.                  CPWSRCTS
008793                 20  RCTS-RF-YTD          PIC X.                  CPWSRCTS
008794                 20  RCTS-RF-ERR          PIC X.                  CPWSRCTS
008795         10  RCTS-DS.                                             CPWSRCTS
024309*****                                                          CD 48681411
025300             15  RCTS-DS-DATA                  OCCURS 96 TIMES.   EFIX1371
008797                 20  RCTS-DS-GTN          PIC X(3).               CPWSRCTS
008798                 20  RCTS-DS-AMT-X        PIC X(7).               CPWSRCTS
008799                 20  RCTS-DS-AMT-N   REDEFINES                    CPWSRCTS
008800                     RCTS-DS-AMT-X        PIC 9(5)V99.            CPWSRCTS
008801                 20  RCTS-DS-SIGN         PIC X.                  CPWSRCTS
008802                 20  RCTS-DS-ERR          PIC X.                  CPWSRCTS
008803         10  RCTS-PCT.                                            CPWSRCTS
025308*****                                                          CD 48681411
026200             15  RCTS-PCT-DATA                 OCCURS 96 TIMES.   EFIX1371
008805                 20  RCTS-PCT-GTN         PIC X(3).               CPWSRCTS
008806                 20  RCTS-PCT-AMT-X       PIC X(7).               CPWSRCTS
008807                 20  RCTS-PCT-AMT-N  REDEFINES                    CPWSRCTS
008808                     RCTS-PCT-AMT-X       PIC 9(5)V99.            CPWSRCTS
008809                 20  RCTS-PCT-ERR         PIC X.                  CPWSRCTS
008810         10  RCTS-SUS.                                            CPWSRCTS
026207*****                                                          CD 48681411
027000             15  RCTS-SUS-DATA                 OCCURS 96 TIMES.   EFIX1371
008812                 20  RCTS-SUS-GTN         PIC X(3).               CPWSRCTS
008820                 20  RCTS-SUS-ERR         PIC X.                  CPWSRCTS
