000100**************************************************************/   36190594
000200*  COPYLIB: UCWSCMNP                                         */   36190594
000300*  RELEASE # ___0594___   SERVICE REQUEST NO(S)____3619______*/   36190594
000400*  NAME __C RIDLON_____   MODIFICATION DATE ____08/09/91_____*/   36190594
000500*  DESCRIPTION                                               */   36190594
000600*                                                            */   36190594
000700*        THIS IS A COMPLETE REPLACEMENT OF ZZWSCMNP          */   36190594
000800*                AS ISSUED IN RELEASE 0541                   */   36190594
000900*                                                            */   36190594
001000**************************************************************/   36190594
001800     05  WS-START                          PIC X(27) VALUE        UCWSCMNP
001900         'WORKING STORAGE STARTS HERE'.                           UCWSCMNP
002000     05  WS-UCPRTMGR                       PIC X(8) VALUE         UCWSCMNP
002100         'UCPRTMGR'.                                              UCWSCMNP
002200     05  WS-UCWPRNT                        PIC X(8) VALUE         UCWSCMNP
002300         'UCWPRNT '.                                              UCWSCMNP
002400     05  WS-MESSAGE-818.                                          UCWSCMNP
002500         10  FILLER                       PIC X(5) VALUE 'UC999'. UCWSCMNP
002600         10  FILLER                        PIC X(41) VALUE        UCWSCMNP
002700         'SQL ERROR -818. REBIND PLAN FOR FUNCTION '.             UCWSCMNP
002800         10  WS-MESSAGE-818-FUNC           PIC X(4) VALUE SPACE.  UCWSCMNP
002900     05  WS-DATE                           PIC X(8).              UCWSCMNP
003000     05  WS-TIME                           PIC X(8).              UCWSCMNP
003100     05  WS-ABSTIME                        PIC S9(15) COMP-3.     UCWSCMNP
003200     05  WS-RESP                           PIC S9(8) COMP.        UCWSCMNP
