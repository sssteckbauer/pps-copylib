000800*==========================================================%      UCSD0102
000200*=    COPY MEMBER: CPWSXFAU                               =%      UCSD0102
000300*=    CHANGE #UCSD0102      PROJ. REQUEST: RELEASE 1138   =%      UCSD0102
000400*=    NAME: G. CHIU         MODIFICATION DATE: 08/27/97   =%      UCSD0102
000500*=                                                        =%      UCSD0102
000600*=    DESCRIPTION                                         =%      UCSD0102
000700*=    CHANGES MADE TO ACCOMMODATE THE LOCAL COA           =%      UCSD0102
000800*=    XFAU-FAU-UNFORMATTED-SAVE WAS ADDED TO STORE DATA   =%      UCSD0102
000900*=    (USED IN CPPDEAPP, CPPDEAPC,CPPDEUCI)               =%      UCSD0102
008000*==========================================================%      UCSD0102
000901**************************************************************/   32021138
000902*  COPYID:   CPWSXFAU                                        */   32021138
000903*  RELEASE # _1138_______ SERVICE REQUEST NO(S)___13202______*/   32021138
000904*  NAME _PHIL_THOMPSON_   MODIFICATION DATE ____08/04/97_____*/   32021138
000905*  DESCRIPTION                                               */   32021138
000906*  - BASE FAU FORMAT.                                        */   32021138
000906*    COMPLETE REPLACEMENT OF RELEASE 1124. FILLER FIELDS     */   32021138
000906*    FOR HYPENS AND BLANKS NAMED TO AVOID COMPILER WARNINGS  */   32021138
000906*    ON MOVE CORRESPONDING.                                  */   32021138
000908**************************************************************/   32021138
001000     03  XFAU-FAU-SLASH-FORMAT.                                   CPWSXFAU
001000*****    05  XFAU-FAU-LOCATION       PIC X(01).                   UCSD0102
001000         05  XFAU-FAU-INDEX          PIC X(07).                   UCSD0102
001000         05  SLASH-1                 PIC X(01)  VALUE '/'.        CPWSXFAU
001000*****    05  XFAU-FAU-ACCOUNT        PIC X(06).                   UCSD0102
001000*****    05  SLASH-2                 PIC X(01)  VALUE '/'.        UCSD0102
001000*****    05  XFAU-FAU-COST-CENTER    PIC X(04).                   UCSD0102
001000*****    05  SLASH-3                 PIC X(01)  VALUE '/'.        UCSD0102
001000*****    05  XFAU-FAU-FUND           PIC X(05).                   UCSD0102
001000         05  XFAU-FAU-FUND           PIC X(06).                   UCSD0102
001000         05  SLASH-4                 PIC X(01)  VALUE '/'.        CPWSXFAU
001000*****    05  XFAU-FAU-PROJECT-CODE   PIC X(06).                   UCSD0102
001000*****    05  SLASH-5                 PIC X(01)  VALUE '/'.        UCSD0102
001000         05  XFAU-FAU-ACCOUNT-SUB    PIC X(01).                   CPWSXFAU
001000     03  XFAU-FAU-HYPHEN-FORMAT.                                  CPWSXFAU
001000*****    05  XFAU-FAU-LOCATION       PIC X(01).                   UCSD0102
001000         05  XFAU-FAU-INDEX          PIC X(07).                   UCSD0102
001000         05  HYPHEN-1                PIC X(01)  VALUE '-'.        CPWSXFAU
001000*****    05  XFAU-FAU-ACCOUNT        PIC X(06).                   UCSD0102
001000*****    05  HYPHEN-2                PIC X(01)  VALUE '-'.        UCSD0102
001000*****    05  XFAU-FAU-COST-CENTER    PIC X(04).                   UCSD0102
001000*****    05  HYPHEN-3                PIC X(01)  VALUE '-'.        UCSD0102
001000*****    05  XFAU-FAU-FUND           PIC X(05).                   UCSD0102
001000         05  XFAU-FAU-FUND           PIC X(06).                   UCSD0102
001000         05  HYPHEN-4                PIC X(01)  VALUE '-'.        CPWSXFAU
001000*****    05  XFAU-FAU-PROJECT-CODE   PIC X(06).                   UCSD0102
001000*****    05  HYPHEN-5                PIC X(01)  VALUE '-'.        UCSD0102
001000         05  XFAU-FAU-ACCOUNT-SUB    PIC X(01).                   CPWSXFAU
001000     03  XFAU-FAU-BLANK-FORMAT.                                   CPWSXFAU
001000*****    05  XFAU-FAU-LOCATION       PIC X(01).                   UCSD0102
001000         05  XFAU-FAU-INDEX          PIC X(07).                   UCSD0102
001000         05  BLANK-1                 PIC X(01)  VALUE ' '.        CPWSXFAU
001000*****    05  XFAU-FAU-ACCOUNT        PIC X(06).                   UCSD0102
001000*****    05  BLANK-2                 PIC X(01)  VALUE ' '.        UCSD0102
001000*****    05  XFAU-FAU-COST-CENTER    PIC X(04).                   UCSD0102
001000*****    05  BLANK-3                 PIC X(01)  VALUE ' '.        UCSD0102
001000*****    05  XFAU-FAU-FUND           PIC X(05).                   UCSD0102
001000         05  XFAU-FAU-FUND           PIC X(06).                   UCSD0102
001000         05  BLANK-4                 PIC X(01)  VALUE ' '.        CPWSXFAU
001000*****    05  XFAU-FAU-PROJECT-CODE   PIC X(06).                   UCSD0102
001000*****    05  BLANK-5                 PIC X(01)  VALUE ' '.        UCSD0102
001000         05  XFAU-FAU-ACCOUNT-SUB    PIC X(01).                   CPWSXFAU
001000     03  XFAU-FAU-FORMATTED          PIC X(16)  VALUE SPACES.     UCSD0102
001000*****03  XFAU-FAU-FORMATTED          PIC X(28)  VALUE SPACES.     UCSD0102
001000     03  XFAU-FAU-UNFORMATTED.                                    CPWSXFAU
001000         05  XFAU-FAU-LOCATION       PIC X(01).                   CPWSXFAU
001000*****    05  XFAU-FAU-ACCOUNT        PIC X(06).                   UCSD0102
001000*****    05  XFAU-FAU-COST-CENTER    PIC X(04).                   UCSD0102
001000*****    05  XFAU-FAU-FUND           PIC X(05).                   UCSD0102
001000*****    05  XFAU-FAU-PROJECT-CODE   PIC X(06).                   UCSD0102
001000         05  XFAU-FAU-INDEX          PIC X(10).                   UCSD0102
001000         05  XFAU-FAU-ORGANIZATION   PIC X(06).                   CPWSXFAU
001000         05  XFAU-FAU-PROGRAM        PIC X(06).                   CPWSXFAU
001000         05  XFAU-FAU-FUND           PIC X(06).                   UCSD0102
001000         05  XFAU-FAU-ACCOUNT-SUB    PIC X(01).                   CPWSXFAU
001000     03  XFAU-FAU-CHAR30             PIC X(30)  VALUE SPACES.     CPWSXFAU
