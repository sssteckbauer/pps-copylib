000100**************************************************************/   74611376
000200*  COPYMEMBER: CPWSXMDF                                      */   74611376
000300*  RELEASE: ___1376______ SERVICE REQUEST(S): ____17461____  */   74611376
000400*  NAME:___STEINITZ______ MODIFICATION DATE:  ___10/31/01__  */   74611376
000500*  DESCRIPTION:                                              */   74611376
000600*  - ADDED LOGIC NEEDED FOR SUB-LOCATION PROCESSING.         */   74611376
000700**************************************************************/   74611376
000000**************************************************************/   37050946
000001*  COPYMEMBER: CPWSXMDF                                      */   37050946
000002*  RELEASE: ___0946______ SERVICE REQUEST(S): ____13705____  */   37050946
000003*  NAME:__H. TRUONG _____ MODIFICATION DATE:  ___12/05/94__  */   37050946
000004*  DESCRIPTION:                                              */   37050946
000005*     THIS IS THE RECORD DESCRIPTION FOR THE MERIT DATA FILE.*/   37050946
000006*     THIS MEMBER IS USED BY PROGRAMS PPP680 AND PPP685.    .*/   37050946
000007*                                                            */   37050946
000008**************************************************************/   37050946
003300*-----------------------------------------------------------------CPWSXMDF
003400*      MERIT DATA FILE                                            CPWSXMDF
003500*-----------------------------------------------------------------CPWSXMDF
003510*01  XMDF-RECORD.                                                 CPWSXMDF
003600     05  XMDF-EMPLOYEE-ID               PIC X(09).                CPWSXMDF
003700     05  XMDF-EMPLOYEE-NAME             PIC X(26).                CPWSXMDF
004500     05  XMDF-TITLE-CODE                PIC X(04).                CPWSXMDF
002400     05  XMDF-SUB-LOCATION              PIC X(02).                74611376
005300     05  XMDF-RATE-CODE                 PIC X.                    CPWSXMDF
005900     05  XMDF-OLD-RATEX                 PIC S9(07).               CPWSXMDF
006310     05  XMDF-OLD-HOURLY-RATE           PIC S9(07).               CPWSXMDF
006400     05  XMDF-NEW-RATEX                 PIC S9(07).               CPWSXMDF
006810     05  XMDF-NEW-HOURLY-RATE           PIC S9(07).               CPWSXMDF
