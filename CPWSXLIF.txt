000010**************************************************************/   37550524
000011*  COPYMEMBER: CPWSXLIF                                      */   37550524
000012*  RELEASE # ____0524____ SERVICE REQUEST NO(S)___3755_______*/   37550524
000013*  NAME __K.KELLER_____   MODIFICATION DATE ____12/03/91_____*/   37550524
000014*  DESCRIPTION                                               */   37550524
000015*  - CHANGED RPT-FICA-YTD-GR TO RPT-OASDI-YTD-GR             */   37550524
000020**************************************************************/   37550524
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXLIF                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   45260396
000200*  COPYMEMBER: CPWSXLIF                                      */   45260396
000300*  RELEASE # ____0396____ SERVICE REQUEST NO(S)__4526________*/   45260396
000400*  NAME ______JCW______   MODIFICATION DATE ____01/31/89_____*/   45260396
000500*                                                            */   45260396
000600*  DESCRIPTION                                               */   45260396
000700*  - RE-RELEASE OF COPY MEMBER ORIGINALLY RELEASED WITH      */   45260396
000800*    RELEASE 390.  MEMBER WAS RELEASED WITH THE GENERIC NAME */   45260396
000900*    'CPWSXRPT' IN THE ORIGINAL CHANGE BOX, IN THE           */   45260396
001000*    IDENTIFICATION COMMENT, AND AS THE ID IN COLUMNS 73-80. */   45260396
001100*    UNUSED FIELD 'RPT-SYSTEM-PARM-152' WAS DELETED AND THE  */   45260396
001200*    FINAL FILLER INCREASED BY THE TWO BYTES RELEASED.       */   45260396
001300**************************************************************/   45260396
001400*    COPYID=CPWSXLIF                                              CPWSXLIF
001500*01  PPEXERPT-INTERFACE.                                          30930413
001600     05  RPT-EMPL-ID-NUMBER           PIC  X(09).                 CPWSXLIF
001700     05  RPT-EMPL-NAME                PIC  X(26).                 CPWSXLIF
001800     05  RPT-EMPLMNT-STAT             PIC  X(01).                 CPWSXLIF
001900     05  RPT-EXEC-LIFE-SALARY         PIC  X(03).                 CPWSXLIF
002000     05  RPT-EXEC-LIFE-FLAG           PIC  X(01).                 CPWSXLIF
002100     05  RPT-EMP-AGE-JAN-1            PIC  X(02).                 CPWSXLIF
002200     05  RPT-SEPARATION-DATE          PIC  X(06).                 CPWSXLIF
002300     05  RPT-LOA-BEGIN-DATE           PIC  X(06).                 CPWSXLIF
002400     05  RPT-LOA-RETURN-DATE          PIC  X(06).                 CPWSXLIF
002500     05  RPT-FICA-ELIG-CODE           PIC  X(01).                 CPWSXLIF
002600*****05  RPT-FICA-YTD-GR              PIC S9(07)V99.              37550524
002610     05  RPT-OASDI-YTD-GR             PIC S9(07)V99.              37550524
002700     05  RPT-MEDICR-YTD-GR            PIC S9(07)V99.              CPWSXLIF
002800     05  RPT-PRI-PAY-SCHED            PIC  X(02).                 CPWSXLIF
002900     05  RPT-APPT-DATA      OCCURS 9.                             CPWSXLIF
003000         10  RPT-APPT-NO              PIC  X(02).                 CPWSXLIF
003100         10  RPT-APPT-BEGN-DATE       PIC  X(06).                 CPWSXLIF
003200         10  RPT-APPT-END-DATE        PIC  X(06).                 CPWSXLIF
003300         10  RPT-APPT-DUR-EMPLMT      PIC  X(01).                 CPWSXLIF
003400         10  RPT-APPT-PCT             PIC  9(01)V99.              CPWSXLIF
003500     05  RPT-IMPUTED-INCOME           PIC S9(07)V99.              CPWSXLIF
003600     05  RPT-IMPUTED-INCOME-DEC       PIC S9(07)V99.              CPWSXLIF
003700     05  RPT-IMPUTED-INCOME-YTD       PIC S9(07)V99.              CPWSXLIF
003800     05  RPT-II-MONTH                 PIC  9(02).                 CPWSXLIF
003900     05  FILLER                       PIC  X(08).                 CPWSXLIF
