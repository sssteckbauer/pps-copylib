000100*    COPYID=CPFDXSOF                                              CPFDXSOF
000200     BLOCK CONTAINS 0 RECORDS                                     CPFDXSOF
000300     RECORDING MODE IS F                                          CPFDXSOF
000400     LABEL RECORDS ARE STANDARD                                   CPFDXSOF
000500     DATA RECORD IS XSOF-RECORD.                                  CPFDXSOF
000600     SKIP1                                                        CPFDXSOF
000700 01  XSOF-RECORD.                                                 CPFDXSOF
000800     05  XSOFR-DATA              PIC X(80).                       CPFDXSOF
000900     05  XSOFR-BATCH             PIC X(3).                        CPFDXSOF
001000     05  XSOFR-SEQUENCE          PIC S9(5)    COMP-3.             CPFDXSOF
