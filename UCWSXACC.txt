000040*==========================================================%      UCSD0056
000050*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD0056
000060*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD0056     =%      UCSD0056
000070*=                                                        =%      UCSD0056
000080*==========================================================%      UCSD0056
000090*==========================================================%      UCSD0056
000091*=    COPY MEMBER: UCWSXACC                               =%      UCSD0056
000092*=    CHANGE # 0056         PROJ. REQUEST: 0056           =%      UCSD0056
000093*=    NAME: RON BLOCK       MODIFICATION DATE: 12/01/94   =%      UCSD0056
000094*=                                                        =%      UCSD0056
000095*=  ADD 'UNIVRL', 'CENTRL', 'DEPT', AND 'PUBLIC' AS       =%      UCSD0056
000096*=  VALID VALUES FOR ACCESS-PRIVILEGE.                    =%      UCSD0056
000104*==========================================================%      UCSD0056
000100**************************************************************/   36190594
000200*  COPYLIB: UCWSXACC                                         */   36190594
000300*  RELEASE # ___0594___   SERVICE REQUEST NO(S)____3619______*/   36190594
000400*  NAME __C RIDLON_____   MODIFICATION DATE ____08/09/91_____*/   36190594
000500*  DESCRIPTION                                               */   36190594
000600*                                                            */   36190594
000700*        THIS IS A COMPLETE REPLACEMENT OF ZZWSXACC          */   36190594
000800*                AS ISSUED IN RELEASE 0556                   */   36190594
000900*                                                            */   36190594
001000**************************************************************/   36190594
001100   05  WS-SUB-ACC            PIC S9(4) COMP VALUE ZERO.           UCWSXACC
001200*  05  WS-SUB-ACC-MAX        PIC S9(4) COMP VALUE +5.             UCSD0056
001200   05  WS-SUB-ACC-MAX        PIC S9(4) COMP VALUE +9.             UCSD0056
001300   05  WS-ACCESS-CODES.                                           UCWSXACC
001400       10 FILLER             PIC X(06)    VALUE 'ALL   '.         UCWSXACC
001500       10 FILLER             PIC 99       VALUE ZERO.             UCWSXACC
001600       10 FILLER             PIC X(06)    VALUE 'DELETE'.         UCWSXACC
001700       10 FILLER             PIC 99       VALUE 1.                UCWSXACC
001800       10 FILLER             PIC X(06)    VALUE 'INSERT'.         UCWSXACC
001900       10 FILLER             PIC 99       VALUE 2.                UCWSXACC
002000       10 FILLER             PIC X(06)    VALUE 'UPDATE'.         UCWSXACC
002100       10 FILLER             PIC 99       VALUE 3.                UCWSXACC
002200       10 FILLER             PIC X(06)    VALUE 'VIEW  '.         UCWSXACC
002300       10 FILLER             PIC 99       VALUE 4.                UCWSXACC
002200       10 FILLER             PIC X(06)    VALUE 'UNIVRL'.         UCSD0056
002300       10 FILLER             PIC 99       VALUE 5.                UCSD0056
002200       10 FILLER             PIC X(06)    VALUE 'CENTRL'.         UCSD0056
002300       10 FILLER             PIC 99       VALUE 6.                UCSD0056
002200       10 FILLER             PIC X(06)    VALUE 'DEPT  '.         UCSD0056
002300       10 FILLER             PIC 99       VALUE 7.                UCSD0056
002200       10 FILLER             PIC X(06)    VALUE 'PUBLIC'.         UCSD0056
002300       10 FILLER             PIC 99       VALUE 8.                UCSD0056
002400   05  WS-ACCESS-TABLE  REDEFINES   WS-ACCESS-CODES.              UCWSXACC
002500*      10  FILLER      OCCURS 05 TIMES.                           UCSD0056
002500       10  FILLER      OCCURS 09 TIMES.                           UCSD0056
002600           15  WS-ACCESS          PIC X(06).                      UCWSXACC
002700           15  WS-ACCESS-LEVEL    PIC 99.                         UCWSXACC
