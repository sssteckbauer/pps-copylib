000000**************************************************************/   32201186
000001*  COPYMEMBER: CPLNKNER                                      */   32201186
000002*  RELEASE: ___1186______ SERVICE REQUEST(S): ____13220____  */   32201186
000003*  NAME:_______QUAN______ CREATION DATE:      ___05/05/98__  */   32201186
000004*  DESCRIPTION:                                              */   32201186
000005*  - LINKAGE FOR PPNERUTL SUBROUTINE.                        */   32201186
000009**************************************************************/   32201186
001000*    COPYID=CPLNKNER                                              CPLNKNER
001100*01  PPNERUTL-INTERFACE.                                          CPLNKNER
001200*                                                                *CPLNKNER
001300******************************************************************CPLNKNER
001400*    P P N E R U T L   I N T E R F A C E                         *CPLNKNER
001500******************************************************************CPLNKNER
001600     05  PPNERUTL-FUNCTION           PIC X(01).                   CPLNKNER
001700         88  DELETE-ROW                        VALUE 'D'.         CPLNKNER
001900         88  INSERT-ROW                        VALUE 'I'.         CPLNKNER
001900         88  KEY-CHANGE                        VALUE 'K'.         CPLNKNER
002000         88  VALID-PPNERUTW-FUNCTION           VALUES             CPLNKNER
002000                                               'D' 'I' 'K'.       CPLNKNER
001600*                                                                *CPLNKNER
001600     05  PPNERUTL-KEY-CHG-FUNCTION.                               CPLNKNER
001700         10  PPNERUTL-OLD-EMPLOYEE-ID PIC X(09).                  CPLNKNER
001700         10  PPNERUTL-NEW-EMPLOYEE-ID PIC X(09).                  CPLNKNER
001600*                                                                *CPLNKNER
002200     05  PPNERUTL-ERROR-SWITCHES.                                 CPLNKNER
002300         10  PPNERUTL-ERROR-SW       PIC X(01).                   CPLNKNER
002400             88  PPNERUTL-ERROR                VALUE 'Y'.         CPLNKNER
002500         10  PPNERUTL-FUNC-ERR-SW    PIC X(01).                   CPLNKNER
002600             88  PPNERUTL-INVALID-FUNCTION     VALUE 'Y'.         CPLNKNER
