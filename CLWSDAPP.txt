000100     EJECT                                                        CLWSDAPP
000200*    COPYID=CLWSDAPP                                              CLWSDAPP
000210******************************************************************UCSDGRDX
000220*                                                                *UCSDGRDX
000230*  GRADE EXPANSION                              10/05/14  (RGM)  *UCSDGRDX
000240*                                                                *UCSDGRDX
000250*    REUSE SPC-HANDLING-CODE AS SECOND BYTE OF EXPANDED APPT     *UCSDGRDX
000260*    GRADE FIELD.  THIS FIELD WAS DEFINED AS A CONTINGENCY FOR   *UCSDGRDX
000270*    LATER EXPANSION, BUT WAS NEVER USED - IT IS NOT EXPRESSED   *UCSDGRDX
000280*    IN THE APP LOAD FILE.                                       *UCSDGRDX
000290*                                                                *UCSDGRDX
000291******************************************************************UCSDGRDX
000300     SKIP1                                                        CLWSDAPP
000400*01  DAPP-APPT-EXTRACT-RECORD      PICTURE  X(275).            ***CLWSDAPP
000500*                                                                 CLWSDAPP
000600*                                                                 CLWSDAPP
000700     05  DAPP-KEY.                                                CLWSDAPP
000710         10  DAPP-EMPLOYEE-ID              PICTURE  X(9).         CLWSDAPP
000720         10  DAPP-CHANGE-VERSION-KEY       PICTURE  X(9).         CLWSDAPP
000721         10  DAPP-APPOINTMENT-NUM-HI       PICTURE  X(1).         CLWSDAPP
000722     05  DAPP-APPOINTMENT-NUM-LO       PICTURE  X(1).             CLWSDAPP
000800*                                                                 CLWSDAPP
000900*                                                                 CLWSDAPP
001000     05  DAPP-FIXED-INFO.                                         CLWSDAPP
001100*                                                                 CLWSDAPP
001200         10  DAPP-RECORD-CHANGE-TYPE       PICTURE  X(1).         CLWSDAPP
002290*                                                                 CLWSDAPP
002291*                                                                 CLWSDAPP
002292     05  DAPP-VERSION-INFO.                                       CLWSDAPP
002293*                                                                 CLWSDAPP
002294         10  DAPP-VERSION-ENTRY            OCCURS  2 .            CLWSDAPP
002600*                                                                 CLWSDAPP
002900             15  DAPP-APPT-TITLE-CODE          PICTURE  X(4).     CLWSDAPP
003000             15  DAPP-APPT-DEPT-FULL.                             CLWSDAPP
003100                 20  DAPP-APPT-DEPT-HIGH2          PICTURE X(2).  CLWSDAPP
003200                 20  DAPP-APPT-DEPT                PICTURE X(4).  CLWSDAPP
003410             15  DAPP-APPOINTMENT-TYPE         PICTURE  X(1).     CLWSDAPP
003420             15  DAPP-PERSONNEL-PROG-CODE      PICTURE  X(1).     CLWSDAPP
003500             15  DAPP-APPT-BEGIN-DATE-FULL.                       CLWSDAPP
003600                 20  DAPP-APPT-BEGIN-CN            PICTURE X(2).  CLWSDAPP
003700                 20  DAPP-APPT-BEGIN-DATE.                        CLWSDAPP
003800                     25  DAPP-APPT-BEGIN-YR        PICTURE X(2).  CLWSDAPP
003900                     25  DAPP-APPT-BEGIN-MO        PICTURE X(2).  CLWSDAPP
004000                     25  DAPP-APPT-BEGIN-DA        PICTURE X(2).  CLWSDAPP
004010             15  DAPP-APPT-END-DATE-FULL.                         CLWSDAPP
004020                 20  DAPP-APPT-END-CN              PICTURE X(2).  CLWSDAPP
004030                 20  DAPP-APPT-END-DATE.                          CLWSDAPP
004040                     25  DAPP-APPT-END-YR          PICTURE X(2).  CLWSDAPP
004050                     25  DAPP-APPT-END-MO          PICTURE X(2).  CLWSDAPP
004060                     25  DAPP-APPT-END-DA          PICTURE X(2).  CLWSDAPP
004070             15  DAPP-APPT-DURATION-CODE       PICTURE  X(1).     CLWSDAPP
004080             15  DAPP-APPT-GRADE-CODE          PICTURE  X(1).     CLWSDAPP
004090             15  DAPP-APPT-PERCENTAGE.                            CLWSDAPP
004091                 20  DAPP-APPT-PERCENTAGE-HIGH1    PICTURE  X(1). CLWSDAPP
004092                 20  DAPP-APPT-PERCENTAGE-LOW2     PICTURE  X(2). CLWSDAPP
004100             15  DAPP-APPT-FIXED-VAR-CODE      PICTURE  X(1).     CLWSDAPP
004200             15  DAPP-APPT-PAY-RATE-AMT.                          CLWSDAPP
004201                 20  DAPP-APPT-PAY-RATE-AMT-HIGH6  PICTURE  X(6). CLWSDAPP
004202                 20  DAPP-APPT-PAY-RATE-AMT-LOW4   PICTURE  X(4). CLWSDAPP
004210             15  DAPP-APPT-PAY-RATE-TYPE-CODE  PICTURE  X(1).     CLWSDAPP
004300             15  DAPP-APPT-PAY-SCHEDULE        PICTURE  X(2).     CLWSDAPP
004310             15  DAPP-APPT-TIME-REPORTING-CODE PICTURE  X(1).     CLWSDAPP
004320             15  DAPP-APPT-LEAVE-ACCR-CODE     PICTURE  X(1).     CLWSDAPP
004330             15  DAPP-APPT-RETIREMENT-CODE     PICTURE  X(1).     CLWSDAPP
004340             15  DAPP-APPT-REPRESENTATION-CODE PICTURE  X(1).     CLWSDAPP
004350             15  DAPP-APPT-WOS-IND             PICTURE  X(1).     CLWSDAPP
004360             15  DAPP-APPT-PAY-BASIS-CODE      PICTURE  X(2).     CLWSDAPP
004370             15  DAPP-APPT-PAID-OVER-CODE      PICTURE  X(2).     CLWSDAPP
004380             15  DAPP-APPT-TITLE-UNIT-CODE     PICTURE  X(2).     CLWSDAPP
004390*            15  DAPP-APPT-SPC-HANDLING-CODE   PICTURE  X(1).     UCSDGRDX
004391             15  DAPP-APPT-GRADE-CODE-2        PICTURE  X(1).     UCSDGRDX
004391             15  DAPP-APPT-ACAD-RANK-CODE      PICTURE  X(1).     CLWSDAPP
004393             15  DAPP-APPT-CTO-CODE            PICTURE  X(3).     CLWSDAPP
004394             15  DAPP-APPT-FLSA-CODE           PICTURE  X(1).     CLWSDAPP
004395             15  DAPP-APPT-SHIFT-ELIG-CODE     PICTURE  X(1).     CLWSDAPP
004396             15  DAPP-APPT-ONCALL-ELIG-CODE    PICTURE  X(1).     CLWSDAPP
004397             15  DAPP-APPT-FILLER1             PICTURE  X(1).     CLWSDAPP
004398             15  DAPP-APPT-TITLE-NAME          PICTURE  X(30).    CLWSDAPP
004399             15  DAPP-APPT-DEPT-NAME           PICTURE  X(30).    CLWSDAPP
016700*                                                                 CLWSDAPP
016800*                                                                 CLWSDAPP
