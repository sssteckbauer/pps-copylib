000800*==========================================================%      UCSD0102
000200*=    COPY MEMBER: CPLNF019                               =%      UCSD0102
000300*=    CHANGE #UCSD0102      PROJ. REQUEST: RELEASE 1138   =%      UCSD0102
000400*=    NAME: R. WOLBERG      MODIFICATION DATE: 10/13/97   =%      UCSD0102
000500*=                                                        =%      UCSD0102
000600*=    DESCRIPTION                                         =%      UCSD0102
000700*=    CHANGES MADE TO ACCOMMODATE THE LOCAL COA           =%      UCSD0102
000800*=    EXPANDED FUND FROM 5 CHARACTERS TO SIX CHARACTERS.  =%      UCSD0102
000900*=                                                        =%      UCSD0102
008000*==========================================================%      UCSD0102
000000**************************************************************/   32021138
000001*  COPYMEMBER: CPLNF019                                      */   32021138
000002*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000003*  NAME:_______WJG_______ CREATION DATE:      ___08/04/97__  */   32021138
000004*  DESCRIPTION:                                              */   32021138
000005*                                                            */   32021138
000006*  Initial release of linkage copymember between calling     */   32021138
000007*  program and module PPFAU019                               */   32021138
000008*                                                            */   32021138
000009**************************************************************/   32021138
000900*                                                            */   CPLNF019
001000*  This copymember is one of the Full Accounting Unit modules*/   CPLNF019
001100*  which each campus may need to modify to accommodate its   */   CPLNF019
001200*  Chart of Accounts structure.                              */   CPLNF019
001300*                                                            */   CPLNF019
002100**************************************************************/   CPLNF019
002300*01  PPFAU019-INTERFACE.                                          CPLNF019
002400    05  F019-INPUTS.                                              CPLNF019
002500        10  F019-CALL-TYPE               PIC X(04).               CPLNF019
002501            88  F019-ACCUMULATE-CALL     VALUE 'ACCU'.            CPLNF019
002502            88  F019-REPORT-CALL         VALUE 'REPT'.            CPLNF019
002504        10  F019-PAR-EARN-DIST.                                   CPLNF019
002505            15  F019-FAU                 PIC X(30).               CPLNF019
002557            15  F019-INTRANS-CD          PIC X(02).               CPLNF019
002562                88  F019-EXP-TRNSFR-1    VALUE 'E1'.              CPLNF019
002563                88  F019-EXP-TRNSFR-3    VALUE 'E3'.              CPLNF019
002596            15  F019-EARN-AMT            PIC S9(05)V99 COMP-3.    CPLNF019
002597        10  F019-GROUP-DEF-ROWS          PIC S9(04) COMP.         CPLNF019
002598        10  F019-GROUP-NO                PIC S9(04) COMP.         CPLNF019
002599        10  F019-FND-GROUP-DATA.                                  CPLNF019
002600            15  F019-FND-GROUP-LOCATION   PIC X(01).              CPLNF019
002601*****       15  F019-FND-GROUP-LOW-RANGE  PIC X(05).              UCSD0102
002602*****       15  F019-FND-GROUP-HIGH-RANGE PIC X(05).              UCSD0102
002601            15  F019-FND-GROUP-LOW-RANGE  PIC X(06).              UCSD0102
002602            15  F019-FND-GROUP-HIGH-RANGE PIC X(06).              UCSD0102
002603            15  F019-FND-GROUP-CODE       PIC X(04).              CPLNF019
002610            15  F019-FND-GROUP-TEXT       PIC X(30).              CPLNF019
002620    05  F019-OUTPUTS.                                             CPLNF019
002630        10  F019-FUND-GROUP-FOUND-FLAG   PIC X(01).               CPLNF019
002640            88  F019-FUND-GROUP-FOUND       VALUE 'Y'.            CPLNF019
002650            88  F019-FUND-GROUP-NOT-FOUND   VALUE 'N'.            CPLNF019
002651        10  F019-FED-NON-FED-FLAG        PIC X(01).               CPLNF019
002652            88  F019-FEDERAL                VALUE 'F'.            CPLNF019
002653            88  F019-NON-FEDERAL            VALUE 'N'.            CPLNF019
002660        10  F019-MAX-PRINT-CALLS         PIC S9(04) COMP SYNC.    CPLNF019
002700        10  F019-PRINT-LINE-1            PIC X(133).              CPLNF019
002710        10  F019-PRINT-LINE-2            PIC X(133).              CPLNF019
002711        10  F019-PRINT-LINES-RETURNED    PIC S9(04) COMP SYNC.    CPLNF019
002712        10  F019-ADVANCE-LINES           PIC S9(03) COMP-3.       CPLNF019
002730        10  F019-FAILURE-CODE            PIC S9(04) COMP SYNC.    CPLNF019
002731            88  F019-VALID               VALUE ZERO.              CPLNF019
002732            88  F019-INVALID-LEVEL-1     VALUE +1.                CPLNF019
002733            88  F019-FAILED              VALUE -1.                CPLNF019
002740        10  F019-FAILURE-TEXT            PIC X(35).               CPLNF019
