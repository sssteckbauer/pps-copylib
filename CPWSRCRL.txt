000100**************************************************************/   36030532
000200*  COPY MODULE:  CPWSRCRL                                    */   36030532
000300*  RELEASE # ____0532____ SERVICE REQUEST NO(S)___3603_______*/   36030532
000400*  NAME __SRS__           MODIFICATION DATE ____07/23/90_____*/   36030532
000500*  DESCRIPTION                                               */   36030532
000600*  -NEW COPYMEMBER FOR RUSH CHECK INPUT RECONCILIATION REPORT*/   36030532
000700**************************************************************/   36030532
003400     SKIP1                                                        CPWSRCRL
003500     03  CPA-LITERALS-INDS.                                       CPWSRCRL
008800          05  FILLER PIC X(38)                                    CPWSRCRL
008900              VALUE '12RETROACTIVE PAY                NYYYY'.     CPWSRCRL
009000          05  FILLER PIC X(38)                                    CPWSRCRL
009100              VALUE '13POSITIVE PAY                   NYYYY'.     CPWSRCRL
009400          05  FILLER PIC X(38)                                    CPWSRCRL
009500              VALUE '     LATE                        NYYYY'.     CPWSRCRL
009600          05  FILLER PIC X(38)                                    CPWSRCRL
009700              VALUE '     REDUCED                     NYYYY'.     CPWSRCRL
009800          05  FILLER PIC X(38)                                    CPWSRCRL
009900              VALUE '     ADDITIONAL                  NYYYY'.     CPWSRCRL
010200          05  FILLER PIC X(38)                                    CPWSRCRL
010300              VALUE '14ONE-TIME PAY/PAY IN LIEU       NYYYY'.     CPWSRCRL
010400          05  FILLER PIC X(38)                                    CPWSRCRL
010500              VALUE '  DEDUCTION RELATED INPUT        NYNNN'.     CPWSRCRL
010600          05  FILLER PIC X(38)                                    CPWSRCRL
010700              VALUE '   15REFUNDS                     NYNNN'.     CPWSRCRL
010800          05  FILLER PIC X(38)                                    CPWSRCRL
010900              VALUE '     OTHER DEDUCTIONS            NYNNN'.     CPWSRCRL
011200          05  FILLER PIC X(38)                                    CPWSRCRL
011300              VALUE '   17PERCENT BASED               NYNNN'.     CPWSRCRL
011400          05  FILLER PIC X(38)                                    CPWSRCRL
011500              VALUE '18SUSPENDED                      NNNNN'.     CPWSRCRL
011400          05  FILLER PIC X(38)                                    CPWSRCRL
011500              VALUE '30SPECIAL GROSS                  NYNNN'.     CPWSRCRL
013200     03  CPA-LIT-INDS REDEFINES CPA-LITERALS-INDS.                CPWSRCRL
013600         05  CPA-LIT-INDS-OCCUR       OCCURS 12.                  CPWSRCRL
013700             07  CPA-PRT-TRANS            PIC XX.                 CPWSRCRL
013800             07  CPA-PRT-DESC             PIC X(31).              CPWSRCRL
013900             07  CPA-PRT-RCDS             PIC X.                  CPWSRCRL
014000             07  CPA-PRT-DOLRS            PIC X.                  CPWSRCRL
014100             07  CPA-PRT-RATE             PIC X.                  CPWSRCRL
014200             07  CPA-PRT-HOURS            PIC X.                  CPWSRCRL
014300             07  CPA-PRT-PCT              PIC X.                  CPWSRCRL
014400     SKIP2                                                        CPWSRCRL
