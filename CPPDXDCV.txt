000100******************************************************************36050446
000200*  COPYMEMBER: CPPDXDCV                                          *36050446
000300*  RELEASE: ____0446____  SERVICE REQUEST(S): ____3605____       *36050446
000400*  NAME:___BRUCE JAMES__  CREATION DATE:      __01/31/90__       *36050446
000500*  DESCRIPTION:                                                  *36050446
000300*    DATE CONVERSION PROCEDURE CODE.  USE IN CONJUNCTION WITH    *36050446
000400*    CPWSXDCV TO CONVERT A DB2 DATE TO YYMMDD.                   *36050446
000500*    TO USE, MOVE THE D2B DATE TO DB2-DATE THEN PERFORM          *36050446
000600*    CONVERT-DB2-DATE.  RESULT WILL BE IN WSX-EDB-DATE.          *36050446
000800******************************************************************CPPDXDCV
000900 CONVERT-DB2-DATE.                                                CPPDXDCV
001000     MOVE DB2-YY TO WSX-EDB-DATE-YY.                              CPPDXDCV
001100     MOVE DB2-MM TO WSX-EDB-DATE-MM.                              CPPDXDCV
001200     MOVE DB2-DD TO WSX-EDB-DATE-DD.                              CPPDXDCV
001300                                                                  CPPDXDCV
