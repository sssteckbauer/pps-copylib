000100**************************************************************/   30930413
000200*  COPYMEMBER: CPFDXTOC                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/02/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100*    COPYID=CPFDXTOC                                              CPFDXTOC
000200*     FILE DESCRIPTION ENTRY FOR UNEMPLOYMENT INSURANCE (UI) AND  CPFDXTOC
000300*      CETA WAGES - TAPE IS PRODUCED QUARTERLY WITHIN TAX         CPFDXTOC
000400*      REPORTING MODULE PPP060.                                   CPFDXTOC
000500     LABEL RECORDS ARE STANDARD                                   CPFDXTOC
000600     RECORDING MODE IS F                                          CPFDXTOC
000700*****BLOCK CONTAINS 25 RECORDS                                    30930413
000700     BLOCK CONTAINS 0  RECORDS                                    30930413
000800     DATA RECORD IS TAPEOUT-UI-CETA.                              CPFDXTOC
000900                                                                  CPFDXTOC
001000 01  TAPEOUT-UI-CETA             PIC X(275).                      CPFDXTOC
