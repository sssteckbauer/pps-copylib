000010**************************************************************/   32021138
000020*  COPYMEMBER: CPWSXPIA                                      */   32021138
000030*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000040*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000050*  DESCRIPTION:                                              */   32021138
000060*  - MODIFIED FOR FULL ACCOUNTING UNIT                       */   32021138
000070**************************************************************/   32021138
000100**************************************************************/   36500900
000200*  COPYMEMBER: CPWSXPIA                                      */   36500900
000300*  RELEASE: ___0900______ SERVICE REQUEST(S): ____3650_____  */   36500900
000400*  NAME: ___ RAB (BIT)___ MODIFICATION DATE:  __05/20/94____ */   36500900
000500*  DESCRIPTION:                                              */   36500900
000600*     MODIFIED FOR COST CENTER AND PROJECT CODE ENHANCEMENT  */   36500900
000700**************************************************************/   36500900
000100**************************************************************/   36280677
000200*  COPYMEMBER: CPWSXPIA                                      */   36280677
000300*  RELEASE: 0677  REF REL: 670 SERVICE REQUEST NO(S)__3628A_ */   36280677
000400*  NAME ________JLT____        MODIFICATION DATE _06/23/92___*/   36280677
000600*  DESCRIPTION                                               */   36280677
000700*   - MODIFIED EXPENSE TRANSFER SORT KEY FOR DUPLICATE       */   36280677
000700*     RECORD EDIT PROCESS.                                   */   36280677
000900**************************************************************/   36280677
000100**************************************************************/   36280670
000200*  COPYMEMBER: CPWSXPIA                                      */   36280670
000300*  RELEASE # ____0670____ SERVICE REQUEST NO(S)___3628A______*/   36280670
000400*  NAME ________JLT____   MODIFICATION DATE ____06/11/92_____*/   36280670
000600*  DESCRIPTION                                               */   36280670
000700*   - MODIFIED EXPENSE TRANSFER CONTROL KEY.                 */   36280670
000900**************************************************************/   36280670
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXPIA                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   14010244
000200*  COPY MODULE:  CPWSXPIA                                    */   14010244
000300*  RELEASE # ___0244___   SERVICE REQUEST NO(S)____1401______*/   14010244
000400*  NAME ___RUB_________   MODIFICATION DATE ____08/14/86_____*/   14010244
000500*  DESCRIPTION                                               */   14010244
000600*  CHANGES MADE FOR COLLECTIVE BARGAINING PROJECT I PAY      */   14010244
000700*  TRANSACTIONS                                              */   14010244
000800**************************************************************/   14010244
000900     SKIP2                                                        14010244
001000**************************************************************/   13190174
001100*  COPY MODULE:  CPWSXPIA                                    */   13190174
001200*  RELEASE # ___0174___   SERVICE REQUEST NO(S)____1319______*/   13190174
001300*  NAME ___JLT_________   MODIFICATION DATE ____12/20/85_____*/   13190174
001400*  DESCRIPTION                                               */   13190174
001500*  FOR PHASE 3 OF THE PAYROLL FILE EXPANSION PROJECT,        */   13190174
001600*  THE PAYROLL INPUT ACTIVITY FILE IS BEING EXPANDED         */   13190174
001700*  BY 22 BYTES.                                              */   13190174
001800**************************************************************/   13190174
001900     SKIP2                                                        13190174
002000*    COPYID=CPWSXPIA                                              CPWSXPIA
002100*01  XPIA-PAYROLL-INPUT-ACTIVITY.                                 30930413
002200     05  XPIA-BATCH-NO           PIC XXX.                         CPWSXPIA
002300     05  XPIA-TRAN-SEQ-CD        PIC XX.                          CPWSXPIA
002400     05  XPIA-SORT-FLD           PIC X(24).                       CPWSXPIA
002500     05  XPIA-CORR-SORT          REDEFINES XPIA-SORT-FLD.         CPWSXPIA
002600         10  XPIA-CORR-SORT-FILL PIC X(16).                       CPWSXPIA
002700         10  XPIA-THF-SEQ-NO     PIC X(5).                        CPWSXPIA
002800         10  XPIA-ACTN-CD        PIC X.                           CPWSXPIA
002900         10  XPIA-CORR-TRAN-SEQ  PIC XX.                          CPWSXPIA
003000     05  XPIA-C-H-SORT           REDEFINES XPIA-SORT-FLD.         CPWSXPIA
003100         10  XPIA-COH-ET-ID      PIC X(9).                        CPWSXPIA
003200         10  XPIA-C-H-CHK        PIC X(7).                        CPWSXPIA
003300         10  XPIA-COH-TRAN-CD    PIC X(2).                        CPWSXPIA
003400         10  XPIA-COH-SORT-FILLER    PIC X(6).                    CPWSXPIA
003500     05  XPIA-O-SORT             REDEFINES XPIA-SORT-FLD.         CPWSXPIA
003600         10  XPIA-O-ID            PIC X(9).                       CPWSXPIA
003700         10  XPIA-O-TRAN-YEAR    PIC 99.                          CPWSXPIA
003800         10  XPIA-O-TRAN-MONTH   PIC 99.                          CPWSXPIA
003900         10  XPIA-O-TRAN-DAY     PIC 99.                          CPWSXPIA
004000         10  XPIA-O-FILLER        PIC X(9).                       CPWSXPIA
004100     05  XPIA-ET-SORT            REDEFINES XPIA-SORT-FLD.         CPWSXPIA
004200         10  XPIA-ET-ID           PIC X(9).                       CPWSXPIA
004300****     10  XPIA-ET-MO           PIC X.                          36280670
004400****     10  XPIA-ET-YR           PIC X.                          36280670
004500****     10  XPIA-ET-PG           PIC X(3).                       36280670
004600         10  XPIA-ET-PAGE         PIC X(5).                       36280670
004600         10  XPIA-ET-LINE         PIC XX.                         CPWSXPIA
004800         10  XPIA-ET-IN-SEQ       PIC S9(4) COMP.                 36280677
004700         10  XPIA-ET-TRANS-CD     PIC XX.                         CPWSXPIA
004800         10  XPIA-ET-FILLER       PIC X(4).                       36280677
004800****     10  XPIA-ET-FILLER       PIC X(6).                       36280677
004900     05  XPIA-TIME-SORT          REDEFINES XPIA-SORT-FLD.         CPWSXPIA
005000         10  XPIA-TIME-FILLER    PIC X(19).                       CPWSXPIA
005100         10  XPIA-TIME-INPT-SEQ  PIC 9(5).                        CPWSXPIA
005200     05  XPIA-TIME-CTL-NO        PIC X(5).                        CPWSXPIA
005300     05  XPIA-INPUT-SEQ          REDEFINES XPIA-TIME-CTL-NO       CPWSXPIA
005400                                 PIC 9(5).                        CPWSXPIA
005500     05  XPIA-EMPL-ID            PIC X(9).                        CPWSXPIA
005600     05  XPIA-TRAN-CD            PIC X(2).                        CPWSXPIA
005700*    05  XPIA-TRAN-IMAGE         PIC X(69).                       13190174
005800     05  XPIA-TRAN-IMAGE.                                         13190174
009100*****    10  XPIA-FILLER-ORG     PIC X(69).                       32021138
009110         10  XPIA-FILLER-ORG     PIC X(87).                       32021138
006000         10  XPIA-FILLER-OP      PIC X(02).                       13190174
006100         10  XPIA-FILLER-COS     PIC X(20).                       13190174
006200     05  XPIA-CONTRL-IMAGE   REDEFINES                            14010244
006300         XPIA-TRAN-IMAGE.                                         14010244
006400         10  XPIA-E-CONTRL.                                       14010244
006500             15  XPIA-TRAN-ET-PAGE  PIC X(5).                     36280670
006500             15  XPIA-TRAN-ET-LINE  PIC X(2).                     36280670
006500****         15  XPIA-E-MO       PIC X.                           36280670
006600****         15  XPIA-E-YR       PIC X.                           36280670
006700****         15  XPIA-E-PAGE     PIC XXX.                         36280670
006800****         15  XPIA-E-LINE     PIC XX.                          36280670
006900         10  XPIA-CH-CONTRL  REDEFINES                            14010244
007000             XPIA-E-CONTRL.                                       14010244
007100             15  XPIA-CH-CHK     PIC X(7).                        14010244
007200         10  XPIA-O-CONTRL   REDEFINES                            14010244
007300             XPIA-E-CONTRL.                                       14010244
007400             15  FILLER          PIC X.                           14010244
007500             15  XPIA-O-DATE.                                     14010244
007600                 20  XPIA-O-MO   PIC XX.                          14010244
007700                 20  XPIA-O-DY   PIC XX.                          14010244
007800                 20  XPIA-O-YR   PIC XX.                          14010244
009600         10  FILLER              PIC X(102).                      32021138
007900     05  XPIA-TIME-TRAN-IMAGE    REDEFINES XPIA-TRAN-IMAGE.       14010244
008000         10  XPIA-PAY-PER-DATE       PIC X(6).                    14010244
008100         10  XPIA-PAY-CYCLE          PIC X.                       14010244
008200         10  XPIA-APPT-DIST-NO       PIC XX.                      14010244
008300         10  XPIA-TC-FILLER          PIC X.                       14010244
008400         10  XPIA-TITLE-CODE         PIC X(4).                    14010244
011900*****    10  XPIA-ACCOUNT.                                        32021138
012000*****        15   XPIA-ACCT-LOC      PIC X.                       32021138
012100*****        15   XPIA-ACCT-ACCT     PIC X(3).                    32021138
012200*****        15   XPIA-ACCT-ACCT2    PIC X(3).                    32021138
012300*****        15   XPIA-ACCT-FUND     PIC X(5).                    32021138
012400*****        15   XPIA-ACCT-SUB      PIC X.                       32021138
012500*****    10  XPIA-ACCT-FILLER        PIC X(7).                    32021138
012510         10  XPIA-FAU                PIC X(30).                   32021138
009200         10  XPIA-RATE-AMT           PIC X(7).                    14010244
009300         10  XPIA-RATE-AMT9          REDEFINES                    14010244
009400             XPIA-RATE-AMT           PIC 999V9999.                14010244
009500         10  XPIA-RATE-A-H           PIC X.                       14010244
009600         10  XPIA-REG-DOS            PIC X(3).                    14010244
009700         10  XPIA-REG-TIME           PIC X(5).                    14010244
009800         10  XPIA-REG-TIME9          REDEFINES                    14010244
009900             XPIA-REG-TIME           PIC 999V99.                  14010244
010000         10  XPIA-REG-HRS-PCT        PIC X.                       14010244
010100         10  XPIA-OT-DOS2            PIC X(3).                    14010244
010200         10  XPIA-OT-HRS2            PIC X(5).                    14010244
010300         10  XPIA-OT-HRS2-9          REDEFINES                    14010244
010400             XPIA-OT-HRS2            PIC 999V99.                  14010244
010500         10  XPIA-OT-DOS3            PIC X(3).                    14010244
010600         10  XPIA-OT-HRS3            PIC X(5).                    14010244
010700         10  XPIA-OT-HRS3-9          REDEFINES                    14010244
010800             XPIA-OT-HRS3            PIC 999V99.                  14010244
010900         10  XPIA-WSP                PIC X.                       14010244
011000*        10  XPIA-TIME-ACTY-FILLER   PIC X.                       14010244
011100         10  XPIA-EMP-REL-CODE       PIC X.                       14010244
011200         10  XPIA-APPT-TYPE-CODE     PIC X.                       14010244
011300         10  XPIA-DIST-UNIT-CODE     PIC X.                       14010244
014710         10  XPIA-FILLER             PIC X(08).                   32021138
014800         10  XPIA-FILLER-COS-2       PIC X(20).                   32021138
014900*****    10  XPIA-FILLER-COS-2.                                   32021138
014910*****        15  XPIA-FILLER-COS2        PIC X(10).               32021138
015100*****        15  XPIA-COST-CENTER        PIC X(04).               32021138
015200*****        15  XPIA-PROJECT-CODE       PIC X(06).               32021138
