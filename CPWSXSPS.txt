000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXSPS                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   14770346
000200*  COPY MODULE:  CPWSXSPS                                    */   14770346
000300*  RELEASE # ___0346___   SERVICE REQUEST NO(S) ____1477____ */   14770346
000400*  NAME ___K.KELLER____   MODIFICATION DATE ____03/07/88_____*/   14770346
000500*  DESCRIPTION                                               */   14770346
000600*       INCREASED LENGTH TO ALLOW FOR SUREPAY EARNINGS       */   14770346
000700*       STATEMENT ON LASER OR DATA MAILER FORMAT.            */   14770346
000800**************************************************************/   14770346
000900**************************************************************/   14020230
001000*  COPY MODULE:  CPWSXSPS                                    */   14020230
001100*  RELEASE # ___0230___   SERVICE REQUEST NO(S) ____1402____ */   14020230
001200*  NAME ______BVB______   MODIFICATION DATE ____08/20/86_____*/   14020230
001300*                                                            */   14020230
001400*  DESCRIPTION                                               */   14020230
001500*       ADDED FOR SUREPAY.  DESCRIBES RECORDS CONTAINED IN   */   14020230
001600*       THE SUREPAY STUB (ADVICE) FILE.                      */   14020230
001700**************************************************************/   14020230
001800*    COPYID=CPWSXSPS                                              CPWSXSPS
001900*01  XSPS-RECORD.                                                 30930413
002000     05  XSPS-CARR-CTL          PIC X(01).                        CPWSXSPS
002100**   05  XSPS-PRINT-LINE        PIC X(74).                        14770346
002200     05  XSPS-PRINT-LINE        PIC X(110).                       14770346
