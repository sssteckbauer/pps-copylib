001600*==========================================================%      UCSD0102
000070*=    COPY MEMBER:   CPWSXTIM                             =%      UCSD0102
000080*=    CHANGE #UCSD0102      PROJ. REQUEST: REL1138        =%      UCSD0102
000090*=    NAME:  G. CHIU        MODIFICATION DATE: 9/13/97    =%      UCSD0102
000091*=                                                        =%      UCSD0102
000092*=    DESCRIPTION:                                        =%      UCSD0102
000093*=    RETAIN THE OLD FILE FORMAT (PRIOR TO REL1138).      =%      UCSD0102
001600*==========================================================%      UCSD0102
000600*==========================================================%      UCSD0039
000700*=    COPY MEMBER:   CPWSXTIM                             =%      UCSD0039
000800*=    CHANGE #DU-039        PROJ. REQUEST: IFIS CONVERSION=%      UCSD0039
000900*=    NAME:  J.E. MCCLANE   MODIFICATION DATE: 6/26/91    =%      UCSD0039
001000*=                                                        =%      UCSD0039
001100*=    DESCRIPTION:  CHANGED 20 BYTE TIME-RCD-FILLER-COS   =%      UCSD0039
001200*=    TO ACCOMMODATE THE ACCOUNT-INDEX, FUND-EXTENSION    =%      UCSD0039
001300*=    ORGANIZATION, AND PROGRAM FIELDS FOR THE IFIS       =%      UCSD0039
001400*=    CONVERSION.                                         =%      UCSD0039
000060*==========================================================%      UCSD0039
000060*==========================================================%      UCSD0023
000070*=    COPY MEMBER:   CPWSXTIM                             =%      UCSD0023
000080*=    CHANGE #UCSD0023      PROJ. REQUEST: IBM OFFLOAD    =%      UCSD0023
000090*=    NAME:  J.E. MCCLANE   MODIFICATION DATE: 9/18/88    =%      UCSD0023
000091*=                                                        =%      UCSD0023
000092*=    DESCRIPTION:  ADDED THREE FIELDS TO END OF THE      =%      UCSD0023
000093*=    COPY MEMBER FOR USE BY THE TIMESHEET FORM.  THEY    =%      UCSD0023
000094*=    ARE TIMEKEEPER CODE AND FILLER FOR A TOTAL OF 9     =%      UCSD0023
000095*=    ADDITIONAL BYTES.                                   =%      UCSD0023
000096*==========================================================%      UCSD0023
000010**************************************************************/   32021138
000020*  COPYMEMBER: CPWSXTIM                                      */   32021138
000030*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000040*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000050*  DESCRIPTION:                                              */   32021138
000060*  - MODIFIED FOR FULL ACCOUNTING UNIT                       */   32021138
000070**************************************************************/   32021138
000097**************************************************************/   36500900
000098*  COPYMEMBER: CPWSXTIM                                      */   36500900
000099*  RELEASE: ___0900______ SERVICE REQUEST(S): ____3650_____  */   36500900
000100*  NAME: ___ WRW (BIT)___ MODIFICATION DATE:  __05/20/94____ */   36500900
000101*  DESCRIPTION                                               */   36500900
000102*   - COST CENTER / PROJECT CODE CHANGES                     */   36500900
000103**************************************************************/   36500900
000097**************************************************************/   30930413
000098*  COPYMEMBER: CPWSXTIM                                      */   30930413
000099*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000100*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
000101*  DESCRIPTION                                               */   30930413
000102*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000103**************************************************************/   30930413
000110**************************************************************/   45020369
000200*  COPY MODULE:  CPWSXTIM                                    */   45020369
000300*  RELEASE # ___0369___   SERVICE REQUEST NO(S)____4502______*/   45020369
000400*  NAME ___CJJ_________   MODIFICATION DATE ____07/18/88_____*/   45020369
000500*  DESCRIPTION                                               */   45020369
000600*  ADDED NEW FIELDS TO THE TIME FILE RECORD AS REQUIRED BY   */   45020369
000601*  PROGRAM PPP305 (PAYROLL TIMESHEET REPRINT)                */   45020369
000610**************************************************************/   45020369
000611     SKIP2                                                        45020369
000612**************************************************************/   14010244
000620*  COPY MODULE:  CPWSXTIM                                    */   14010244
000630*  RELEASE # ___0244___   SERVICE REQUEST NO(S)____1401______*/   14010244
000640*  NAME ___RUB_________   MODIFICATION DATE ____08/13/86_____*/   14010244
000650*  DESCRIPTION                                               */   14010244
000660*  CHANGES MADE FOR COLLECTIVE BARGAINING PROJECT I PAY      */   14010244
000700*  TRANSACTIONS                                              */   14010244
000800**************************************************************/   14010244
000900     SKIP2                                                        14010244
001000**************************************************************/   13190159
001100*  COPY MODULE:  CPWSXTIM                                    */   13190159
001200*  RELEASE # ___0159___   SERVICE REQUEST NO(S)____1319______*/   13190159
001300*  NAME ___JAL_________   MODIFICATION DATE ____09/20/85_____*/   13190159
001400*  DESCRIPTION                                               */   13190159
001500*  FOR PHASE 2 OF THE PAYROLL FILE EXPANSION PROJECT,        */   13190159
001600*  USER-CODED TRANSACTION INPUT RECORD LENGTHS WILL BE       */   13190159
001700*  EXPANDED BY 22 BYTES, AND RECORDS AND WORK AREAS          */   13190159
001800*  RELATING TO THE EDB FILE WILL BE EXPANDED AS WELL.        */   13190159
001900**************************************************************/   13190159
002000     SKIP2                                                        13190159
002100**************************************************************/   21860149
002200*  COPYMEMBER: CPWSXTIM                                      */   21860149
002300*  RELEASE # ___0149___   SERVICE REQUEST NO(S)____2186_____ */   21860149
002400*  NAME ___M.NISHI_____   MODIFICATION DATE ____05/08/85_____*/   21860149
002500*  DESCRIPTION                                               */   21860149
002600*    COPYMEMBER DESCRIBING THE TIME FILE OUT OF PPP300       */   21860149
002700*    TO BE UPDATED BY PPP920 (RATE ADJUSTMENT TIME FILE      */   21860149
002800*    UPDATE PROGRAM)  AND INPUT TO PPP350.                   */   21860149
002900**************************************************************/   21860149
003000     SKIP1                                                        CPWSXTIM
003100*    COPYID=CPWSXTIM                                              CPWSXTIM
003200*                                                                 CPWSXTIM
003210*01  XTIM-TIME-RECORD.                                            30930413
003400     05  TIME-RCD-CTL-NO         PIC X(5).                        CPWSXTIM
003500     05  TIME-RCD-EMPL-ID        PIC X(9).                        CPWSXTIM
003600     05  TIME-RCD-EMPL-NAME      PIC X(26).                       CPWSXTIM
003700     05  TIME-RCD-ACCT-DIST-NO   PIC XX.                          CPWSXTIM
003800     05  TIME-RCD-DIST-NO        REDEFINES                        14010244
003900         TIME-RCD-ACCT-DIST-NO   PIC XX.                          14010244
004000     05  TIME-RCD-FILLER-A       PIC X.                           CPWSXTIM
004100     05  TIME-RCD-FILLER         REDEFINES                        14010244
004200         TIME-RCD-FILLER-A       PIC X.                           14010244
004300     05  TIME-RCD-TITLE-CODE     PIC X(4).                        CPWSXTIM
006700     05  TIME-RCD-ACCOUNT-FUND.                                   UCSD0102
006800         10   TIME-RCD-LOC       PIC X.                           UCSD0102
007000         10   TIME-RCD-ACCT.                                      UCSD0102
007100              15  FILLER         PIC X(3).                        UCSD0102
007200              15  TIME-RCD-ACCT2 PIC X(3).                        UCSD0102
007300         10   TIME-RCD-FUND      PIC X(5).                        UCSD0102
007400         10   TIME-RCD-SUB       PIC X.                           UCSD0102
007500     05  TIME-RCD-ACCOUNT        REDEFINES                        UCSD0102
007600         TIME-RCD-ACCOUNT-FUND   PIC X(13).                       UCSD0102
007610*****05  TIME-RCD-FAU            PIC X(30).                       UCSD0102
005400*    05  TIME-RCD-FILLER-B       PIC X(4).                        14010244
005500     05  TIME-RCD-FILLER-B       PIC X(3).                        14010244
005600     05  TIME-RCD-DIST-UNIT-CODE PIC X.                           14010244
005700     05  TIME-RCD-TUC            PIC X(2).                        CPWSXTIM
005800     05  TIME-RCD-APPT-TUC       REDEFINES                        14010244
005900         TIME-RCD-TUC            PIC X(2).                        14010244
006000***  THE FOLLOWING FIELD IS ALSO CALLED THE APPT COVERAGE IND.    CPWSXTIM
006100     05  TIME-RCD-APPT-REP-CD    PIC X(1).                        CPWSXTIM
006200     05  TIME-RCD-APPT-COV-IND   REDEFINES                        14010244
006300         TIME-RCD-APPT-REP-CD    PIC X(1).                        14010244
006400     05  TIME-RCD-RATE-AMT       PIC X(7).                        CPWSXTIM
006500     05  TIME-RCD-RATE           REDEFINES                        CPWSXTIM
006600         TIME-RCD-RATE-AMT       PIC 999V9999.                    CPWSXTIM
006700     05  TIME-RCD-AMOUNT         REDEFINES                        CPWSXTIM
006800         TIME-RCD-RATE-AMT       PIC 99999V99.                    CPWSXTIM
006900     05  TIME-RCD-AH-CODE        PIC X.                           CPWSXTIM
007000     05  TIME-RCD-AH-IND         REDEFINES                        14010244
007100         TIME-RCD-AH-CODE        PIC X.                           14010244
007200     05  TIME-RCD-DOS-1          PIC X(3).                        CPWSXTIM
007300     05  TIME-RCD-REG-TIME       PIC X(5).                        CPWSXTIM
007400     05  TIME-RCD-REG-TIME9      REDEFINES                        CPWSXTIM
007500         TIME-RCD-REG-TIME       PIC 999V99.                      CPWSXTIM
007600     05  TIME-RCD-TIME-1         REDEFINES                        14010244
007700         TIME-RCD-REG-TIME9      PIC 999V99.                      14010244
007800     05  TIME-RCD-TIME-1-X       REDEFINES                        14010244
007900         TIME-RCD-TIME-1         PIC X(5).                        14010244
008000     05  TIME-RCD-PRCNT-1        REDEFINES                        14010244
008100         TIME-RCD-TIME-1         PIC 9V9999.                      14010244
008200     05  TIME-RCD-HRS-PCT        PIC X.                           CPWSXTIM
008300     05  TIME-RCD-H-PRCNT        REDEFINES                        14010244
008400         TIME-RCD-HRS-PCT        PIC X.                           14010244
008500     05  TIME-RCD-DOS-2          PIC X(3).                        CPWSXTIM
008600     05  TIME-RCD-OT-TIME        PIC X(5).                        CPWSXTIM
008700     05  TIME-RCD-OT-TIME9       REDEFINES                        CPWSXTIM
008800         TIME-RCD-OT-TIME        PIC 999V99.                      CPWSXTIM
008900     05  TIME-RCD-TIME-2         REDEFINES                        14010244
009000         TIME-RCD-OT-TIME9       PIC 999V99.                      14010244
009100     05  TIME-RCD-PRCNT-2        REDEFINES                        14010244
009200         TIME-RCD-TIME-2         PIC 9V9999.                      14010244
009300     05  TIME-RCD-DOS-3          PIC X(3).                        CPWSXTIM
009400     05  TIME-RCD-OT-TIME-2      PIC X(5).                        CPWSXTIM
009500     05  TIME-RCD-OT-TIME-2-9    REDEFINES                        CPWSXTIM
009600         TIME-RCD-OT-TIME-2      PIC 999V99.                      CPWSXTIM
009700     05  TIME-RCD-TIME-3         REDEFINES                        14010244
009800         TIME-RCD-OT-TIME-2-9    PIC 999V99.                      14010244
009900     05  TIME-RCD-PRCNT-3        REDEFINES                        14010244
010000         TIME-RCD-TIME-3         PIC 9V9999.                      14010244
010100     05  TIME-RCD-WSP            PIC X.                           CPWSXTIM
010200     05  TIME-RCD-WSP-CODE       REDEFINES                        14010244
010300         TIME-RCD-WSP            PIC X.                           14010244
010400     05  TIME-RCD-EMP-REL-CODE   PIC X.                           14010244
010500     05  TIME-RCD-APPT-TYPE-CODE PIC X.                           14010244
010600*    05  TIME-RCD-PAY-END-DATE   PIC X(6).                        14010244
010700*    05  TIME-RCD-PAY-CYCLE      PIC X.                           14010244
010800*    05  TIME-RCD-FILLER-OP      PIC X(02).                       14010244
013200*****05  TIME-RCD-FILLER-COS     PIC X(20).                       UCSD0102
013300*****05  TIME-RCD-FILLER-COS.                                     32021138
013400*****    10  TIME-RCD-FILLER-COS10  PIC X(10).                    32021138
013500*****    10  TIME-RCD-COST-CENTER   PIC X(4).                     32021138
013600*****    10  TIME-RCD-PROJECT-CODE  PIC X(6).                     32021138
015100     05  TIME-RCD-ACCOUNT-INDEX  PIC X(7).                        UCSD0039
015200     05  TIME-RCD-ACCOUNT-INDEX-GROUP        REDEFINES            UCSD0039
015300         TIME-RCD-ACCOUNT-INDEX.                                  UCSD0039
015400         10  TIME-RCD-ACCOUNT-INDEX-DEPT     PIC X(3).            UCSD0039
015500         10  TIME-RCD-ACCOUNT-INDEX-NUMBER   PIC X(4).            UCSD0039
015600     05  TIME-RCD-FUND-EXTENSION PIC X(1).                        UCSD0039
015700     05  TIME-RCD-ORGANIZATION   PIC X(6).                        UCSD0039
015800     05  TIME-RCD-PROGRAM        PIC X(6).                        UCSD0039
015900     05  TIME-RCD-PROGRAM-GROUP              REDEFINES            UCSD0039
016000         TIME-RCD-PROGRAM.                                        UCSD0039
016100         10  TIME-RCD-PROGRAM-FUNCTION       PIC X(2).            UCSD0039
016200         10  TIME-RCD-PROGRAM-SUB-FUNCTION   PIC X(4).            UCSD0039
011000     05  TIME-RCD-PAY-END-DATE   PIC X(6).                        14010244
011100     05  TIME-RCD-END-DATE       REDEFINES                        14010244
011200         TIME-RCD-PAY-END-DATE   PIC X(6).                        14010244
011300     05  TIME-RCD-PAY-CYCLE      PIC X.                           14010244
011310     05  TIME-RCD-DEPT           PIC X(6).                        45020369
011320     05  TIME-RCD-PART-PER       PIC X(3).                        45020369
011321     05  TIME-RCD-RPT-CD         PIC X(1).                        45020369
011322       88 TYPE1                 VALUE 'A' 'T'.                    45020369
011323     05  TIME-RCD-PERQ           PIC X(5).                        45020369
011324     05  TIME-RCD-PAGE           PIC 9(4).                        45020369
011325     05  TIME-RCD-CYCLE          PIC XX.                          45020369
011330     05  TIME-RCD-TMKP-DEPTNBR   PIC X(8).                        UCSD0023
011340     05  FILLER                  PIC X(1).                        UCSD0023
011400     SKIP3                                                        CPWSXTIM
