000100*==========================================================%      DS795
000200*=    COPY MEMBER: CLPDP900                               =%      DS795
000300*=    CHANGE # DS795        PROJ. REQUEST: LE/370         =%      DS795
000400*=                                         CONVERSION     =%      DS795
000500*=    NAME: G. CHIU         MODIFICATION DATE: 09/11/95   =%      DS795
000600*=                                                        =%      DS795
000700*=    DESCRIPTION:                                        =%      DS795
000800*=     REPLACED TIME 'ACCEPT' STATEMENT WITH              =%      DS795
000900*=     LE/370 VERSION OF CPPDTIME                         =%      DS795
001000*==========================================================%      DS795
      *                                                                 CLPDP900
      *      CONTROL REPORT PPP900CR                                    CLPDP900
      *                                                                 CLPDP900
      *                                                                 CLPDP900
       PRINT-CONTROL-REPORT SECTION.                                    CLPDP900
                                                                        CLPDP900
           OPEN OUTPUT CONTROLREPORT.                                   CLPDP900
                                                                        CLPDP900
           MOVE 'PPP900CR'             TO CR-HL1-RPT.                   CLPDP900
           MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP900
           WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP900
                                                                        CLPDP900
           MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP900
           WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP900
                                                                        CLPDP900
           MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP900
           MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP900
           MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP900
           MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP900
           WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP900
                                                                        CLPDP900
           MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP900
           WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP900
                                                                        CLPDP900
           MOVE SPEC-CARD              TO CR-DL5-CTL-CARD.              CLPDP900
           MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP900
           WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP900
                                                                        CLPDP900
           MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP900
           WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP900
                                                                        CLPDP900
004200*****ACCEPT TIME-WORK-AREA FROM TIME.                             DS795
004300     COPY CPPDTIME.                                               DS795
004400     MOVE PRE-EDIT-TIME          TO TIME-WORK-AREA.               DS795
      *    MOVE TIME-OF-DAY            TO TIME-WORK-AREA.               CLPDP900
           MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP900
           MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP900
           MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP900
           WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP900
                                                                        CLPDP900
      *                                                                 CLPDP900
           MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP900
           WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP900
                                                                        CLPDP900
           CLOSE CONTROLREPORT.                                         CLPDP900
      *                                                                 CLPDP900
       PRINT-CONTROL-REPORT-EXIT.                                       CLPDP900
            EXIT.                                                       CLPDP900
