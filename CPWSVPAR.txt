000100**************************************************************/   74611376
000200*  COPYMEMBER: CPWSVPAR                                      */   74611376
000300*  RELEASE: ___1376______ SERVICE REQUEST(S): ____17461____  */   74611376
000400*  NAME:___STEINITZ______ MODIFICATION DATE:  ___10/31/01__  */   74611376
000500*  DESCRIPTION:                                              */   74611376
000600*  - ADDED DE 2009      FOR SUB-LOCATION PROCESSING.         */   74611376
000700**************************************************************/   74611376
000010**************************************************************/   32021138
000020*  COPYMEMBER: CPWSVPAR                                      */   32021138
000030*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000040*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000050*  DESCRIPTION:                                              */   32021138
000060*  - MODIFIED FOR FULL ACCOUNTING UNIT                       */   32021138
000070**************************************************************/   32021138
000100**************************************************************/   36500900
000200*  COPYMEMBER: CPWSVPAR                                      */   36500900
000300*  RELEASE: ___0900______ SERVICE REQUEST(S): ____3650_____  */   36500900
000400*  NAME: ___ WRW (BIT)___ MODIFICATION DATE:  __05/20/94____ */   36500900
000500*  DESCRIPTION:                                              */   36500900
000600*     MODIFIED FOR COST CENTER AND PROJECT CODE ENHANCEMENT  */   36500900
000700**************************************************************/   36500900
000100**************************************************************/   36060628
000200*  COPYMEMBER: CPWSVPAR                                      */   36060628
000300*  RELEASE # ____0628____ SERVICE REQUEST NO(S)___3606_______*/   36060628
000400*  NAME _ G. STEINITZ _   DATE CREATED: ________01/10/92_____*/   36060628
000500*  DESCRIPTION                                               */   36060628
000600*     MADE VARIABLE PAR LAYOUT A COPY MEMBER FOR PPP390 AND  */   36060628
000700*     ITS SUBROUTINES. ADDED WORK STUDY CURRENT STUDENT FLAG */   36060628
000800*     FINAID CATEGORY, CLASS TITLE OUTLINE, AND TYPE.        */   36060628
000900*     KEPT OCCURS AT MAXIMUM AS WAS DONE PREVIOUSLY, BUT IT  */   36060628
001000*     WOULD MAKE MORE SENSE TO USE OCCURS DEPENDING ON.      */   36060628
001100*                                                            */   36060628
001200*     ADDED WPAR-BRSC TO MATCH CPWSXPAR LAYOUT               */   36060628
001300**************************************************************/   36060628
001400*COPYID=CPWSVPAR                                             */   CPWSVPAR
001500**************************************************************/   CPWSVPAR
001600*                                                                 CPWSVPAR
001700*01  WORK-PAR-VARIABLE-FLDS.                                      CPWSVPAR
001800     05  WPAR-VARIABLE-LNGH-INDS                     COMP.        CPWSVPAR
001900         10  WPAR-NO-HRS-ADJ         PIC S9(04)      SYNC.        CPWSVPAR
002000         10  WPAR-NO-ADJ             PIC S9(04)      SYNC.        CPWSVPAR
002100         10  WPAR-NO-ACCTS           PIC S9(04)      SYNC.        CPWSVPAR
002200         10  WPAR-NO-DED-REF         PIC S9(04)      SYNC.        CPWSVPAR
002300         10  WPAR-NO-WORK-FIL        PIC S9(04)      SYNC.        CPWSVPAR
002400     05  WPAR-VARIABLE-PORTION.                                   CPWSVPAR
002500*                                                                 CPWSVPAR
002600***       HOURS ADJUSTMENTS                                       CPWSVPAR
002700*                                                                 CPWSVPAR
002800         10  WPAR-HRS-ADJUSTMENTS                    OCCURS 10.   CPWSVPAR
002900             15  WPAR-HRS-CD         PIC  9(04)      COMP SYNC.   CPWSVPAR
003000             15  WPAR-HRS-ADJ        PIC S9(05)V9(06)    COMP-3.  CPWSVPAR
003100*                                                                 CPWSVPAR
003200***       DOLLAR ADJUSTMENTS                                      CPWSVPAR
003300*                                                                 CPWSVPAR
003400         10  WPAR-ADJ-FLDS                           OCCURS 20.   CPWSVPAR
003500             15  WPAR-ADJ-CD         PIC  9(04).                  CPWSVPAR
003600             15  WPAR-ADJ-TYPE       PIC  X(01).                  CPWSVPAR
003700             15  WPAR-ADJ-AMT        PIC S9(07)V9(02)    COMP-3.  CPWSVPAR
003800*                                                                 CPWSVPAR
003900***       EARNINGS DISTRIBUTION                                   CPWSVPAR
004000*                                                                 CPWSVPAR
004100         10  WPAR-EARNGS-DIST-ARRAY.                              CPWSVPAR
004200           12  WPAR-EARN-DIST                        OCCURS 99.   CPWSVPAR
004300             15  WPAR-DIST-NO        PIC  X(02).                  CPWSVPAR
004400             15  WPAR-DIST-NO-NUMERIC REDEFINES WPAR-DIST-NO      CPWSVPAR
004500                                     PIC  9(02).                  CPWSVPAR
005300*****        15  WPAR-ACCT-FUND.                                  32021138
005400*****            20  WPAR-AF-LOC     PIC  X(01).                  32021138
005500*****            20  WPAR-ACCT       PIC  9(06).                  32021138
005600*****            20  WPAR-CC         PIC  X(04).                  32021138
005700*****            20  WPAR-FUND       PIC  9(05).                  32021138
005800*****            20  WPAR-PC         PIC  X(06).                  32021138
005900*****            20  WPAR-AF-SUB     PIC  9(01).                  32021138
006000*****        15  WPAR-ACCT-FUND-ELEM REDEFINES WPAR-ACCT-FUND     32021138
006100****                                 PIC  X(13).                  36500900
006200*****                                PIC  X(23).                  32021138
006300             15  WPAR-FAU            PIC  X(30).                  32021138
005300             15  WPAR-TITLE-UNIT-CODE                             CPWSVPAR
005400                                     PIC  X(02).                  CPWSVPAR
005500             15  WPAR-EARN-SPCL-HNDLG-CODE                        CPWSVPAR
005600                                     PIC  X(01).                  CPWSVPAR
005700             15  WPAR-EARN-COVERAGE-IND                           CPWSVPAR
005800                                     PIC  X(01).                  CPWSVPAR
005900             15  WPAR-EARN-DIST-UNIT-CODE                         CPWSVPAR
006000                                     PIC  X(01).                  CPWSVPAR
006100             15  WPAR-EARN-EMP-REL-CODE                           CPWSVPAR
006200                                     PIC  X(01).                  CPWSVPAR
006300             15  WPAR-LV-ASSESSMNT-IND                            CPWSVPAR
006400                                     PIC  X(01).                  CPWSVPAR
006500             15  WPAR-ACC-MGE-B      PIC  X(01).                  CPWSVPAR
006600             15  WPAR-PAYCY-CODE     PIC  X(01).                  CPWSVPAR
006700             15  WPAR-RETR-ELIG      PIC  X(01).                  CPWSVPAR
006800             15  WPAR-PAYRATE-HR     PIC S9(03)V9(04)    COMP-3.  CPWSVPAR
006900             15  WPAR-PAYRATE-SAL REDEFINES WPAR-PAYRATE-HR       CPWSVPAR
007000                                     PIC S9(05)V9(02)    COMP-3.  CPWSVPAR
007100             15  WPAR-RATE-TYPE      PIC  X(01).                  CPWSVPAR
007200             15  WPAR-EXPS-XFER      PIC  X(01).                  CPWSVPAR
007300             15  WPAR-LV-AC-CODE     PIC  X(01).                  CPWSVPAR
007400             15  WPAR-WK-STDY-CD     PIC  X(01).                  CPWSVPAR
007500             15  WPAR-INTRANS-CD     PIC  X(02).                  CPWSVPAR
007600             15  WPAR-PER-END-DATE.                               CPWSVPAR
007700                 20  WPAR-PER-END-YY PIC  9(02).                  CPWSVPAR
007800                 20  WPAR-PER-END-MM PIC  9(02).                  CPWSVPAR
007900                 20  WPAR-PER-END-DD PIC  9(02).                  CPWSVPAR
008000             15  WPAR-TITLE.                                      CPWSVPAR
008100                 20  FILLER          PIC  9(01).                  CPWSVPAR
008200                 20  WPAR-TTL-CD     PIC  9(04).                  CPWSVPAR
008300             15  WPAR-EARN-TYPE      PIC  X(03).                  CPWSVPAR
008400             15  WPAR-ADJ-IND        PIC  X(01).                  CPWSVPAR
008500             15  WPAR-TIME           PIC S9(03)V9(02)    COMP-3.  CPWSVPAR
008600             15  WPAR-EARN-AMT       PIC S9(05)V9(02)    COMP-3.  CPWSVPAR
008700             15  WPAR-EARN-PCT       PIC S9(01)V9(04)    COMP-3.  CPWSVPAR
008800             15  WPAR-CURR-STUDENT-FLAG                           CPWSVPAR
008900                                     PIC  X(01).                  CPWSVPAR
009000             15  WPAR-FINAID-CATEGORY                             CPWSVPAR
009100                                     PIC  X(01).                  CPWSVPAR
009200             15  WPAR-CLASS-TTL-OUTLINE                           CPWSVPAR
009300                                     PIC  X(03).                  CPWSVPAR
009400             15  WPAR-FINAID-TYPE    PIC  X(01).                  CPWSVPAR
009500             15  WPAR-WSP-EXEMPT-SW  PIC  X(01).                  CPWSVPAR
009600             15  WPAR-EARN-FILLER-OP PIC  X(05).                  CPWSVPAR
009700             15  WPAR-EARN-FILLER-COS                             CPWSVPAR
009800                                     PIC  X(20).                  CPWSVPAR
009900             15  WPAR-DOS-PRIORITY   PIC  9(01).                  CPWSVPAR
010000             15  WPAR-APPT-TYPE      PIC  X(01).                  CPWSVPAR
011100*****        15  FILLER              PIC  X(01).                  32021138
012700***          15  FILLER              PIC  X(09).                  74611376
012710             15  WPAR-SUB-LOCATION   PIC  X(02).                  74611376
012720             15  FILLER              PIC  X(07).                  74611376
010200*                                                                 CPWSVPAR
010300***       DEDUCTION RELATED DATA                                  CPWSVPAR
010400*                                                                 CPWSVPAR
010500         10  WPAR-CUR-DED-REF.                                    CPWSVPAR
010600             15  WPAR-CUR-DED-ENTRY                  OCCURS 40.   CPWSVPAR
010700                 20  WPAR-CUR-CD     PIC  9(03).                  CPWSVPAR
010800                 20  WPAR-GTN-TYPE   PIC  X(01).                  CPWSVPAR
010900                 20  WPAR-GTN-GROUP  PIC  X(01).                  CPWSVPAR
011000                 20  WPAR-SPEC-TRANS PIC  X(01).                  CPWSVPAR
011100                 20  WPAR-SOURCE-CD  PIC  X(01).                  CPWSVPAR
011200                 20  WPAR-DED-QTR-CD PIC  X(01).                  CPWSVPAR
011300                 20  WPAR-DED-YR-CD  PIC  X(01).                  CPWSVPAR
011400                 20  WPAR-DED-FIL    PIC  X(01).                  CPWSVPAR
011500                 20  WPAR-CUR-AMT    PIC S9(05)V9(02)    COMP-3.  CPWSVPAR
011600                 20  WPAR-BRSC       PIC  X(05).                  CPWSVPAR
011700*                                                                 CPWSVPAR
011800***       WORK AREA FILLER                                        CPWSVPAR
011900*                                                                 CPWSVPAR
012000         10  WPAR-FILLER-DATA                        OCCURS 20.   CPWSVPAR
012100             15  WPAR-WORK-AREA.                                  CPWSVPAR
012200               20 WPAR-WK-AMT        PIC S9(07)V9(02)    COMP-3.  CPWSVPAR
012300               20 WPAR-WK-CHR        PIC  X(01).                  CPWSVPAR
012400     05  WPAR-ADVANCE-INFO.                                       CPWSVPAR
012500         10  WPAR-ADV-PAY            PIC S9(05)V9(02)    COMP-3.  CPWSVPAR
012600         10  ADVANCE-DED-AMT         PIC S9(05)V9(02)    COMP-3.  CPWSVPAR
