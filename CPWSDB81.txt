000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSDB81                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   30860362
000200*  COPYMEMBER: CPWSDB81                                      */   30860362
000300*  RELEASE # ____0362____ SERVICE REQUEST NO(S)____3086______*/   30860362
000400*  NAME ______JLT______   MODIFICATION DATE ____05/26/87_____*/   30860362
000500*                                                            */   30860362
000600*  DESCRIPTION                                               */   30860362
000700*   - DEFINE LEAVE HISTORY SEGMENTS (SEGS 8100 THRU 8500).   */   30860362
000800**************************************************************/   30860362
000900     SKIP2                                                        CPWSDB81
001000*    COPYID=CPWSDB81                                              CPWSDB81
001100*01  XDBS-8100-SEGMENTS.                                          30930413
001200*                                                                *CPWSDB81
001300******************************************************************CPWSDB81
001400*   L E A V E   H I S T O R I C A L   E N T R I E S               CPWSDB81
001500*                  S E G M E N T   8 1 0 0   T H R U   8 5 0 0    CPWSDB81
001600******************************************************************CPWSDB81
001700*                                                                 CPWSDB81
001800     03  XDBS-8100-SEGMENT       OCCURS  5.                       CPWSDB81
001900         05  XDBS-DELETE-81              PICTURE X.               CPWSDB81
002000             88  XDBS-SEG-DELETED-81         VALUE HIGH-VALUE.    CPWSDB81
002100         05  XDBS-KEY-81.                                         CPWSDB81
002200             07  XDBS-ID-NUMBER-81       PICTURE X(9).            CPWSDB81
002300             07  XDBS-SEGMENT-81.                                 CPWSDB81
002400                 09  XDBS-SEG-NO-81      PICTURE XX.              CPWSDB81
002500                     88  XDBS-SEG-8100       VALUE '81'.          CPWSDB81
002600                     88  XDBS-SEG-8200       VALUE '82'.          CPWSDB81
002700                     88  XDBS-SEG-8300       VALUE '83'.          CPWSDB81
002800                     88  XDBS-SEG-8400       VALUE '84'.          CPWSDB81
002900                     88  XDBS-SEG-8500       VALUE '85'.          CPWSDB81
003000                 09  FILLER              PICTURE XX.              CPWSDB81
003100         05  XDBS-8100-SEG-DATA.                                  CPWSDB81
003200             07  XDBS-8100-LV-HIST-ACRU-PER   OCCURS 3.           CPWSDB81
003300                 09  XDBS-8100-LV-HIST-ENTRY      OCCURS 4.       CPWSDB81
003400                     11  XDBS-8101-LV-HIST-KEY    PICTURE X(6).   CPWSDB81
003500*                           CONTENTS: -TITLE TYPE  X              CPWSDB81
003600*                                     -TUC         XX             CPWSDB81
003700*                                     -AREP        X              CPWSDB81
003800*                                     -SHC         X              CPWSDB81
003900*                                     -DUC         X              CPWSDB81
004000*                                                                 CPWSDB81
004100                     11  XDBS-8102-LV-HIST-LV-PLN PICTURE X.      CPWSDB81
004200                     11  XDBS-8103-LV-HIST-HRS-TWRD-LV            CPWSDB81
004300                                         PICTURE S9(3)V99 COMP-3. CPWSDB81
004400                     11  XDBS-8104-LV-HIST-HRS-CD PICTURE X.      CPWSDB81
004500                     11  XDBS-8105-LV-HIST-VAC-ACCRUED            CPWSDB81
004600                                         PICTURE S9(3)V99 COMP-3. CPWSDB81
004700                     11  XDBS-8106-LV-HIST-SKL-ACCRUED            CPWSDB81
004800                                         PICTURE S9(3)V99 COMP-3. CPWSDB81
004900                     11  XDBS-8107-LV-HIST-PTO-ACCRUED            CPWSDB81
005000                                         PICTURE S9(3)V99 COMP-3. CPWSDB81
005100*                                                                 CPWSDB81
005200             07  XDBS-8100-FILLER        PICTURE X(36).           CPWSDB81
005300*                                                                 CPWSDB81
005400*  *  *  *  *  *                                                  CPWSDB81
