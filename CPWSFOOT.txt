000100**************************************************************/   36370967
000200*  COPYMEMBER: CPWSFOOT                                      */   36370967
000300*  RELEASE: ___0967______ SERVICE REQUEST(S): _____3637____  */   36370967
000400*  NAME:_______PXP_______ MODIFICATION DATE:  __03/10/95____ */   36370967
000500*  DESCRIPTION:                                              */   36370967
000600*  ADD FIELDS USED IN THF ENTRY/UPDATE                       */   36370967
000700**************************************************************/   36370967
000100**************************************************************/   36430911
000200*  COPYMEMBER: CPWSFOOT                                      */   36430911
000300*  RELEASE: ___0911______ SERVICE REQUEST(S): _____3643____  */   36430911
000400*  NAME:_______MLG_______ MODIFICATION DATE:  __06/30/94____ */   36430911
000500*  DESCRIPTION:                                              */   36430911
000600*    ADD FIELDS USED IN PAR INQUIRY FOOTER TO SUPPORT        */   36430911
000700*    OLD SYSTEM CONVERSION.                                  */   36430911
000800**************************************************************/   36430911
000000**************************************************************/   36430795
000001*  COPYMEMBER: CPWSFOOT                                      */   36430795
000002*  RELEASE: ___0795______ SERVICE REQUEST(S): _____3643____  */   36430795
000003*  NAME:_______MLG_______ MODIFICATION DATE:  __07/15/93____ */   36430795
000004*  DESCRIPTION:                                              */   36430795
000005*    EDB ENTRY/UPDATE FOOTER MAP DATA                        */   36430795
000006*                                                            */   36430795
000007**************************************************************/   36430795
000100*01  CPWSFOOT   EXTERNAL.                                         CPWSFOOT
000200     05  CPWSFOOT-EXT-AREA                 PIC X(1024).           CPWSFOOT
000300     05  CPWSFOOT-EXT-DATA REDEFINES CPWSFOOT-EXT-AREA.           CPWSFOOT
009100         10  CPWSFOOT-ENTERED-FUNC         PIC X(04).             CPWSFOOT
009200         10  CPWSFOOT-ENTERED-ID           PIC X(09).             CPWSFOOT
009300         10  CPWSFOOT-ENTERED-NAME         PIC X(26).             CPWSFOOT
009400         10  CPWSFOOT-ENTERED-SSN          PIC X(09).             CPWSFOOT
002400         10  CPWSFOOT-ENTERED-PAY-CYCLE    PIC X(02).             36430911
002500         10  CPWSFOOT-ENTERED-CHECK-DATE   PIC X(06).             36430911
002600         10  CPWSFOOT-ENTERED-EFF-DATE     PIC X(06).             36430911
003400         10  CPWSFOOT-ENTERED-TRAN-SEQ     PIC X(5).              36370967
003500         10  CPWSFOOT-ENTERED-TRAN-SEQ-9  REDEFINES               36370967
003600             CPWSFOOT-ENTERED-TRAN-SEQ     PIC 9(5).              36370967
003700         10  CPWSFOOT-ENTERED-PPE-DATE.                           36370967
003800             15 CPWSFOOT-ENTERED-PPE-DATE-YY PIC X(2).            36370967
003900             15 CPWSFOOT-ENTERED-PPE-DATE-MM PIC X(2).            36370967
004000             15 CPWSFOOT-ENTERED-PPE-DATE-DD PIC X(2).            36370967
004100         10  CPWSFOOT-FILLER               PIC X(951).            36370967
004200*****    10  CPWSFOOT-FILLER               PIC X(962).            36370967
002800*********10  CPWSFOOT-FILLER               PIC X(976).            36430911
