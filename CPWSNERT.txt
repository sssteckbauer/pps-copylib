000000**************************************************************/   32201186
000001*  COPYMEMBER: CPWSNERT                                      */   32201186
000002*  RELEASE: ___1186______ SERVICE REQUEST(S): ____13220____  */   32201186
000003*  NAME:_______QUAN______ CREATION DATE:      ___04/17/98__  */   32201186
000004*  DESCRIPTION:                                              */   32201186
000050*    - THIS COPYMEMBER CONTAINS THE RECORD LAYOUTS FOR THE   */   32201186
000060*      NEW EMPLOYEE REGISTRY REPORTING TAPE SENT TO EDD.     */   32201186
000070*      THERE ARE THREE RECORD LAYOUTS: E4, W4, T4.           */   32201186
000090**************************************************************/   32201186
012300     SKIP2                                                        CPWSNERT
012400*01  NRTP-NER-REPORTING-RECORDS.                                  CPWSNERT
016600     05  E4-RECORD.                                               CPWSNERT
016700         10  E4-RECORD-CODE          PIC X(02)     VALUE 'E4'.    CPWSNERT
017100         10  E4-FED-EMPLOYER-ID      PIC 9(09).                   CPWSNERT
017200         10  E4-FED-EMPLOYER-IDX     REDEFINES                    CPWSNERT
017300             E4-FED-EMPLOYER-ID      PIC X(09).                   CPWSNERT
017400         10  E4-STATE-EMPLOYER-ID    PIC X(08).                   CPWSNERT
017500         10  E4-BRANCH-CODE          PIC X(03).                   CPWSNERT
017510         10  E4-EMPLOYER-NAME        PIC X(45).                   CPWSNERT
017600         10  E4-EMPLOYER-ADDRESS     PIC X(40).                   CPWSNERT
017700         10  E4-EMPLOYER-CITY        PIC X(25).                   CPWSNERT
017900         10  E4-EMPLOYER-STATE       PIC X(02).                   CPWSNERT
018100         10  E4-EMPLOYER-ZIP-CODE.                                CPWSNERT
018300             15  E4-EMPLOYER-ZIP     PIC X(05).                   CPWSNERT
018400             15  E4-EMPLOYER-ZIP-EXT PIC X(04).                   CPWSNERT
019100         10  FILLER                  PIC X(32)     VALUE SPACES.  CPWSNERT
019700     SKIP2                                                        CPWSNERT
019800     05  W4-RECORD.                                               CPWSNERT
019900         10  W4-RECORD-CODE          PIC X(02)     VALUE 'W4'.    CPWSNERT
020000         10  W4-EMPLOYEE-SSN         PIC X(09).                   CPWSNERT
020100         10  W4-EMPLOYEE-FIRST-NAME  PIC X(16).                   CPWSNERT
020200         10  W4-EMPLOYEE-MIDDLE-INIT PIC X(01).                   CPWSNERT
020300         10  W4-EMPLOYEE-LAST-NAME   PIC X(30).                   CPWSNERT
020500         10  W4-EMPLOYEE-ADDRESS     PIC X(40).                   CPWSNERT
020600         10  W4-EMPLOYEE-CITY        PIC X(25).                   CPWSNERT
020800         10  W4-EMPLOYEE-STATE       PIC X(02).                   CPWSNERT
021000         10  W4-EMPLOYEE-ZIP-CODE.                                CPWSNERT
021200             15  W4-EMPLOYEE-ZIP     PIC X(05).                   CPWSNERT
021210             15  W4-EMPLOYEE-ZIP-EXT PIC X(04).                   CPWSNERT
021300         10  W4-EMPLOYEE-FOREIGN-POSTAL-CD REDEFINES              CPWSNERT
021400             W4-EMPLOYEE-ZIP-CODE.                                CPWSNERT
021500             15  W4-EMPLOYEE-FOREIGN-CD-1 PIC X(05).              CPWSNERT
021600             15  W4-EMPLOYEE-FOREIGN-CD-2 PIC X(04).              CPWSNERT
021700         10  W4-EMPLOYEE-HIRE-DATE   PIC X(08).                   CPWSNERT
024600         10  FILLER                  PIC X(33)     VALUE SPACES.  CPWSNERT
034500     SKIP2                                                        CPWSNERT
034600     05  T4-RECORD.                                               CPWSNERT
034700         10  T4-RECORD-CODE          PIC X(02)     VALUE 'T4'.    CPWSNERT
034800         10  T4-EMPLOYEE-COUNT       PIC 9(11).                   CPWSNERT
037900         10  FILLER                  PIC X(162)    VALUE SPACES.  CPWSNERT
