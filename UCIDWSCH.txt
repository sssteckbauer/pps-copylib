000000**************************************************************/   36430821
000001*  COPYMEMBER: UCIDWSCH                                      */   36430821
000002*  RELEASE: ___0821______ SERVICE REQUEST(S): _____3643____  */   36430821
000003*  NAME:_______STC_______ MODIFICATION DATE:  __10/05/93____ */   36430821
000004*  DESCRIPTION:                                              */   36430821
000005*     CHECK DIGIT PROGRAM INTERFACE                          */   36430821
000006*                                                            */   36430821
000007**************************************************************/   36430821
001000*01  UCIDWSCH-INTERFACE EXTERNAL.                                 UCIDWSCH
001100     05  UCIDWSCH-NUM-WITHOUT-CHK-DGT    PICTURE 9(06).           UCIDWSCH
001200     05  UCIDWSCH-CHK-DIGIT-APPENDED     PICTURE 9(07).           UCIDWSCH
