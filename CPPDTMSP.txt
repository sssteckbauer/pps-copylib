000100*    COPYID=CPPDTMSP                                              CPPDTMSP
000200******************************************************************CPPDTMSP
000300* TIME-SPAN ROUTINE CALCULATES THE LENGTH OF TIME BETWEEN TWO   * CPPDTMSP
000400* DATES SPECIFIED AS INPUT PARAMETERS.                          * CPPDTMSP
000500******************************************************************CPPDTMSP
000600      MOVE ZERO TO ADJ.                                           CPPDTMSP
000700      IF FROM-DATE-DD > TO-DATE-DD                                CPPDTMSP
000800          MOVE -1 TO ADJ                                          CPPDTMSP
000900      ELSE                                                        CPPDTMSP
001000          IF FROM-DATE-DD = 01 AND (TO-DATE-DD > 28 OR            CPPDTMSP
001100                                    TO-DATE-DD = 28) AND          CPPDTMSP
001200                                    TO-DATE-MM = 02               CPPDTMSP
001300          OR FROM-DATE-DD = 01 AND (TO-DATE-DD > 31 OR            CPPDTMSP
001400                                    TO-DATE-DD = 31) AND          CPPDTMSP
001500                                    TO-DATE-MM = 01               CPPDTMSP
001600          OR FROM-DATE-DD = 01 AND (TO-DATE-DD > 31 OR            CPPDTMSP
001700                                    TO-DATE-DD = 31) AND          CPPDTMSP
001800                                    TO-DATE-MM = 03               CPPDTMSP
001900          OR FROM-DATE-DD = 01 AND (TO-DATE-DD > 31 OR            CPPDTMSP
002000                                   TO-DATE-DD = 31) AND           CPPDTMSP
002100                                   TO-DATE-MM = 05                CPPDTMSP
002200          OR FROM-DATE-DD = 01 AND (TO-DATE-DD > 31 OR            CPPDTMSP
002300                                    TO-DATE-DD = 31) AND          CPPDTMSP
002400                                    TO-DATE-MM = 07               CPPDTMSP
002500          OR FROM-DATE-DD = 01 AND (TO-DATE-DD > 31 OR            CPPDTMSP
002600                                    TO-DATE-DD = 31) AND          CPPDTMSP
002700                                    TO-DATE-MM = 08               CPPDTMSP
002800          OR FROM-DATE-DD = 01 AND (TO-DATE-DD > 31 OR            CPPDTMSP
002900                                   TO-DATE-DD = 31) AND           CPPDTMSP
003000                                    TO-DATE-MM = 10               CPPDTMSP
003100          OR FROM-DATE-DD = 01 AND (TO-DATE-DD > 31 OR            CPPDTMSP
003200                                    TO-DATE-DD = 31) AND          CPPDTMSP
003300                                    TO-DATE-MM = 12               CPPDTMSP
003400         OR FROM-DATE-DD = 01 AND (TO-DATE-DD > 30 OR             CPPDTMSP
003500                                   TO-DATE-DD = 30) AND           CPPDTMSP
003600                                   TO-DATE-MM = 04                CPPDTMSP
003700     OR FROM-DATE-DD = 01 AND (TO-DATE-DD > 30 OR                 CPPDTMSP
003800                                   TO-DATE-DD = 30) AND           CPPDTMSP
003900                                  TO-DATE-MM = 06                 CPPDTMSP
004000          OR FROM-DATE-DD = 01 AND (TO-DATE-DD > 30 OR            CPPDTMSP
004100                                    TO-DATE-DD = 30) AND          CPPDTMSP
004200                                    TO-DATE-MM = 09               CPPDTMSP
004300          OR FROM-DATE-DD = 01 AND (TO-DATE-DD > 30 OR            CPPDTMSP
004400                                    TO-DATE-DD = 30) AND          CPPDTMSP
004500                                    TO-DATE-MM = 11               CPPDTMSP
004600      MOVE 1 TO ADJ.                                              CPPDTMSP
004700      COMPUTE     ELAPSED-MONTHS = ((TO-DATE-YY * 12) +           CPPDTMSP
004800                                     TO-DATE-MM)                  CPPDTMSP
004900                                  -((FROM-DATE-YY * 12) +         CPPDTMSP
005000                                   FROM-DATE-MM) +  ADJ.          CPPDTMSP
005100      COMPUTE     ELAPSED-YRS-MONTHS-YY = ELAPSED-MONTHS / 12.    CPPDTMSP
005200      COMPUTE     ELAPSED-YRS-MONTHS-MM = ELAPSED-MONTHS -        CPPDTMSP
005300                                    (ELAPSED-YRS-MONTHS-YY * 12). CPPDTMSP
