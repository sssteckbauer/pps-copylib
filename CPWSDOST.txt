000008**************************************************************/   48861503
000009*  COPYMEMBER: CPWSDOST                                      */   48861503
000010*  RELEASE: ___1503______ SERVICE REQUEST(S): ____14886____  */   48861503
000011*  NAME:__S.ISAACS_______ MODIFICATION DATE:  ___07/01/03__  */   48861503
000012*  DESCRIPTION:                                              */   48861503
000013*  - INCREASE OCCURS ON DOST-GROSS-IND FROM 10 TO 12 TO      */   48861503
000014*     ACCOMODATE FLAT TAX ON BONUSES                         */   48861503
000015**************************************************************/   48861503
000000**************************************************************/   52191347
000001*  COPYMEMBER: CPWSDOST                                      */   52191347
000002*  RELEASE: ___1347______ SERVICE REQUEST(S): ____15219____  */   52191347
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___03/15/01__  */   52191347
000004*  DESCRIPTION:                                              */   52191347
000005*  - ADDED FACULTY SUMMER SALARY INDICATOR                   */   52191347
000007**************************************************************/   52191347
000100**************************************************************/   71921294
000200*  COPYMEMBER: CPWSDOST                                      */   71921294
000300*  RELEASE: ___1294______ SERVICE REQUESTS: ____7192_______  */   71921294
000400*  NAME _______JLT_____   MODIFICATION DATE ____04/01/00_____*/   71921294
000500*  DESCRIPTION                                               */   71921294
000600*   - REDEFINED THE "SUBJECT GROSS" INDICATORS SO THAT THEY  */   71921294
000700*     MAY BE USED INDIVIDUALLY.                              */   71921294
000800**************************************************************/   71921294
000100**************************************************************/   28171053
000200*  COPYMEMBER: CPWSDOST                                      */   28171053
000300*  RELEASE: ___1053______ SERVICE REQUEST(S): ____12817____  */   28171053
000400*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___03/01/96__  */   28171053
000500*  DESCRIPTION:                                              */   28171053
000600*  - ADDED TIME-ON-CALL RATE INDICATOR FOR SR 12817          */   28171053
000700*  - ADDED FLSA REGULAR RATE INDICATOR FOR FUTURE USE        */   28171053
000800*  - ADDED CSER INDICATOR FOR FUTURE USE                     */   28171053
000900**************************************************************/   28171053
000010**************************************************************/   36520886
000020*  COPYMEMBER: CPWSDOST                                      */   36520886
000031*  RELEASE: ___0886______ SERVICE REQUESTS: 3652,11618_____  */   36520886
000040*  NAME __________JLT__   MODIFICATION DATE ____03/30/94_____*/   36520886
000050*  DESCRIPTION                                               */   36520886
000052*   - ADDED TO FIELDS TO DOS TABLE:                          */   36520886
000053*        1) SABBATICAL IND (SRN 3652).                       */   36520886
000054*        2) HS-COMP-IND (SRN 11618).                         */   36520886
000070**************************************************************/   36520886
000100**************************************************************/   36060628
000200*  COPYMEMBER: CPWSDOST                                      */   36060628
000300*  RELEASE # ____0628____ SERVICE REQUEST NO(S)___3606_______*/   36060628
000400*  NAME __ NEWHOUSE ___   MODIFICATION DATE ____01/10/92_____*/   36060628
000500*  DESCRIPTION                                               */   36060628
000600*   - ADDED EXCLUDE FROM WORK STUDY SPLITTING FLAG           */   36060628
000100**************************************************************/   40210592
000200*  COPYMEMBER: CPWSDOST                                      */   40210592
000300*  RELEASE: ____0592____  SERVICE REQUEST(S): ____4021____   */   40210592
000400*  NAME:    __J.QUAN____  MODIFICATION DATE:  __05/14/91__   */   40210592
000500*  DESCRIPTION:                                              */   40210592
000600*                                                            */   40210592
000700*    * NEW COPYMEMBER DEFINING AN ARRAY WHICH HOLDS ALL      */   40210592
000800*      DOS TABLE ENTRIES, WITH EACH SEPARATE FIELD           */   40210592
000900*      DISTINGUISHED.                                        */   40210592
001000*    * TABLE ROWS MIMIC THE CPWXDOS  RECORD LAYOUT.          */   40210592
001100*    * ONLY ACTUAL ROWS ARE LOADED INTO THE TABLE.           */   40210592
001200*    * DOST-MAXIMUM-ALLOWED IS SET TO EQUAL THE CURRENT VALUE*/   40210592
001300*      OF IDC-EARN-TBL-MAX. THIS LIMIT REFLECTS THE MAXIMUM  */   40210592
001400*      NUMBER OF DOS ENTRIES ALLOWED TO BE LOADED.           */   40210592
001500*    * DOST-MAX-LOADED IS SET TO ACTUAL NUMBER OF ENTRIES    */   40210592
001600*      LOADED IN THE TABLE.                                  */   40210592
001700*    * USE DOST-INDEX TO LOOP WITHIN THE TABLE.              */   40210592
001800**************************************************************/   40210592
001900*    COPYID=CPWSDOST                                              CPWSDOST
002000*01  DOST-DESCR-OF-SERV-TABLE.                                    CPWSDOST
002100*-----------------------------------------------------------------CPWSDOST
002200*      D E S C R I P T I O N  OF  S E R V I C E   T A B L E       CPWSDOST
002300*-----------------------------------------------------------------CPWSDOST
002400     02  DOST-MAXIMUM-ALLOWED   PIC S9(04) COMP SYNC.             CPWSDOST
002500     02  DOST-MAXIMUM-LOADED    PIC S9(04) COMP SYNC.             CPWSDOST
002600     02  DOST-DOS-TABLE.                                          CPWSDOST
002700         03  DOST-ITEMS         OCCURS 0 TO 999 TIMES             CPWSDOST
002800                                DEPENDING ON DOST-MAXIMUM-LOADED  CPWSDOST
002900                                INDEXED BY DOST-INDEX.            CPWSDOST
003000             04  DOST-KEY-DATA.                                   CPWSDOST
003100                 05  DOST-DELETE                    PIC X.        CPWSDOST
003200                 05  DOST-KEY.                                    CPWSDOST
003300                     07  DOST-CONSTANT              PIC X(7).     CPWSDOST
003400                     07  DOST-SEQUENCE              PIC X(3).     CPWSDOST
003500                     07  DOST-EARNINGS-TYPE         PIC X(3).     CPWSDOST
003600                 05  DOST-DESCRIPTION               PIC X(25).    CPWSDOST
003700                 05  DOST-DESC                      PIC X(12).    CPWSDOST
003800                 05 DOST-PAY-CATEGORY               PIC X.        CPWSDOST
003900*                                                                 CPWSDOST
004000*                     VALUES ARE AS FOLLOWS:                      CPWSDOST
004100*                     N = REGULAR, ON GOING TYPES OF PAY          CPWSDOST
004200*                     L = LEAVE PAYMENTS                          CPWSDOST
004300*                     A = ADDITIONAL, ONE-TIME PAYMENTS           CPWSDOST
004400*                     D = DIFFERENTIALS                           CPWSDOST
004500*                     P = PEREQUISTES                             CPWSDOST
004600*                     S = SUSPENSE PAYMENTS                       CPWSDOST
004700*                                                                 CPWSDOST
004800                 05 DOST-HOURS-CODE PIC X.                        CPWSDOST
004900*                                                                 CPWSDOST
005000*                     VALUES ARE AS FOLLOWS:                      CPWSDOST
005100*                     R = REGULAR AND LEAVE HOURS                 CPWSDOST
005200*                     O = OVERTIME HOURS                          CPWSDOST
005300*                     N = NONWORKED HOURS                         CPWSDOST
005400*                     P = OVERTIME PREMIUM PORTION                CPWSDOST
005500*                     BLANK = HOURS NOT APPLICABLE                CPWSDOST
005600*                                                                 CPWSDOST
005700                 05 DOST-GROSS-INDICATORS.                        CPWSDOST
009000                     07 DOST-GROSS-IND OCCURS 12                  48861503
009010*********               DOST-GROSS-IND OCCURS 10                  48861503
005900                                                    PIC S9 COMP-3.CPWSDOST
006000*                                                                 CPWSDOST
006100*                    THE GROSSES ARE USED AS NOTED BELOW:         CPWSDOST
006200*                    1 = TOTAL GROSS                              CPWSDOST
006300*                    2 = FWT GROSS                                CPWSDOST
006310*                    3 = FICA GROSS                               CPWSDOST
006400*                    4 = STATE GROSS                              CPWSDOST
006500*                    5 = UCRS GROSS                               CPWSDOST
006600*                    6 = PERS GROSS                               CPWSDOST
006700*                    7 = WORKERS COMPENSATION GROSS               CPWSDOST
006800*                    8 = UNEMPLOYMENT INSURANCE GROSS             CPWSDOST
006810*                    9 = SPECIAL FWT GROSS (TAXED AT 20%)         CPWSDOST
006900*                   10 = SPECIAL STATE GROSS                      CPWSDOST
007000*                                 (TAXED AT FLAT PERCENT)         CPWSDOST
010410*                   11 = SPECIAL BONUS FEDERAL GROSS              48861503
010420*                                 (TAXED AT FLAT PERCENT)         48861503
010430*                   12 = SPECIAL BONUS STATE GROSS                48861503
010440*                                 (TAXED AT FLAT PERCENT)         48861503
010450*                                                                 48861503
007100*                                                                 CPWSDOST
007200*                    VALUES FOR THE INDICATORS ARE SET AS:        CPWSDOST
007300*                    +1 TO ADD TO THE SUBJECT GROSS               CPWSDOST
007400*                    -1 TO SUBTRACT FROM THE SUBJECT GROSS        CPWSDOST
007500*                     0 IF THERE IS NO EFFECT ON THE SUBJECT GROSSCPWSDOST
007600*                                                                 CPWSDOST
007700                 05 DOST-GROSS-IND-X REDEFINES                    CPWSDOST
011200                              DOST-GROSS-INDICATORS PIC X(12).    48861503
011210*********                     DOST-GROSS-INDICATORS PIC X(10).    48861503
011300                 05  DOST-EARN-GROSS-INDS   REDEFINES             71921294
011400                              DOST-GROSS-INDICATORS.              71921294
011500                     07  DOST-EARN-PAYABLE-IND     PIC S9 COMP-3. 71921294
011600                     07  DOST-EARN-1ST-FWT-IND     PIC S9 COMP-3. 71921294
011700                     07  DOST-EARN-FICA-IND        PIC S9 COMP-3. 71921294
011800                     07  DOST-EARN-1ST-SWT-IND     PIC S9 COMP-3. 71921294
011900                     07  DOST-EARN-UCRS-IND        PIC S9 COMP-3. 71921294
012000                     07  DOST-EARN-PERS-IND        PIC S9 COMP-3. 71921294
012100                     07  DOST-EARN-WRKRS-COMP-IND  PIC S9 COMP-3. 71921294
012200                     07  DOST-EARN-UI-GRS-IND      PIC S9 COMP-3. 71921294
012300                     07  DOST-EARN-2ND-FWT-IND     PIC S9 COMP-3. 71921294
012400                     07  DOST-EARN-2ND-SWT-IND     PIC S9 COMP-3. 71921294
012410                     07  DOST-EARN-3RD-FWT-IND     PIC S9 COMP-3. 48861503
012420                     07  DOST-EARN-3RD-SWT-IND     PIC S9 COMP-3. 48861503
007900                 05 DOST-SPCL-PROCESS-RTNS.                       CPWSDOST
008000                     07 DOST-PRE-PROCESS            PIC XX.       CPWSDOST
008100                     07 DOST-CALC-PROCESS           PIC XX.       CPWSDOST
008200                     07 DOST-POST-PROCESS           PIC XX.       CPWSDOST
008300                 05 DOST-CALC-FUNCTION              PIC X.        CPWSDOST
008400*                                                                 CPWSDOST
008500*                   VALUES ARE SET AS FOLLOWS BASED ON THE VALUE  CPWSDOST
008600*                   OF DOST-CALC-PROCESS:                         CPWSDOST
008700*                     R = RATE TIMES TIME                         CPWSDOST
008800*                     F = RATE TIMES TIME TIMES FACTOR            CPWSDOST
008900*                     A = AMOUNT ADDED TO GROSS                   CPWSDOST
009000*                SPACES = NO PAYMENT CALCULATED                   CPWSDOST
009100*                                                                 CPWSDOST
009200                 05 DOST-FACTOR              COMP-3 PIC S9(4)V999.CPWSDOST
009300                 05 DOST-HOUR-ELMT        OCCURS 5  PIC X(4).     CPWSDOST
009400                 05 DOST-DOLLAR-ELMT      OCCURS 5  PIC X(4).     CPWSDOST
009500                 05 DOST-OBJECT-CODES.                            CPWSDOST
009600                     07 DOST-ACAD-OBJ-CODE          PIC X(4).     CPWSDOST
009700                     07 DOST-STAFF-OBJ-CODE         PIC X(4).     CPWSDOST
009800                 05 DOST-DISTRIB-PRIORITY           PIC X.        CPWSDOST
009900                 05 DOST-INDICATORS.                              CPWSDOST
010000                     07 DOST-HRS-PAY-STAT-IND       PIC X.        CPWSDOST
010100                     07 DOST-TIME-BEN-IND           PIC X.        CPWSDOST
010200                     07 DOST-LEAVE-ASMT-IND         PIC X.        CPWSDOST
010300                     07 DOST-RANGE-ADJ-IND          PIC X.        CPWSDOST
010400                     07 DOST-INS-BEN-IND            PIC X.        CPWSDOST
010500*                                                                 CPWSDOST
010600*                        Y = ELIGIBLE                             CPWSDOST
010700*                        N = NOT ELIGIBLE                         CPWSDOST
010800*                                                                 CPWSDOST
010900                 05 DOST-INTERFACE-CODES.                         CPWSDOST
010901                     07 DOST-NSETC-CODES.                         CPWSDOST
010902                         09 DOST-NSETC-CD  OCCURS 2 PIC X.        CPWSDOST
010903                            88 DOST-VALID-NSETC                   CPWSDOST
010904                                        VALUE 'N' 'S' 'E' 'T' 'C'.CPWSDOST
010905*                                                                 CPWSDOST
010906*                        N = SUBJECT TO FICA ONLY                 CPWSDOST
010907*                        S = SUBJECT TO RETIREMENT AND FICA       CPWSDOST
010908*                        E = NOT SUBJECT TO RETIREMENT OR FICA    CPWSDOST
010909*                        T = SUBJECT TO RETIREMENT, FICA, AND     CPWSDOST
011000*                            3% SPECIAL RETIREMENT CONTRIBUTION   CPWSDOST
011100*                        C = SUBJECT TO FICA AND 8.36% ADDITIONAL CPWSDOST
011200*                            RETIREMENT BENEFIT                   CPWSDOST
011300*                                                                 CPWSDOST
011400                     07 DOST-TIME-CD                PIC X(1).     CPWSDOST
011500*                                                                 CPWSDOST
011600*                        VALID TIME CODES ARE:                    CPWSDOST
011700*                                1 THRU 5, AND 7 THRU 9           CPWSDOST
011800*                                                                 CPWSDOST
011900*                    07 DOST-TYPE-PAY-CD            PIC X(2).     CPWSDOST
012000*                                                                 CPWSDOST
012100*                        VALID TYPE PAY CODES ARE:                CPWSDOST
012200*                            00, 02 THRU 11 AND 80                CPWSDOST
012300*                                                                 CPWSDOST
012400                 05 DOST-FCP-DOS                    PIC X(2).     CPWSDOST
014200                 05 DOST-WSP-EXEMPT                 PIC X(01).    36060628
014210                 05 DOST-SABB-IND                   PIC X(01).    36520886
014220                 05 DOST-HS-COMP-IND                PIC X(01).    36520886
016300                 05 DOST-TOC-IND                    PIC X(01).    28171053
016400                 05 DOST-FLSA-IND                   PIC X(01).    28171053
016500                 05 DOST-CSER-IND                   PIC X(01).    28171053
018600                 05 DOST-FCSS-IND                   PIC X(01).    52191347
018610                 05 DOST-FILLER                     PIC X(77).    52191347
018700*****            05 DOST-FILLER                     PIC X(78).    52191347
018800*****                                                         CD  52191347
012600                 05 DOST-LAST-UPDT-INFO.                          CPWSDOST
012700                     07 DOST-LAST-UPDT-ACTION       PIC X.        CPWSDOST
012800                     07 DOST-LAST-UPDT-DATE         PIC X(6).     CPWSDOST
012900*                         FORMAT IS YYMMDD                        CPWSDOST
