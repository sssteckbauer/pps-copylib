000100**************************************************************/   32021138
000200*  COPYMEMBER: CPPDIUCI                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_PHIL THOMPSON___ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*  BASE FAU RELATED PROCEDURE CODE FOR ONLINE FUNCTION IUCI. */   32021138
000700*  THIS CODE ONLY WORKS WITH THE BASE PROGRAM PPWIUCI AND    */   32021138
000800*  BASE MAP PPIUCI0.                                         */   32021138
000900**************************************************************/   32021138
001000 CPPD-DECLARE-CURSOR       SECTION.                               CPPDIUCI
001100**************************************************************/   CPPDIUCI
001200     EXEC SQL DECLARE FBA-CURS CURSOR FOR                         CPPDIUCI
001400           SELECT                                                 CPPDIUCI
001500              SUBSTR (FBA_FULL_ACCT_UNIT,2,6)                     CPPDIUCI
001501             ,FBA_FULL_ACCT_UNIT                                  CPPDIUCI
001510             ,FBA_PERCENT                                         CPPDIUCI
001600           FROM PPPVZFBA_FBA                                      CPPDIUCI
001700           WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID                    CPPDIUCI
001800           ORDER BY 1                                             CPPDIUCI
001900     END-EXEC.                                                    CPPDIUCI
002000**************************************************************/   CPPDIUCI
002100 CPPD-INIT-SCREEN          SECTION.                               CPPDIUCI
002200**************************************************************/   CPPDIUCI
002300     MOVE UCCOMMON-CLR-FLD-LABEL TO                               CPPDIUCI
002310                            L-FBA-FAUC                            CPPDIUCI
002310                            L-FAU-ONEC                            CPPDIUCI
002700                            L-FAU-TWOC                            CPPDIUCI
003010     MOVE UCCOMMON-HLT-FLD-LABEL TO                               CPPDIUCI
002310                            L-FBA-FAUH                            CPPDIUCI
003100                            L-FAU-ONEH                            CPPDIUCI
003500                            L-FAU-TWOH                            CPPDIUCI
003810     PERFORM VARYING WS-SUB FROM 1 BY 1 UNTIL WS-SUB > 5          CPPDIUCI
003820         MOVE UCCOMMON-CLR-NORM-DISPL TO                          CPPDIUCI
003900                      LOC-ONEC  (WS-SUB)                          CPPDIUCI
004000                      ACCT-ONEC (WS-SUB)                          CPPDIUCI
004100                      CC-ONEC   (WS-SUB)                          CPPDIUCI
004200                      FUND-ONEC (WS-SUB)                          CPPDIUCI
004200                      PROJ-ONEC (WS-SUB)                          CPPDIUCI
003900                      LOC-TWOC  (WS-SUB)                          CPPDIUCI
004300                      ACCT-TWOC (WS-SUB)                          CPPDIUCI
004400                      CC-TWOC   (WS-SUB)                          CPPDIUCI
004500                      FUND-TWOC (WS-SUB)                          CPPDIUCI
004500                      PROJ-TWOC (WS-SUB)                          CPPDIUCI
004510         MOVE UCCOMMON-HLT-NORM-DISPL TO                          CPPDIUCI
004600                      LOC-ONEH  (WS-SUB)                          CPPDIUCI
004700                      ACCT-ONEH (WS-SUB)                          CPPDIUCI
004800                      CC-ONEH   (WS-SUB)                          CPPDIUCI
004900                      FUND-ONEH (WS-SUB)                          CPPDIUCI
004900                      PROJ-ONEH (WS-SUB)                          CPPDIUCI
005000                      LOC-TWOH  (WS-SUB)                          CPPDIUCI
005100                      ACCT-TWOH (WS-SUB)                          CPPDIUCI
005200                      CC-TWOH   (WS-SUB)                          CPPDIUCI
005300                      FUND-TWOH (WS-SUB)                          CPPDIUCI
005300                      PROJ-TWOH (WS-SUB)                          CPPDIUCI
005310     END-PERFORM.                                                 CPPDIUCI
005400**************************************************************/   CPPDIUCI
005500 CPPD-MOVE-FBA-TO-SCRN     SECTION.                               CPPDIUCI
005600**************************************************************/   CPPDIUCI
005700     MOVE FBA-FULL-ACCT-UNIT TO XFAU-FAU-UNFORMATTED.             CPPDIUCI
005800     IF WS-SUB < 6                                                CPPDIUCI
005900        MOVE XFAU-FAU-LOCATION OF XFAU-FAU-UNFORMATTED            CPPDIUCI
005910                              TO LOC-ONEO  (WS-SUB-TWO)           CPPDIUCI
006000        MOVE XFAU-FAU-ACCOUNT OF XFAU-FAU-UNFORMATTED             CPPDIUCI
006010                              TO ACCT-ONEO (WS-SUB-TWO)           CPPDIUCI
006100        MOVE XFAU-FAU-COST-CENTER OF XFAU-FAU-UNFORMATTED         CPPDIUCI
006110                              TO CC-ONEO   (WS-SUB-TWO)           CPPDIUCI
006200        MOVE XFAU-FAU-FUND OF XFAU-FAU-UNFORMATTED                CPPDIUCI
006210                              TO FUND-ONEO (WS-SUB-TWO)           CPPDIUCI
006200        MOVE XFAU-FAU-PROJECT-CODE OF XFAU-FAU-UNFORMATTED        CPPDIUCI
006210                              TO PROJ-ONEO (WS-SUB-TWO)           CPPDIUCI
006300     ELSE                                                         CPPDIUCI
006400        MOVE XFAU-FAU-LOCATION OF XFAU-FAU-UNFORMATTED            CPPDIUCI
006500                              TO LOC-TWOO  (WS-SUB-TWO)           CPPDIUCI
006600        MOVE XFAU-FAU-ACCOUNT OF XFAU-FAU-UNFORMATTED             CPPDIUCI
006700                              TO ACCT-TWOO (WS-SUB-TWO)           CPPDIUCI
006710        MOVE XFAU-FAU-COST-CENTER OF XFAU-FAU-UNFORMATTED         CPPDIUCI
006720                              TO CC-TWOO   (WS-SUB-TWO)           CPPDIUCI
006730        MOVE XFAU-FAU-FUND OF XFAU-FAU-UNFORMATTED                CPPDIUCI
006740                              TO FUND-TWOO (WS-SUB-TWO)           CPPDIUCI
006730        MOVE XFAU-FAU-PROJECT-CODE OF XFAU-FAU-UNFORMATTED        CPPDIUCI
006740                              TO PROJ-TWOO (WS-SUB-TWO)           CPPDIUCI
006800     END-IF.                                                      CPPDIUCI
