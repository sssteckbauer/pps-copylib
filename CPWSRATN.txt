000100**************************************************************/   36330704
000200*  COPYMEMBER: CPWSRATN                                      */   36330704
000300*  RELEASE: ____0704____  SERVICE REQUEST(S): ____3633____   */   36330704
000400*  NAME:    ___B.I.T.___  CREATION DATE:      __10/02/92__   */   36330704
000500*  DESCRIPTION:                                              */   36330704
000600*     COBOL DESCRIPTION OF PPPATN TABLE                      */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900******************************************************************CPWSRATN
001000* COBOL DECLARATION FOR TABLE PAYCXD.PPPVATN1_ATN                *CPWSRATN
001100******************************************************************CPWSRATN
001200*01  DCLPPPVATN1-ATN.                                             CPWSRATN
001300     10 EMPLID               PIC X(9).                            CPWSRATN
001400     10 ITERATION-NUMBER     PIC X(3).                            CPWSRATN
001500     10 SYSTEM-ENTRY-DATE    PIC X(10).                           CPWSRATN
001600     10 SYSTEM-ENTRY-TIME    PIC X(8).                            CPWSRATN
001700     10 APPT-NUM             PIC S9(4) USAGE COMP.                CPWSRATN
001800     10 DIST-NUM             PIC S9(4) USAGE COMP.                CPWSRATN
001900     10 ACTION-CODE          PIC X(2).                            CPWSRATN
002000     10 DELETE-FLAG          PIC X(1).                            CPWSRATN
002100     10 ERH-INCORRECT        PIC X(1).                            CPWSRATN
002200     10 ACTION-EFF-DATE      PIC X(10).                           CPWSRATN
002300     10 ACTION-EFF-DATE-C    PIC X(1).                            CPWSRATN
002400******************************************************************CPWSRATN
002500* THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 9       *CPWSRATN
002600******************************************************************CPWSRATN
