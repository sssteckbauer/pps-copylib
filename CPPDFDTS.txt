000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPPDFDTS                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    NEW PROCEDURE DIVISION COPYLIB TO FORMAT AND            *|*36410717
000900*|*    INITIALIZE DATE FIELDS.                                 *|*36410717
001000*|**************************************************************|*36410717
001100*----------------------------------------------------------------*36410717
001200                                                                  CPPDFDTS
001300     MOVE '0001-01-01'              TO ISO-ZERO-DATE.             CPPDFDTS
001400                                                                  CPPDFDTS
001500     MOVE '-'                       TO CUTOFF-DASH-1,             CPPDFDTS
001600                                       CUTOFF-DASH-1,             CPPDFDTS
001700                                       ISO-TEMP-DATE-DASH-1,      CPPDFDTS
001800                                       ISO-TEMP-DATE-DASH-1.      CPPDFDTS
001900                                                                  CPPDFDTS
002000     INITIALIZE CONSECUTIVE-EARNING-MONTHS.                       CPPDFDTS
002100     INITIALIZE WSP-FISCAL-YR-CCYY.                               CPPDFDTS
002200                                                                  CPPDFDTS
002300     MOVE 10                        TO  CUTOFF-ISO-MO.            CPPDFDTS
002400     MOVE 01                        TO  CUTOFF-ISO-DA.            CPPDFDTS
002500                                                                  CPPDFDTS
