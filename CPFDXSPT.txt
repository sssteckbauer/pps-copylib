000100*************************************************************/    73820210
000200*  COPYMEMBER:  CPFDXSPT                                    */    73820210
000300*  RELEASE:  0210                 SERVICE REQUEST:  7382    */    73820210
000400*  NAME:     G. STEINITZ    MODIFICATION DATE:  05/29/86    */    73820210
000500*  DESCRIPTION                                              */    73820210
000600*     THIS IS THE FILE DESCRIPTION FOR THE SPECIAL PER-     */    73820210
000700*     FORMANCE AWARD TRANSACTION FILE.                      */    73820210
000800*************************************************************/    73820210
000900     BLOCK CONTAINS 0 RECORDS                                     CPFDXCTS
001000     RECORD CONTAINS 80 CHARACTERS                                CPFDXCTS
001100     LABEL RECORD IS STANDARD                                     CPFDXCTS
001200     DATA RECORD IS SPA-TRAN-REC.                                 CPFDXCTS
001300                                                                  CPFDXCTS
001400 01  SPA-TRAN-REC                    PIC X(80).                   CPFDXCTS
