000000**************************************************************/   48531363
000001*  COPYMEMBER: CPFDXSSF                                      */   48531363
000002*  RELEASE: ___1363______ SERVICE REQUEST(S): ____14853____  */   48531363
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___07/18/01__  */   48531363
000004*  DESCRIPTION:                                              */   48531363
000005*  - CHANGED LENGTH TO 167. SUMMER SALARY RECORDS NO LONGER  */   48531363
000006*    CONTAIN MULTI-OCCURRENCE SUMMER SALARY EARNINGS. EACH   */   48531363
000007*    SUMMER SALARY EARNING IS WRITTEN TO SEPARATE RECORD.    */   48531363
000008**************************************************************/   48531363
000000**************************************************************/   48531356
000001*  COPYMEMBER: CPFDXSSF                                      */   48531356
000002*  RELEASE: ___1356______ SERVICE REQUEST(S): ____14853____  */   48531356
000003*  NAME:_______QUAN______ CREATION DATE:      ___06/08/01__  */   48531356
000004*  DESCRIPTION:                                              */   48531356
000005*  - FILE DESCRIPTION FOR SUMMER SALARY EMPLOYER CONTR FILE. */   48531356
000007**************************************************************/   48531356
011300*FD  SUM-SAL-FILE                                                 CPDSXSSF
011310                                                                  CPDSXSSF
011400     BLOCK CONTAINS 0 RECORDS                                     CPDSXSSF
011500     RECORDING MODE IS F                                          CPDSXSSF
011600     LABEL RECORDS STANDARD                                       CPDSXSSF
011700     DATA RECORD IS SUM-SAL-OUT-RECORD.                           CPDSXSSF
011800*01  SUM-SAL-OUT-RECORD              PIC X(453).                  48531363
011900 01  SUM-SAL-OUT-RECORD              PIC X(167).                  48531363
