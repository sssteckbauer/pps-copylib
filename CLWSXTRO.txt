000100*01  TROUT-RECORD           PIC X(262).                           CLWSXTRO
000200     05  TROUT-ID-NUMBER     PICTURE 9(9).                        CLWSXTRO
000300     05  TROUT-APPOINTMENT.                                       CLWSXTRO
000400         10  TROUT-APPT-NO       PICTURE 99.                      CLWSXTRO
000500         10  TROUT-APPT-BEGN-DATE.                                CLWSXTRO
000600             15  TROUT-APPT-BEGN-YR  PIC 99.                      CLWSXTRO
000700             15  TROUT-APPT-BEGN-MO  PIC 99.                      CLWSXTRO
000800             15  TROUT-APPT-BEGN-DA  PIC 99.                      CLWSXTRO
000900         10  TROUT-APPT-END-DATE.                                 CLWSXTRO
001000             15  TROUT-APPT-END-YR   PIC 99.                      CLWSXTRO
001100             15  TROUT-APPT-END-MO   PIC 99.                      CLWSXTRO
001200             15  TROUT-APPT-END-DA   PIC 99.                      CLWSXTRO
001300         10  TROUT-DUR-EMPLMT    PICTURE X.                       CLWSXTRO
001400             88  TROUT-INDEF     VALUE 'I'.                       CLWSXTRO
001500             88  TROUT-TENURE    VALUE 'T'.                       CLWSXTRO
001600             88  TROUT-NON-TENR  VALUE 'N'.                       CLWSXTRO
001700             88  TROUT-SECURE    VALUE 'S'.                       CLWSXTRO
001800         10  TROUT-TITLE-CODE.                                    CLWSXTRO
001900             15  TROUT-TTL-FILLER    PICTURE 9.                   CLWSXTRO
002000             15  TROUT-TTL-CODE      PICTURE 9(04).               CLWSXTRO
002100         10  TROUT-PERS-PROG     PICTURE X.                       CLWSXTRO
002200         10  TROUT-GRADE         PICTURE X.                       CLWSXTRO
002300         10  TROUT-X-FILLER1     PICTURE X(04).                   CLWSXTRO
002400         10  TROUT-BASIS         PICTURE 99.                      CLWSXTRO
002500         10  TROUT-PAID-OVER     PICTURE 99.                      CLWSXTRO
002600         10  TROUT-PCT-FULL-TIME PICTURE 9V99.                    CLWSXTRO
002700         10  TROUT-FXD-VAR       PICTURE X.                       CLWSXTRO
002800             88  TROUT-FXD       VALUE 'F'.                       CLWSXTRO
002900             88  TROUT-VAR       VALUE 'V'.                       CLWSXTRO
003000         10  TROUT-ANNL-HRLY-RATE.                                CLWSXTRO
003100             15  TROUT-ANNL-RATE     PICTURE 9(6)V99.             CLWSXTRO
003200             15  TROUT-HRLY-RATE REDEFINES                        CLWSXTRO
003300                 TROUT-ANNL-RATE     PICTURE 9(4)V9(4).           CLWSXTRO
003400         10  TROUT-RATE-CODE     PICTURE X.                       CLWSXTRO
003500             88  TROUT-ANNL      VALUE 'A'.                       CLWSXTRO
003600             88  TROUT-HRLY      VALUE 'H'.                       CLWSXTRO
003700             88  TROUT-BY-AGREE  VALUE 'B'.                       CLWSXTRO
003800         10  TROUT-PAY-SCHED     PICTURE XX.                      CLWSXTRO
003900             88  TROUT-SEMI-MO   VALUE 'SM'.                      CLWSXTRO
004000             88  TROUT-MO-CUR    VALUE 'MO'.                      CLWSXTRO
004100             88  TROUT-MO-ARRS   VALUE 'MA'.                      CLWSXTRO
004200             88  TROUT-BIWKLY    VALUE 'BW'.                      CLWSXTRO
004300         10  TROUT-TIME-CODE     PICTURE X.                       CLWSXTRO
004400         10  TROUT-LEAVE         PICTURE X.                       CLWSXTRO
004500         10  TROUT-X-FILLER2     PICTURE X.                       CLWSXTRO
004600         10  TROUT-APPT-TYPE     PICTURE X.                       13880212
004700         10  TROUT-RETIRE        PICTURE 9.                       CLWSXTRO
004800             88  TROUT-NOT-ELIG  VALUE 0.                         CLWSXTRO
004900             88  TROUT-ASSOC     VALUE 1.                         CLWSXTRO
005000             88  TROUT-SAFETY    VALUE 2.                         CLWSXTRO
005100             88  TROUT-REGENTS-3 VALUE 3.                         CLWSXTRO
005200             88  TROUT-REGENTS-4 VALUE 4.                         CLWSXTRO
005300         10  TROUT-APPT-ADC-CODE PICTURE X.                       CLWSXTRO
005400             88  TROUT-APPT-ADD  VALUE 'A'.                       CLWSXTRO
005500             88  TROUT-APPT-DEL  VALUE 'D'.                       CLWSXTRO
005600             88  TROUT-APPT-CHNG VALUE 'C'.                       CLWSXTRO
005700         10  TROUT-X-FILLER3     PICTURE X(4).                    CLWSXTRO
005800         10  TROUT-X-FILLER4     PICTURE X(4).                    14000283
005900         10  TROUT-APPT-WOS      PICTURE X.                       CLWSXTRO
006000             88  TROUT-DOS-EQ-WOS                                 CLWSXTRO
006100                                 VALUE 'Y'.                       CLWSXTRO
006200         10  TROUT-2029-APPT-PERB.                                CLWSXTRO
006300             15  TROUT-APPT-PERB-CHAR1   PICTURE X.               CLWSXTRO
006400             15  TROUT-APPT-PERB-CHAR2   PICTURE X.               CLWSXTRO
006500         10  TROUT-2030-APPT-SPCL    PICTURE X.                   CLWSXTRO
006600         10  TROUT-2031-REP-CODE     PICTURE X.                   CLWSXTRO
006700         10  TROUT-EMP-REL           PICTURE X.                   CLWSXTRO
006800         10  TROUT-APPT-DIST-COUNT   PICTURE 9.                   CLWSXTRO
006900         10  TROUT-X-FILLER5         PICTURE X(04).               CLWSXTRO
007000         10  TROUT-2037-FILLER-OP    PICTURE X(09).               CLWSXTRO
007100*        10  TROUT-2038-FILLER-COS   PICTURE X(05).               CLWSXTRO
007100         10  TROUT-NXT-SAL-REV       PICTURE X.                   CLWSXTRO
007100         10  TROUT-NXT-SAL-DATE.                                  CLWSXTRO
010000             15  TROUT-NXT-SAL-YR    PIC 99.                      CLWSXTRO
010100             15  TROUT-NXT-SAL-MO    PIC 99.                      CLWSXTRO
007100         10  TROUT-APPT-DEPT         PICTURE 9(4).                CLWSXTRO
007100         10  TROUT-HOME-DEPT         PICTURE 9(4).                CLWSXTRO
007200     05  TROUT-GEN-NO            PICTURE 9(4).                    CLWSXTRO
007300     05  TROUT-NAME              PICTURE X(26).                   CLWSXTRO
007400     05  TROUT-CST-EFF-DATE.                                      CLWSXTRO
007500         10  TROUT-CST-EFF-YR    PIC 99.                          CLWSXTRO
007600         10  TROUT-CST-EFF-MO    PIC 99.                          CLWSXTRO
007700         10  TROUT-CST-EFF-DA    PIC 99.                          CLWSXTRO
007800     05  TROUT-NEW-EFF-DATE.                                      CLWSXTRO
007900         10  TROUT-NEW-EFF-YR    PIC 99.                          CLWSXTRO
008000         10  TROUT-NEW-EFF-MO    PIC 99.                          CLWSXTRO
008100         10  TROUT-NEW-EFF-DA    PIC 99.                          CLWSXTRO
008200     05  TROUT-CBUC              PICTURE XX.                      CLWSXTRO
008300     05  TROUT-0255-EMP-UNIT-CODE  PICTURE XX.                    CLWSXTRO
008400     05  TROUT-0256-EMP-SPLC       PICTURE X.                     CLWSXTRO
008500     05  TROUT-APPT-ERROR-CODE   PICTURE XX.                      CLWSXTRO
008600     05  TROUT-APPT-NEW          PICTURE XX.                      CLWSXTRO
008700     03  TROUT-DISTRIBUTION.                                      CLWSXTRO
008800         10  TROUT-DIST-NO       PICTURE 99.                      CLWSXTRO
008900         10  TROUT-X-FILLER6     PICTURE 9.                       CLWSXTRO
009000         10  TROUT-LOC           PICTURE X.                       CLWSXTRO
009100         10  TROUT-ACCOUNT       PICTURE 9(6).                    CLWSXTRO
009200         10  TROUT-X-FILLER7     PICTURE 9.                       CLWSXTRO
009300         10  TROUT-FUND          PICTURE 9(5).                    CLWSXTRO
009400         10  TROUT-X-FILLER8     PICTURE 9.                       CLWSXTRO
009500         10  TROUT-SUB           PICTURE X.                       CLWSXTRO
009600         10  TROUT-DIST-DEPT     PICTURE 9(4).                    CLWSXTRO
009700         10  TROUT-FTE           PICTURE 9V99.                    CLWSXTRO
009800         10  TROUT-DIST-PCT      PICTURE 9V9999.                  CLWSXTRO
009900         10  TROUT-PAY-BEGN-DATE.                                 CLWSXTRO
010000             15  TROUT-PAY-BEGN-YR   PIC 99.                      CLWSXTRO
010100             15  TROUT-PAY-BEGN-MO   PIC 99.                      CLWSXTRO
010200             15  TROUT-PAY-BEGN-DA   PIC 99.                      CLWSXTRO
010300         10  TROUT-PAY-END-DATE.                                  CLWSXTRO
010400             15  TROUT-PAY-END-YR    PIC 99.                      CLWSXTRO
010500             15  TROUT-PAY-END-MO    PIC 99.                      CLWSXTRO
010600             15  TROUT-PAY-END-DA    PIC 99.                      CLWSXTRO
010700         10  TROUT-RATE-AMOUNT.                                   CLWSXTRO
010800             15  TROUT-RATE          PICTURE 9(3)V9(4).           CLWSXTRO
010900             15  TROUT-AMOUNT        REDEFINES                    CLWSXTRO
011000                 TROUT-RATE          PICTURE 9(5)V99.             CLWSXTRO
011100         10  TROUT-DOS           PICTURE XXX.                     CLWSXTRO
011200         10  TROUT-PERQ          PICTURE XXX.                     CLWSXTRO
011300         10  TROUT-DIST-ADC-CODE PICTURE X.                       CLWSXTRO
011400             88  TROUT-DIST-ADD      VALUE 'A'.                   CLWSXTRO
011500             88  TROUT-DIST-DEL      VALUE 'D'.                   CLWSXTRO
011600             88  TROUT-DIST-CHNG     VALUE 'C'.                   CLWSXTRO
011700         10  TROUT-2059-DUC          PICTURE X.                   CLWSXTRO
011800         10  TROUT-2060-RDUC         PICTURE X.                   CLWSXTRO
011900         10  TROUT-X-FILLER10        PICTURE X(06).               CLWSXTRO
012000         10  TROUT-STEP              PICTURE X(03).               CLWSXTRO
012100         10  TROUT-OFF-ABV-SCALE     PICTURE X.                   CLWSXTRO
012200         10  TROUT-WORK-STUDY-PGM    PICTURE X.                   CLWSXTRO
012300         10  TROUT-X-FILLER11        PICTURE X.                   CLWSXTRO
012400         10  TROUT-2064-FILLER-OP    PICTURE X(02).               CLWSXTRO
012500         10  TROUT-2065-FILLER-COS   PICTURE X(20).               CLWSXTRO
012600         10  TROUT-IFIS-INFO         REDEFINES                    DT033
012700             TROUT-2065-FILLER-COS.                               DT033
012800             15 TROUT-IFIS-INDEX         PICTURE X(7).            DT033
012900             15 TROUT-IFIS-FUND-EXTEN    PICTURE X(1).            DT033
013000             15 TROUT-IFIS-ORGANIZATION  PICTURE X(6).            DT033
013100             15 TROUT-IFIS-PROGRAM       PICTURE X(6).            DT033
013200     05  TROUT-DIST-RATE-CODE    PICTURE X.                       CLWSXTRO
013300     05  TROUT-NEW-RATE-AMOUNT.                                   CLWSXTRO
013400         10  TROUT-NEW-RATE          PICTURE S9(3)V9(4).          CLWSXTRO
013500         10  TROUT-NEW-AMOUNT        REDEFINES                    CLWSXTRO
013600             TROUT-NEW-RATE          PICTURE S9(5)V99.            CLWSXTRO
013700     05  TROUT-ERROR-CODE        PICTURE 99.                      CLWSXTRO
013800     05  TROUT-DIST-NEW          PICTURE XX.                      CLWSXTRO
013900     05  TROUT-EMPLOYEE-SUFFIX   PICTURE X(4).                    CLWSXTRO
014000     05  TROUT-0295-COV-IND      PICTURE X.                       CLWSXTRO
014100     05  TROUT-0257-EMP-DUC      PICTURE X.                       CLWSXTRO
