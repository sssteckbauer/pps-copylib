000100**************************************************************/   36371028
000200*  COPYMEMBER: CPWSGTHF                                      */   36371028
000300*  RELEASE: ___1028______ SERVICE REQUEST(S): _____3637____  */   36371028
000400*  NAME:_______JLT_______ MODIFICATION DATE:  __10/24/95____ */   36371028
000500*  DESCRIPTION:                                              */   36371028
000600*   - ADD CPWSGTHF-ITTL-SAVE-PPI FOR BROWSE FUNCTION.        */   36371028
000700**************************************************************/   36371028
000100**************************************************************/   36370967
000200*  COPYMEMBER: CPWSGTHF                                      */   36370967
000300*  RELEASE: ___0967______ SERVICE REQUEST(S): _____3637____  */   36370967
000400*  NAME:_______PXP_______ MODIFICATION DATE:  __03/10/95____ */   36370967
000500*  DESCRIPTION:                                              */   36370967
000600*    INTERFACE TO PPGETHFO                                   */   36370967
000800**************************************************************/   36370967
000900*01  CPWSGTHF EXTERNAL.                                           CPWSGTHF
001000     05  CPWSGTHF-AREA   PIC X(1024).                             CPWSGTHF
001100     05  CPWSGTHF-DATA REDEFINES CPWSGTHF-AREA.                   CPWSGTHF
001200         07  CPWSGTHF-KEYS.                                       CPWSGTHF
001300         10  CPWSGTHF-EMPLOYEE-ID          PIC X(09).             CPWSGTHF
001400         10  CPWSGTHF-TRAN-SEQ-NUM         PIC S9(09) COMP.       CPWSGTHF
001500         10  CPWSGTHF-PAY-CYCLE-TYPE       PIC X(02).             CPWSGTHF
001600         10  CPWSGTHF-PPE-DATE.                                   CPWSGTHF
001700             15 CPWSGTHF-PPE-YY            PIC X(02).             CPWSGTHF
001800             15 CPWSGTHF-PPE-MM            PIC X(02).             CPWSGTHF
001900             15 CPWSGTHF-PPE-DD            PIC X(02).             CPWSGTHF
002000         10  CPWSGTHF-PPE-DATE-DB2         PIC X(10).             CPWSGTHF
002100         07  CPWSGTHF-MISC.                                       CPWSGTHF
002200         10  CPWSGTHF-FUNCTION             PIC S9(03) COMP-3.     CPWSGTHF
002300             88  CPWSGTHF-FETCHPCR-FUNCTION     VALUE +101.       CPWSGTHF
002400             88  CPWSGTHF-FETCHTHF-FUNCTION     VALUE +102.       CPWSGTHF
002500             88  CPWSGTHF-NEXT-PAGE-FUNCTION    VALUE +103.       CPWSGTHF
002600             88  CPWSGTHF-PREV-PAGE-FUNCTION    VALUE +104.       CPWSGTHF
002700         10  CPWSGTHF-FUNCTION-ID.                                CPWSGTHF
002800           15  FILLER                      PIC X(02).             CPWSGTHF
002900           15  FILLER                      PIC X(02).             CPWSGTHF
003000             88  CPWSGTHF-FUNCTION-VALID   VALUE 'AP'             CPWSGTHF
003100                 'LR' 'RA' 'TE' 'TX' 'SB' 'TL'                    CPWSGTHF
003200                 'UB' 'FT' 'DA' 'DD' 'HA' 'LA'.                   CPWSGTHF
003300             88  CPWSGTHF-SCREEN-3-LINE    VALUE 'AP'             CPWSGTHF
003400                 'LR' 'RA' 'TE' 'TX'.                             CPWSGTHF
003500             88  CPWSGTHF-SCREEN-9-LINE    VALUE 'SB'             CPWSGTHF
003600                 'UB'.                                            CPWSGTHF
003700             88  CPWSGTHF-SCREEN-10-LINE   VALUE 'FT'.            CPWSGTHF
003800             88  CPWSGTHF-SCREEN-11-LINE   VALUE 'DA'             CPWSGTHF
003900                 'DD' 'HA' 'LA'.                                  CPWSGTHF
004000             88  CPWSGTHF-SCREEN-12-LINE   VALUE 'TL'.            CPWSGTHF
004100         10  CPWSGTHF-RETURN               PIC S9(03) COMP-3.     CPWSGTHF
004200             88  CPWSGTHF-OK-RETURN             VALUE +101 +105.  CPWSGTHF
004300             88  CPWSGTHF-NOTFND-RETURN         VALUE +102.       CPWSGTHF
004400             88  CPWSGTHF-EOF-RETURN            VALUE +103.       CPWSGTHF
004500             88  CPWSGTHF-SEQ-NOTFND-RETURN     VALUE +104.       CPWSGTHF
004600             88  CPWSGTHF-SEQ-FOUND-RETURN      VALUE +105.       CPWSGTHF
004700             88  CPWSGTHF-SEQ-CONFLICT-RETURN   VALUE +106.       CPWSGTHF
004710             88  CPWSGTHF-PAST-PCR-RETURN       VALUE +107.       CPWSGTHF
004800         10  CPWSGTHF-TRAN-SEQ-LAST        PIC S9(09) COMP.       CPWSGTHF
004900         10  CPWSGTHF-TRAN-SEQ-FIRST       PIC S9(09) COMP.       CPWSGTHF
005000         10  CPWSGTHF-CURRENT-PAGE         PIC S9(03) COMP-3.     CPWSGTHF
005100         10  CPWSGTHF-PPI-PTR              PIC S9(03) COMP-3.     CPWSGTHF
005200         10  CPWSGTHF-THF-CNT              PIC S9(03) COMP-3.     CPWSGTHF
005300         10  CPWSGTHF-PPI-CNT              PIC S9(03) COMP-3.     CPWSGTHF
005400         10  CPWSGTHF-PPIS.                                       CPWSGTHF
005500         15  CPWSGTHF-PPI                  PIC X(08)              CPWSGTHF
005600             OCCURS 100.                                          CPWSGTHF
005700         10  CPWSGTHF-FIRST-SEQ            PIC S9(09) COMP.       CPWSGTHF
005800         10  CPWSGTHF-FIRST-PPI            PIC X(08).             CPWSGTHF
005900         10  CPWSGTHF-ITTL-TRAN-SEQ-NUM    PIC S9(09) COMP.       CPWSGTHF
006000         10  CPWSGTHF-HIGHEST-PAGE         PIC S9(03) COMP-3.     CPWSGTHF
006100         10  CPWSGTHF-ITTL-SAVE-SEQ-NUM    PIC S9(09) COMP.       CPWSGTHF
006110         10  CPWSGTHF-ITTL-PPI-PTR         PIC S9(03) COMP-3.     CPWSGTHF
007000         10  CPWSGTHF-ITTL-SAVE-PPI        PIC X(08).             36371028
007100         10  CPWSGTHF-FILLER               PIC X(137).            36371028
007200******** 10  CPWSGTHF-FILLER               PIC X(145).            36371028
006200***************    END OF SOURCE - CPWSGTHF    *******************CPWSGTHF
