000100**************************************************************/   32021138
000200*  COPYMEMBER: CPPFAU03                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_______JLT_______ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*   Initial release of Procedure Division copymember which   */   32021138
000700*   is used by program PPP520 in conjunction with copymember */   32021138
000800*   CPWSFAUE.                                                */   32021138
000900**************************************************************/   32021138
001000*                                                             *   CPPFAU03
001100*  This copymember is one of the Full Accounting Unit modules *   CPPFAU03
001200*  which campuses may need to modify to accomodate their own  *   CPPFAU03
001300*  Chart of Accounts structure.                               *   CPPFAU03
001400*                                                             *   CPPFAU03
001500***************************************************************   CPPFAU03
001600*                                                                 CPPFAU03
001700***************************************************************** CPPFAU03
001800*  ADJUST OBJECT CODE (OR FAU) FOR LEAVE TAKEN (VAC, SKL, PTO,    CPPFAU03
001900*  OR TRM) ENTRIES BASED ON FAU ELEMENTS.  NOTE THAT THE DOS      CPPFAU03
002000*  INDEX MAY BE SET TO ZERO TO PREVENT THE OBJECT CODE FROM       CPPFAU03
002100*  OVERLAID BY THE DOS TABLE VALUE.                               CPPFAU03
002200***************************************************************** CPPFAU03
002300                                                                  CPPFAU03
002400     MOVE FAUE-FAU-CHAR30      TO FAUE-FAU-UNFORMATTED            CPPFAU03
002500     IF   FAUE-FAU-SUB-ACCOUNT   = '6'                            CPPFAU03
002600      AND FAUE-WORK-GROSS-AMT  NOT NEGATIVE                       CPPFAU03
002700         MOVE '8930' TO  FAUE-OBJECT-CODE                         CPPFAU03
002800         MOVE ZERO   TO  FAUE-DOS-INDX                            CPPFAU03
002900     ELSE                                                         CPPFAU03
003000         IF FAUE-WORK-GROSS-AMT   NEGATIVE                        CPPFAU03
003100             IF FAUE-FAU-SUB-ACCOUNT   = '6'                      CPPFAU03
003200                 MOVE '8932' TO  FAUE-OBJECT-CODE                 CPPFAU03
003300                 MOVE ZERO   TO  FAUE-DOS-INDX                    CPPFAU03
003400             ELSE                                                 CPPFAU03
003500                 IF   FAUE-FAU-SUB-ACCOUNT   = '0'                CPPFAU03
003600                   OR FAUE-FAU-SUB-ACCOUNT   = '1'                CPPFAU03
003700                   OR FAUE-FAU-SUB-ACCOUNT   = '2'                CPPFAU03
003800                     MOVE '8931' TO  FAUE-OBJECT-CODE             CPPFAU03
003900                     MOVE ZERO   TO  FAUE-DOS-INDX                CPPFAU03
004000                 END-IF                                           CPPFAU03
004100             END-IF                                               CPPFAU03
004200         END-IF                                                   CPPFAU03
004300     END-IF                                                       CPPFAU03
004400     MOVE FAUE-FAU-UNFORMATTED TO FAUE-FAU-CHAR30                 CPPFAU03
004500                                                                  CPPFAU03
004600************** END OF COPY CPPFAU03 ***************************   CPPFAU03
