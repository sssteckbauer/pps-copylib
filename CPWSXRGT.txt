000010*==========================================================%      UCSD0033
000020*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD0033
000030*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD9999     =%      UCSD0033
000040*=                                                        =%      UCSD0033
000050*==========================================================%      UCSD0033
000200*==========================================================%      UCSD0033
000300*=    COPY MEMBER: CPWSXRGT                               =%      UCSD0033
000400*=    CHANGE # 00033        PROJ. REQUEST: 00033          =%      UCSD0033
000500*=    NAME: RON BLOCK       MODIFICATION DATE: 02/14/95   =%      UCSD0033
000600*=                                                        =%      UCSD0033
000700*=    DESCRIPTION:                                        =%      UCSD0033
000800*=     ADDED IFIS FIELDS.                                 =%      UCSD0033
001000*=                                                        =%      UCSD0033
001100*==========================================================%      UCSD0033
000000**************************************************************/   32031150
000001*  COPYMEMBER: CPWSXRGT                                      */   32031150
000002*  RELEASE: ___1150______ SERVICE REQUEST(S): ____13202____  */   32031150
000003*  NAME:_______WJG_______ MODIFICATION DATE:  ___10/31/97__  */   32031150
000004*  DESCRIPTION:                                              */   32031150
000005*                                                            */   32031150
000006*  IMPLEMENTATION OF THE GENERIC FULL ACCOUNTING UNIT        */   32031150
000007*                                                            */   32031150
000008**************************************************************/   32031150
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXRGT                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000010**************************************************************/   13310150
000020*  COPYMEMBER:  CPWSXRGT                                     */   13310150
000030*  RELEASE # ___0150___   SERVICE REQUEST NO  ___1331___     */   13310150
000040*  NAME __STEINITZ_____   MODIFICATION DATE ____06/02/85_____*/   13310150
000050*  DESCRIPTION                                               */   13310150
000060*   CPWSXRGT DESCRIBES THE OPEN PROVISION RANGE ADJUSTMENT   */   13310150
000070*   TRANSACTIONS. THIS FILE IS CREATED, MODIFIED, AND USED   */   13310150
000080*   BY PPP950 TO COST OPEN PROVISION RANGE ADFJUSTMENTS.     */   13310150
000090*   SEVERAL FIELDS WERE ADDED IN THIS RELEASE TO CONFORM TO  */   13310150
000100*   CHANGES IN COLLECTIVE BARGAINING CODING CHANGES.         */   13310150
000110**************************************************************/   13310150
000120*01  XRGT-RANGE-TRANSACTION-RECORD.                               30930413
000130     05  XRGT-KEY.                                                CPWSXRGT
002800****     10  XRGT-LOCATION.                                       32021150
002900****         15  XRGT-LOC-LEFT       PIC  X(01).                  32021150
003000****         15  XRGT-LOC-RIGHT      PIC  X(01).                  32021150
003100****     10  XRGT-SAU                PIC  X(01).                  32021150
003200****     10  XRGT-SUB-CAMPUS         PIC  X(01).                  32021150
003300****     10  XRGT-ACCOUNT            PIC  X(07).                  32021150
003500****     10  XRGT-FUND               PIC  X(06).                  32021150
003700****     10  XRGT-SUB-BUDGET         PIC  X(02).                  32021150
003710         10  XRGT-FAU.                                            32021150
003720             15  XRGT-LOCATION-DATA  PIC  X(04).                  32021150
003730             15  XRGT-FAU-REMAINDER  PIC  X(27).                  32021150
000220         10  XRGT-TITLE              PIC  X(04).                  CPWSXRGT
000230         10  FILLER                  PIC  X(01).                  CPWSXRGT
000240         10  XRGT-PROVISION-NO       PIC  X(06).                  CPWSXRGT
000250     05  XRGT-PERB-CODE              PIC  X(02).                  CPWSXRGT
000260     05  XRGT-PROVISION-TYPE         PIC  X(01).                  CPWSXRGT
000270     05  XRGT-PROVISION-DESCR        PIC  X(30).                  CPWSXRGT
000280     05  XRGT-OP-EFF-DATE.                                        CPWSXRGT
000290         10  XRGT-OP-EFF-YEAR        PIC  9(02).                  CPWSXRGT
000300         10  XRGT-OP-EFF-MONTH       PIC  9(02).                  CPWSXRGT
000310         10  XRGT-OP-EFF-DAY         PIC  9(02).                  CPWSXRGT
000320     05  XRGT-RA-EFF-DATE.                                        CPWSXRGT
000330         10  XRGT-RA-EFF-YEAR        PIC  9(02).                  CPWSXRGT
000340         10  XRGT-RA-EFF-MONTH       PIC  9(02).                  CPWSXRGT
000350         10  XRGT-RA-EFF-DAY         PIC  9(02).                  CPWSXRGT
000360     05  XRGT-OLD-MONTHLY-RATE       PIC S9(05)V9(02).            CPWSXRGT
000370     05  XRGT-FTE                    PIC S9(03)V9(02).            CPWSXRGT
000380     05  XRGT-OLD-ANNUAL-AMT         PIC S9(07).                  CPWSXRGT
000390     05  XRGT-NEW-MONTHLY-RATE       PIC S9(05)V9(02).            CPWSXRGT
000400     05  XRGT-NEW-ANNUAL-AMT         PIC S9(07).                  CPWSXRGT
000410     05  XRGT-STATUS                 PIC  X(01).                  CPWSXRGT
000420         88  XRGT-DONE               VALUE 'D'.                   CPWSXRGT
000430         88  XRGT-WAITING            VALUE 'W'.                   CPWSXRGT
000440         88  XRGT-RPT-ONLY           VALUE 'R'.                   13310150
000450     05  XRGT-TITLE-SPECL-HANDLING-CODE                           13310150
000460                                     PIC  X(01).                  13310150
000470     05  XRGT-APPT-REPRESENTATION-CODE                            13310150
000480                                     PIC  X(01).                  13310150
000490     05  XRGT-RANGE-ADJ-DISTR-UNIT-CODE                           13310150
000500                                     PIC  X(01).                  13310150
000490     05  XRGT-IFIS-IFOAPAL.                                       UCSD0033
000490         10  XRGT-IFIS-INDEX             PIC X(10).               UCSD0033
000490         10  XRGT-IFIS-FUND              PIC X(06).               UCSD0033
000490         10  XRGT-IFIS-ORG               PIC X(06).               UCSD0033
000490         10  XRGT-IFIS-ACCOUNT           PIC X(06).               UCSD0033
000490         10  XRGT-IFIS-PROGRAM           PIC X(06).               UCSD0033
000490         10  XRGT-IFIS-ACTIVITY          PIC X(06).               UCSD0033
000490         10  XRGT-IFIS-LOCATION          PIC X(06).               UCSD0033
000490     05  XRGT-IFIS-TITLES.                                        UCSD0033
000490         10  XRGT-IFIS-INDEX-TITLE       PIC X(35).               UCSD0033
000490         10  XRGT-IFIS-FUND-TITLE        PIC X(35).               UCSD0033
000490         10  XRGT-IFIS-ORG-TITLE         PIC X(35).               UCSD0033
000490         10  XRGT-IFIS-ACCOUNT-TITLE     PIC X(35).               UCSD0033
000490         10  XRGT-IFIS-PROGRAM-TITLE     PIC X(35).               UCSD0033
000490         10  XRGT-IFIS-ACTIVITY-TITLE    PIC X(35).               UCSD0033
000490         10  XRGT-IFIS-LOCATION-TITLE    PIC X(35).               UCSD0033
