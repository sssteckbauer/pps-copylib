000100**************************************************************/   14240278
000200*  COPYLIB:  CPLNKOCE                                        */   14240278
000300*  REL#:  0278   REF: NONE  SERVICE REQUESTS:   ____1424____ */   14240278
000400*  NAME ___BMB_________   MODIFICATION DATE ____01/12/87_____*/   14240278
000500*  DESCRIPTION                                               */   14240278
000600*    ADDED NEW LINKAGE                                       */   14240278
000700*                                                            */   14240278
000800**************************************************************/   14240278
000900*COPYID=CPLNKOCE                                                  CPLNKOCE
001000******************************************************************CPLNKOCE
001100*                        C P L N K O C E                         *CPLNKOCE
001200******************************************************************CPLNKOCE
001300*                                                                *CPLNKOCE
001400*01  PPBENOCE-INTERFACE.                                          CPLNKOCE
001500*                                                                 CPLNKOCE
001600******************************************************************CPLNKOCE
001700*      THE FOLLOWING FLAGS AND FIELDS ARE PASSED BY CALLER       *CPLNKOCE
001800******************************************************************CPLNKOCE
001900*                                                                 CPLNKOCE
002000     05  KOCE-ACTION-FLAG            PIC X(01).                   CPLNKOCE
002100         88  KOCE-ACTION-INITIALIZING            VALUE '1'.       CPLNKOCE
002200         88  KOCE-ACTION-RATE-RETRIEVAL          VALUE '2'.       CPLNKOCE
002300         88  KOCE-ACTION-PREMIUM-CALC            VALUE '3'.       CPLNKOCE
002400*                                                                 CPLNKOCE
002500     05  KOCE-SYSTEM-CODE            PIC X(01).                   CPLNKOCE
002600         88  KOCE-VALID-SYSTEM-CODE              VALUE IS 'O'.    CPLNKOCE
002700*                                                                 CPLNKOCE
002800     05  KOCE-RETIREMENT-GROSS       PIC 9(7)V9(2).               CPLNKOCE
002900         88  KOCE-INVALID-RETIREMENT-GROSS       VALUE IS ZERO.   CPLNKOCE
003000*                                                                 CPLNKOCE
003100     05  KOCE-RETIREMENT-RATE        PIC 9(5)V9(2).               CPLNKOCE
003200         88  KOCE-INVALID-RETIREMENT-RATE        VALUE IS ZERO.   CPLNKOCE
003300*                                                                 CPLNKOCE
003400******************************************************************CPLNKOCE
003500*      THE FOLLOWING FLAGS AND FIELDS ARE RETURNED TO CALLER     *CPLNKOCE
003600******************************************************************CPLNKOCE
003700*                                                                 CPLNKOCE
003800     05  KOCE-RETURN-STATUS-FLAG     PIC X(01).                   CPLNKOCE
003900         88  KOCE-RETURN-STATUS-NORMAL           VALUE '0'.       CPLNKOCE
004000         88  KOCE-RETURN-STATUS-ABORTING         VALUE '1'.       CPLNKOCE
004100*                                                                 CPLNKOCE
004200     05  KOCE-INVALID-LOOKUP-ARG-FLAG PIC X(01).                  CPLNKOCE
004300         88  KOCE-INVALID-LOOKUP-ARG             VALUE '1'.       CPLNKOCE
004400*                                                                 CPLNKOCE
004500     05  KOCE-RETURN-FIELDS.                                      CPLNKOCE
004600         10  KOCE-DED-AMOUNT         PIC 9(7)V9(2).               CPLNKOCE
