000008**************************************************************/   37640580
000009*  COPYMEMBER: CPWSRC10                                      */   37640580
000010*  RELEASE: ___0580______ SERVICE REQUEST(S): _____3764____  */   37640580
000011*  NAME: __MADELYN_SANO__ MODIFICATION DATE:  ___05/29/91__  */   37640580
000012*  DESCRIPTION:                                              */   37640580
000013*  - ADDED THE FY COVERED COMP RETIREMENT CODES              */   37640580
000016**************************************************************/   37640580
000017**************************************************************/   01900580
000018*  COPYMEMBER: CPWSRC10                                      */   01900580
000019*  RELEASE: ___0580______ SERVICE REQUEST(S): ____10190____  */   01900580
000020*  NAME:___LOU DULANEY___ MODIFICATION DATE:  ___05/29/91__  */   01900580
000030*  DESCRIPTION:                                              */   01900580
000040*  - ADDED THE SAFE HARBOR RETIREMENT CODE                   */   01900580
000050**************************************************************/   01900580
000100**************************************************************/   37370537
000200*  COPYMEMBER: CPWSRC10                                      */   37370537
000300*  RELEASE: ___0537______ SERVICE REQUEST(S): _____3737____  */   37370537
000400*  NAME:___M. BAPTISTA___ MODIFICATION DATE:  ___01/24/91__  */   37370537
000500*  DESCRIPTION:                                              */   37370537
000600*  - COMMENTED 88-LEVEL RC11-CSTRS-RETR                      */   37370537
000700**************************************************************/   37370537
000100**************************************************************/   36030532
000200*  COPY MODULE:  CPWSRC10.                                   */   36030532
000300*  RELEASE __0532__           SERVICE REQUEST __3603__       */   36030532
000400*  NAME __SRS___              CREATION DATE ____06/25/90_____*/   36030532
000500*  DESCRIPTION                                               */   36030532
000600*  CONTAINS RUSH CHECK OPTION 1 REQUEST TRANSACTION FORMATS  */   36030532
000700**************************************************************/   36030532
000800     SKIP3                                                        CPWSRC10
000900*01  CPWSRC10.                                                    CPWSRC10
001000     03  RC10-TRAN-ID                   PIC X(04).                CPWSRC10
001100     03  RC10-EMPL-ID                   PIC X(09).                CPWSRC10
001200     03  RC10-TRAN-DATE.                                          CPWSRC10
001300         10  RC10-TRAN-DT-MM            PIC 99.                   CPWSRC10
001400         10  RC10-TRAN-DT-DD            PIC 99.                   CPWSRC10
001500         10  RC10-TRAN-DT-YY            PIC 99.                   CPWSRC10
001600     03  RC10-TRAN-DATA.                                          CPWSRC10
001700         05 RC10-EMPL-NAME              PIC X(26).                CPWSRC10
001800         05 RC10-EIN-EDB-FLAG           PIC X(01).                CPWSRC10
001900             88 RC10-EIN-ON-EDB-YES                VALUE ' ' 'Y'. CPWSRC10
002000             88 RC10-EIN-ON-EDB-NO                 VALUE 'N'.     CPWSRC10
002100         05 RC10-PRINT-ADVICE-FLAG      PIC X(01).                CPWSRC10
002200             88 RC10-PRINT-ADVICE-YES              VALUE ' ' 'Y'. CPWSRC10
002300             88 RC10-PRINT-ADVICE-NO               VALUE 'N'.     CPWSRC10
002400         05 RC10-PCT-DED-SUPPRESS-FLAG  PIC X.                    CPWSRC10
002500             88 RC10-PCT-DED-SUPPRESSED            VALUE 'Y'.     CPWSRC10
002600         05 RC10-SUSPENSE-SUPPRESS-FLAG PIC X.                    CPWSRC10
002700             88 RC10-SUSPENSE-SUPPRESSED           VALUE 'Y'.     CPWSRC10
002800         05  RC10-PRIOR-YEAR-EDB-FLAG   PIC X.                    CPWSRC10
002900             88 RC10-PRIOR-YEAR-EDB-TRANS          VALUE 'Y'.     CPWSRC10
003000         05 FILLER                      PIC X(62).                CPWSRC10
003100     03  RC11-TRAN-DATA REDEFINES RC10-TRAN-DATA.                 CPWSRC10
003200         05 RC11-LIABILITY-ACCT      PIC X(6).                    CPWSRC10
003300         05 RC11-RETIRE-CODE         PIC X.                       CPWSRC10
003400             88 RC11-VALID-RETR-CODE VALUE ' ' 'B' 'C' 'F' 'N'    CPWSRC10
004200                       'O' 'P' 'S' 'U' 'H' '1' '2' '3' '4' '5'.   01900580
004210***********************************************'O' 'P' 'S' 'U'.   01900580
003600             88 RC11-NO-RETR-PLAN                       VALUE ' '.CPWSRC10
003700             88 RC11-UCRS-RETR-ND                       VALUE 'B'.CPWSRC10
003800             88 RC11-UCRS-RETR                          VALUE 'U'.CPWSRC10
003900             88 RC11-PERS-RETR                          VALUE 'P'.CPWSRC10
004700*************88 RC11-CSTRS-RETR                         VALUE 'C'.37370537
004100             88 RC11-FCSRS-RETR                         VALUE 'F'.CPWSRC10
004200             88 RC11-SCERA-RETR                         VALUE 'S'.CPWSRC10
004300             88 RC11-OCERS-RETR                         VALUE 'O'.CPWSRC10
004400             88 RC11-NOT-ELIG                           VALUE 'N'.CPWSRC10
005110             88 RC11-SFHBR-RETR                         VALUE 'H'.01900580
005120             88 RC11-UCRS-FY-ND                         VALUE '1'.37640580
005130             88 RC11-PERS-FY-ND                         VALUE '2'.37640580
005140             88 RC11-FCSRS-FY-ND                        VALUE '3'.37640580
005150             88 RC11-SCERA-FY-ND                        VALUE '4'.37640580
005160             88 RC11-OCERS-FY-ND                        VALUE '5'.37640580
004500         05 RC11-FICA-ELIG-CODE           PIC X.                  CPWSRC10
004600             88 RC11-VALID-FICA-ELIG      VALUE ' ' 'E' 'M' 'N'.  CPWSRC10
004700             88 RC11-ELIG-FICA                          VALUE 'E'.CPWSRC10
004800             88 RC11-ELIG-MED-FICA                      VALUE 'M'.CPWSRC10
004900             88 RC11-NOT-ELIG-FICA                      VALUE 'N'.CPWSRC10
005000         05 RC11-TIP-INDICATOR            PIC X.                  CPWSRC10
005100             88 RC11-VALID-TIP-IND        VALUE ' ' 'Y' 'N' 'H'.  CPWSRC10
005200         05 RC11-STUDENT-STATUS           PIC X.                  CPWSRC10
005300             88 RC11-VALID-STUDENT-STATUS VALUE ' ' '1' THRU '4'. CPWSRC10
005400             88 RC11-STUDENT-NOT-REGISTED               VALUE '1'.CPWSRC10
005500             88 RC11-STUDENT-NOT-REG-DEGREE             VALUE '2'.CPWSRC10
005600             88 RC11-STUDENT-UNDERGRAD                  VALUE '3'.CPWSRC10
005700             88 RC11-STUDENT-GRADUATE                   VALUE '4'.CPWSRC10
005800         05 RC11-LEAVE-ACCRUAL-CODE       PIC X.                  CPWSRC10
005900             88 RC11-VALID-LEAVE-ACC      VALUE ' ' 'A' THRU 'N'. CPWSRC10
006000         05 RC11-FICA-YTD-GRS-OVERRIDE    PIC 9(7)V99.            CPWSRC10
006200         05 RC11-MED-YTD-GRS-OVERRIDE     PIC 9(7)V99.            CPWSRC10
006210         05 RC11-FICA-YTD-DED-OVERRIDE    PIC 9(7)V99.            CPWSRC10
006300         05 RC11-MED-YTD-DED-OVERRIDE     PIC 9(7)V99.            CPWSRC10
006400         05 RC11-FED-TX-MARITAL-STATUS    PIC X.                  CPWSRC10
006500             88 RC11-VALID-FED-TX-MRT     VALUE ' ' 'S' 'M'.      CPWSRC10
006600             88 RC11-FED-TX-SINGLE                      VALUE 'S'.CPWSRC10
006700             88 RC11-FED-TX-MARRIED                     VALUE 'M'.CPWSRC10
006800         05 RC11-FED-TX-EXEMPT            PIC 9(03).              CPWSRC10
006900             88 RC11-VALID-FED-TX-EXEMPT  VALUE 000 THRU 150      CPWSRC10
006910                                                     998 999.     CPWSRC10
007000             88 RC11-W4E-ON-FILE                        VALUE 998.CPWSRC10
007100             88 RC11-EXMPT-FED-TX                       VALUE 999.CPWSRC10
007200         05 RC11-ST-TX-MARITAL-STATUS     PIC X.                  CPWSRC10
007300             88 RC11-VALID-ST-TX-MRT      VALUE ' ' 'S' 'M' 'H'.  CPWSRC10
007400             88 RC11-ST-TX-SINGLE                       VALUE 'S'.CPWSRC10
007500             88 RC11-ST-TX-MARRIED                      VALUE 'M'.CPWSRC10
007600             88 RC11-ST-TX-HD-HOUSE                     VALUE 'H'.CPWSRC10
007700         05 RC11-ST-TX-PER-DED            PIC 9(03).              CPWSRC10
007800             88 RC11-VALID-ST-TX-PER-DED  VALUE 000 THRU 150      CPWSRC10
007810                                                     998 999.     CPWSRC10
007900             88 RC11-ST-TX-EXMPT                        VALUE 997.CPWSRC10
008000             88 RC11-ST-TX-W4-ON-FILE                   VALUE 998.CPWSRC10
008100         05 RC11-ST-TX-ITM-DED            PIC 9(03).              CPWSRC10
008200         05 FILLER                        PIC X(35).              CPWSRC10
008300     03  RC13-TRAN-DATA REDEFINES RC10-TRAN-DATA.                 CPWSRC10
008400         05 RC13-PAY-TRAN-CODE       PIC X(02).                   CPWSRC10
008500         05 RC13-TRAN-IMAGE.                                      CPWSRC10
008600             10 RC13-PAY-END-DATE.                                CPWSRC10
008700                 15 RC13-PAY-MO      PIC 9(02).                   CPWSRC10
008800                 15 RC13-PAY-DA      PIC 9(02).                   CPWSRC10
008900                 15 RC13-PAY-YR      PIC 9(02).                   CPWSRC10
009000             10 RC13-PAY-CYCLE       PIC X.                       CPWSRC10
009100             10 RC13-DIST-NO.                                     CPWSRC10
009200                 15 RC13-APPT        PIC X.                       CPWSRC10
009300                 15 RC13-DIST        PIC X.                       CPWSRC10
009400             10 RC13-TITLE-CODE.                                  CPWSRC10
009500                15 FILLER            PIC X.                       CPWSRC10
009600                15 RC13-TTL-CODE     PIC X(04).                   CPWSRC10
009700             10 RC13-LOC-ACCT-FND-SUB.                            CPWSRC10
009800                 15 RC13-LOC         PIC X.                       CPWSRC10
009900                 15 RC13-ACCT-FND.                                CPWSRC10
010000                     20 RC13-ACCT    PIC X(06).                   CPWSRC10
010100                     20 RC13-FND     PIC X(05).                   CPWSRC10
010200                 15 RC13-SUB         PIC X.                       CPWSRC10
010300                 15 FILLER           PIC X(07).                   CPWSRC10
010400             10 RC13-RATEX           PIC X(07).                   CPWSRC10
010500             10 RC13-RATE9       REDEFINES                        CPWSRC10
010600                 RC13-RATEX          PIC 9(5)V99.                 CPWSRC10
010700             10 RC13-RATE9W4     REDEFINES                        CPWSRC10
010800                 RC13-RATEX          PIC 9(3)V9(04).              CPWSRC10
010900             10 RC13-ANNL-HRLY-IND   PIC X.                       CPWSRC10
011000             10 RC13-EARN-AND-TIME.                               CPWSRC10
011100                 15 RC13-1ST.                                     CPWSRC10
011200                     20 RC13-1ST-DOS PIC X(3).                    CPWSRC10
011300                     20 RC13-1ST-TIMEX PIC X(5).                  CPWSRC10
011400                     20 RC13-1ST-TIME9 REDEFINES                  CPWSRC10
011500                         RC13-1ST-TIMEX PIC 999V99.               CPWSRC10
011600                     20 RC13-1ST-PCT REDEFINES                    CPWSRC10
011700                         RC13-1ST-TIMEX PICTURE 9V9(04).          CPWSRC10
011800                     20 RC13-1ST-HR-PCT PIC X.                    CPWSRC10
011900                 15 RC13-2ND.                                     CPWSRC10
012000                     20 RC13-2ND-DOS PIC X(3).                    CPWSRC10
012100                     20 RC13-2ND-TIMEX PIC X(5).                  CPWSRC10
012200                     20 RC13-2ND-TIME9 REDEFINES                  CPWSRC10
012300                         RC13-2ND-TIMEX PIC 999V99.               CPWSRC10
012400                 15 RC13-3RD.                                     CPWSRC10
012500                     20 RC13-3RD-DOS PIC X(3).                    CPWSRC10
012600                     20 RC13-3RD-TIMEX PIC X(5).                  CPWSRC10
012700                     20 RC13-3RD-TIME9 REDEFINES                  CPWSRC10
012800                         RC13-3RD-TIMEX PIC 999V99.               CPWSRC10
012900             10 RC13-WSP             PIC X.                       CPWSRC10
013000             10 RC13-EMP-REL-CODE    PIC X.                       CPWSRC10
013100             10 RC13-APPT-TYPE-CODE  PIC X.                       CPWSRC10
013200             10 RC13-DIST-UNIT-CODE  PIC X.                       CPWSRC10
013300             10 RC13-FILLER-COS      PIC X(20).                   CPWSRC10
013400     03  RC14-TRAN-DATA REDEFINES RC10-TRAN-DATA.                 CPWSRC10
013500         05 RC14-PAY-TRAN-CODE       PIC X(02).                   CPWSRC10
013600         05 RC14-TRAN-IMAGE.                                      CPWSRC10
013600           07 RC14-TRAN-IMAGE1.                                   CPWSRC10
013700             10 RC14-LOC-ACCT-FND-SUB.                            CPWSRC10
013800                 15 RC14-LOC         PIC X.                       CPWSRC10
013900                 15 RC14-ACCT-FND.                                CPWSRC10
014000                     20 RC14-ACCT    PIC X(06).                   CPWSRC10
014100                     20 RC14-FND     PIC X(05).                   CPWSRC10
014200                 15 RC14-SUB         PIC X.                       CPWSRC10
013600           07 RC14-TRAN-IMAGE2.                                   CPWSRC10
014400             10 RC14-TITLE-CODE.                                  CPWSRC10
014500                 15 FILLER           PIC X.                       CPWSRC10
014600                 15 RC14-TTL-CODE    PIC X(04).                   CPWSRC10
014700             10 RC14-DOS             PIC X(03).                   CPWSRC10
014800             10 RC14-GROSSX          PIC X(07).                   CPWSRC10
014900             10 RC14-GROSS9      REDEFINES                        CPWSRC10
015000                 RC14-GROSSX         PIC 9(05)V99.                CPWSRC10
015100             10 RC14-RATE9       REDEFINES                        CPWSRC10
015200                 RC14-GROSSX         PIC 9(03)V9(4).              CPWSRC10
015300             10 RC14-SIGN            PIC X.                       CPWSRC10
015400             10 RC14-TIMEX           PIC X(05).                   CPWSRC10
015500             10 RC14-TIME9       REDEFINES                        CPWSRC10
015600                 RC14-TIMEX          PIC 999V99.                  CPWSRC10
015700             10 RC14-TIME-PCT    REDEFINES                        CPWSRC10
015800                 RC14-TIMEX          PIC 9V9999.                  CPWSRC10
015900             10 RC14-HR-PCT          PIC X.                       CPWSRC10
016000             10 RC14-PAY-END.                                     CPWSRC10
016100                 15 RC14-PAY-MO      PIC XX.                      CPWSRC10
016200                 15 RC14-PAY-DA      PIC XX.                      CPWSRC10
016300                 15 RC14-PAY-YR      PIC XX.                      CPWSRC10
016400             10 RC14-PAY-CYCLE       PIC X.                       CPWSRC10
016500             10 RC14-RATE-ADJ-IND    PIC X.                       CPWSRC10
016600             10 RC14-WSP             PIC X.                       CPWSRC10
016700             10 FILLER               PIC X.                       CPWSRC10
016800             10 RC14-EMP-REL-CODE    PIC X.                       CPWSRC10
016900             10 RC14-APPT-TYPE-CODE  PIC X.                       CPWSRC10
017000             10 RC14-DIST-UNIT-CODE  PIC X.                       CPWSRC10
017100             10 FILLER               PIC X(14).                   CPWSRC10
017200             10 RC14-FILLER-OP       PIC X(02).                   CPWSRC10
017300             10 RC14-FILLER-COS      PIC X(20).                   CPWSRC10
017400         05  FILLER                  PIC X(7).                    CPWSRC10
017500     03  RC15-TRAN-DATA REDEFINES RC10-TRAN-DATA.                 CPWSRC10
017600         05  RC15-ELEMENTS       OCCURS 4.                        CPWSRC10
017700             10  RC15-ELEM-NO.                                    CPWSRC10
017800                 15  RC15-SEG-NO-1   PIC X.                       CPWSRC10
017900                 15  RC15-ELEM-NO-3  PIC 9(03).                   CPWSRC10
018000                 15  RC15-ELEM-NO-3X REDEFINES                    CPWSRC10
018100                     RC15-ELEM-NO-3  PIC X(03).                   CPWSRC10
018200             10  RC15-BAL-TYPE       PIC X.                       CPWSRC10
018300             10  RC15-AMTX           PIC X(07).                   CPWSRC10
018400             10  RC15-AMT9       REDEFINES                        CPWSRC10
018500                 RC15-AMTX           PIC 9(05)V99.                CPWSRC10
018600             10  RC15-SIGN           PIC X.                       CPWSRC10
018700         05  RC15-QTR-CD             PIC X.                       CPWSRC10
018800         05  RC15-YEAR               PIC X.                       CPWSRC10
018900         05  RC15-XFOOTX             PIC X(07).                   CPWSRC10
019000         05  RC15-XFOOT9         REDEFINES                        CPWSRC10
019100             RC15-XFOOTX             PIC 9(05)V99.                CPWSRC10
019200         05  FILLER                  PIC X(32).                   CPWSRC10
019300     03  RC16-TRAN-DATA REDEFINES RC10-TRAN-DATA.                 CPWSRC10
019400         05  RC16-ELEMENTS       OCCURS 4.                        CPWSRC10
019500             10  RC16-ELEM-NO.                                    CPWSRC10
019600                 15  RC16-SEG-NO-1   PIC X.                       CPWSRC10
019700                 15  RC16-ELEM-NO-3  PIC 9(03).                   CPWSRC10
019800                 15  RC16-ELEM-NO-3X REDEFINES                    CPWSRC10
019900                     RC16-ELEM-NO-3  PIC X(03).                   CPWSRC10
020000             10  RC16-BAL-TYPE       PIC X.                       CPWSRC10
020100             10  RC16-AMTX           PIC X(07).                   CPWSRC10
020200             10  RC16-AMT9       REDEFINES                        CPWSRC10
020300                 RC16-AMTX           PIC 9(05)V99.                CPWSRC10
020400             10  RC16-SIGN           PIC X.                       CPWSRC10
020500         05  FILLER                  PIC XX.                      CPWSRC10
020600         05  RC16-XFOOTX             PIC X(07).                   CPWSRC10
020700         05  RC16-XFOOT9         REDEFINES                        CPWSRC10
020800             RC16-XFOOTX             PIC 9(05)V99.                CPWSRC10
020900         05  FILLER                  PIC X(32).                   CPWSRC10
021000     03  RC17-TRAN-DATA REDEFINES RC10-TRAN-DATA.                 CPWSRC10
021100         05  RC17-ELEMENTS       OCCURS 4.                        CPWSRC10
021200             10  RC17-ELEM-NO.                                    CPWSRC10
021300                 15  RC17-SEG-NO-1   PIC X.                       CPWSRC10
021400                 15  RC17-ELEM-NO-3  PIC 9(03).                   CPWSRC10
021500                 15  RC17-ELEM-NO-3X REDEFINES                    CPWSRC10
021600                     RC17-ELEM-NO-3  PIC X(03).                   CPWSRC10
021700             10  RC17-BAL-TYPE       PIC X.                       CPWSRC10
021800             10  RC17-AMTX           PIC X(07).                   CPWSRC10
021900             10  RC17-AMT9       REDEFINES                        CPWSRC10
022000                 RC17-AMTX           PIC 9(05)V99.                CPWSRC10
022100             10  RC17-SIGN           PIC X.                       CPWSRC10
022200         05  FILLER                  PIC XX.                      CPWSRC10
022300         05  RC17-XFOOTX             PIC X(07).                   CPWSRC10
022400         05  RC17-XFOOT9         REDEFINES                        CPWSRC10
022500             RC17-XFOOTX             PIC 9(05)V99.                CPWSRC10
022600         05  FILLER                  PIC X(32).                   CPWSRC10
022700     03  RC18-TRAN-DATA REDEFINES RC10-TRAN-DATA.                 CPWSRC10
022800         05 RC18-SUSPENSE-ELMTS.                                  CPWSRC10
022900             10 RC18-SUSP-DED-ARRAY  OCCURS 7 TIMES.              CPWSRC10
023000                 15 RC18-SUSP-DED-ALPHA      PIC X(03).           CPWSRC10
023100                 15 RC18-SUSP-DED-ELMT REDEFINES                  CPWSRC10
023200                          RC18-SUSP-DED-ALPHA PIC 9(03).          CPWSRC10
023300         05  FILLER                  PIC X(72).                   CPWSRC10
023400     03  RC19-TRAN-DATA REDEFINES RC10-TRAN-DATA.                 CPWSRC10
023500         05 RC19-VENDOR-DATA         PIC X(90).                   CPWSRC10
023600         05  FILLER                  PIC X(3).                    CPWSRC10
