000800*==========================================================%      CLLNF501
000200*=    COPY MEMBER: CLLNF501                               =%      CLLNF501
000300*=    CHANGE UCSD0102       PROJ. REQUEST: RELEASE 1138   =%      CLLNF501
000400*=    NAME:  J.E. MCCLANE   MODIFICATION DATE: 09/29/97   =%      CLLNF501
000500*=                                                        =%      CLLNF501
000600*=    DESCRIPTION:                                        =%      CLLNF501
000700*=    THIS IS A NEW COPY MEMBER TO VALIDATE SUB, TITLE    =%      CLLNF501
000700*=    DOS  FOR WORK STUDY FUNDS.                          =%      CLLNF501
000800*=                                                        =%      CLLNF501
008000*==========================================================%      CLLNF501
000700**************************************************************/   CLLNF501
000800*    COPYID=CLLNF501                                              CLLNF501
000900*01  PLFAU501-INTERFACE.                                          CLLNF501
001000*--------------------------------------------------------------   CLLNF501
001100*    W O S - D O S - F U N D - S U B                         *    CLLNF501
001200*--------------------------------------------------------------   CLLNF501
001300     05  F501-INPUT.                                              CLLNF501
001300         10  F501-FAU                   PIC X(30).                CLLNF501
001300         10  FAU-F501-TITLE             PIC 9(4).                 CLLNF501
001300             88  FAU-F501-TITLE-ACAD    VALUES 0800 THRU 3999.    CLLNF501
001400         10  FAU-F501-DOS               PIC X(3).                 CLLNF501
001300             88  FAU-F501-DOS-POSTDOC   VALUES 'FEN' 'FES' 'FEL'. CLLNF501
001900     05  F501-OUTPUT.                                             CLLNF501
002310         10  F501-RETURN-VALUE          PIC X.                    CLLNF501
002310             88  F501-FAU-VALID             VALUE 'V'.            CLLNF501
002310             88  F501-INVALID               VALUE 'I'.            CLLNF501
002310             88  F501-ROUTINE-FAILURE       VALUE 'X'.            CLLNF501
002310         10  F501-MSG-COUNT             PIC S9(4) COMP.           CLLNF501
002310         10  F501-MESSAGES.                                       CLLNF501
002310             15  FILLER OCCURS 1 TIMES.                           CLLNF501
002310                 20  F501-MSG           PIC X(5).                 CLLNF501
