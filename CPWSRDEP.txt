000000**************************************************************/   54541281
000001*  COPYMEMBER: CPWSRDEP                                      */   54541281
000002*  RELEASE: ___1281______ SERVICE REQUEST(S): ____15454____  */   54541281
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___06/22/99__  */   54541281
000004*  DESCRIPTION:                                              */   54541281
000005*  -  ADDED DEPENDENT MEDICAL COVERAGE END DATE (EDB 0659),  */   54541281
000006*           DEPENDENT DENTAL  COVERAGE END DATE (EDB 0656),  */   54541281
000007*           DEPENDENT VISION  COVERAGE END DATE (EDB 0657),  */   54541281
000008*           DEPENDENT LEGAL   COVERAGE END DATE (EDB 0658),  */   54541281
000009*  -  REPLACED DEPENDENT INSURANCE DE-ENROLLMENT INDICATOR   */   54541281
000009*     WITH FILLER FIELD.                                     */   54541281
000010**************************************************************/   54541281
000000**************************************************************/   16990906
000001*  COPYMEMBER: CPWSRDEP                                      */   16990906
000002*  RELEASE: ___0906______ SERVICE REQUEST(S): ____11699____  */   16990906
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___05/18/94__  */   16990906
000004*  DESCRIPTION:                                              */   16990906
000005*  -  ADDED DEP-INS-DEENROLL                                 */   16990906
000006*                                                            */   16990906
000007**************************************************************/   16990906
000000**************************************************************/   36260760
000001*  COPYMEMBER: CPWSRDEP                                      */   36260760
000002*  RELEASE: ___0760______ SERVICE REQUEST(S): _____3626____  */   36260760
000003*  NAME:__PKD____________ CREATION DATE:      ___04/30/93__  */   36260760
000004*  DESCRIPTION:                                              */   36260760
000005*  - WORKING-STORAGE LAYOUT OF DEP TABLE ROW.                */   36260760
000007*                                                            */   36260760
000008**************************************************************/   36260760
004200*    COPYID=CPWSRDEP                                              CPWSRDEP
004300*01  DEP-ROW.                                                     CPWSRDEP
004301     10 EMPLOYEE-ID          PIC X(9).                            CPWSRDEP
004302     10 DEP-NUM              PIC X(2).                            CPWSRDEP
004303     10 DEP-ADC-CODE         PIC X(1).                            CPWSRDEP
004304     10 DEP-NAME             PIC X(26).                           CPWSRDEP
004305     10 DEP-BIRTH-DATE       PIC X(10).                           CPWSRDEP
004306     10 DEP-REL-TO-EMP       PIC X(1).                            CPWSRDEP
004307     10 DEP-SSN              PIC X(9).                            CPWSRDEP
004308     10 DEP-SEX-CODE         PIC X(1).                            CPWSRDEP
004309     10 DEP-DISABLED-CODE    PIC X(1).                            CPWSRDEP
004310     10 DEP-HLTH-COVEFFDT    PIC X(10).                           CPWSRDEP
004311     10 DEP-DENTL-COVEFFDT   PIC X(10).                           CPWSRDEP
004312     10 DEP-VIS-COVEFFDT     PIC X(10).                           CPWSRDEP
004313     10 DEP-LEGAL-COVEFFDT   PIC X(10).                           CPWSRDEP
004314*****10 DEP-INS-DEENROLL     PIC X(1).                            54541281
004314     10 DEP-FILLER           PIC X(1).                            54541281
004315     10 DEP-HLTH-COVENDDT    PIC X(10).                           54541281
004316     10 DEP-DENTL-COVENDDT   PIC X(10).                           54541281
004317     10 DEP-VIS-COVENDDT     PIC X(10).                           54541281
004318     10 DEP-LEGAL-COVENDDT   PIC X(10).                           54541281
