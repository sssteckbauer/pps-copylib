000010*==========================================================%      DS795
000020*=    COPY MEMBER: CLPDP340                               =%      DS795
000030*=    CHANGE # DS795        PROJ. REQUEST: LE/370         =%      DS795
000040*=                                         CONVERSION     =%      DS795
000050*=    NAME: R. SWIDERSKI    MODIFICATION DATE: 09/01/93   =%      DS795
000060*=                                                        =%      DS795
000070*=    DESCRIPTION:                                        =%      DS795
000080*=     REPLACED TIME 'ACCEPT' STATEMENT WITH              =%      DS795
000090*=     LE/370 VERSION OF CPPDTIME                         =%      DS795
000091*==========================================================%      DS795
000100*                                                                 CLPDP340
000200*      CONTROL REPORT PPP340CR                                    CLPDP340
000300*                                                                 CLPDP340
000400*                                                                 CLPDP340
000500 PRINT-CONTROL-REPORT SECTION.                                    CLPDP340
000600                                                                  CLPDP340
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP340
000800                                                                  CLPDP340
000900     MOVE 'PPP340CR'             TO CR-HL1-RPT.                   CLPDP340
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP340
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP340
001200                                                                  CLPDP340
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP340
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP340
001500                                                                  CLPDP340
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP340
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP340
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP340
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP340
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP340
002100                                                                  CLPDP340
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP340
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP340
002400                                                                  CLPDP340
002500     MOVE SPEC-CARD-RCD          TO CR-DL5-CTL-CARD.              CLPDP340
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP340
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP340
002800                                                                  CLPDP340
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP340
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP340
003100                                                                  CLPDP340
003110*****ACCEPT TIME-WORK-AREA FROM TIME.                             DS795
003120     COPY CPPDTIME.                                               DS795
003200     MOVE PRE-EDIT-TIME          TO TIME-WORK-AREA.               DS795
003300     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP340
003400     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP340
003500     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP340
003600     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP340
003700                                                                  CLPDP340
003800     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP340
003900     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP340
004000*                                                                 CLPDP340
004100*  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP340
004200*                                                                 CLPDP340
004300*  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP340
004400*  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP340
004500*                                                                 CLPDP340
004600*                                                                 CLPDP340
004700*                                                                 CLPDP340
004800     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP340
004900     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP340
005000                                                                  CLPDP340
005100     CLOSE CONTROLREPORT.                                         CLPDP340
