000000**************************************************************/   16110865
000001*  COPYMEMBER: CPWSWGTN                                      */   16110865
000002*  RELEASE: ___0865______ SERVICE REQUEST(S): ____11611____  */   16110865
000003*  NAME:_______QUAN______ CREATION DATE:      ___12/14/93__  */   16110865
000004*  DESCRIPTION:                                              */   16110865
000006*  NEW COPYMEMBER FOR TRACKING GTN NUMBERS USED IN SCREEN    */   16110865
000007*  PROGRAMS.                                                 */   16110865
000009**************************************************************/   16110865
000424*01  CPWSWGTN EXTERNAL.                                           CPWSWGTN
013100     03  CPWSWGTN-GTN-ARRAY.                                      CPWSWGTN
013210         05  CPWSWGTN-USED-GTN   OCCURS 999 TIMES   PIC X.        CPWSWGTN
013300             88  CPWSWGTN-GTN-USED  VALUE 'Y'.                    CPWSWGTN
013500                                                                  CPWSWGTN
013508**************************************************************/   CPWSWGTN
013510***************    END OF SOURCE - CPWSWGTN    *******************CPWSWGTN
