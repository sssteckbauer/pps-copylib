000100******************************************************************36090513
000200*  COPYMEMBER: CPLNKDNT                                          *36090513
000300*  RELEASE: ____0513____  SERVICE REQUEST(S): ____3609____       *36090513
000400*  NAME:______JAG_______  CREATION DATE:      __11/05/90__       *36090513
000500*  DESCRIPTION:                                                  *36090513
000600*    CONTROL LINKAGE DEFINITION FOR PPEDBDET.                    *36090513
000700******************************************************************36090513
000800*    COPYID=CPLNKDNT                                              CPLNKDNT
000900*01  PPEDBDET-INTERFACE.                                          CPLNKDNT
001000*                                                                *CPLNKDNT
001100******************************************************************CPLNKDNT
001200*    P P E D B D E T   I N T E R F A C E                         *CPLNKDNT
001300******************************************************************CPLNKDNT
001400*                                                                *CPLNKDNT
001500     05  PPEDBDET-ERROR-SWITCHES.                                 CPLNKDNT
001600         10  PPEDBDET-ERROR-SW       PIC X(01).                   CPLNKDNT
001700             88  PPEDBDET-ERROR                VALUE 'Y'.         CPLNKDNT
001800         10  PPEDBDET-D-W-ERR-SW     PIC X(01).                   CPLNKDNT
001900             88  PPEDBDET-D-E-ERROR            VALUE 'Y'.         CPLNKDNT
