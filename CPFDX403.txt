000000**************************************************************/   34791102
000001*  COPYMEMBER: CPFDX403                                      */   34791102
000002*  RELEASE: ___1102______ SERVICE REQUEST(S): ___13479_____  */   34791102
000003*  NAME:_____S.ISAACS____ MODIFICATION DATE:  ___12/10/96__  */   34791102
000004*  DESCRIPTION:                                              */   34791102
000005*      - NEW FD FOR BENEFITS 403B INTERFACE RECORD           */   34791102
000007**************************************************************/   34791102
004510*                                                                 CPFDX403
004600*    COPYID=CPFDX403                                              CPFDX403
004700*                                                                 CPFDX403
004800     LABEL RECORDS ARE STANDARD                                   CPFDX403
004900     RECORDING MODE IS F                                          CPFDX403
005000     BLOCK CONTAINS 0 RECORDS                                     CPFDX403
005300     RECORD CONTAINS 63  CHARACTERS                               CPFDX403
005400     DATA RECORD IS B403-EMPLOYEE-REC.                            CPFDX403
005500     SKIP1                                                        CPFDX403
005800 01  B403-EMPLOYEE-REC               PIC X(63).                   CPFDX403
