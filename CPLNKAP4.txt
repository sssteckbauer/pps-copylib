000100**************************************************************/   17040930
000200*  COPYMEMBER: CPLNKAP4                                      */   17040930
000300*  RELEASE: ___0930______ SERVICE REQUEST(S): _____1704____  */   17040930
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___09/08/94__  */   17040930
000500*  DESCRIPTION:                                              */   17040930
000600*  - NEW COPYMEMBER DESCRIBING THE LINKAGE BETWEEN CALLING   */   17040930
000700*    PROGRAMS AND PROGRAM PPAPTLAC.                   .      */   17040930
000800**************************************************************/   17040930
000900******************************************************************CPLNKAP4
001000*                                                                *CPLNKAP4
001100*  THIS COPYMEMBER IDENTIFIES THE INTERFACE FIELDS USED BETWEEN  *CPLNKAP4
001200* A CALLING PROGRAM (E.G. PPEM005)        AND THE APPOINTMENT    *CPLNKAP4
001300* DEPARTMENT CODE DERIVATION ROUTINE PPAPTLAC.                   *CPLNKAP4
001400*  THIS INTERFACE COMMUNICATES INPUT DATA TO AND RESULTS         *CPLNKAP4
001500* FROM PPAPTLAC.   INPUT DATA CONSIST OF THE TOTAL EMPLOYEE      *CPLNKAP4
001600* SERVICE CREDITS (THE EMPLOYMENT SERVICE CREDIT (DE 0719) PLUS  *CPLNKAP4
001700* PRIOR MONTHS SERVICE (DE 0146) AND IS COMPUTED BY THE          *CPLNKAP4
001800* CALLING PROGRAM FOR EACH EMPLOYEE.  THE OUTPUT                 *CPLNKAP4
001900* 'RESULTS' CONSIST OF TWO TABLES WHICH MAY, DEPENDING ON THE    *CPLNKAP4
002000* CALLING PROGRAM, BE USED OR IGNORED. THE TWO OUTPUT TABLES     *CPLNKAP4
002100* ARE THE 'AUDIT' TABLE, IDENTIFYING THOSE ELEMENTS WHICH        *CPLNKAP4
002200* HAVE BEEN CHANGED AS A RESULT OF PPAPTLAC CODE DERIVATION,     *CPLNKAP4
002300* AND THE 'ERROR' TABLE, IDENTIFYING ALL ERRORS ENCOUNTERED      *CPLNKAP4
002400* DURING CODE DERIVATION.                                        *CPLNKAP4
002500******************************************************************CPLNKAP4
002600                                                                  CPLNKAP4
002700*01  PPAPTLAC-INTERFACE.                                          CPLNKAP4
002800   03  PPAPTLAC-MAIN-DATA.                                        CPLNKAP4
002900     05  AP04-TOTAL-ESC           PIC S9(04) COMP.                CPLNKAP4
003000     05  AP03-DELETED-DIST-SWITCH PIC X OCCURS 9 TIMES.           CPLNKAP4
003100******************************************************************CPLNKAP4
003200*                                                                *CPLNKAP4
003300* MESSAGE LITERALS:                                              *CPLNKAP4
003400*                                                                *CPLNKAP4
003500*  THE MODULE CALLING PPAPTLAC MUST PROVIDE A LIST OF ERROR      *CPLNKAP4
003600* MESSAGE LITERALS FOR ASSIGNMENT BY PPCB01. MESSAGE LITERALS    *CPLNKAP4
003700* MUST CORRESPOND, PRECISELY, TO THE ERROR CONDITION REPRESENTED *CPLNKAP4
003800* BY THE POSITION WITHIN THE LIST, AS IDENTIFIED BELOW:          *CPLNKAP4
003900*                                                                *CPLNKAP4
004000*                                                                *CPLNKAP4
004100*  (NOTE: IF ANY ERROR MESSAGE LITERAL IS ZERO, THEN THE         *CPLNKAP4
004200*   NO MESSAGE WILL BE POSTED BY PPAPTLAC).                      *CPLNKAP4
004300*                                                                *CPLNKAP4
004400*                                                                *CPLNKAP4
004500* POSITION    ERROR CONDITION                                    *CPLNKAP4
004600* --------    ------------------------------------------------   *CPLNKAP4
004700*                                                                *CPLNKAP4
004800*    1        PPAPTLAC-INTERFACE AUDIT-TABLE OVERFLOW            *CPLNKAP4
004900*    2        PPAPTLAC-INTERFACE ERROR-TABLE-OVERFLOW            *CPLNKAP4
005000*    3        SYSTEM DERIVED APPOINTMENT PAY RATE DIFFERS        *CPLNKAP4
005100*             FROM INPUT; INPUT OVERRIDDEN.                      *CPLNKAP4
005200******************************************************************CPLNKAP4
005300                                                                  CPLNKAP4
005400                                                                  CPLNKAP4
005500   03  PPAPTLAC-OTHER-DATA.                                       CPLNKAP4
005600     05  PPAPTLAC-MESSAGE-LITERALS.                               CPLNKAP4
005700         10  PPAPTLAC-MESSAGE-LITERAL OCCURS 03 TIMES PIC 9(05).  CPLNKAP4
005800                                                                  CPLNKAP4
005900                                                                  CPLNKAP4
006000******************************************************************CPLNKAP4
006100* THE AUDIT TABLE COMMUNICATES, TO THE CALLING PROGRAM, A LIST   *CPLNKAP4
006200* OF DATA ELEMENTS CHANGED AS A RESULT OF PPAP04 CODE DERIVATION.*CPLNKAP4
006300* THIS LIST MAY BE USED BY THE CALLING PROGRAM TO POST DATA      *CPLNKAP4
006400* ELEMENTS CHANGES TO AN AUDIT FILE. THE NUMBER OF ENTRIES       *CPLNKAP4
006500* IN THE TABLE IS IDENTIFIED BY AUDIT-TABLE-SIZE, WHICH IS       *CPLNKAP4
006600* SET BY PPAPTLAC. AUDIT-TABLE-PTR IS A UTILITY INDEX WHICH      *CPLNKAP4
006700* MAY BE USED BY THE CALLING PROGRAM.                            *CPLNKAP4
006800******************************************************************CPLNKAP4
006900                                                                  CPLNKAP4
007000     05  AP04-AUDIT-TABLE-PTR          PIC 9(03).                 CPLNKAP4
007100     05  AP04-AUDIT-TABLE-SIZE         PIC 9(03).                 CPLNKAP4
007200     05  AP04-AUDIT-TABLE-ENTRY  OCCURS  9    TIMES.              CPLNKAP4
007300         10  AP04-AUDIT-FIELD          PIC 9(04).                 CPLNKAP4
007400         10  AP04-AUDIT-LENGTH         PIC 9(02).                 CPLNKAP4
007500         10  AP04-AUDIT-APPT           PIC 9(02).                 CPLNKAP4
007600                                                                  CPLNKAP4
007700                                                                  CPLNKAP4
007800******************************************************************CPLNKAP4
007900*  THE ERROR TABLE COMMUNICATES, TO THE CALLING PROGRAM, A LIST  *CPLNKAP4
008000* OF ERRORS ENCOUNTERED BY PPAPTLAC FOR AN EMPLOYEE RECORD. THIS *CPLNKAP4
008100* LIST MAY BE USED BY THE CALLING PROGRAM TO POST ERROR MESSAGES *CPLNKAP4
008200* TO AN ERROR REPORT OR FILE. THE NUMBER OF ENTRIES IN THE TABLE *CPLNKAP4
008300* IS IDENTIFIED BY ERROR-TABLE-SIZE, WHICH IS SET BY PPAPTLAC.   *CPLNKAP4
008400* ERROR-TABLE-PTR IS A UTILITY INDEX WHICH MAY BE USED BY THE    *CPLNKAP4
008500* CALLING PROGRAM.                                               *CPLNKAP4
008600******************************************************************CPLNKAP4
008700                                                                  CPLNKAP4
008800                                                                  CPLNKAP4
008900     05  AP04-ERROR-TABLE-PTR          PIC 9(03).                 CPLNKAP4
009000     05  AP04-ERROR-TABLE-SIZE         PIC 9(03).                 CPLNKAP4
009100     05  AP04-ERROR-TABLE-ENTRY  OCCURS 10  TIMES.                CPLNKAP4
009200         10  AP04-ERROR-TABLE-NO       PIC 9(05).                 CPLNKAP4
009300         10  AP04-ERROR-TABLE-FIELD    PIC 9(04).                 CPLNKAP4
009400                                                                  CPLNKAP4
009500**********************   END OF CPWSXAP4   ***********************CPLNKAP4
