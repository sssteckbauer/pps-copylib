000000**************************************************************/   32021138
000001*  COPYMEMBER: CPLNKHCU                                      */   32021138
000002*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000003*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000004*  DESCRIPTION:                                              */   32021138
000005*  - MODIFIED FOR FULL ACCOUNTING UNIT                       */   32021138
000007**************************************************************/   32021138
000100**************************************************************/   40280572
000200*  COPYMEMBER: CPLNKHCU                                      */   40280572
000300*  RELEASE: ___0572______ SERVICE REQUEST(S): _____4028____  */   40280572
000400*  NAME:_____J.WILCOX____ CREATION DATE:      ___05/29/91__  */   40280572
000500*  DESCRIPTION:                                              */   40280572
000600*  - NEW LINKAGE MEMBER BETWEEN CALLING PROGRAMS AND MODULE  */   40280572
000700*    PPHCUUTL.  SEE THAT MODULE FOR A DESCRIPTION OF FIELD   */   40280572
000800*    VALUES AND THEIR INTERPRETATION.                        */   40280572
000900**************************************************************/   40280572
001000*COPYID=CPLNKHCU                                                 *CPLNKHCU
001100******************************************************************CPLNKHCU
001200*                        C P L N K H C U                         *CPLNKHCU
001300******************************************************************CPLNKHCU
001400*01  PPHCUUTL-INTERFACE.                                          CPLNKHCU
001500     05  KHCU-INPUT-VALUES.                                       CPLNKHCU
001600*****    10  KHCU-ACCT-DEPT            PIC X(06).                 32021138
001600         10  KHCU-FAU-DEPT             PIC X(30).                 32021138
001700     05  KHCU-OUTPUT-VALUES.                                      CPLNKHCU
001800         10  KHCU-CU-ID                PIC X(06).                 CPLNKHCU
001900         10  KHCU-CU-NAME              PIC X(30).                 CPLNKHCU
002000         10  KHCU-CU-PERCENTAGE        PIC S99V99  COMP-3.        CPLNKHCU
002100     05  KHCU-CONTROL-VALUES.                                     CPLNKHCU
002200         10  KHCU-RETURN-CODE          PIC 9(02).                 CPLNKHCU
002300             88  KHCU-NORMAL-RETURN      VALUE ZEROES.            CPLNKHCU
002400             88  KHCU-UNKNOWN-UNIT       VALUE 01.                CPLNKHCU
002500             88  KHCU-NO-HCU-DELIMITER   VALUE 91.                CPLNKHCU
002600             88  KHCU-EMPTY-HCU-TABLE    VALUE 92.                CPLNKHCU
002700             88  KHCU-CTL-FILE-ERROR     VALUE 98.                CPLNKHCU
002800             88  KHCU-PRIOR-ABEND        VALUE 99.                CPLNKHCU
002900         10  KHCU-REF                  PIC X(80).                 CPLNKHCU
