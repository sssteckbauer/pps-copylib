000100**************************************************************/   36190541
000200*  COPYMEMBER: CPPDSCP2                                      */   36190541
000300*  RELEASE: ____0541____  SERVICE REQUEST(S): ____3619____   */   36190541
000400*  NAME:    _C._RIDLION_  CREATION DATE:      __02/28/91__   */   36190541
000500*  DESCRIPTION:                                              */   36190541
000600*    - NEW FOR EDB ONLINE INQUIRY                            */   36190541
000700**************************************************************/   36190541
000800                                                                  CPPDSCP2
000900 9097-SET-HDRS-LABELS.                                            CPPDSCP2
001000****************************************************************  CPPDSCP2
001100* THIS PARAGRAPH SETS THE HEADING INFO ON THE TOP OF ALL DETAIL*  CPPDSCP2
001200* SCREENS, SETS THE APPROPRIATE PF-KEY LABELS AND IF THE       *  CPPDSCP2
001300* CURRENT FUNCTION IS IN THE USERS SCRIPT ALSO SETS UP THE     *  CPPDSCP2
001400* NEXT FUNCTION MESSAGE                                        *  CPPDSCP2
001500****************************************************************  CPPDSCP2
001600     PERFORM 9087-SET-DATE-TIME.                                  CPPDSCP2
001700     MOVE WS-DATE TO DATEO.                                       CPPDSCP2
001800     MOVE WS-TIME TO TIMEO.                                       CPPDSCP2
001900                                                                  CPPDSCP2
002000     MOVE WS-PF01 TO PF01O.                                       CPPDSCP2
002100     MOVE WS-PF03 TO PF03O.                                       CPPDSCP2
002200     MOVE WS-PF09 TO PF09O.                                       CPPDSCP2
002300     MOVE WS-PF12 TO PF12O.                                       CPPDSCP2
002400                                                                  CPPDSCP2
002500     IF CA-PRINT-IND = 'Y'                                        CPPDSCP2
002600        MOVE WS-PF04 TO PF04O                                     CPPDSCP2
002700     END-IF.                                                      CPPDSCP2
002800                                                                  CPPDSCP2
002900     IF CA-SCROLL = 'Y'                                           CPPDSCP2
003000        MOVE WS-PF07 TO PF07O                                     CPPDSCP2
003100        MOVE WS-PF08 TO PF08O                                     CPPDSCP2
003200     ELSE                                                         CPPDSCP2
003300     IF CA-SCROLL = 'F'                                           CPPDSCP2
003400        MOVE SPACES  TO PF07O                                     CPPDSCP2
003500        MOVE WS-PF08 TO PF08O                                     CPPDSCP2
003600     ELSE                                                         CPPDSCP2
003700     IF CA-SCROLL = 'B'                                           CPPDSCP2
003800        MOVE WS-PF07 TO PF07O                                     CPPDSCP2
003900        MOVE SPACES  TO PF08O                                     CPPDSCP2
004000     ELSE                                                         CPPDSCP2
004100        MOVE SPACES  TO PF07O                                     CPPDSCP2
004200        MOVE SPACES  TO PF08O.                                    CPPDSCP2
004300                                                                  CPPDSCP2
004400     MOVE CA-USERID TO USERIDO.                                   CPPDSCP2
004500     MOVE CA-UNIQUE-EE-ID TO IDO.                                 CPPDSCP2
004600     MOVE CA-EMP-NAME     TO NAMEO.                               CPPDSCP2
004700     MOVE CA-SOC-SEC-NUM  TO SSNO.                                CPPDSCP2
004800     MOVE CA-HOME-DEPT    TO HOME-DEPTO.                          CPPDSCP2
004900     MOVE CA-DEPT-SHORT-NAME TO HOME-DEPT-NAME-SHORTO.            CPPDSCP2
005000     MOVE CA-EMP-STATUS   TO EMPLMT-STATUSO.                      CPPDSCP2
005100     MOVE CA-LAST-CHG-DATE TO LAST-CHG-DATEO.                     CPPDSCP2
005200     MOVE CA-LAST-CHG-TIME TO LAST-CHG-TIMEO.                     CPPDSCP2
005300                                                                  CPPDSCP2
005400     IF CA-SCRIPT-POSN  = ZERO                                    CPPDSCP2
005500        MOVE SPACES TO PF10O PF11O                                CPPDSCP2
005600     ELSE                                                         CPPDSCP2
005700        MOVE WS-PF10  TO PF10O                                    CPPDSCP2
005800        MOVE WS-PF11  TO PF11O                                    CPPDSCP2
005900                                                                  CPPDSCP2
006000        IF CA-MESSAGE-NO = SPACES OR LOW-VALUES OR MP0008         CPPDSCP2
006100                                                                  CPPDSCP2
006200           MOVE MP0008 TO CA-MESSAGE-NO WS-MESSAGE-NO             CPPDSCP2
006300           PERFORM 8999-SELECT-MSG                                CPPDSCP2
006400                                                                  CPPDSCP2
006500           IF SQLCODE = ZERO                                      CPPDSCP2
006600              MOVE CA-SCRIPT-POSN TO WS-SCRIPT-POSN               CPPDSCP2
006700              ADD +1 TO WS-SCRIPT-POSN                            CPPDSCP2
006800                                                                  CPPDSCP2
006900              IF WS-SCRIPT-POSN > CA-SCRIPT-MAX                   CPPDSCP2
007000                 MOVE +1 TO WS-SCRIPT-POSN                        CPPDSCP2
007100              END-IF                                              CPPDSCP2
007200                                                                  CPPDSCP2
007300              MOVE CA-SCRIPT-SEQ (WS-SCRIPT-POSN) TO              CPPDSCP2
007400              WS-MESSAGE-FUNC                                     CPPDSCP2
007500              MOVE WS-MESSAGE-LINE TO MESSAGEO                    CPPDSCP2
007600           END-IF                                                 CPPDSCP2
007700        END-IF                                                    CPPDSCP2
007800     END-IF.                                                      CPPDSCP2
007900                                                                  CPPDSCP2
008000                                                                  CPPDSCP2
008100 9098-PF10.                                                       CPPDSCP2
008200****************************************************************  CPPDSCP2
008300* THIS PARAGRAPH PROCESSES THE PF10 FUNCTION. THAT IS IT       *  CPPDSCP2
008400* DETERMINES THE PREVIOUS FUNCTION IN THE SCRIPT SEQUENCE AND  *  CPPDSCP2
008500* DOES AN XCTL TO ITS ASSOCIATED PROGRAM.   IF THE CURRENT     *  CPPDSCP2
008600* FUNCTION IS THE FIRST ON THE SCRIPT THEN PF10 WILL XCTL TO   *  CPPDSCP2
008700* THE LAST  FUNCTION IN THE SCRIPT.                            *  CPPDSCP2
008800****************************************************************  CPPDSCP2
008900                                                                  CPPDSCP2
009000     IF CA-SCRIPT-POSN NOT = ZERO                                 CPPDSCP2
009100        SUBTRACT 1 FROM CA-SCRIPT-POSN                            CPPDSCP2
009200                                                                  CPPDSCP2
009300        IF CA-SCRIPT-POSN < 1                                     CPPDSCP2
009400           MOVE CA-SCRIPT-MAX TO CA-SCRIPT-POSN                   CPPDSCP2
009500        END-IF                                                    CPPDSCP2
009600                                                                  CPPDSCP2
009700        MOVE CA-SCRIPT-SEQ (CA-SCRIPT-POSN) TO                    CPPDSCP2
009800             CA-NEXT-FUNCTION                                     CPPDSCP2
009900                                                                  CPPDSCP2
010000        PERFORM 9095-CALL-PPFCVALD                                CPPDSCP2
010100                                                                  CPPDSCP2
010200        IF CA-PPFCVALD-RETURN-CODE NOT = ZERO                     CPPDSCP2
010300           MOVE SPACES TO CA-NEXT-FUNCTION                        CPPDSCP2
010400        ELSE                                                      CPPDSCP2
010500           PERFORM 9092-XCTL                                      CPPDSCP2
010600        END-IF                                                    CPPDSCP2
010700     ELSE                                                         CPPDSCP2
010800        MOVE MP0004  TO CA-MESSAGE-NO WS-MESSAGE-NO               CPPDSCP2
010900     END-IF.                                                      CPPDSCP2
011000                                                                  CPPDSCP2
011100 9099-PF11.                                                       CPPDSCP2
011200****************************************************************  CPPDSCP2
011300* THIS PARAGRAPH PROCESSES THE PF11 FUNCTION. THAT IS IT       *  CPPDSCP2
011400* DETERMINES THE NEXT FUNCTION IN THE SCRIPT SEQUENCE AND      *  CPPDSCP2
011500* DOES AN XCTL TO ITS ASSOCIATED PROGRAM.   IF THE CURRENT     *  CPPDSCP2
011600* FUNCTION IS THE LAST ON THE SCRIPT THEN PF11 WILL XCTL TO    *  CPPDSCP2
011700* THE FIRST FUNCTION IN THE SCRIPT.                            *  CPPDSCP2
011800****************************************************************  CPPDSCP2
011900                                                                  CPPDSCP2
012000     IF CA-SCRIPT-POSN NOT = ZERO                                 CPPDSCP2
012100        ADD  +1 TO  CA-SCRIPT-POSN                                CPPDSCP2
012200                                                                  CPPDSCP2
012300        IF CA-SCRIPT-POSN > CA-SCRIPT-MAX                         CPPDSCP2
012400           MOVE +1  TO CA-SCRIPT-POSN                             CPPDSCP2
012500        END-IF                                                    CPPDSCP2
012600                                                                  CPPDSCP2
012700        MOVE CA-SCRIPT-SEQ (CA-SCRIPT-POSN) TO                    CPPDSCP2
012800             CA-NEXT-FUNCTION                                     CPPDSCP2
012900                                                                  CPPDSCP2
013000        PERFORM 9095-CALL-PPFCVALD                                CPPDSCP2
013100                                                                  CPPDSCP2
013200        IF CA-PPFCVALD-RETURN-CODE NOT = ZERO                     CPPDSCP2
013300           MOVE SPACES TO CA-NEXT-FUNCTION                        CPPDSCP2
013400        ELSE                                                      CPPDSCP2
013500           PERFORM 9092-XCTL                                      CPPDSCP2
013600        END-IF                                                    CPPDSCP2
013700     ELSE                                                         CPPDSCP2
013800        MOVE MP0004  TO CA-MESSAGE-NO WS-MESSAGE-NO               CPPDSCP2
013900     END-IF.                                                      CPPDSCP2
014000                                                                  CPPDSCP2
