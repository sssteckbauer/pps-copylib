000100*==========================================================%      DS1180
000200*=    COPY MEMBER: CLPDP560                               =%      DS1180
000300*=    CHANGE # DS1180       PROJ. REQUEST: RELEASE 1180   =%      DS1180
000500*=    NAME: G. CHIU         MODIFICATION DATE: 04/10/98   =%      DS1180
000600*=                                                        =%      DS1180
000700*=    DESCRIPTION:                                        =%      DS1180
000800*=     TRAILER RECORD WAS ADDED.  ADD 2 TO RECORD COUNT   =%      DS1180
001000*==========================================================%      DS1180
000100*==========================================================%      DS795
000200*=    COPY MEMBER: CLPDP560                               =%      DS795
000300*=    CHANGE # DS795        PROJ. REQUEST: LE/370         =%      DS795
000400*=                                         CONVERSION     =%      DS795
000500*=    NAME: G. CHIU         MODIFICATION DATE: 03/22/94   =%      DS795
000600*=                                                        =%      DS795
000700*=    DESCRIPTION:                                        =%      DS795
000800*=     REPLACED TIME 'ACCEPT' STATEMENT WITH              =%      DS795
000900*=     LE/370 VERSION OF CPPDTIME                         =%      DS795
001000*==========================================================%      DS795
000100*                                                                 CLPDP560
000200*   CONTROL REPORT PPP560CR                                       CLPDP560
000300*                                                                 CLPDP560
000400*                                                                 CLPDP560
000500 PRINT-CONTROL-REPORT SECTION.                                    CLPDP560
000600                                                                  CLPDP560
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP560
000800                                                                  CLPDP560
000900     MOVE 'PPP560CR'             TO CR-HL1-RPT.                   CLPDP560
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP560
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP560
001200                                                                  CLPDP560
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP560
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP560
001500                                                                  CLPDP560
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP560
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP560
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP560
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP560
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP560
002100                                                                  CLPDP560
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP560
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP560
002400                                                                  CLPDP560
002500     MOVE WQ-SPEC-OR-TLP-UPDT-REC TO CR-DL5-CTL-CARD.             CLPDP560
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP560
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP560
002800                                                                  CLPDP560
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP560
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP560
003100                                                                  CLPDP560
004200*****ACCEPT TIME-WORK-AREA FROM TIME.                             DS795
004300     COPY CPPDTIME.                                               DS795
004400     MOVE PRE-EDIT-TIME          TO TIME-WORK-AREA.               DS795
003200*    MOVE TIME-OF-DAY            TO TIME-WORK-AREA.               CLPDP560
003300     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP560
003400     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP560
003500     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP560
003600     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP560
003700                                                                  CLPDP560
003800     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP560
003900     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP560
004000                                                                  CLPDP560
004100     MOVE 'FNLPAR.PREV'          TO CR-DL9-FILE.                  CLPDP560
004200     MOVE SO-READ                TO CR-DL9-ACTION.                CLPDP560
004300     MOVE WG-PAR-CT              TO CR-DL9-VALUE.                 CLPDP560
004400     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP560
004500     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP560
004600                                                                  CLPDP560
004700     MOVE 'DEDUCT  '             TO CR-DL9-FILE.                  CLPDP560
004800     MOVE SO-CREATED             TO CR-DL9-ACTION.                CLPDP560
004900*****ADD +1                      TO WG-DEDUCT-CNT.                DS1180
004900     ADD +2                      TO WG-DEDUCT-CNT.                DS1180
005000     MOVE WG-DEDUCT-CNT          TO CR-DL9-VALUE.                 CLPDP560
005100     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP560
005200     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP560
005300                                                                  CLPDP560
005400     MOVE 'TLPDA   '             TO CR-DL9-FILE.                  CLPDP560
005500     MOVE SO-CREATED             TO CR-DL9-ACTION.                CLPDP560
005600     MOVE WK-TLP-ADJ-REC-CNT     TO CR-DL9-VALUE.                 CLPDP560
005700     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP560
005800     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP560
005900                                                                  CLPDP560
006000     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP560
006100     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP560
006200                                                                  CLPDP560
006300     CLOSE CONTROLREPORT.                                         CLPDP560
006400                                                                  CLPDP560
