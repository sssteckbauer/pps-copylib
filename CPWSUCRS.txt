000000**************************************************************/   06941549
000001*  COPYMEMBER: CPWSUCRS                                      */   06941549
000002*  RELEASE: ___1549______ SERVICE REQUEST(S): ____80694____  */   06941549
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___01/12/04__  */   06941549
000004*  DESCRIPTION:                                              */   06941549
000005*  - ADDED BELI (EDB 0360) AND DEPT LOCATION IND TO FIXED    */   06941549
000006*    SEGMENT OF UCRS TRANSACTION FILE.                       */   06941549
000007**************************************************************/   06941549
000000**************************************************************/   03861495
000001*  COPYMEMBER: CPWSUCRS                                      */   03861495
000002*  RELEASE: ___1495______ SERVICE REQUEST(S): ____80386____  */   03861495
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___05/03/03__  */   03861495
000004*  DESCRIPTION:                                              */   03861495
000005*  - ADDED BUDGET-INITIATIVE-FLAG FIELD TO IDENTIFY SERVICE  */   03861495
000006*    CREDIT SEGMENTS RELATED TO START.                       */   03861495
000007**************************************************************/   03861495
000000**************************************************************/   02821477
000001*  COPYMEMBER: CPWSUCRS                                      */   02821477
000002*  RELEASE: ___1477______ SERVICE REQUEST(S): ____80282____  */   02821477
000003*  NAME:_____S.ISAACS____ MODIFICATION DATE:  ___12/20/02__  */   02821477
000004*  DESCRIPTION:                                              */   02821477
000005*  - ADDED TWO NEW FIELDS TO UCRS-CONTRIBUTION-DATA;         */   02821477
000006*     UCRS-EARN-COVERAGE-IND AND UCRS-TITLE-UNIT-CODE.       */   02821477
000009**************************************************************/   02821477
000000**************************************************************/   49801227
000001*  COPYMEMBER: CPWSUCRS                                      */   49801227
000002*  RELEASE: ___1227______ SERVICE REQUEST(S): ____14980____  */   49801227
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___01/15/99__  */   49801227
000004*  DESCRIPTION:                                              */   49801227
000005*  - A NEW SEGMENT TYPE 'T' HAS BEEN ADDED TO THE RECORD.    */   49801227
000006*    THE NUMBER OF 'T' SEGMENTS COUNT HAS BEEN ADDED TO THE  */   49801227
000007*    FIXED PORTION. A SECOND TRAILER RECORD HAS BEEN ADDED   */   49801227
000008*    TO REPORT ON TOTALS FOR THE 'T' SEGMENTS.               */   49801227
000009**************************************************************/   49801227
000000**************************************************************/   49051194
000001*  COPYMEMBER: CPWSUCRS                                      */   49051194
000002*  RELEASE: ___1194______ SERVICE REQUEST(S): ____14905____  */   49051194
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___06/03/98__  */   49051194
000004*  DESCRIPTION:                                              */   49051194
000005*  - CONVERTED ALL DATES TO INCLUDE THE CENTURY. ADDED EDB   */   49051194
000006*    5502, 0481, 5501, AND 0767 TO FIXED PORTION.            */   49051194
000007*  - INCLUDED CENTURY (CC) PAY PERIOD END DATE IN SERVICE    */   49051194
000008*    CREDIT SEGMENT, CONTRIBUTION TAX YEAR IN CONTRIBUTION   */   49051194
000009*    SEGMENT, AND CNTL CYCLE DATE IN CONTROL REC.            */   49051194
000009*  - ALSO ADD 31 BYTES OF FILLER TO THE FIXED PORTION OF REC.*/   49051194
000010**************************************************************/   49051194
000000**************************************************************/   25791111
000001*  COPYMEMBER: CPWSUCRS                                      */   25791111
000002*  RELEASE: ___1111______ SERVICE REQUEST(S): ____12579____  */   25791111
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___01/16/97__  */   25791111
000004*  DESCRIPTION:                                              */   25791111
000005*  - ADDED GTN FIELD TO CONTRIBUTION SEGMENT.                */   25791111
000006*                                                            */   25791111
000007**************************************************************/   25791111
000000**************************************************************/   17690915
000001*  COPYMEMBER: CPWSUCRS                                      */   17690915
000002*  RELEASE: ___0915______ SERVICE REQUEST(S): 11769 & 11770  */   17690915
000003*  NAME:__H. TRUONG _____ MODIFICATION DATE:  ___08/01/94__  */   17690915
000004*  DESCRIPTION:                                              */   17690915
000005*  -  SR 11770                                               */   17690915
009600**       REMOVED UCRS-EMP-REL-CODE                           */   17690915
009700**               UCRS-EMP-UNIT-CODE                          */   17690915
009800**               UCRS-EMP-SPCL-HNDLG-CODE                    */   17690915
009900**               UCRS-EMP-DIST-UNIT-CODE                     */   17690915
010000**               UCRS-EMP-COVERAGE-IND.                      */   17690915
000005*  -  SR 11769                                               */   17690915
000005*        ADDED APUC AND MEDCOMP INDICATOR.                   */   17690915
000006*                                                            */   17690915
000007**************************************************************/   17690915
000000**************************************************************/   05530761
000001*  COPYMEMBER: CPWSUCRS                                      */   05530761
000002*  RELEASE: ___0761______ SERVICE REQUEST(S): _____10553___  */   05530761
000003*  NAME:_____J. QUAN_____ MODIFICATION DATE:  ___03/03/93__  */   05530761
000004*  DESCRIPTION:                                              */   05530761
000005*  - ADDED PARTIAL-YEAR-IND TO SERVICE ENTRY DATA OF UCRS    */   05530761
000005*    INTERFACE RECORD.                                       */   05530761
000007**************************************************************/   05530761
000000**************************************************************/   01990610
000001*  COPYMEMBER: CPWSUCRS                                      */   01990610
000002*  RELEASE: ___0610______ SERVICE REQUEST(S): ____10199____  */   01990610
000003*  NAME:___M. BAPTISTA___ MODIFICATION DATE:  ___10/15/91__  */   01990610
000004*  DESCRIPTION:                                              */   01990610
000005*  - ADDED INDIVIDUAL YTD CONTRIBUTION FOR EACH 403B AND     */   01990610
000006*    DCP PLAN.                                               */   01990610
000007**************************************************************/   01990610
000100**************************************************************/   01900588
000200*  COPYMEMBER: CPWSUCRS                                      */   01900588
000300*  RELEASE: ___0588______ SERVICE REQUEST(S): __1090,4029__  */   01900588
000400*  NAME:___M. BAPTISTA___ MODIFICATION DATE:  ___07/16/91__  */   01900588
000500*  DESCRIPTION:                                              */   01900588
000600*  - ADDED 6 NEW PLANS, DCP CASUAL, AND DROPPED TWO FROM     */   01900588
000700*    BEING PASSED, MED PLUS AND FIDELITY.  ALSO CHANGED      */   01900588
000800*    NUMBER OF PLANS OCCURS FROM BEING HARDCODED TO A        */   01900588
000900*    DEPENDING ON CLAUSE TO CONFORM WITH SAME CHANGE DONE    */   01900588
001000*    IN PPI730.                                              */   01900588
001100*  - SWAPPED UCRS-YTD-TOTAL-GRS FOR UCRS-403B-YTD-CNTRB FIELD./   40290588
001200*    NOTE THAT THIS CHANGE IN THIS COPYMEMBER IS PURELY FOR  */   40290588
001300*    COSMETIC REASONS AT THIS TIME BECAUSE PPI750 IS THE ONLY*/   40290588
001400*    PROGRAM THAT USES THIS COPYMEMBER AND THE THE FIELD     */   40290588
001500*    BEING SWAPPED IS NOT REFERRENCED IN THE PROGRAM.        */   40290588
001600**************************************************************/   01900588
000100**************************************************************/   40070515
000200*  COPYMEMBER: CPWSUCRS                                      */   40070515
000300*  RELEASE # ____0515____ SERVICE REQUEST NO(S)___4007_______*/   40070515
000400*  NAME _______KXK_____   MODIFICATION DATE ____10/22/90_____*/   40070515
000500*  DESCRIPTION                                               */   40070515
000600*   - ADDED SIX MORE OCCURENCES OF UCRS-CTL-CONTRIBUTION-    */   40070515
000700*     ARRAY TO REFLECT THE ADDITION OF SIX NEW DCP PLAN      */   40070515
000800*     CODES.                                                 */   40070515
000900**************************************************************/   40070515
000100**************************************************************/   37280477
000200*  COPYMEMBER: CPWSUCRS                                      */   37280477
000300*  RELEASE # ____0477____ SERVICE REQUEST NO(S)___3728_______*/   37280477
000400*  NAME __K.KELLER_____   MODIFICATION DATE ____05/03/90_____*/   37280477
000500*  DESCRIPTION                                               */   37280477
000600*   - DELETE PREVIOUS 'CD' LINES                             */   37280477
000700*   - DELETE CODE PREVIOUSLY COMMENTED-OUT AND MARK 'CD'     */   37280477
000800*   - CHANGE NUMBER OF OCCURRENCES TO REFLECT TWO NEW PLANS  */   37280477
000900*     AND DELETION OF ONE PLAN                               */   37280477
001000**************************************************************/   37280477
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSUCRS                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000901**************************************************************/   45330408
000902*  COPYMEMBER: CPWSUCRS                                      */   45330408
000903*  RELEASE # ____0408____ SERVICE REQUEST NO(S)____4533______*/   45330408
000904*  NAME ______TXT______   MODIFICATION DATE ____04/14/89_____*/   45330408
000905*  DESCRIPTION                                               */   45330408
000906*   - CHANGE TRAILER RECORD OCCURS TO 22.                    */   45330408
000907**************************************************************/   45330408
000908**************************************************************/   29250334
000909*  COPYMEMBER: CPWSUCRS                                      */   29250334
001000*  RELEASE # ____0334____ SERVICE REQUEST NO(S)____2925______*/   29250334
001100*  NAME ______JLT______   MODIFICATION DATE ____12/09/87_____*/   29250334
001200*  DESCRIPTION                                               */   29250334
001300*   - ADD YTD TOTAL GROSS TO FIXED PORTION OF RECORD.        */   29250334
001400*   - ADD PAY (CHECK DATE) YEAR TO CONTRIBUTION ENTRY AND    */   29250334
001500*     FIXED PORTION OF RECORD.                               */   29250334
001600*   - CHANGE TRAILER RECORD OCCURS TO 28 AS PER REL #309.    */   29250334
001700**************************************************************/   29250334
001800**************************************************************/   14410288
001900*  COPY MODULE: CPWSUCRS                                     */   14410288
002000*  RELEASE # ___0288___   SERVICE REQUEST NO __1441,1450_    */   14410288
002100*  NAME __K.KELLER_____   MODIFICATION DATE ____04/10/87____ */   14410288
002200*  DESCRIPTION -                                             */   14410288
002300*     INCREASE NUMBER OF PLAN CODES FROM 25 TO 29.           */   14410288
002400**************************************************************/   14060277
002500*  COPY MODULE: CPWSUCRS                                     */   14060277
002600*  RELEASE # ___0277___   SERVICE REQUEST NO __1406______    */   14060277
002700*  NAME __DBS__________   MODIFICATION DATE ____02/11/87____ */   14060277
002800*  DESCRIPTION -                                             */   14060277
002900*     THIS IS A NEW COPY MODULE.  IT DESCRIBES THE UCRS      */   14060277
003000*     CONTRIBUTION FILE THAT IS CREATED BY PPI730 AND READ   */   14060277
003100*     BY PPI750.                                             */   14060277
003200**************************************************************/   14060277
003300     SKIP3                                                        CPWSUCRS
003400*01  UCRS-DETAIL-RECORD.                                          30930413
003500*                                                                 CPWSUCRS
003600     05  UCRS-RECORD-CODE                 PIC X.                  CPWSUCRS
003700         88  UCRS-DET-RECORD  VALUE '1'.                          CPWSUCRS
003800     05  UCRS-KEY.                                                CPWSUCRS
003900         10  UCRS-SOCIAL.                                         CPWSUCRS
004000             15  UCRS-SS-POS-13           PIC X(3).               CPWSUCRS
004100             15  UCRS-SS-POS-45           PIC X(2).               CPWSUCRS
004200             15  UCRS-SS-POS-69           PIC X(4).               CPWSUCRS
004300         10  UCRS-LOCATION.                                       CPWSUCRS
004400             15  UCRS-LOC-POS1            PIC 9.                  CPWSUCRS
004500             15  UCRS-LOC-POS2            PIC 9.                  CPWSUCRS
004600         10  UCRS-DATE-PAID.                                      CPWSUCRS
010109             15  UCRS-CENTURY             PIC 99.                 49051194
004700             15  UCRS-YEAR                PIC 99.                 CPWSUCRS
004800             15  UCRS-MONTH               PIC 99.                 CPWSUCRS
004900             15  UCRS-DAY                 PIC 99.                 CPWSUCRS
005000     05  UCRS-NAME                        PIC X(26).              CPWSUCRS
005100     05  UCRS-EMPLOYEE-SUFFIX             PIC XXXX.               CPWSUCRS
005200     05  UCRS-EMPLOYEE-NUMBER             PIC 9(9).               CPWSUCRS
005300     05  UCRS-RETIREMENT-CODE             PIC X.                  CPWSUCRS
010000     05  UCRS-ACAD-PROG-UNIT-CD      PIC X(6)      VALUE SPACES.  17690915
010300     05  UCRS-403B-YTD-CNTRB         PIC 9(07)V99.                40290588
010500     05  UCRS-FIXED-PAY-CCYY.                                     49051194
010501         07  UCRS-FIXED-PAY-CENTURY  PIC 99.                      49051194
010502         07  UCRS-FIXED-PAY-YEAR     PIC 99.                      49051194
010520     05  UCRS-DEPT-LOC-IND           PIC X(01).                   06941549
010521     05  UCRS-BELI-IND               PIC X(01).                   06941549
010522*****05  FILLER-AVAIL                PIC X(63).                   06941549
010530     05  FILLER-AVAIL                PIC X(61).                   06941549
006400     05  UCRS-DETAIL-COUNT                PIC S9(6).              CPWSUCRS
006500     05  UCRS-SERVICE-COUNT               PIC S9999.              CPWSUCRS
006600     05  UCRS-CONTRIB-COUNT               PIC S9999.              CPWSUCRS
010810     05  UCRS-TAX-SEG-COUNT               PIC S9999.              49801227
006700     05  UCRS-DETAIL-ARRAY OCCURS 0 TO 200 TIMES DEPENDING ON     CPWSUCRS
006800              UCRS-DETAIL-COUNT, INDEXED BY UCRS-X1.              CPWSUCRS
006900         10  UCRS-SERVICE-DATA.                                   CPWSUCRS
007000             15  UCRS-SERVICE-TYPE        PIC X.                  CPWSUCRS
007100             15  UCRS-TITLE-CODE.                                 CPWSUCRS
007200                 20  UCRS-TITLE-POS1      PIC 9.                  CPWSUCRS
007300                 20  UCRS-TITLE-POS25     PIC 9999.               CPWSUCRS
007400             15  UCRS-DATE-WORKED.                                CPWSUCRS
011700                 20  UCRS-CENTURY-WORKED  PIC 99.                 49051194
007500                 20  UCRS-YEAR-WORKED     PIC 99.                 CPWSUCRS
007600                 20  UCRS-MONTH-WORKED    PIC 99.                 CPWSUCRS
007700                 20  UCRS-DAY-WORKED      PIC 99.                 CPWSUCRS
007800             15  UCRS-SAFETY-CODE         PIC X.                  CPWSUCRS
007900             15  UCRS-PAYMENT-CYCLE       PIC X.                  CPWSUCRS
008000             15  UCRS-COVERAGE-INDICATOR          PIC X.          CPWSUCRS
008100             15  UCRS-RETRO-INDICATOR     PIC X.                  CPWSUCRS
008200             15  UCRS-HOURS-WORKED        PIC S999V99.            CPWSUCRS
008300             15  UCRS-PERCENT-WORKED REDEFINES UCRS-HOURS-WORKED  CPWSUCRS
008400                                          PIC S9V9999.            CPWSUCRS
008500             15  UCRS-RATE-CODE           PIC X.                  CPWSUCRS
008600             15  UCRS-COVERED-GROSS       PIC S9(5)V99.           CPWSUCRS
008700             15  UCRS-MONTHLY-SALARY      PIC S9(5)V99.           CPWSUCRS
008800             15  UCRS-HOURLY-RATE  REDEFINES                      CPWSUCRS
008900                      UCRS-MONTHLY-SALARY PIC S999V9999.          CPWSUCRS
013203             15  UCRS-MEDCOMP-IND         PIC X    VALUE SPACE.   17690915
013200             15  UCRS-PARTIAL-YEAR-IND    PIC X.                  05530761
013205             15  UCRS-TITLE-UNIT-CODE     PIC X(02).              02821477
013206             15  UCRS-EARN-COVERAGE-IND   PIC X.                  02821477
013207             15  UCRS-BUDGET-INIT-FLAG    PIC X(02).              03861495
013208             15  FILLER                   PIC X(18).              03861495
013209*****                                                          CD 06941549
009100         10  UCRS-CONTRIBUTION-DATA REDEFINES                     CPWSUCRS
009200                   UCRS-SERVICE-DATA.                             CPWSUCRS
009300             15  UCRS-CONTRIBUTION-TYPE   PIC X.                  CPWSUCRS
009400             15  UCRS-PLAN-CODE           PIC 99.                 CPWSUCRS
009500             15  UCRS-CONTRIBUTION-AMOUNT PIC S9(5)V99.           CPWSUCRS
013802             15  UCRS-PAY-CCYY.                                   49051194
013803                 17  UCRS-PAY-CENTURY     PIC 99.                 49051194
013804                 17  UCRS-PAY-YEAR        PIC 99.                 49051194
013810             15  UCRS-YTD-CONTRB-AMOUNT   PIC S9(7)V99.           01990610
013820             15  UCRS-BUYBACK-GTN         PIC X(03).              25791111
013903             15  FILLER-AVAIL-C           PIC X(37).              02821477
013904         10  UCRS-TAX-DATA REDEFINES                              49801227
013905                   UCRS-SERVICE-DATA.                             49801227
013906             15  UCRS-TAX-SEGMENT-TYPE    PIC X.                  49801227
013907             15  UCRS-TAX-SEG-CODE        PIC 99.                 49801227
013908             15  UCRS-TAX-MONTHLY-AMOUNT  PIC S9(5)V99.           49801227
013910             15  UCRS-TAX-CCYY.                                   49801227
013920                 17  UCRS-TAX-CENTURY     PIC 99.                 49801227
013930                 17  UCRS-TAX-YEAR        PIC 99.                 49801227
013931             15  UCRS-TAX-CCYY-REDEF  REDEFINES                   49801227
013932                 UCRS-TAX-CCYY            PIC 9(04).              49801227
013940             15  UCRS-TAX-YTD-AMOUNT      PIC S9(7)V99.           49801227
013960             15  FILLER-AVAIL-T           PIC X(40).              02821477
009900*                                                                 CPWSUCRS
010000*   CONTROL RECORD  FORMAT FOR UCRS                               CPWSUCRS
010100*                                                                 CPWSUCRS
010200 01  UCRS-CONTROL-RECORD.                                         CPWSUCRS
010300     05  UCRS-CTL-RECORD-CODE             PIC X.                  CPWSUCRS
010400         88  UCRS-CTL-RECORD VALUE '9'.                           CPWSUCRS
010500     05  UCRS-CTL-KEY.                                            CPWSUCRS
014620         10  UCRS-CTL-TRAILER-TYPE        PIC 9(9).               49801227
010700         10  UCRS-CTL-LOCATION            PIC XX.                 CPWSUCRS
010800         10  UCRS-CTL-CYCLE-DATE.                                 CPWSUCRS
015000             15  UCRS-CTL-CENTURY         PIC 99.                 49051194
010900             15  UCRS-CTL-YEAR            PIC 99.                 CPWSUCRS
011000             15  UCRS-CTL-MONTH           PIC 99.                 CPWSUCRS
011200     05  UCRS-CTL-RECORD-COUNT            PIC S9(6).              CPWSUCRS
011300     05  UCRS-CTL-COVERED-GROSS           PIC S9(9)V99.           CPWSUCRS
011400     05  UCRS-CTL-CONTRIBUTION-COUNT      PIC S9(6).              CPWSUCRS
015510     05  FILLER-AVAIL-CTL3                PIC X(09).              05530761
015700     05  UCRS-CTL-PLAN-COUNT              PIC S9(4).              01900588
016100     05  UCRS-CTL-CONTRIBUTION-ARRAY OCCURS 0 TO 99 TIMES         01900588
016200              DEPENDING ON UCRS-CTL-PLAN-COUNT                    01900588
012000              INDEXED BY UCRS-CTL-X1.                             CPWSUCRS
012100         10  UCRS-CTL-CONTRIBUTION-DATA.                          CPWSUCRS
012200             15  UCRS-CTL-PLAN-CODE           PIC 99.             CPWSUCRS
012300             15  UCRS-CTL-CONTRIBUTION-AMOUNT PIC S9(9)V99.       CPWSUCRS
016700*                                                                 49801227
016800*   'T' TYPE CONTROL RECORD  FORMAT FOR UCRS                      49801227
016900*                                                                 49801227
017000 01  UCRS-T-CONTROL-RECORD.                                       49801227
017100     05  UCRS-T-CTL-RECORD-CODE           PIC X.                  49801227
017300         88  UCRS-T-CTL-RECORD VALUE '9'.                         49801227
017400     05  UCRS-T-CTL-KEY.                                          49801227
017410         10  UCRS-T-CTL-TRAILER-TYPE      PIC 9(9).               49801227
017600         10  UCRS-T-CTL-LOCATION          PIC XX.                 49801227
017700         10  UCRS-T-CTL-CYCLE-DATE.                               49801227
017800             15  UCRS-T-CTL-CENTURY       PIC 99.                 49801227
017900             15  UCRS-T-CTL-YEAR          PIC 99.                 49801227
018000             15  UCRS-T-CTL-MONTH         PIC 99.                 49801227
018200     05  UCRS-T-CTL-RECORD-COUNT          PIC S9(6).              49801227
018300     05  UCRS-T-CTL-COVERED-GROSS         PIC S9(9)V99.           49801227
018400     05  UCRS-T-CTL-TAX-SEGMENT-COUNT     PIC S9(6).              49801227
018500     05  FILLER-AVAIL-T-CTL3              PIC X(09).              49801227
018600     05  UCRS-T-CTL-PLAN-COUNT            PIC S9(4).              49801227
018700     05  UCRS-T-CTL-TAX-ARRAY OCCURS 0 TO 99 TIMES                49801227
018800              DEPENDING ON UCRS-T-CTL-PLAN-COUNT                  49801227
018900              INDEXED BY UCRS-T-CTL-X1.                           49801227
019000         10  UCRS-T-CTL-TAX-DATA.                                 49801227
019100             15  UCRS-T-CTL-PLAN-CODE     PIC 99.                 49801227
019200             15  UCRS-T-CTL-TAX-AMOUNT    PIC S9(9)V99.           49801227
