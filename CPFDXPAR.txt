000010**************************************************************/   32021138
000020*  COPYMEMBER: CPWSXPAR                                      */   32021138
000030*  RELEASE: ___1138______ SERVICE REQUEST(S): ___13202_____  */   32021138
000040*  NAME:______WJG________ MODIFICATION DATE:  ___03/21/97__  */   32021138
000050*  DESCRIPTION:                                              */   32021138
000060*                                                            */   32021138
000070*  - EXPAND EARNINGS DISTRIBUTION RECORD                     */   32021138
000080*    FOR FLEXIBLE FULL ACCOUNTING UNIT PROJECT               */   32021138
000090*                                                            */   32021138
000091**************************************************************/   32021138
000100**************************************************************/   36500900
000200*  COPYMEMBER: CPFDXPAR                                      */   36500900
000300*  RELEASE: ___0900______ SERVICE REQUEST(S): ____3650_____  */   36500900
000400*  NAME: ___ RAB (BIT)___ MODIFICATION DATE:  __05/20/94____ */   36500900
000500*  DESCRIPTION:                                              */   36500900
000600*     MODIFIED FOR COST CENTER AND PROJECT CODE ENHANCEMENT  */   36500900
000700**************************************************************/   36500900
000100**************************************************************/   14420448
000200*  COPY MODULE:  CPFDXPAR                                    */   14420448
000300*  RELEASE # ___0448___   SERVICE REQUEST NO(S)____1442______*/   14420448
000400*  NAME ___M SANO______   MODIFICATION DATE ____02/07/90_____*/   14420448
000500*  DESCRIPTION                                               */   14420448
000600*  THE P.A.R. FILE IS BEING EXPANDED BY 200 BYTES ON THE     */   14420448
000700*  VARIABLE (EARNINGS DISTRIBUTION) PORTION OF THE RECORD.   */   14420448
000800**************************************************************/   14420448
000100**************************************************************/   13190174
000200*  COPY MODULE:  CPFDXPAR                                    */   13190174
000300*  RELEASE # ___0174___   SERVICE REQUEST NO(S)____1319______*/   13190174
000400*  NAME ___JLT_________   MODIFICATION DATE ____12/20/85_____*/   13190174
000500*  DESCRIPTION                                               */   13190174
000600*  FOR PHASE 3 OF THE PAYROLL FILE EXPANSION PROJECT,        */   13190174
000700*  THE P.A.R. FILE IS BEING EXPANDED BY 40 BYTES ON THE      */   13190174
000800*  FIXED PORTION OF THE RECORD AND BY 32 BYTES ON THE        */   13190174
000900*  VARIABLE (EARNINGS DISTRIBUTION) PORTION OF THE RECORD.   */   13190174
001000**************************************************************/   13190174
001100     SKIP2                                                        13190174
001200*    COPYID=CPFDXPAR                                              CPFDXPAR
001300     BLOCK CONTAINS 0 RECORDS                                     CPFDXPAR
001400     RECORDING MODE IS V                                          CPFDXPAR
001500*    RECORD CONTAINS 276 TO 7473 CHARACTERS                       13190174
002400*****RECORD CONTAINS 316 TO 10681 CHARACTERS                      14420448
003200**** RECORD CONTAINS 316 TO 10881 CHARACTERS                      36500900
003300**** RECORD CONTAINS 316 TO 11871 CHARACTERS                      32021138
003310     RECORD CONTAINS 358 TO 13398 CHARACTERS                      32021138
001700     LABEL RECORDS ARE STANDARD                                   CPFDXPAR
001800     DATA RECORD IS PAY-AUDIT-RECORD.                             CPFDXPAR
001900     SKIP1                                                        CPFDXPAR
