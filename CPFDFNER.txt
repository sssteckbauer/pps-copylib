000000**************************************************************/   32201186
000001*  COPYMEMBER: CPFDFNER                                      */   32201186
000002*  RELEASE: ___1186______ SERVICE REQUEST(S): ____13220____  */   32201186
000003*  NAME:_______QUAN______ CREATION DATE:      ___04/21/98__  */   32201186
000004*  DESCRIPTION:                                              */   32201186
000005*  - FILE DESCRIPTION FOR THE NEW EMPLOYEE REGISTRY FILE.    */   32201186
000007**************************************************************/   32201186
011300*FD  STATE-NER-FILE                                               CPFDFNER
011310                                                                  CPFDFNER
011400     BLOCK CONTAINS 0 RECORDS                                     CPFDFNER
011500     RECORDING MODE IS F                                          CPFDFNER
011600     LABEL RECORDS STANDARD                                       CPFDFNER
011700     DATA RECORD IS NER-OUT-RECORD.                               CPFDFNER
011800 01  NER-OUT-RECORD              PIC X(175).                      CPFDFNER
