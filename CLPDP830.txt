000100*==========================================================%      DS795
000200*=    COPY MEMBER: CLPDP830                               =%      DS795
000300*=    CHANGE # DS795        PROJ. REQUEST: LE/370         =%      DS795
000400*=                                         CONVERSION     =%      DS795
000500*=    NAME: G CHIU          MODIFICATION DATE: 12/06/94   =%      DS795
000600*=                                                        =%      DS795
000700*=    DESCRIPTION:                                        =%      DS795
000800*=     REPLACED TIME 'ACCEPT' STATEMENT WITH              =%      DS795
000900*=     LE/370 VERSION OF CPPDTIME                         =%      DS795
001000*==========================================================%      DS795
000100*                                                                 CLPDP830
000200*      CONTROL REPORT PPP830CR                                    CLPDP830
000300*                                                                 CLPDP830
000400 PRINT-CONTROL-REPORT SECTION.                                    CLPDP830
000500                                                                  CLPDP830
000600     OPEN OUTPUT CONTROLREPORT.                                   CLPDP830
000700                                                                  CLPDP830
000800     MOVE 'PPP830CR'             TO CR-HL1-RPT.                   CLPDP830
000900     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP830
001000     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP830
001100                                                                  CLPDP830
001200     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP830
001300     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP830
001400                                                                  CLPDP830
001500     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP830
001600     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP830
001700     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP830
001800     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP830
001900     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP830
002000                                                                  CLPDP830
002100     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP830
002200     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP830
002300                                                                  CLPDP830
002400     MOVE WS-SPEC-CARD           TO CR-DL5-CTL-CARD.              CLPDP830
002500     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP830
002600     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP830
002700                                                                  CLPDP830
002800     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP830
002900     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP830
003000                                                                  CLPDP830
004100*    ACCEPT TIME-WORK-AREA FROM TIME.                             DS795
004200     COPY CPPDTIME.                                               DS795
004300     MOVE PRE-EDIT-TIME          TO TIME-WORK-AREA.               DS795
003200     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP830
003300     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP830
003400     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP830
003500     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP830
003600                                                                  CLPDP830
003700     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP830
003800     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP830
003900*                                                                 CLPDP830
004000     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP830
004100     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP830
004200                                                                  CLPDP830
004300     CLOSE CONTROLREPORT.                                         CLPDP830
