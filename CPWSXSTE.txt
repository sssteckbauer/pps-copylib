000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPWSXSTE                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    INITIAL INSTALLATION OF COPY MEMBER.                    *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100*    COPYID=CPWSXSTE                                              CPWSXSTE
001200*                                                                 CPWSXSTE
001300*01  SET-TRANS-ELEMENT-TABLE.                                     CPWSXSTE
001400*                                                                 CPWSXSTE
001500******************************************************************CPWSXSTE
001600** WARNING: THE NUMBER OF ENTRIES IN THE SET-TRANS-ELEMENT-TBL  **CPWSXSTE
001700**          MUST BE EQUAL OR GREATER THAN THE NUMBER OF ENTRIES **CPWSXSTE
001800**          CONTAINED IN "IDC-SET-TRANSACTIONS".                **CPWSXSTE
001900******************************************************************CPWSXSTE
002000*                                                                 CPWSXSTE
002100     05  SET-TRANS-X            PIC S9(04) COMP SYNC VALUE ZERO.  CPWSXSTE
002200     05  MAX-SET-TRANS-ELEMENTS PIC S9(04) COMP SYNC              CPWSXSTE
002300                                  VALUE +200.                     CPWSXSTE
002400     05  SET-TRANS-ELMTS-TABLE.                                   CPWSXSTE
002500         10  FILLER              OCCURS  200 TIMES.               CPWSXSTE
002600             15  SET-TRANS-ELMT-LNGH       PIC 9(02).             CPWSXSTE
002700             15  SET-TRANS-CHAR-OR-NUM     PIC X(01).             CPWSXSTE
002800             15  SET-TRANS-LAST-KEY-POSN   PIC X(01).             CPWSXSTE
002900*                                                                 CPWSXSTE
003000******************************************************************CPWSXSTE
