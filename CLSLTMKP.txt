000100*    COPYID=CLSLTMKP                                              CLSLTMKP
000200     SELECT TIMEKEEPER-FILE                                       CLSLTMKP
000300         ASSIGN TO TIMEKEEP                                       CLSLTMKP
000400         ORGANIZATION IS INDEXED                                  CLSLTMKP
000500         ACCESS MODE IS DYNAMIC                                   CLSLTMKP
000600         RECORD KEY IS TIMEKEEPER-KEY                             CLSLTMKP
000700         ALTERNATE RECORD KEY IS TIMEKEEPER-NAME                  CLSLTMKP
000800             WITH DUPLICATES                                      CLSLTMKP
000900         ALTERNATE RECORD KEY IS TIMEKEEPER-DEPT-NAME             CLSLTMKP
001000             WITH DUPLICATES                                      CLSLTMKP
001100         ALTERNATE RECORD KEY IS TIMEKEEPER-MAILCODE              CLSLTMKP
001200             WITH DUPLICATES                                      CLSLTMKP
001300         FILE STATUS IS TIMEKEEPER-FILE-STATUS.                   CLSLTMKP
001400*                                                                 CLSLTMKP
