000100**************************************************************/   26001140
000200*  COPYMEMBER: CPLNKXLI                                      */   26001140
000300*  RELEASE: ___1140______ SERVICE REQUEST(S): ____12600____  */   26001140
000400*  NAME:_____J.WILCOX____ MODIFICATION DATE:  ___07/16/97__  */   26001140
000500*  DESCRIPTION:                                              */   26001140
000600*  - Added new return field for Capped Salary Base.          */   26001140
000700*  - Increased size of premium return field.                 */   26001140
000800**************************************************************/   26001140
000000**************************************************************/   28521087
000001*  COPYMEMBER: CPLNKXLI                                      */   28521087
000002*  RELEASE: ___1087______ SERVICE REQUEST(S): ____12852____  */   28521087
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/07/96__  */   28521087
000004*  DESCRIPTION:                                              */   28521087
000005*  - ADDED 88 LEVEL FOR INVALID EMPLOYEE AGE RANGE.          */   28521087
000005*  - INCREASED THE SIZE OF THE AGE.                          */   28521087
000007**************************************************************/   28521087
000000**************************************************************/   28521025
000001*  COPYMEMBER: CPLNKXLI                                      */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/15/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    CHANGED KXLI-COVERAGE-EFFECT-DATE TO BE STORED AS AN ISO*/   28521025
000007*    DATE IN THE FORMAT OF CCYY-MM-DD.                       */   28521025
000009**************************************************************/   28521025
000100**************************************************************/   36220651
000200*  COPYLIB: CPLNKXLI                                         */   36220651
000300*  RELEASE: ___0651_____  SERVICE REQUESTS:   ___3622___     */   36220651
000500*  NAME:    _______KXK_   MODIFICATION DATE:  _04/13/92_     */   36220651
000600*                                                            */   36220651
000700*  DESCRIPTION:                                              */   36220651
000800*   - CHANGED KXLI-EARNINGS-EFFECT-DATE TO                   */   36220651
000800*             KXLI-COVERAGE-EFFECT-DATE.                     */   36220651
001000**************************************************************/   36220651
000100**************************************************************/   37320536
000200*  COPYLIB: CPLNKXLI                                         */   37320536
000300*  RELEASE: ____0536____  SERVICE REQUESTS:   ___3732___     */   37320536
000400*                                                            */   37320536
000500*  NAME:    _L.DULANEY_   MODIFICATION DATE:  _01/22/91_     */   37320536
000600*                                                            */   37320536
000700*  DESCRIPTION:                                              */   37320536
000800*   - ADDED KXLI-INS-COVERAGE-AMT TO CARRY THE (SALARY BASE  */   37320536
000900*     * PLAN) VALUE FOR USE BY PROGRAMS NEEDING THIS FIGURE  */   37320536
001000**************************************************************/   37320536
000100**************************************************************/   14420448
000200*  COPYLIB: CPLNKXLI                                         */   14420448
000300*  RELEASE:  _____0448_____  SERVICE REQUESTS: ____1442____  */   14420448
000400*                                                            */   14420448
000500*  NAME ___M. SANO_____   MODIFICATION DATE ____02/07/90____ */   14420448
000600*  DESCRIPTION:                                              */   14420448
000700*      ADDED BRSC TO LINKAGE                                 */   14420448
000800*                                                            */   14420448
000900**************************************************************/   14420448
000100**************************************************************/   45240381
000200*  COPYLIB: CPLNKXLI                                         */   45240381
000300*  RELEASE:  _____0381_____  SERVICE REQUESTS: ____4524____  */   45240381
000400*                                                            */   45240381
000500*  NAME ___J. WILCOX___   MODIFICATION DATE ____10/03/88____ */   45240381
000600*  DESCRIPTION:                                              */   45240381
000700*                                                            */   45240381
000800*  RE-RELEASE OF COPYMEMBER DUE TO SEPARATION OF             */   45240381
000900*  EMPLOYEE-PAID LIFE INSURANCE FROM DEPENDENT LIFE          */   45240381
001000*  INSURANCE.                                                */   45240381
001100*                                                            */   45240381
001200**************************************************************/   45240381
001300*COPYID=CPLNKXLI                                                  CPLNKXLI
001400******************************************************************CPLNKXLI
001500*                        C P L N K X L I                         *CPLNKXLI
001600******************************************************************CPLNKXLI
001700*                                                                *CPLNKXLI
001800*01  PPBENXLI-INTERFACE.                                          CPLNKXLI
001900*                                                                 CPLNKXLI
002000******************************************************************CPLNKXLI
002100*      THE FOLLOWING FLAGS AND FIELDS ARE PASSED BY CALLER       *CPLNKXLI
002200******************************************************************CPLNKXLI
002300*                                                                 CPLNKXLI
002400     05  KXLI-ACTION-FLAG            PIC X(01).                   CPLNKXLI
002500         88  KXLI-ACTION-INITIALIZING            VALUE '1'.       CPLNKXLI
002600         88  KXLI-ACTION-RATE-RETRIEVAL          VALUE '2'.       CPLNKXLI
002700         88  KXLI-ACTION-PREMIUM-CALC            VALUE '3'.       CPLNKXLI
002800*                                                                 CPLNKXLI
008200*****                                                          CD 26001140
004800     05  KXLI-LI-EMP-AGE             PIC 9(3).                    28521087
004810         88  KXLI-INVALID-AGE                    VALUES ZERO      28521087
004820                                                   THRU 15.       28521087
003000*                                                                 CPLNKXLI
003100     05  KXLI-LI-BASE-SALARY         PIC 9(3).                    CPLNKXLI
003200*                                                                 CPLNKXLI
003300     05  KXLI-PLAN-CODE              PIC X(01).                   CPLNKXLI
003400         88  KXLI-VALID-PLAN-CODE                VALUES ARE       CPLNKXLI
003500                                               'F', '1' THRU '4'. CPLNKXLI
003600         88  KXLI-FLAT-RATE                      VALUE 'F'.       CPLNKXLI
003700         88  KXLI-1-TIMES                        VALUE '1'.       CPLNKXLI
003800         88  KXLI-2-TIMES                        VALUE '2'.       CPLNKXLI
003900         88  KXLI-3-TIMES                        VALUE '3'.       CPLNKXLI
004000         88  KXLI-4-TIMES                        VALUE '4'.       CPLNKXLI
004100*                                                                 CPLNKXLI
009800*****                                                          CD 26001140
006120     05  KXLI-COVERAGE-EFFECT-DATE   PIC X(10).                   28521025
004300     05  KXLI-PERIOD-END-DATE        PIC X(06).                   CPLNKXLI
005300     05  KXLI-BRSC                   PIC X(05).                   14420448
004400*                                                                 CPLNKXLI
004500******************************************************************CPLNKXLI
004600*      THE FOLLOWING FLAGS AND FIELDS ARE RETURNED TO CALLER     *CPLNKXLI
004700******************************************************************CPLNKXLI
004800*                                                                 CPLNKXLI
004900     05  KXLI-RETURN-STATUS-FLAG     PIC X(01).                   CPLNKXLI
006100         88  KXLI-RETURN-STATUS-NORMAL           VALUE '0' '2'.   14420448
005100         88  KXLI-RETURN-STATUS-ABORTING         VALUE '1'.       CPLNKXLI
005200*                                                                 CPLNKXLI
005300     05  KXLI-INVALID-LOOKUP-ARG-FLAG PIC X(01).                  CPLNKXLI
005400         88  KXLI-INVALID-LOOKUP-ARG             VALUE '1'.       CPLNKXLI
005500*                                                                 CPLNKXLI
005600     05  KXLI-NOT-ENROLLED-IN-LIFE-FLAG                           CPLNKXLI
005700                                      PIC X(01).                  CPLNKXLI
005800         88  KXLI-ENROLLED-IN-LIFE               VALUE '0'.       CPLNKXLI
005900         88  KXLI-NOT-ENROLLED-IN-LIFE           VALUE '1'.       CPLNKXLI
006000*                                                                 CPLNKXLI
006100     05  KXLI-RETURN-FIELDS.                                      CPLNKXLI
006200         10  KXLI-FLAT-RATE-AMT      PIC 9(3).                    CPLNKXLI
006300         10  KXLI-INS-RATE-AMT       PIC 9V9(3).                  CPLNKXLI
012200*********10  KXLI-INS-PREMIUM-AMT    PIC 9(3)V9(2).               26001140
012300         10  KXLI-INS-PREMIUM-AMT    PIC 9(4)V9(2).               26001140
008600         10  KXLI-INS-COVERAGE-AMT   PIC 9(4).                    37320536
012500         10  KXLI-ACTUAL-SALARY-BASE PIC 9(3).                    26001140
006500*                                                                *CPLNKXLI
006600***********> END OF CPLNKXLI <************************************CPLNKXLI
