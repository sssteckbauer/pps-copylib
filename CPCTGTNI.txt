000000**************************************************************/   48881487
000001*  COPYMEMBER: CPCTGTNI                                      */   48881487
000002*  RELEASE: ___1487______ SERVICE REQUEST(S): ____14888____  */   48881487
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___04/28/03__  */   48881487
000004*  DESCRIPTION:                                              */   48881487
000005*  - ADD GTN DIRECT DEPOSIT INDICATOR                        */   48881487
000007**************************************************************/   48881487
000000**************************************************************/   30871460
000001*  COPYMEMBER: CPCTGTNI                                      */   30871460
000002*  RELEASE: ___1460______ SERVICE REQUEST(S): _____3087____  */   30871460
000003*  NAME:_____SRS_________ CREATION DATE:      ___01/22/03__  */   30871460
000004*  DESCRIPTION:                                              */   30871460
000005*  - NEW COPY MEMBER FOR GROSS-TO-NET TABLE INPUT            */   30871460
000007**************************************************************/   30871460
006400*    COPYID=CPCTGTNI                                              CPCTGTNI
011500*01  GROSS-TO-NET-TABLE-INPUT.                                    CPCTGTNI
019630     05  GTNI-PRIORITY                   PIC X(4).                CPCTGTNI
019640     05  GTNI-NUMBER                     PIC XXX.                 CPCTGTNI
019650         88  INVALID-GTN-NO VALUE '130' THRU '140'.               CPCTGTNI
019660     05  GTNI-ENTRY-NO                   PIC X.                   CPCTGTNI
019670     05  GTNI-DESC                       PIC X(12).               CPCTGTNI
019720     05  GTNI-TYPE                       PIC X.                   CPCTGTNI
019730     05  GTNI-GROUP                      PIC X.                   CPCTGTNI
019740     05  GTNI-MATCH-ELEMENT              PIC XXX.                 CPCTGTNI
019770     05  GTNI-SCHEDULES.                                          CPCTGTNI
019780         07  GTNI-SM-SCHED-CODE          PIC X.                   CPCTGTNI
019790         07  GTNI-BI-SCHED-CODE          PIC X.                   CPCTGTNI
019800         07  GTNI-MN-SCHED-CODE          PIC X.                   CPCTGTNI
019810     05  GTNI-YTD                        PIC X.                   CPCTGTNI
019820     05  GTNI-SUSPENSE                   PIC X.                   CPCTGTNI
019830     05  GTNI-QTD                        PIC X.                   CPCTGTNI
019840     05  GTNI-DECLINING-BAL              PIC X.                   CPCTGTNI
019850     05  GTNI-ETD                        PIC X.                   CPCTGTNI
019860     05  GTNI-FYTD                       PIC X.                   CPCTGTNI
019870     05  GTNI-USER                       PIC X.                   CPCTGTNI
019890     05  GTNI-FULL-PART                  PIC X.                   CPCTGTNI
019900     05  GTNI-USAGE                      PIC X.                   CPCTGTNI
019910     05  GTNI-BASE                       PIC X.                   CPCTGTNI
019920     05  GTNI-REDUCE-BASE                PIC X.                   CPCTGTNI
019930     05  GTNI-SP-CALC-NO                 PIC XX.                  CPCTGTNI
019940     05  GTNI-SP-CALC-IND                PIC X.                   CPCTGTNI
019950     05  GTNI-SP-UPDATE-NO               PIC XX.                  CPCTGTNI
019960     05  GTNI-STATUS                     PIC X.                   CPCTGTNI
019970     05  GTNI-STOP-AT-TERM               PIC X.                   CPCTGTNI
019980     05  GTNI-VALUE-RNG-EDIT             PIC X.                   CPCTGTNI
019990     05  GTNI-ACCEPTBL-VALUES.                                    CPCTGTNI
020000         07  GTNI-VALUE1                 PIC 9(5)V99.             CPCTGTNI
020010         07  GTNI-VALUE1-X REDEFINES GTNI-VALUE1                  CPCTGTNI
020020                                         PIC X(7).                CPCTGTNI
020030         07  GTNI-VALUE2                 PIC 9(5)V99.             CPCTGTNI
020040         07  GTNI-VALUE2-X REDEFINES GTNI-VALUE2                  CPCTGTNI
020050                                         PIC X(7).                CPCTGTNI
020070     05  GTNI-DED-CODE                   PIC X(2).                CPCTGTNI
020080     05  GTNI-CON-EDIT-RTN               PIC X(3).                CPCTGTNI
020090     05  GTNI-DIR-DEP-IND                PIC X(1).                48881487
020130     05  GTNI-LIABILITY-REV-FAU          PIC X(30).               CPCTGTNI
020140     05  GTNI-LIABILITY-REV-OBJ          PIC X(4).                CPCTGTNI
020151     05  GTNI-LINK-GTN-NUM               PIC 9(3).                CPCTGTNI
020151     05  GTNI-LINK-GTN-NUM-X REDEFINES GTNI-LINK-GTN-NUM          CPCTGTNI
020151                                         PIC X(3).                CPCTGTNI
020160     05  GTNI-BEN-CODE                   PIC X(1).                CPCTGTNI
020170     05  GTNI-REDUCTION-INDS.                                     CPCTGTNI
020180         07  GTNI-REDUCTION-FWT          PIC X.                   CPCTGTNI
020190         07  GTNI-REDUCTION-SWT          PIC X.                   CPCTGTNI
020200         07  GTNI-REDUCTION-FICA         PIC X.                   CPCTGTNI
020211     05  GTNI-INIT-LINK-FLAG             PIC X.                   CPCTGTNI
020220     05  GTNI-CB-ELIG-IND                PIC X(1).                CPCTGTNI
020230     05  GTNI-MAX-AMOUNT                 PIC 9(5)V99.             CPCTGTNI
020240     05  GTNI-MAX-AMOUNT-X REDEFINES GTNI-MAX-AMOUNT              CPCTGTNI
020250                                         PIC X(7).                CPCTGTNI
020260     05  GTNI-CB-BEHAV-CODE              PIC X.                   CPCTGTNI
020270     05  GTNI-BENEFIT-TYPE               PIC X.                   CPCTGTNI
020280     05  GTNI-BENEFIT-PLAN               PIC X(02).               CPCTGTNI
020290     05  GTNI-SET-INDICATOR              PIC X(01).               CPCTGTNI
020300     05  GTNI-DISPLAY-IGTN               PIC X(01).               CPCTGTNI
020310     05  GTNI-DISPLAY-IDED               PIC X(01).               CPCTGTNI
020320     05  GTNI-DISPLAY-IRTR               PIC X(01).               CPCTGTNI
020330     05  GTNI-DISPLAY-IRET               PIC X(01).               CPCTGTNI
020340     05  GTNI-OVERRIDE-DED               PIC 9(3).                CPCTGTNI
020350     05  GTNI-OVERRIDE-DEDX REDEFINES GTNI-OVERRIDE-DED           CPCTGTNI
020360                                         PIC X(3).                CPCTGTNI
020370     05  GTNI-ICED-INDICATOR             PIC X(01).               CPCTGTNI
020390     05  GTNI-DEPT-PAR-IND               PIC X(01).               CPCTGTNI
020440     05  GTNI-RECEIVABLE-FAU             PIC X(30).               CPCTGTNI
020450     05  GTNI-PREPAYMENT-FAU             PIC X(30).               CPCTGTNI
