000000**************************************************************/   74221352
000001*  COPYMEMBER: CPWSXRTN                                      */   74221352
000002*  RELEASE: ___1352______ SERVICE REQUEST(S): ____17422____  */   74221352
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___06/15/01__  */   74221352
000004*  DESCRIPTION:                                              */   74221352
000005*  ADDED PROGRAM PPDUEUTL                                    */   74221352
000007**************************************************************/   74221352
000000**************************************************************/   48441325
000001*  COPYMEMBER: CPWSXRTN                                      */   48441325
000002*  RELEASE: ___1325______ SERVICE REQUEST(S): ____14844____  */   48441325
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___11/01/00__  */   48441325
000004*  DESCRIPTION:                                              */   48441325
000005*  ADDED PROGRAM PPESTRP3                                    */   48441325
000007**************************************************************/   48441325
000000**************************************************************/   54541281
000001*  COPYMEMBER: CPWSXRTN                                      */   54541281
000002*  RELEASE: ___1281______ SERVICE REQUEST(S): ____15454____  */   54541281
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/11/99__  */   54541281
000004*  DESCRIPTION:                                              */   54541281
000005*  ADDED PROGRAM  PPFCCRPT                                   */   54541281
000007**************************************************************/   54541281
000000**************************************************************/   54181253
000001*  COPYMEMBER: CPWSXRTN                                      */   54181253
000002*  RELEASE: ___1253______ SERVICE REQUEST(S): _15418, 15419  */   54181253
000003*  NAME:_______WJG_______ MODIFICATION DATE:  ___06/18/99__  */   54181253
000004*  DESCRIPTION:                                              */   54181253
000005*                                                            */   54181253
000006*  ADDED MODULES PPESTXXX AND COMMENTED REFERENCES TO ALL    */   54181253
000007*  MODULES PPBIVXXX.                                         */   54181253
000008*                                                            */   54181253
000009**************************************************************/   54181253
000100**************************************************************/   32591166
000200*  COPYMEMBER: CPWSXRTN                                      */   32591166
000300*  RELEASE: _____1166____ SERVICE REQUEST(S): ____13259____  */   32591166
000400*  NAME:_____R. STAPLES__ MODIFICATION DATE:  ___01/12/98__  */   32591166
000500*  DESCRIPTION:                                              */   32591166
000600*  ADDED PROGRAM  PPBIVREJ                                   */   32591166
000800**************************************************************/   32591166
000100**************************************************************/   32591157
000200*  COPYMEMBER: CPWSXRTN                                      */   32591157
000300*  RELEASE: _____1157____ SERVICE REQUEST(S): ____13259____  */   32591157
000400*  NAME:_____R. STAPLES__ MODIFICATION DATE:  ___12/15/97__  */   32591157
000500*  DESCRIPTION:                                              */   32591157
000600*  ADDED PROGRAMS PPBIVUTL, PPBIVEDB, PPBIVRP1, PPBIVRP2     */   32591157
000700*                                                            */   32591157
000800**************************************************************/   32591157
000100**************************************************************/   17810964
000200*  COPYMEMBER: CPWSXRTN                                      */   17810964
000300*  RELEASE: ___0964______ SERVICE REQUEST(S): ____11781____  */   17810964
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___02/23/95__  */   17810964
000500*  DESCRIPTION:                                              */   17810964
000600*  ADD PROGRAM PPFICRET                                      */   17810964
000700*                                                            */   17810964
000800**************************************************************/   17810964
000100**************************************************************/   36560949
000200*  COPYMEMBER: CPWSXRTN                                      */   36560949
000300*  RELEASE: ____0949____  SERVICE REQUEST(S): ____3656____   */   36560949
000400*  NAME:    __J.LUCAS___  CREATION DATE:      __12/14/94__   */   36560949
000500*  DESCRIPTION:                                              */   36560949
000600*  ADD PROGRAMS - PPPGSR01, PPPGSR02 AND PPEI507             */   36560949
000700**************************************************************/   36560949
000100**************************************************************/   17040930
000200*  COPYMEMBER: CPWSXRTN                                      */   17040930
000300*  RELEASE: ___0930______ SERVICE REQUEST(S): _____1704____  */   17040930
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___09/12/94__  */   17040930
000500*  DESCRIPTION:                                              */   17040930
000600*   ADDED PPAPTLAC                                           */   17040930
000700*                                                            */   17040930
000800**************************************************************/   17040930
000101**************************************************************/   36430795
000102*  COPYMEMBER: CPWSXRTN                                      */   36430795
000103*  RELEASE: ___0795______ SERVICE REQUEST(S): _____3643____  */   36430795
000104*  NAME:_______PXP_______ MODIFICATION DATE:  __07/15/93____ */   36430795
000105*  DESCRIPTION:                                              */   36430795
000106*  ADD NEW PROGRAMS                                          */   36430795
000107**************************************************************/   36430795
000100******************************************************************36360750
000200*  COPYMEMBER: CPWSXRTN                                          *36360750
000300*  RELEASE: ___0750______ SERVICE REQUEST NO(S):___3636____      *36360750
000400*  NAME:    M.CLARK______ MODIFICATION DATE:  ___03/15/93__      *36360750
000500*                                                                *36360750
000600*  DESCRIPTION: MODIFIED FOR TITLE CODE CONVERSION TO DB2.       *36360750
000700*               ADDED ENTRY FOR PGM-PPTCTUTL.                    *36360750
000800*               COMMENTED-OUT ENTRY FOR PGM-PPTTLUTL (PPTTLUT2). *36360750
000900******************************************************************36360750
000001*----------------------------------------------------------------*36410717
000002*|**************************************************************|*36410717
000003*|* COPYMEMBER: CPWSXRTN                                       *|*36410717
000004*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000005*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000006*|*  DESCRIPTION:                                              *|*36410717
000007*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000008*|*    NEW COPYLIB TO STORE THE CALLED ROUTINE NAMES           *|*36410717
000009*|*    THOSE MODULES THAT ARE DYNAMICALLY CALLED BY ROUTINE    *|*36410717
000010*|*    MANAGER WILL NOT NEED TO BE DEFINED HERE SINCE THE      *|*36410717
000011*|*    RESIDE ON THE ROUTINE DEFINITION TABLE                  *|*36410717
000012*|**************************************************************|*36410717
000013*----------------------------------------------------------------*36410717
000094*01  CALLED-ROUTINES.                                             CPWSXRTN
000095                                                                  CPWSXRTN
000101     05  PGM-PPABYRPT              PIC X(08) VALUE 'PPABYRPT'.    CPWSXRTN
000102     05  PGM-PPACTION              PIC X(08) VALUE 'PPACTION'.    CPWSXRTN
000104     05  PGM-PPAPTDPT              PIC X(08) VALUE 'PPAPTDPT'.    CPWSXRTN
000106     05  PGM-PPAPTRAT              PIC X(08) VALUE 'PPAPTRAT'.    CPWSXRTN
000108     05  PGM-PPBENRPT              PIC X(08) VALUE 'PPBENRPT'.    CPWSXRTN
000110     05  PGM-PPBRSC                PIC X(08) VALUE 'PPBRSC  '.    CPWSXRTN
000112     05  PGM-PPBUTRPT              PIC X(08) VALUE 'PPBUTRPT'.    CPWSXRTN
000115     05  PGM-PPBUTUTL              PIC X(08) VALUE 'PPBUTUT2'.    CPWSXRTN
000117     05  PGM-PPCB01                PIC X(08) VALUE 'PPCB01  '.    CPWSXRTN
003302     05  PGM-PPDEPINT              PIC X(08) VALUE 'PPDEPINT'.    36430795
000119     05  PGM-PPDOSUTL              PIC X(08) VALUE 'PPDOSUT2'.    CPWSXRTN
003402     05  PGM-PPEDBDET              PIC X(08) VALUE 'PPEDBDET'.    36430795
000121     05  PGM-PPEDBFET              PIC X(08) VALUE 'PPEDBFET'.    CPWSXRTN
000123     05  PGM-PPEDBUPD              PIC X(08) VALUE 'PPEDBUPD'.    CPWSXRTN
000125     05  PGM-PPEMPSTA              PIC X(08) VALUE 'PPEMPSTA'.    CPWSXRTN
000127     05  PGM-PPERRWRT              PIC X(08) VALUE 'PPERRWRT'.    CPWSXRTN
007010     05  PGM-PPESTEDB              PIC X(08) VALUE 'PPESTEDB'.    54181253
007020     05  PGM-PPESTREJ              PIC X(08) VALUE 'PPESTREJ'.    54181253
007030     05  PGM-PPESTRP1              PIC X(08) VALUE 'PPESTRP1'.    54181253
007040     05  PGM-PPESTRP2              PIC X(08) VALUE 'PPESTRP2'.    54181253
007041     05  PGM-PPESTRP3              PIC X(08) VALUE 'PPESTRP3'.    48441325
007050     05  PGM-PPESTUTL              PIC X(08) VALUE 'PPESTUTL'.    54181253
001500     05  PGM-PPEXECLF              PIC X(08) VALUE 'PPEXECLF'.    CPWSXRTN
001700     05  PGM-PPEXECSP              PIC X(08) VALUE 'PPEXECSP'.    CPWSXRTN
001900     05  PGM-PPEXERPT              PIC X(08) VALUE 'PPEXERPT'.    CPWSXRTN
002100     05  PGM-PPHDAUTL              PIC X(08) VALUE 'PPHDAUTL'.    CPWSXRTN
002300     05  PGM-PPINAFP               PIC X(08) VALUE 'PPINAFP '.    CPWSXRTN
002600     05  PGM-PPKEYCHD              PIC X(08) VALUE 'PPKEYCHD'.    CPWSXRTN
002900     05  PGM-PPMSGUTL              PIC X(08) VALUE 'PPMSGUT2'.    CPWSXRTN
003100     05  PGM-PPMSSG                PIC X(08) VALUE 'PPMSSG2 '.    CPWSXRTN
007900     05  PGM-PPNAMGEN              PIC X(08) VALUE 'PPNAMGEN'.    54181253
003300     05  PGM-PPNRARPT              PIC X(08) VALUE 'PPNRARPT'.    CPWSXRTN
003500     05  PGM-PPPAVRAG              PIC X(08) VALUE 'PPPAVRAG'.    CPWSXRTN
003700     05  PGM-PPPBELI               PIC X(08) VALUE 'PPPBELI '.    CPWSXRTN
003800     05  PGM-PPPRMUTL              PIC X(08) VALUE 'PPPRMUT2'.    CPWSXRTN
004000     05  PGM-PPPTITLE              PIC X(08) VALUE 'PPPTITLE'.    CPWSXRTN
004200     05  PGM-PPRTNMGR              PIC X(08) VALUE 'PPRTNMGR'.    CPWSXRTN
004400     05  PGM-PPSTARPT              PIC X(08) VALUE 'PPSTARPT'.    CPWSXRTN
005400     05  PGM-PPTCTUTL              PIC X(08) VALUE 'PPTCTUTL'.    36360750
004800     05  PGM-PPWSPUTL              PIC X(08) VALUE 'PPWSPUT2'.    CPWSXRTN
004900     05  PGM-PPDETUTL              PIC X(08) VALUE 'PPDETUTL'.    CPWSXRTN
005702     05  PGM-PPXBUUTL              PIC X(08) VALUE 'PPXBUUTL'.    36430795
007600     05  PGM-PPAPTLAC              PIC X(08) VALUE 'PPAPTLAC'.    17040930
008400     05  PGM-PPPGSR01              PIC X(08) VALUE 'PPPGSR01'.    36560949
008500     05  PGM-PPPGSR02              PIC X(08) VALUE 'PPPGSR02'.    36560949
008600     05  PGM-PPEI507               PIC X(08) VALUE 'PPEI507'.     36560949
009400     05  PGM-PPFICRET              PIC X(08) VALUE 'PPFICRET'.    17810964
009500     05  PGM-UCIIDMNT              PIC X(08) VALUE 'UCIIDMNT'.    54181253
009500     05  PGM-PPFCCRPT              PIC X(08) VALUE 'PPFCCRPT'.    54541281
009700     05  PGM-PPDUEUTL              PIC X(08) VALUE 'PPDUEUTL'.    74221352
