000100**************************************************************/   48711432
000200*  COPYMEMBER: CPWSBRGH                                      */   48711432
000300*  RELEASE: ___1432______ SERVICE REQUEST(S): ____14871____  */   48711432
000400*  NAME:_____S.ISAACS____ MODIFICATION DATE:  ___08/29/02__  */   48711432
000500*  DESCRIPTION:                                              */   48711432
000600*  - Added one column for reduced deduction amount.          */   48711432
000800**************************************************************/   48711432
000100**************************************************************/   71641280
000200*  COPYMEMBER: CPWSBRGH                                      */   71641280
000300*  RELEASE: ___1280______ SERVICE REQUEST(S): ____17164____  */   71641280
000400*  NAME:_____J.WILCOX____ MODIFICATION DATE:  ___03/14/00__  */   71641280
000500*  DESCRIPTION:                                              */   71641280
000600*  - Added two columns for hours threshold percentage and    */   71641280
000700*    deduction threshold to support calc routine 18 GTNs.    */   71641280
000800**************************************************************/   71641280
000000**************************************************************/   32131177
000001*  COPYMEMBER: CPWSBRGH                                      */   32131177
000002*  RELEASE: ___1177______ SERVICE REQUEST(S): ____13213____  */   32131177
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___03/17/98__  */   32131177
000004*  DESCRIPTION:                                              */   32131177
000005*  - ADD TWO COLUMNS FOR SECOND LEVEL CAP AND CAP GROSS      */   32131177
000007**************************************************************/   32131177
000100**************************************************************/   36330704
000200*  COPYMEMBER: CPWSBRGH                                      */   36330704
000300*  RELEASE: ____0704____  SERVICE REQUEST(S): ____3633____   */   36330704
000400*  NAME:    ___B.I.T.___  CREATION DATE:      __10/02/92__   */   36330704
000500*  DESCRIPTION:                                              */   36330704
000600*    DCLGEN FOR THE PPPVBRGH VIEW                            */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900******************************************************************CPWSBRGH
001000* COBOL DECLARATION FOR TABLE PAYCXD.PPPVBRGH_BRGH               *CPWSBRGH
001100******************************************************************CPWSBRGH
001200*01  DCLPPPVBRGH-BRGH.                                            CPWSBRGH
001300     10 BRG-CBUC             PIC X(2).                            CPWSBRGH
001400     10 BRG-REP              PIC X(1).                            CPWSBRGH
001500     10 BRG-SHC              PIC X(1).                            CPWSBRGH
001600     10 BRG-DUC              PIC X(1).                            CPWSBRGH
001700     10 BRG-GTN-NUMBER       PIC X(3).                            CPWSBRGH
001800     10 SYSTEM-ENTRY-DATE    PIC X(10).                           CPWSBRGH
001900     10 SYSTEM-ENTRY-TIME    PIC X(8).                            CPWSBRGH
002000     10 BRG-RATE-AMOUNT      PIC S99999V9999 USAGE COMP-3.        CPWSBRGH
002100     10 BRG-MONTHLY-CAP      PIC S99999V9999 USAGE COMP-3.        CPWSBRGH
002200     10 BRG-LAST-ACTION      PIC X(1).                            CPWSBRGH
002300     10 BRG-LAST-ACTION-DT   PIC X(10).                           CPWSBRGH
002100     10 BRG-MONTHLY-CAP2     PIC S99999V9999 USAGE COMP-3.        32131177
002100     10 BRG-CAP-GROSS        PIC S99999V9999 USAGE COMP-3.        32131177
004100     10 BRG-THRESHOLD-PCT    PIC S99999V9999 USAGE COMP-3.        71641280
004200     10 BRG-DEDUCTION-PCT    PIC S99999V9999 USAGE COMP-3.        71641280
004210     10 BRG-RED-DED-AMT      PIC S99999V9999 USAGE COMP-3.        48711432
002400******************************************************************CPWSBRGH
004400* THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 15      *71641280
002600******************************************************************CPWSBRGH
