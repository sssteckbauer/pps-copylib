000000**************************************************************/   36430943
000001*  COPYMEMBER: CPWSPRNT                                      */   36430943
000002*  RELEASE: ___0943______ SERVICE REQUEST(S): _____3643____  */   36430943
000003*  NAME:_______MLG_______ MODIFICATION DATE:  __11/10/94____ */   36430943
000004*  DESCRIPTION:                                              */   36430943
000005*    ADDED CPWSPRNT-TABLE-UPDATED-SW                         */   36430943
000007**************************************************************/   36430943
000000**************************************************************/   36510832
000001*  COPYMEMBER: CPWSPRNT                                      */   36510832
000002*  RELEASE: ___0832______ SERVICE REQUEST(S): _____3651____  */   36510832
000003*  NAME:_______DDM_______ MODIFICATION DATE:  __11/12/93____ */   36510832
000004*  DESCRIPTION:                                              */   36510832
000005*    DSS - DISTRIBUTED SYSTEMS SERVICES - PRINTER SELECTION  */   36510832
000006*    AND   PRINTER TABLE MAINTENANCE                         */   36510832
000007*                                                            */   36510832
000008**************************************************************/   36510832
000100*01  CPWSPRNT   EXTERNAL.                                         CPWSPRNT
000200     05  CPWSPRNT-EXT-AREA                 PIC X(1024).           CPWSPRNT
000300     05  CPWSPRNT-EXT-DATA REDEFINES CPWSPRNT-EXT-AREA.           CPWSPRNT
000301         10  PRNT-SCROLL-CONTROL.                                 CPWSPRNT
000302             15  PRNT-FIRST-PRT-ID-OF-SCRN PIC X(08).             CPWSPRNT
000303             15  PRNT-LAST-PRT-ID-OF-SCRN  PIC X(08).             CPWSPRNT
000304             15  PRNT-LAST-PRT-ID-PLUS-1   PIC X(08).             CPWSPRNT
000340         10  PRNT-UPD-AREA.                                       CPWSPRNT
009100             15  PRNT-CURRENT-SELECTION    PIC X(04).             CPWSPRNT
009510             15  PRNT-PRT-ID               PIC X(08).             CPWSPRNT
009520             15  PRNT-LPR1                 PIC X(70).             CPWSPRNT
009530             15  PRNT-LPR2                 PIC X(70).             CPWSPRNT
009531         10  PRNT-COPIES-GRP.                                     CPWSPRNT
009541             15  PRNT-COPIES               PIC 9(02).             CPWSPRNT
009542         10  PRNT-COPIES-ORIG              PIC X(02).             CPWSPRNT
009545         10  EPRT-UPD-AREA.                                       CPWSPRNT
009546             15  EPRT-ACT                  PIC X(01).             CPWSPRNT
009547             15  EPRT-PRT-ID               PIC X(08).             CPWSPRNT
009548             15  EPRT-NODE                 PIC X(08).             CPWSPRNT
009549             15  EPRT-TYPE                 PIC X(01).             CPWSPRNT
009550             15  EPRT-CLASS                PIC X(01).             CPWSPRNT
009551             15  EPRT-FORMS                PIC X(04).             CPWSPRNT
009560             15  EPRT-DESC                 PIC X(20).             CPWSPRNT
009570             15  EPRT-NW-ADDR              PIC X(50).             CPWSPRNT
009700         10  CPWSPRNT-INITIAL-WRITE-SW     PIC X(01).             CPWSPRNT
009710         10  CPWSPRNT-TABLE-UPDATED-SW     PIC X(01).             36430943
009720             88  CPWSPRNT-TABLE-UPDATED        VALUE 'Y'.         36430943
009800         10  FILLER                        PIC X(749).            36430943
009900*********10  FILLER                        PIC X(750).            36430943
