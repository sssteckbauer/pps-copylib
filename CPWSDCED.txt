000000**************************************************************/   54541281
000001*  COPYMEMBER: CPWSDCED                                      */   54541281
000002*  RELEASE: ___1281______ SERVICE REQUEST(S): ____15454____  */   54541281
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___11/20/99__  */   54541281
000004*  DESCRIPTION:                                              */   54541281
000005*  -  ADDED SPECIAL INITIAL SWITCH TO INITIALIZE A DEPENDENT */   54541281
000006*     DCED DEPENDING ON THE DCED AND FUTURE ENROLLMENT DATE. */   54541281
000007**************************************************************/   54541281
000000**************************************************************/   36260760
000001*  COPYMEMBER: CPWSDCED                                      */   36260760
000002*  RELEASE: ___0760______ SERVICE REQUEST(S): _____3626____  */   36260760
000003*  NAME:__PHIL THOMPSON__ CREATION DATE:      ___04/30/93__  */   36260760
000004*  DESCRIPTION:                                              */   36260760
000005*    LINKAGE FOR CALL TO PPDEPINT. CONTROL DETERMINES ARRAY  */   36260760
000006*    OCCURRENCES TO BE INITIALIZED. PLAN DETERMINES THE      */   36260760
000007*    DEPENDENT COVERAGE EFFECTIVE DATES TO BE INITIALIZED.   */   36260760
000008**************************************************************/   36260760
032610*01  INIT-DCED-LINKAGE.                                           CPWSDCED
032670     05  INIT-OCCURRENCES-SW  PIC X(01).                          CPWSDCED
032680         88  INIT-CURRENT-OCCURRENCES          VALUE 'C'.         CPWSDCED
032690         88  INIT-ALL-OCCURRENCES              VALUE 'A'.         CPWSDCED
032690         88  INIT-ROLL-OCCURRENCES             VALUE 'R'.         CPWSDCED
032692         88  INIT-SPECIFIC-OCCURRENCES         VALUE 'S'.         54541281
032691     05  INIT-PLAN-SW         PIC X(01).                          CPWSDCED
032692         88  INIT-HEALTH-PLAN-DCED             VALUE 'H'.         CPWSDCED
032693         88  INIT-DENTAL-PLAN-DCED             VALUE 'D'.         CPWSDCED
032694         88  INIT-VISION-PLAN-DCED             VALUE 'V'.         CPWSDCED
032695         88  INIT-LEGAL-PLAN-DCED              VALUE 'L'.         CPWSDCED
