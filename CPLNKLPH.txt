000100******************************************************************36090513
000200*  COPYMEMBER: CPLNKLPH                                          *36090513
000300*  RELEASE: ____0513____  SERVICE REQUEST(S): ____3609____       *36090513
000400*  NAME:__PXP___________  CREATION DATE:      __11/05/90__       *36090513
000500*  DESCRIPTION:                                                  *36090513
000600*    LINKAGE FOR PPLPHUTL SUBROUTINE CREATED FOR DB2 EDB         *36090513
000700*    CONVERSION.                                                 *36090513
000800*                                                                *36090513
000900******************************************************************CPLNKLPH
001000*    COPYID=CPLNKLPH                                              CPLNKLPH
001100*01  PPLPHUTL-INTERFACE.                                          CPLNKLPH
001200*                                                                *CPLNKLPH
001300******************************************************************CPLNKLPH
001400*    P P L P H U T L   I N T E R F A C E                         *CPLNKLPH
001500******************************************************************CPLNKLPH
001600*                                                                *CPLNKLPH
001700     05  PPLPHUTL-ERROR-SW       PICTURE X(1).                    CPLNKLPH
001800     88  PPLPHUTL-ERROR VALUE 'Y'.                                CPLNKLPH
001900     05  PPLPHUTL-EMPLOYEE-ID   PICTURE X(9).                     CPLNKLPH
002000     05  PPLPHUTL-ROW-COUNT     PICTURE 9(3).                     CPLNKLPH
002100     05  PPLPHUTL-ENTRIES.                                        CPLNKLPH
002200     10  PPLPHUTL-ENTRY     OCCURS 7    TIMES.                    CPLNKLPH
002300         15 LVPLAN-KEY           PIC X(6).                        CPLNKLPH
002400         15 LVPLAN-STARTDATE     PIC X(10).                       CPLNKLPH
002500         15 LVPLAN-CODE          PIC X(1).                        CPLNKLPH
