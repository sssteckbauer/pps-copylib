000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXCCR                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100*    COPYID=CPWSXCCR                                              CPWSXCCR
000200*01  XCCR-CAMPUS-CONTROL-RECORD.                                  30930413
000300     03  XCCR-DELETE             PICTURE X(01).                   CPWSXCCR
000400     03  XCCR-KEY                PICTURE X(13).                   CPWSXCCR
000500*            KEY CONTAINS '     CAMPUSCR'                         CPWSXCCR
000600     03  XCCR-LOCATION           PICTURE 9(02).                   CPWSXCCR
000700     03  XCCR-STATEWIDE          PICTURE X(01).                   CPWSXCCR
000800     03  XCCR-CAMPUS-NAME        PICTURE X(20).                   CPWSXCCR
000900     03  XCCR-CAMPUS-ABRV        PICTURE X(04).                   CPWSXCCR
001000     03  XCCR-USER-DATA-1        PICTURE X(36).                   CPWSXCCR
001100     03  XCCR-USER-DATA-2        PICTURE X(63).                   CPWSXCCR
001200     03  XCCR-DATA-LAST-CHANGED.                                  CPWSXCCR
001300         05  XCCR-DATE-YR        PICTURE 9(02).                   CPWSXCCR
001400         05  XCCR-DATE-MO        PICTURE 9(02).                   CPWSXCCR
001500         05  XCCR-DATE-DA        PICTURE 9(02).                   CPWSXCCR
001600     03  XCCR-FILLER             PICTURE X(78).                   CPWSXCCR
