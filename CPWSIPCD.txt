000100**************************************************************/   36330704
000200*  COPYBOOK: CPWSIPCD                                        */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME __B.I.T._______   CREATION DATE     ____10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*                                                            */   36330704
000700*       CPWSIPCD IS A NEW COPY BOOK FOR RELEASE # 0704       */   36330704
000800*                                                            */   36330704
000900**************************************************************/   36330704
001000*-----------------------------------------------------------------CPWSIPCD
001100*    PCD INTERFACE AREA                                           CPWSIPCD
001200*    USED BY EMPLOYEE HISTORY MAINTENANCE PROGRAMS                CPWSIPCD
001300*-----------------------------------------------------------------CPWSIPCD
001400*01  IPCD-INTERFACE.                                              CPWSIPCD
001500     05  IPCD-INTERFACE-AREA.                                     CPWSIPCD
001600        10  IPCD-TABLE           PIC X(3).                        CPWSIPCD
001700            88 IPCD-HDE                        VALUE 'HDE'.       CPWSIPCD
001800            88 IPCD-SYS                        VALUE 'SYS'.       CPWSIPCD
001900            88 IPCD-HPM                        VALUE 'HPM'.       CPWSIPCD
002000        10  IPCD-PROCESS         PIC X.                           CPWSIPCD
002100            88 IPCD-SEQ-READ                   VALUE 'S'.         CPWSIPCD
002200            88 IPCD-RAND-READ                  VALUE 'R'.         CPWSIPCD
002300            88 IPCD-UPDATE                     VALUE 'U'.         CPWSIPCD
002400            88 IPCD-INSERT                     VALUE 'I'.         CPWSIPCD
002500            88 IPCD-CLOSE-CURSOR               VALUE 'C'.         CPWSIPCD
002600        10  IPCD-ERROR           PIC XX.                          CPWSIPCD
002700            88 IPCD-OK                         VALUE '00'.        CPWSIPCD
002800            88 IPCD-EOF                        VALUE '02'.        CPWSIPCD
002900            88 IPCD-BAD-RETURN                 VALUE '07'.        CPWSIPCD
003000     05  IPCD-ERROR-DATA         PIC X(65).                       CPWSIPCD
003100     05  IPCD-ROW-DATA           PIC X(4096).                     CPWSIPCD
