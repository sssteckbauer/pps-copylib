000100**************************************************************/   32021138
000200*  COPYMEMBER: CPWSWPAR                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ___13202_____  */   32021138
000400*  NAME: ______JLT_______ MODIFICATION DATE:  __08/04/97____ */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*     MODIFIED FOR GENERIC FAU PROCESSING.                   */   32021138
000700**************************************************************/   32021138
000100**************************************************************/   36500900
000200*  COPYMEMBER: CPWSWPAR                                      */   36500900
000300*  RELEASE: ___0900______ SERVICE REQUEST(S): ____3650_____  */   36500900
000400*  NAME: ___ RAB (BIT)___ MODIFICATION DATE:  __05/20/94____ */   36500900
000500*  DESCRIPTION:                                              */   36500900
000600*     MODIFIED FOR COST CENTER AND PROJECT CODE ENHANCEMENT  */   36500900
000700**************************************************************/   36500900
000100**************************************************************/   14420448
000200*  COPYMEMBER: CPWSWPAR                                      */   14420448
000300*  RELEASE # ____0448____ SERVICE REQUEST NO(S)___1442_______*/   14420448
000400*  NAME ____M SANO_____   MODIFICATION DATE ____02/07/90_____*/   14420448
000500*  DESCRIPTION                                               */   14420448
000600*   ADDED 5 BYTES TO THE DEDUCTION ARRAY INFORMATION FOR BRSC*/   14420448
000700**************************************************************/   14420448
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSWPAR                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   13190174
000200*  COPY MODULE:  CPWSWPAR                                    */   13190174
000300*  RELEASE # ___0174___   SERVICE REQUEST NO(S)____1319______*/   13190174
000400*  NAME ___JLT_________   MODIFICATION DATE ____12/20/85_____*/   13190174
000500*  DESCRIPTION                                               */   13190174
000600*  FOR PHASE 3 OF THE PAYROLL FILE EXPANSION PROJECT,        */   13190174
000700*  THE P.A.R. FILE IS BEING EXPANDED BY 40 BYTES ON THE      */   13190174
000800*  FIXED PORTION OF THE RECORD AND BY 32 BYTES ON THE        */   13190174
000900*  VARIABLE (EARNINGS DISTRIBUTION) PORTION OF THE RECORD.   */   13190174
001000**************************************************************/   13190174
001100     SKIP2                                                        13190174
001200*    COPYID=CPWSWPAR                                              CPWSWPAR
001300                                                                  CPWSWPAR
001400*01  MNTHLY-AUDIT-RCD.                                            30930413
001500     03  WPAR-AUDIT-RCD.                                          CPWSWPAR
001600         05  WPAR-KEY.                                            CPWSWPAR
001700             07  WPAR-ID-NO          PIC X(9).                    CPWSWPAR
001800             07  WPAR-PRI-GRS-CTL    PIC S9(3).                   CPWSWPAR
001900             07  WPAR-PAY-END-DATE   PIC X(6).                    CPWSWPAR
002000             07  WPAR-PAY-CYCLE      PIC XX.                      CPWSWPAR
002100             07  WPAR-TRANS-SEQ-CD   PIC XX.                      CPWSWPAR
002200         05  WPAR-STOP-PAY-IND       PIC X.                       CPWSWPAR
002300         05  WPAR-UNCLAIM-CK-CODE    PIC X.                       CPWSWPAR
002400         05  WPAR-VARIABLE-LENGTH-INDS.                           CPWSWPAR
002500             07  WPAR-NO-HRS-ADJ     PIC S9(4)       COMP    SYNC.CPWSWPAR
002600             07  WPAR-NO-ADJS        PIC S9(4)       COMP    SYNC.CPWSWPAR
002700             07  WPAR-NO-ACCT        PIC S9(4)       COMP    SYNC.CPWSWPAR
002800             07  WPAR-NO-DED-REF     PIC S9(4)       COMP    SYNC.CPWSWPAR
002900             07  WPAR-NO-FILLER      PIC S9(4)       COMP    SYNC.CPWSWPAR
003000         05  WPAR-RCD-FIXED.                                      CPWSWPAR
003100             07  WPAR-PR-CTL-RCD     PIC X(224).                  CPWSWPAR
005300********     07  FILLER              PIC X(18).                   32021138
005300             07  FILLER              PIC X(60).                   32021138
003300             07  WPAR-FILLER-OP      PIC X(40).                   13190174
003400         05  WPAR-VARIABLE-DATA.                                  CPWSWPAR
003500             07  WPAR-HRS-ADJ-FLDS   OCCURS  0   TO  10  TIMES    CPWSWPAR
003600                                 DEPENDING   ON  WPAR-NO-HRS-ADJ. CPWSWPAR
003700                 09  FILLER          PIC X(8).                    CPWSWPAR
003800             07  WPAR-ADJ-FLDS       OCCURS  0   TO  20  TIMES    CPWSWPAR
003900                                 DEPENDING   ON  WPAR-NO-ADJS.    CPWSWPAR
004000                 09  FILLER          PIC X(10).                   CPWSWPAR
004100             07  WPAR-ACCT-DIST      OCCURS  0   TO  99  TIMES    CPWSWPAR
004200                                 DEPENDING   ON  WPAR-NO-ACCT.    CPWSWPAR
006400*                                                              CD 32021138
006600****             09  FILLER          PIC X(105).                  32021138
006600                 09  FILLER          PIC X(120).                  32021138
004500             07  WPAR-CUR-DED-REF    OCCURS  0   TO  40  TIMES    CPWSWPAR
004600                                 DEPENDING   ON  WPAR-NO-DED-REF. CPWSWPAR
006100*****            09  FILLER          PIC X(14).                   14420448
006200                 09  FILLER          PIC X(19).                   14420448
004800             07  WPAR-FILLER         OCCURS  0   TO  20  TIMES    CPWSWPAR
004900                                 DEPENDING   ON  WPAR-NO-FILLER.  CPWSWPAR
005000                 09  FILLER          PIC X(06).                   CPWSWPAR
