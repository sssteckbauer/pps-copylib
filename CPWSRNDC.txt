000000**************************************************************/   36490863
000001*  COPYMEMBER: CPWSRNDC                                      */   36490863
000002*  RELEASE: ___0863______ SERVICE REQUEST(S): _____3649____  */   36490863
000003*  NAME:_______DDM_______ MODIFICATION DATE:  __01/27/94____ */   36490863
000004*  DESCRIPTION: NEW NOTIFICATION DISTRIBUTION CLASSIFICATION */   36490863
000005*    TABLE FOR PAN IMPLEMENTATION.                           */   36490863
000900******************************************************************36490863
001000* COBOL DECLARATION FOR TABLE        PPPVNDC1_NDC                *CPWSRNDC
001100******************************************************************CPWSRNDC
001200*01  DCLPPPVNDC1-NDC.                                             CPWSRNDC
001300     10 NDC-EMPLOYEE-ID      PIC X(9).                            CPWSRNDC
001700     10 NDC-NOTIF-CLASS      PIC X(8).                            CPWSRNDC
002500******************************************************************CPWSRNDC
002600* THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS  2      *CPWSRNDC
002700******************************************************************CPWSRNDC
