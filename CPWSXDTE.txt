000100**************************************************************/   36330704
000200*  COPYLIB: CPWSXDTE                                         */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME __B.I.T._______   CREATION DATE ________10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*                                                            */   36330704
000700*        THIS IS A NEW COPYMEMBER FOR RELEASE # 0704         */   36330704
000800*        AND IS USED IN CONJUNCTION WITH COPYMEMBER          */   36330704
000900*        CPPDXDTE.                                           */   36330704
001000*                                                            */   36330704
001100**************************************************************/   36330704
001200                                                                  CPWSXDTE
001300     05  WS-DIVIDEND                PIC 9(4) COMP VALUE ZERO.     CPWSXDTE
001400     05  WS-QUOTIENT                PIC 9(4) COMP VALUE ZERO.     CPWSXDTE
001500     05  WS-REMAIN                  PIC 9(4) COMP VALUE ZERO.     CPWSXDTE
001600                                                                  CPWSXDTE
001700     05  WS-EDIT-DATE.                                            CPWSXDTE
001800         10  WS-EDIT-MM             PIC XX.                       CPWSXDTE
001900         10  WS-EDIT-99-MM REDEFINES WS-EDIT-MM   PIC 99.         CPWSXDTE
002000                                                                  CPWSXDTE
002100         10  WS-EDIT-DD             PIC XX.                       CPWSXDTE
002200         10  WS-EDIT-99-DD REDEFINES WS-EDIT-DD   PIC 99.         CPWSXDTE
002300                                                                  CPWSXDTE
002400         10  WS-EDIT-YY             PIC XX.                       CPWSXDTE
002500         10  WS-EDIT-99-YY REDEFINES WS-EDIT-YY   PIC 99.         CPWSXDTE
002600                                                                  CPWSXDTE
