000100******************************************************************PARQMODC
000200*                                                                *PARQMODC
000300*                P A R Q M O D C                                 *PARQMODC
000400*                                                                *PARQMODC
000500*    THIS MEMBER DEFINES THE COMMON MODULE COMMUNICATION AREA    *PARQMODC
000600*    WITHIN THE PAR SYSTEM.  IN CALLING MODULES THIS AREA IS     *PARQMODC
000700*    DEFINED IN WORKING STORAGE.   IN CALLED MODULES THIS AREA   *PARQMODC
000800*    IS IN LINKAGE SECTION.                                      *PARQMODC
000900*                                                                *PARQMODC
001000*    THIS COPY MEMBER CHANGED FOR QUARTERLY ONLY PROCESSING      *PARQMODC
001100*                                                                *PARQMODC
001200******************************************************************PARQMODC
001300                                                                  PARQMODC
001400*01  MODULE-COMMON-AREA.                                          PARQMODC
001500                                                                  PARQMODC
001600     05  TEST-SWITCH-LS           PIC X.                          PARQMODC
001700         88  TEST-RUN             VALUE 'Y'.                      PARQMODC
001800         88  TEST-REPORT          VALUE 'Y' 'Z'.                  PARQMODC
001900                                                                  PARQMODC
002000     05  RUN-DATE-LS              PIC 9(6).                       PARQMODC
002100     05  FILLER                   REDEFINES RUN-DATE-LS.          PARQMODC
002200         10  RUN-DATE-MM-LS       PIC 99.                         PARQMODC
002300         10  RUN-DATE-DD-LS       PIC 99.                         PARQMODC
002400         10  RUN-DATE-YY-LS       PIC 99.                         PARQMODC
002500                                                                  PARQMODC
002600     05  TYPE-OF-CALL-LS          PIC X.                          PARQMODC
002700         88  INITIALIZATION       VALUE 'I'.                      PARQMODC
002800         88  PROCESS              VALUE 'P'.                      PARQMODC
002900         88  TERMINATION          VALUE 'T'.                      PARQMODC
003000                                                                  PARQMODC
003100     05  LOCATION-CODE-LS         PIC 99.                         PARQMODC
003200*    05  RETURN-CODE              PIC 99.                         PARQMODC
003300     05  QUARTER-LS               PIC 9.                          PARQMODC
003400     05  FISCAL-YEAR-LS           PIC 99.                         PARQMODC
