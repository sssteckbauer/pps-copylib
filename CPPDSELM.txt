000000**************************************************************/   28521087
000001*  COPYMEMBER: CPPDSELM                                      */   28521087
000002*  RELEASE: ___1087______ SERVICE REQUEST(S): ____12852____  */   28521087
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___07/17/96__  */   28521087
000004*  DESCRIPTION:                                              */   28521087
000005*  ** DATE CONVERSION II **                                  */   28521087
000006*  - CALL LE/370 DATE ROUTINE TO GET CURRENT DATE.           */   28521087
000008**************************************************************/   28521087
000100**************************************************************/   36330704
000200*  COPY MEMBER: CPPDSELM                                     */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME ____B.I.T______   CREATION     DATE ____10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*    THE SELECT MODULE MAIN ROUTINE                          */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900*                                                                 CPPDSELM
001000 1000-MAIN-PROCESS SECTION.                                       CPPDSELM
001100*                                                                 CPPDSELM
001200     IF XSEL-PROCESSING-REQUEST NOT = 'S'                         CPPDSELM
001300         SET XSEL-INVALID-REQUEST TO TRUE                         CPPDSELM
001400         GO TO 1000-END                                           CPPDSELM
001500     END-IF.                                                      CPPDSELM
001600*                                                                 CPPDSELM
001700     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID.                        CPPDSELM
001800     SET XSEL-OK TO TRUE.                                         CPPDSELM
001900*                                                                 CPPDSELM
002000     MOVE XSEL-EMPLID    TO WS-EMPLOYEE-ID.                       CPPDSELM
002100*                                                                 CPPDSELM
002200     IF XSEL-DATE = SPACES OR '0001-01-01'                        CPPDSELM
002300         PERFORM XDC3-GET-CURRENT-DATE                            28521087
002301         MOVE XDC3-GREG-CC TO XSEL-DATE-CC                        28521087
002302         MOVE XDC3-GREG-YY TO XSEL-DATE-YY                        28521087
002303         MOVE XDC3-GREG-MM TO XSEL-DATE-MM                        28521087
002304         MOVE XDC3-GREG-DD TO XSEL-DATE-DD                        28521087
002310*********ACCEPT WS-DATE FROM DATE                                 28521087
002400*********IF WS-DATE-YY < '25'                                     28521087
002500*********   MOVE '20'    TO XSEL-DATE-CC                          28521087
002600*********ELSE                                                     28521087
002700*********   MOVE '19'    TO XSEL-DATE-CC                          28521087
002800*********END-IF                                                   28521087
002900*********MOVE WS-DATE-YY TO XSEL-DATE-YY                          28521087
003000*********MOVE WS-DATE-MM TO XSEL-DATE-MM                          28521087
003100*********MOVE WS-DATE-DD TO XSEL-DATE-DD                          28521087
003200         MOVE '-'        TO XSEL-DATE-FILL-1                      CPPDSELM
003300                            XSEL-DATE-FILL-2                      CPPDSELM
003400     END-IF.                                                      CPPDSELM
003500     MOVE XSEL-DATE TO WS-QUERY-DATE.                             CPPDSELM
003600*                                                                 CPPDSELM
003700     IF XSEL-TIME     = SPACES OR '00.00.00'                      CPPDSELM
003800         ACCEPT WS-TIME FROM TIME                                 CPPDSELM
003900         MOVE WS-TIME-HH TO XSEL-TIME-HH                          CPPDSELM
004000         MOVE WS-TIME-MM TO XSEL-TIME-MM                          CPPDSELM
004100         MOVE WS-TIME-SS TO XSEL-TIME-SS                          CPPDSELM
004200         MOVE '.'        TO XSEL-TIME-FILL-1                      CPPDSELM
004300                            XSEL-TIME-FILL-2                      CPPDSELM
004400     END-IF.                                                      CPPDSELM
004500     MOVE XSEL-TIME TO WS-QUERY-TIME.                             CPPDSELM
004600*                                                                 CPPDSELM
004700     IF XSEL-ITERATION = SPACES                                   CPPDSELM
004800         MOVE '001' TO XSEL-ITERATION                             CPPDSELM
004900     END-IF.                                                      CPPDSELM
005000     MOVE XSEL-ITERATION TO WS-ITERATION.                         CPPDSELM
005100*                                                                 CPPDSELM
005200     MOVE XSEL-ADDITIONAL-KEY TO WS-ADDITIONAL-KEY.               CPPDSELM
005300*                                                                 CPPDSELM
005400     PERFORM 2000-SELECT.                                         CPPDSELM
005500*                                                                 CPPDSELM
005600 1000-END.                                                        CPPDSELM
005700*                                                                 CPPDSELM
005800     GOBACK.                                                      CPPDSELM
005900*                                                                 CPPDSELM
006000 1000-EXIT.                                                       CPPDSELM
006100     EXIT.                                                        CPPDSELM
006200*                                                                 CPPDSELM
