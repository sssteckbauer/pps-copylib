000000**************************************************************/   30871401
000001*  COPYMEMBER: CPLNKTCL                                      */   30871401
000002*  RELEASE: ___1401______ SERVICE REQUEST(S): _____3087____  */   30871401
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___03/20/02__  */   30871401
000004*  DESCRIPTION:                                              */   30871401
000005*  - CHANGE GRADE BASED RATE LOOKUP CODE 88 VALUE TO MATCH   */   30871401
000006*    CURRENT VALUE ON PPPTSL TABLE.                          */   30871401
000007**************************************************************/   30871401
000100**************************************************************/   74611376
000200*  COPYMEMBER: CPLNKTCL                                      */   74611376
000300*  RELEASE: ___1376______ SERVICE REQUEST(S): ____17461____  */   74611376
000400*  NAME:____STEINITZ_____ MODIFICATION DATE:  ___10/31/01__  */   74611376
000500*  DESCRIPTION:                                              */   74611376
000600*                                                            */   74611376
000700*  THIS MEMBER REPLACES COMPLETELY THE PRIOR VERSION OF      */   74611376
000800*  CPLNKTCL, WHICH WAS LAST MODIFIED IN RELEASE 1304.        */   74611376
000900*  A NEW CALL TYPE, XTCL-READ-ABS-MIN-MAX, HAS BEEN ADDED    */   74611376
001000*  FOR USE BY PROGRAMS PPP635 AND PPWHLOF.  UNUSED FIELDS    */   74611376
001100*  HAVE BEEN ELIMINATED AND THE OVERALL LENGTH OF THE        */   74611376
001200*  STATEMENT HAS BEEN REDUCED. DATA NAMES WERE NOT CHANGED   */   74611376
001300*  TO START WITH KTCL TO AVOID NUMEROUS PROGRAM CHANGES.     */   74611376
001400*                                                            */   74611376
001500**************************************************************/   74611376
001600*01  PPTCTUTL-INTERFACE.                                          CPLNKTCL
001700     05  XTCL-CONTROL-INFORMATION.                                CPLNKTCL
001800         10  FILLER              PIC  X(0027).                    CPLNKTCL
001900             88  XTCL-PPTCTUTL-INTERFACE-BEGINS  VALUE            CPLNKTCL
002000                                 '<PPTCTUTL-INTERFACE BEGINS>'.   CPLNKTCL
002100         10  XTCL-MODULE-CALLS-CHAR.                              CPLNKTCL
002200             15  XTCL-MODULE-CALLS                                CPLNKTCL
002300                                 PIC S9(0007).                    CPLNKTCL
002400         10  XTCL-EVENT-CODE     PIC  X(0020).                    CPLNKTCL
002500             88  XTCL-READ-ALL-TITLES-FIRST      VALUE            CPLNKTCL
002600                                          'READ ALL, FIRST'.      CPLNKTCL
002700             88  XTCL-READ-ALL-TITLES-NEXT       VALUE            CPLNKTCL
002800                                          'READ ALL, NEXT'.       CPLNKTCL
002900             88  XTCL-READ-CHOSEN-TITLE          VALUE            CPLNKTCL
003000                                          'READ CHOSEN'.          CPLNKTCL
003100             88  XTCL-VERIFY-CHOSEN-TITLE        VALUE            CPLNKTCL
003200                                          'VERIFY CHOSEN'.        CPLNKTCL
003300             88  XTCL-READ-ALL-TTL-FIRST-LONG    VALUE            CPLNKTCL
004300                                          'READ ALL,FIRST,LONG '. CPLNKTCL
003500             88  XTCL-READ-ALL-TTL-NEXT-LONG     VALUE            CPLNKTCL
004500                                          'READ ALL, NEXT, LONG'. CPLNKTCL
003700             88  XTCL-READ-CHOSEN-TITLE-MEDIUM   VALUE            CPLNKTCL
003800                                          'READ CHOSEN MEDIUM  '. CPLNKTCL
003900             88  XTCL-READ-CHOSEN-TITLE-LONG     VALUE            CPLNKTCL
004700                                          'READ CHOSEN LONG    '. CPLNKTCL
004100             88  XTCL-READ-CHOSEN                VALUE            CPLNKTCL
004900                                          'READ CHOSEN         '  CPLNKTCL
004300                                          'READ CHOSEN MEDIUM  '  CPLNKTCL
004400                                          'FRAME REGULAR RATE  '  CPLNKTCL
004500                                          'READ SHIFT RATE     '  CPLNKTCL
004600                                          'READ ON CALL RATE   '  CPLNKTCL
004700                                          'CHECK FORMER APS    '  CPLNKTCL
004800                                          'READ ABS MIN MAX    '  CPLNKTCL
005000                                          'READ CHOSEN LONG    '. CPLNKTCL
005000             88  XTCL-READ-FIRST                 VALUE            CPLNKTCL
005200                                          'READ ALL, FIRST     '  CPLNKTCL
005300                                          'READ ALL,FIRST,LONG '. CPLNKTCL
005300             88  XTCL-READ-NEXT                  VALUE            CPLNKTCL
005500                                          'READ ALL, NEXT      '  CPLNKTCL
005600                                          'READ ALL, NEXT, LONG'. CPLNKTCL
005600             88  XTCL-READ-MEDIUM                VALUE            CPLNKTCL
005700                                          'READ CHOSEN MEDIUM  '. CPLNKTCL
005800             88  XTCL-READ-SHORT                 VALUE            CPLNKTCL
005900                                          'READ ALL, FIRST'       CPLNKTCL
006000                                          'READ ALL, NEXT'        CPLNKTCL
006100                                          'READ CHOSEN'.          CPLNKTCL
006200             88  XTCL-FRAME-REGULAR-RATE         VALUE            CPLNKTCL
006300                                          'FRAME REGULAR RATE  '. CPLNKTCL
006400             88  XTCL-READ-SHIFT-RATE            VALUE            CPLNKTCL
006500                                          'READ SHIFT RATE     '. CPLNKTCL
006600             88  XTCL-READ-ON-CALL-RATE          VALUE            CPLNKTCL
006700                                          'READ ON CALL RATE   '. CPLNKTCL
006800             88  XTCL-CHECK-FORMER-APS           VALUE            CPLNKTCL
006900                                          'CHECK FORMER APS    '. CPLNKTCL
007000             88  XTCL-READ-ABS-MIN-MAX           VALUE            CPLNKTCL
007100                                          'READ ABS MIN MAX    '. CPLNKTCL
007200             88  XTCL-READ-LONG                  VALUE            CPLNKTCL
005800                                          'READ CHOSEN LONG    '  CPLNKTCL
005900                                          'READ ALL,FIRST,LONG '  CPLNKTCL
006000                                          'READ ALL, NEXT, LONG'. CPLNKTCL
003300         10  XTCL-CHOSEN-TITLE-CODE                               CPLNKTCL
003400                                 PIC  X(0004).                    CPLNKTCL
007800             88  XTCL-CHOSEN-TITLE-EMPTY         VALUE SPACE.     CPLNKTCL
007900         10  XTCL-CHOSEN-SUB-LOCATION                             CPLNKTCL
008000                                 PIC  X(0002).                    CPLNKTCL
008100             88  XTCL-CHOSEN-SUB-LOC-EMPTY       VALUE SPACE.     CPLNKTCL
008200             88  XTCL-CHOSEN-SUB-LOC-DEFAULT     VALUE '**'.      CPLNKTCL
008300         10  XTCL-CHOSEN-PAY-REP-CODE                             CPLNKTCL
008400                                 PIC  X(0003).                    CPLNKTCL
008500             88  XTCL-CHOSEN-PAY-REP-COVERED     VALUE 'COV'.     CPLNKTCL
008600             88  XTCL-CHOSEN-PAY-REP-UNCOVERED   VALUE 'UNC'.     CPLNKTCL
008700         10  XTCL-CHOSEN-STEP    PIC  X(0003).                    CPLNKTCL
008800             88  XTCL-CHOSEN-STEP-EMPTY          VALUE SPACE.     CPLNKTCL
008900         10  FILLER REDEFINES XTCL-CHOSEN-STEP.                   CPLNKTCL
009000             15  XTCL-CHOSEN-STEP-CHAR                            CPLNKTCL
009100                                 PIC  X(0001)    OCCURS 3.        CPLNKTCL
009200         10  FILLER  REDEFINES XTCL-CHOSEN-STEP.                  CPLNKTCL
009300             15  XTCL-CHOSEN-STEP-NUM                             CPLNKTCL
009400                                 PIC  9(0001)    OCCURS 3.        CPLNKTCL
009500         10  XTCL-CHOSEN-GRADE   PIC  X(0002).                    CPLNKTCL
009600             88  XTCL-CHOSEN-GRADE-EMPTY         VALUE SPACE.     CPLNKTCL
009700         10  FILLER REDEFINES XTCL-CHOSEN-GRADE.                  CPLNKTCL
009800             15  XTCL-CHOSEN-GRADE-CHAR                           CPLNKTCL
009900                                 PIC  X(0001)    OCCURS 2.        CPLNKTCL
010000         10  XTCL-CHOSEN-SHIFT-TYPE                               CPLNKTCL
010100                                 PIC  X(0002).                    CPLNKTCL
010200             88  XTCL-CHOSEN-SHIFT-TYPE-EMPTY    VALUE SPACE.     CPLNKTCL
010300         10  XTCL-CHOSEN-ON-CALL-CODE                             CPLNKTCL
010400                                 PIC  X(0001).                    CPLNKTCL
010500             88  XTCL-CHOSEN-ON-CALL-CODE-EMPTY  VALUE SPACE.     CPLNKTCL
010600         10  XTCL-CHOSEN-PAY-RATE                                 CPLNKTCL
010700                                 PIC S9(05)V9(04)        COMP-3.  CPLNKTCL
010800             88  XTCL-CHOSEN-RATE-EMPTY          VALUE ZERO.      CPLNKTCL
003600         10  XTCL-READ-CHOSEN-DATE.                               CPLNKTCL
003700                 88  XTCL-READ-CHOSEN-DATE-CURRENT                CPLNKTCL
011100                                                 VALUE 'CURRENT'. CPLNKTCL
011200                 88  XTCL-READ-CHOSEN-DATE-EMPTY VALUE SPACE.     CPLNKTCL
004100             15  FILLER          PIC  X(0002).                    CPLNKTCL
004200             15  FILLER          PIC  X(0001).                    CPLNKTCL
004300                 88  XTCL-READ-CHOSEN-DATE-SLASH-01               CPLNKTCL
011600                                                 VALUE '/'.       CPLNKTCL
004500             15  FILLER          PIC  X(0002).                    CPLNKTCL
004600             15  FILLER          PIC  X(0001).                    CPLNKTCL
004700                 88  XTCL-READ-CHOSEN-DATE-SLASH-02               CPLNKTCL
012000                                                 VALUE '/'.       CPLNKTCL
004900             15  FILLER          PIC  X(0004).                    CPLNKTCL
005000         10  XTCL-STATUS-CODE    PIC  X(0015).                    CPLNKTCL
012300             88  XTCL-STATUS-ERROR-DB2           VALUE            CPLNKTCL
012400                                          'ERROR/DB2'.            CPLNKTCL
012500             88  XTCL-STATUS-ERROR-EVENT         VALUE            CPLNKTCL
012600                                          'ERROR/EVENT'.          CPLNKTCL
012700             88  XTCL-STATUS-NORMAL-END          VALUE            CPLNKTCL
012800                                          'NORMAL END'.           CPLNKTCL
012900             88  XTCL-STATUS-NORMAL-END-ALL      VALUE            CPLNKTCL
013000                                          'NORMAL END/ALL'.       CPLNKTCL
013100             88  XTCL-STATUS-UNUSED              VALUE 'UNUSED'.  CPLNKTCL
006000         10  XTCL-ERROR-CODES.                                    CPLNKTCL
013300                 88  XTCL-ERROR-CODES-UNUSED     VALUE SPACE.     CPLNKTCL
006200             15  FILLER          PIC  X(0001).                    CPLNKTCL
013500                 88  XTCL-ERROR-CODE-DATE        VALUE 'Y'.       CPLNKTCL
009200             15  FILLER          PIC  X(0001).                    CPLNKTCL
013700                 88  XTCL-ERROR-CODE-BADFIRST    VALUE 'Y'.       CPLNKTCL
009400             15  FILLER          PIC  X(0001).                    CPLNKTCL
013900                 88  XTCL-ERROR-CODE-BADNEXT     VALUE 'Y'.       CPLNKTCL
009600             15  FILLER          PIC  X(0001).                    CPLNKTCL
014100                 88  XTCL-ERROR-CODE-BADCALL     VALUE 'Y'.       CPLNKTCL
009800             15  FILLER          PIC  X(0001).                    CPLNKTCL
014300                 88  XTCL-ERROR-CODE-BADRECALL   VALUE 'Y'.       CPLNKTCL
010000             15  FILLER          PIC  X(0001).                    CPLNKTCL
014500                 88  XTCL-ERROR-CODE-PPROG       VALUE 'Y'.       CPLNKTCL
010200             15  FILLER          PIC  X(0001).                    CPLNKTCL
014700                 88  XTCL-ERROR-CODE-TITLE       VALUE 'Y'.       CPLNKTCL
014800             15  FILLER          PIC  X(0001).                    CPLNKTCL
014900                 88  XTCL-ERROR-CODE-SUB-LOC     VALUE 'Y'.       CPLNKTCL
015000             15  FILLER          PIC  X(0001).                    CPLNKTCL
015100                 88  XTCL-ERROR-CODE-PAY-REP     VALUE 'Y'.       CPLNKTCL
015200             15  FILLER          PIC  X(0001).                    CPLNKTCL
015300                 88  XTCL-ERROR-CODE-STEP        VALUE 'Y'.       CPLNKTCL
015400             15  FILLER          PIC  X(0001).                    CPLNKTCL
015500                 88  XTCL-ERROR-CODE-GRADE       VALUE 'Y'.       CPLNKTCL
015600             15  FILLER          PIC  X(0001).                    CPLNKTCL
015700                 88  XTCL-ERROR-SHIFT-TYPE       VALUE 'Y'.       CPLNKTCL
015800             15  FILLER          PIC  X(0001).                    CPLNKTCL
015900                 88  XTCL-ERROR-ON-CALL-CODE     VALUE 'Y'.       CPLNKTCL
016000             15  FILLER          PIC  X(0001).                    CPLNKTCL
016100                 88  XTCL-ERROR-RATE-TYPE        VALUE 'Y'.       CPLNKTCL
010400             15  FILLER          PIC  X(0001).                    CPLNKTCL
016300                 88  XTCL-ERROR-CODE-SYSTEM      VALUE 'Y'.       CPLNKTCL
010600             15  FILLER          PIC  X(0001).                    CPLNKTCL
016500                 88  XTCL-ERROR-CODE-NO-TITLES   VALUE 'Y'.       CPLNKTCL
010800             15  FILLER          PIC  X(0016).                    CPLNKTCL
006500         10  XTCL-TITLE-STATUS-CODE                               CPLNKTCL
006600                                 PIC  X(0015).                    CPLNKTCL
016900             88  XTCL-TITLE-EXISTS               VALUE            CPLNKTCL
017000                                          'TSL ROW FOUND  ',      CPLNKTCL
017100                                          'TSL ROW NOT FND',      CPLNKTCL
017200                                          'TPA ROW FOUND  ',      CPLNKTCL
017300                                          'TPA ROW NOT FND',      CPLNKTCL
017400                                          'RATES FOUND    ',      CPLNKTCL
017500                                          'RATES NOT FOUND',      CPLNKTCL
017600                                          'TITLE EXISTS'.         CPLNKTCL
017700             88  XTCL-TITLE-FOUND                VALUE            CPLNKTCL
017800                                          'TITLE EXISTS   '.      CPLNKTCL
017900             88  XTCL-TITLE-NOT-FOUND            VALUE            CPLNKTCL
018000                                          'TITLE NOT FOUND'.      CPLNKTCL
018100             88  XTCL-TSL-ROW-FOUND              VALUE            CPLNKTCL
018200                                          'TSL ROW FOUND  ',      CPLNKTCL
018300                                          'RATES FOUND    ',      CPLNKTCL
018400                                          'RATES NOT FOUND'.      CPLNKTCL
018500             88  XTCL-TSL-ROW-EXISTS             VALUE            CPLNKTCL
018600                                          'TSL ROW FOUND  '.      CPLNKTCL
018700             88  XTCL-TSL-ROW-NOT-FOUND          VALUE            CPLNKTCL
018800                                          'TSL ROW NOT FND'.      CPLNKTCL
018900             88  XTCL-TPA-ROW-FOUND              VALUE            CPLNKTCL
019000                                          'TPA ROW FOUND  ',      CPLNKTCL
019100                                          'RATES FOUND    ',      CPLNKTCL
019200                                          'RATES NOT FOUND'.      CPLNKTCL
019300             88  XTCL-TPA-ROW-EXISTS             VALUE            CPLNKTCL
019400                                          'TPA ROW FOUND  '.      CPLNKTCL
019500             88  XTCL-TPA-ROW-NOT-FOUND          VALUE            CPLNKTCL
019600                                          'TPA ROW NOT FND'.      CPLNKTCL
019700             88  XTCL-TITLE-RATES-FOUND          VALUE            CPLNKTCL
019800                                          'RATES FOUND    '.      CPLNKTCL
019900             88  XTCL-TITLE-RATES-NOT-FOUND      VALUE            CPLNKTCL
020000                                          'RATES NOT FOUND'.      CPLNKTCL
020100             88  XTCL-TITLE-STATUS-UNUSED        VALUE 'UNUSED'.  CPLNKTCL
020200         10  XTCL-DB2-SQLCODE    PIC S9(0009)            COMP.    CPLNKTCL
020300             88  XTCL-DB2-END-OF-CURSOR          VALUE +100.      CPLNKTCL
020400             88  XTCL-DB2-SQLCODE-UNUSED         VALUE ZERO.      CPLNKTCL
020500             88  XTCL-DB2-SUCCESSFUL             VALUE ZERO.      CPLNKTCL
020600             88  XTCL-DB2-TITLE-EXISTS           VALUE ZERO.      CPLNKTCL
020700             88  XTCL-DB2-TITLE-NOT-FOUND        VALUE +100.      CPLNKTCL
020800             88  XTCL-DB2-TIMESTAMP-ERROR        VALUE -818.      CPLNKTCL
020900             88  XTCL-DB2-RESOURCE-UNAVAILABLE   VALUE -904.      CPLNKTCL
012900         10  XTCL-DB2MSG-MESSNO  PIC  X(0005).                    CPLNKTCL
021100             88  XTCL-DB2MSG-MESSNO-UNUSED       VALUE ZERO.      CPLNKTCL
013100         10  XTCL-DB2-STATUS-ERRORS                               CPLNKTCL
013200                                 PIC S9(0005).                    CPLNKTCL
021400             88  XTCL-DB2-STATUS-ERRORS-ZERO     VALUE ZERO.      CPLNKTCL
021500             88  XTCL-DB2-STATUS-ERRORS-EXIST    VALUE +1         CPLNKTCL
013500                                                THRU   +99999.    CPLNKTCL
013600         10  FILLER              PIC  X(0003).                    CPLNKTCL
009300*                                                                 CPLNKTCL
009400     05  XTCL-TITLE-COMMON-INFO.                                  CPLNKTCL
022000             88  XTCL-TITLE-CI-EMPTY             VALUE SPACE.     CPLNKTCL
009600         10  XTCL-TITLE-CODE.                                     CPLNKTCL
009700             15  XTCL-TITLE-CODE-NUMERIC                          CPLNKTCL
009800                                 PIC  9(0004).                    CPLNKTCL
009900         10  XTCL-CI-EFFECTIVE-DATE                               CPLNKTCL
010000                                 PIC  X(0010).                    CPLNKTCL
010100         10  XTCL-TITLE-NAME     PIC  X(0150).                    CPLNKTCL
010200         10  XTCL-TITLE-NAME-ABBREVIATED                          CPLNKTCL
010300                                 PIC  X(0030).                    CPLNKTCL
010400         10  XTCL-PERSONNEL-PROGRAM-CODE                          CPLNKTCL
010500                                 PIC  X(0001).                    CPLNKTCL
023100             88  XTCL-PERSONNEL-PROG-ACADEMIC    VALUE 'A'.       CPLNKTCL
023200             88  XTCL-PERSONNEL-PROG-SSP         VALUE '1'.       CPLNKTCL
023300             88  XTCL-PERSONNEL-PROG-MSP         VALUE '2'.       CPLNKTCL
011100         10  XTCL-FLSA-STATUS-CODE                                CPLNKTCL
011200                                 PIC  X(0001).                    CPLNKTCL
011300         10  XTCL-CTO-OSC.                                        CPLNKTCL
011400             15  XTCL-CTO-OSC-NUMERIC                             CPLNKTCL
011500                                 PIC  9(0003).                    CPLNKTCL
011600         10  XTCL-FOC            PIC  X(0001).                    CPLNKTCL
011700         10  XTCL-FOC-SUBCATEGORY-CODE                            CPLNKTCL
011800                                 PIC  X(0002).                    CPLNKTCL
011900         10  XTCL-RETIREMENT-CODE-1                               CPLNKTCL
012000                                 PIC  X(0001).                    CPLNKTCL
012100         10  XTCL-RETIREMENT-CODE-2                               CPLNKTCL
012200                                 PIC  X(0001).                    CPLNKTCL
012500         10  XTCL-TITLE-ABOLISHED-FLAG                            CPLNKTCL
012600                                 PIC  X(0001).                    CPLNKTCL
024800             88  XTCL-TITLE-NOT-ABOLISHED        VALUE 'N'.       CPLNKTCL
024900             88  XTCL-TITLE-ABOLISHED            VALUE 'Y'.       CPLNKTCL
012900         10  XTCL-TITLE-ABOLISHED-DATE                            CPLNKTCL
013000                                 PIC  X(0010).                    CPLNKTCL
013100         10  XTCL-LAST-CLASS-REVIEW-DATE                          CPLNKTCL
013200                                 PIC  X(0010).                    CPLNKTCL
013300         10  XTCL-TITLE-UNIT-CODE                                 CPLNKTCL
013400                                 PIC  X(0002).                    CPLNKTCL
025600         10  XTCL-RELATED-UNIT   PIC  X(0002).                    CPLNKTCL
013500         10  XTCL-TITLE-SPECIAL-HANDLING                          CPLNKTCL
013600                                 PIC  X(0001).                    CPLNKTCL
013800         10  XTCL-STANDARD-HOURS PIC S9(0002)V9(02).              CPLNKTCL
026000             88  XTCL-STANDARD-HOURS-3750        VALUE 37.50.     CPLNKTCL
014000         10  XTCL-BY-AGREEMENT-FLAG                               CPLNKTCL
014100                                 PIC  X(0001).                    CPLNKTCL
014200         10  XTCL-LINKAGE-CODE-TITLE                              CPLNKTCL
014300                                 PIC  X(0003).                    CPLNKTCL
014400         10  XTCL-USE-OF-TITLE   PIC  X(0001).                    CPLNKTCL
018810         10  XTCL-PAY-REP-ALL-FLAG                                CPLNKTCL
018811                                 PIC  X(0001).                    CPLNKTCL
018820         10  XTCL-SOC-CODE       PIC  X(0003).                    CPLNKTCL
014500         10  XTCL-RESTRICTED-TITLE-FLAG                           CPLNKTCL
014600                                 PIC  X(0001).                    CPLNKTCL
014700         10  XTCL-FROZEN-TITLE-FLAG                               CPLNKTCL
014800                                 PIC  X(0001).                    CPLNKTCL
014900         10  XTCL-FROZEN-TITLE-DATE                               CPLNKTCL
015000                                 PIC  X(0010).                    CPLNKTCL
015100         10  XTCL-OVERTIME-EXEMPT-CODE                            CPLNKTCL
015200                                 PIC  X(0001).                    CPLNKTCL
015300         10  XTCL-PAYSCALE-CODE  PIC  X(0001).                    CPLNKTCL
027800             88  XTCL-PAYSCALE-CODE-ANNUAL       VALUE 'A'.       CPLNKTCL
027900             88  XTCL-PAYSCALE-CODE-HOURLY       VALUE 'H'.       CPLNKTCL
028000             88  XTCL-PAYSCALE-CODE-MONTHLY      VALUE 'M'.       CPLNKTCL
020110         10  FILLER              PIC  X(0018).                    CPLNKTCL
028200         10  XTCL-SIX-MONTH-CODE             PIC  X(01).          CPLNKTCL
028300         10  XTCL-JOB-GROUP-IDENTIFIER       PIC  X(03).          CPLNKTCL
028400         10  XTCL-RATE-LOOKUP-CODE           PIC  X(01).          CPLNKTCL
028500*****        88  XTCL-GRADE-BASED-RATES          VALUE 'C'.       30871401
028510             88  XTCL-GRADE-BASED-RATES          VALUE 'G'.       30871401
028600             88  XTCL-STEP-BASED-RATES           VALUE 'S'.       CPLNKTCL
028700             88  XTCL-MERIT-NO-GRADE             VALUE 'M'.       CPLNKTCL
028800             88  XTCL-NO-RATES                   VALUE 'X'.       CPLNKTCL
028900         10  XTCL-HEALTH-FLAG                PIC  X(01).          CPLNKTCL
029000         10  XTCL-SUPERVISOR-FLG             PIC  X(01).          CPLNKTCL
029100         10  XTCL-FORMER-APS-TITLE-FLAG      PIC  X(01).          CPLNKTCL
029200             88  XTCL-FORMER-APS-TITLE           VALUE 'Y'.       CPLNKTCL
029300     05  XTCL-RETRIEVED-RATES-INFO.                               CPLNKTCL
029400         10  XTCL-NO-PAY-INTRVLS             PIC S9(04)  COMP.    CPLNKTCL
029500         10  XTCL-REGULAR-RATES-FRAME            OCCURS 125.      CPLNKTCL
029600             15  XTCL-PAY-INTERVAL           PIC S9(04)  COMP.    CPLNKTCL
029700             15  XTCL-FRAMED-REG-RATES                   COMP-3.  CPLNKTCL
029800                 20  XTCL-MO-RATE            PIC S9(05)V9(02).    CPLNKTCL
029900                 20  XTCL-HR-RATE            PIC S9(03)V9(04).    CPLNKTCL
030000                 20  XTCL-ALT-RATE           PIC S9(05)V9(02).    CPLNKTCL
030100                 20  XTCL-ANN-RATE           PIC S9(07)V9(02).    CPLNKTCL
030200         10  XTCL-GRADED-RATE-RANGE                      COMP-3.  CPLNKTCL
030300             15  XTCL-SGT-MINIMUM            PIC S9(07)V9(04).    CPLNKTCL
030400             15  XTCL-SGT-MIDPOINT           PIC S9(07)V9(04).    CPLNKTCL
030500             15  XTCL-SGT-MAXIMUM            PIC S9(07)V9(04).    CPLNKTCL
030600         10  XTCL-SHIFT-RATE-TYPE            PIC  X(01).          CPLNKTCL
030700         10  XTCL-SHIFT-RATE                 PIC S9(03)V9(04)     CPLNKTCL
030800                                                         COMP-3.  CPLNKTCL
030900         10  XTCL-ON-CALL-RATE-TYPE          PIC  X(01).          CPLNKTCL
031000         10  XTCL-ON-CALL-RATE               PIC S9(03)V9(04)     CPLNKTCL
031100                                                         COMP-3.  CPLNKTCL
031200     05  XTCL-SUB-LOCATION-INFORMATION           OCCURS 4.        CPLNKTCL
031300         10  XTCL-SUB-LOCATION               PIC  X(02).          CPLNKTCL
031400         10  XTCL-TSL-PAY-REP-INFO               OCCURS 2.        CPLNKTCL
031500             15  XTCL-TSL-PAY-REP-CODE       PIC  X(03).          CPLNKTCL
031600             15  XTCL-TSL-EFFECTIVE-DATE     PIC  X(10).          CPLNKTCL
031700             15  XTCL-TSL-FLSA-STATUS-CODE   PIC  X(01).          CPLNKTCL
031800             15  XTCL-TSL-OVERTIME-EXEMPT-CODE                    CPLNKTCL
031900                                             PIC  X(01).          CPLNKTCL
032000             15  XTCL-TSL-SIX-MONTH-CODE     PIC  X(01).          CPLNKTCL
032100             15  XTCL-TSL-RATE-LOOKUP-CODE   PIC  X(01).          CPLNKTCL
032200             15  XTCL-TSL-JOB-GROUP-ID       PIC  X(03).          CPLNKTCL
032300         10  XTCL-TGB-PAY-REP-INFO               OCCURS 2.        CPLNKTCL
032400             15  XTCL-TGB-PAY-REP-CODE       PIC  X(03).          CPLNKTCL
032500             15  XTCL-TGB-DETAIL-DATA            OCCURS 99.       CPLNKTCL
032600                 20  XTCL-TGB-GRADE-TYPE     PIC  X(02).          CPLNKTCL
032700                 20  XTCL-TGB-GRADE          PIC  X(02).          CPLNKTCL
032800                 20  XTCL-TGB-EFFECTIVE-DATE PIC  X(10).          CPLNKTCL
032900                 20  XTCL-SGT-RANGE-MIN      PIC S9(07)V99        CPLNKTCL
033000                                                         COMP-3.  CPLNKTCL
033100                 20  XTCL-SGT-RANGE-MID      PIC S9(07)V99        CPLNKTCL
033200                                                         COMP-3.  CPLNKTCL
033300                 20  XTCL-SGT-RANGE-MAX      PIC S9(07)V99        CPLNKTCL
033400                                                         COMP-3.  CPLNKTCL
033500         10  XTCL-TSB-PAY-REP-INFO               OCCURS 2.        CPLNKTCL
033600             15  XTCL-TSB-PAY-REP-CODE       PIC  X(03).          CPLNKTCL
033700             15  XTCL-TSB-EFFECTIVE-DATE     PIC  X(10).          CPLNKTCL
033800             15  XTCL-TSB-END-DATE           PIC  X(10).          CPLNKTCL
033900             15  XTCL-TSB-RANGE-ADJ-ID       PIC  X(12).          CPLNKTCL
034000             15  XTCL-TSB-NO-STEPS           PIC S9(04)  COMP.    CPLNKTCL
034100             15  XTCL-TSB-NO-PAY-INTRVLS     PIC S9(04)  COMP.    CPLNKTCL
034200             15  XTCL-TSB-SSP-SS-EXCPT       PIC  X(01).          CPLNKTCL
034300             15  XTCL-TSB-MERIT-BASED        PIC  X(01).          CPLNKTCL
034400             15  XTCL-TSB-MEMO-GRADE         PIC  X(02).          CPLNKTCL
034500             15  XTCL-TRR-RATES                  OCCURS 125.      CPLNKTCL
034600                 20  XTCL-TRR-PAY-INTERVAL   PIC S9(04)  COMP.    CPLNKTCL
034700                 20  XTCL-TRR-MO-RATE        PIC S9(05)V99        CPLNKTCL
034800                                                         COMP-3.  CPLNKTCL
034900                 20  XTCL-TRR-HR-RATE        PIC S9(03)V9(04)     CPLNKTCL
035000                                                         COMP-3.  CPLNKTCL
035100                 20  XTCL-TRR-ALT-RATE       PIC S9(05)V99        CPLNKTCL
035200                                                         COMP-3.  CPLNKTCL
035300         10  XTCL-TSR-PAY-REP-INFO               OCCURS 2.        CPLNKTCL
035400             15  XTCL-TSR-PAY-REP-CODE       PIC  X(03).          CPLNKTCL
035500             15  XTCL-TSR-EFFECTIVE-DATE     PIC  X(10).          CPLNKTCL
035600             15  XTCL-TSR-END-DATE           PIC  X(10).          CPLNKTCL
035700             15  XTCL-TSR-RANGE-ADJ-ID       PIC  X(12).          CPLNKTCL
035800             15  XTCL-TSR-RATE-DATA              OCCURS 7.        CPLNKTCL
035900                 20  XTCL-TSR-SHIFT-TYPE     PIC  X(02).          CPLNKTCL
036000                 20  XTCL-TSR-RATE-TYPE      PIC  X(01).          CPLNKTCL
036100                 20  XTCL-TSR-SHIFT-RATE     PIC S9(03)V9(04)     CPLNKTCL
036200                                                         COMP-3.  CPLNKTCL
036300         10  XTCL-TOR-PAY-REP-INFO               OCCURS 2.        CPLNKTCL
036400             15  XTCL-TOR-PAY-REP-CODE       PIC  X(03).          CPLNKTCL
036500             15  XTCL-TOR-EFFECTIVE-DATE     PIC  X(10).          CPLNKTCL
036600             15  XTCL-TOR-END-DATE           PIC  X(10).          CPLNKTCL
036700             15  XTCL-TOR-RANGE-ADJ-ID       PIC  X(12).          CPLNKTCL
036800             15  XTCL-TOR-RATE-DATA              OCCURS 7.        CPLNKTCL
036900                 20  XTCL-TOR-ON-CALL-CODE   PIC  X(03).          CPLNKTCL
037000                 20  XTCL-TOR-RATE-TYPE      PIC  X(01).          CPLNKTCL
037100                 20  XTCL-TOR-ON-CALL-RATE   PIC S9(03)V9(04)     CPLNKTCL
037200                                                         COMP-3.  CPLNKTCL
015900     05  XTCL-PPGM-COMMON-INFO   PIC  X(0050).                    CPLNKTCL
037400         88  XTCL-PPGM-COMMON-INFO-EMPTY         VALUE SPACE.     CPLNKTCL
016200     05  FILLER                  REDEFINES XTCL-PPGM-COMMON-INFO. CPLNKTCL
016300         10  XTCL-ACAD-CI-EFFECTIVE-DATE                          CPLNKTCL
016400                                 PIC  X(0010).                    CPLNKTCL
016500         10  XTCL-ACAD-RANK      PIC  X(0001).                    CPLNKTCL
016600         10  XTCL-ACAD-SABB-ELIG-CODE                             CPLNKTCL
016700                                 PIC  X(0001).                    CPLNKTCL
016800         10  XTCL-ACAD-SERVICE-LIMIT                              CPLNKTCL
016900                                 PIC S9(0004).                    CPLNKTCL
017000         10  XTCL-ACAD-APPT-TYPE-CODE                             CPLNKTCL
017100                                 PIC  X(0001).                    CPLNKTCL
017200         10  XTCL-ACAD-APPT-BASIS-CODE                            CPLNKTCL
017300                                 PIC  X(0001).                    CPLNKTCL
017400         10  XTCL-ACAD-PAY-PERIOD                                 CPLNKTCL
017500                                 PIC  X(0001).                    CPLNKTCL
038900             88  XTCL-ACAD-PAY-PERIOD-NOT-APPL   VALUE '0'.       CPLNKTCL
039000             88  XTCL-ACAD-PAY-PERIOD-09-MONTHS  VALUE '1'.       CPLNKTCL
039100             88  XTCL-ACAD-PAY-PERIOD-12-MONTHS  VALUE '2'.       CPLNKTCL
017900         10  XTCL-ACAD-SICK-LEAVE-CODE                            CPLNKTCL
018000                                 PIC  X(0001).                    CPLNKTCL
018100         10  XTCL-ACAD-HSCP-FLAG PIC  X(0001).                    CPLNKTCL
018200         10  XTCL-ACAD-OFF-SCALE-FLAG                             CPLNKTCL
018300                                 PIC  X(0001).                    CPLNKTCL
018400         10  FILLER              PIC  X(0028).                    CPLNKTCL
022200     05  XTCL-RATE-INFORMATION.                                   CPLNKTCL
039900             88  XTCL-RATE-INFORMATION-EMPTY     VALUE SPACE.     CPLNKTCL
022400         10  XTCL-RATE-SET-INDEXES.                               CPLNKTCL
022900             15  XTCL-RSET-INDEX-COV                              CPLNKTCL
040200                                 PIC S9(0004)            COMP.    CPLNKTCL
040300                 88  XTCL-RSET-INDEX-COV-UNUSED  VALUE ZERO.      CPLNKTCL
040400                 88  XTCL-RSET-INDEX-COV-USED    VALUE +1.        CPLNKTCL
023300             15  XTCL-RSET-INDEX-UNC                              CPLNKTCL
040600                                 PIC S9(0004)            COMP.    CPLNKTCL
040700                 88  XTCL-RSET-INDEX-UNC-UNUSED  VALUE ZERO.      CPLNKTCL
040800                 88  XTCL-RSET-INDEX-UNC-USED    VALUE +1, +2.    CPLNKTCL
040900         10  XTCL-RATE-SET-GROUP                 OCCURS 2         CPLNKTCL
024000                                 INDEXED  BY XTCL-RATE-SET-INDEX. CPLNKTCL
041100                 88  XTCL-RATE-SET-GROUP-EMPTY   VALUE SPACE.     CPLNKTCL
024200             15  XTCL-RATE-SET.                                   CPLNKTCL
024300                 20  XTCL-RSET-EFFECTIVE-DATE                     CPLNKTCL
024400                                 PIC  X(0010).                    CPLNKTCL
028810                 20  XTCL-RSET-EFFECT-END-DATE                    CPLNKTCL
028820                                 PIC  X(0010).                    CPLNKTCL
024500                 20  XTCL-PAY-REP-CODE                            CPLNKTCL
024600                                 PIC  X(0003).                    CPLNKTCL
024700                 20  XTCL-RANGE-ADJ-ID                            CPLNKTCL
024800                                 PIC  X(0012).                    CPLNKTCL
029310                 20  FILLER      PIC  X(0065).                    CPLNKTCL
025100             15  FILLER          REDEFINES XTCL-RATE-SET.         CPLNKTCL
029610                 20  FILLER      PIC  X(0035).                    CPLNKTCL
025300                 20  XTCL-ACAD-NO-STEPS                           CPLNKTCL
025400                                 PIC S9(0004).                    CPLNKTCL
025500                 20  XTCL-ACAD-NO-PAY-INTERVALS                   CPLNKTCL
025600                                 PIC S9(0004).                    CPLNKTCL
025700                 20  XTCL-ACAD-COMP-GROUP                         CPLNKTCL
025800                                 PIC  X(0003).                    CPLNKTCL
030310                 20  FILLER      PIC  X(0054).                    CPLNKTCL
027100             15  XTCL-RATE-INDEXES.                               CPLNKTCL
027200                 20  XTCL-RATE-INDEX-PPGM-FIRST                   CPLNKTCL
043300                                 PIC S9(0004)            COMP.    CPLNKTCL
027400                     88  XTCL-RATE-INDEX-PPGM-UNUSED              CPLNKTCL
043500                                                 VALUE ZERO.      CPLNKTCL
027600                     88  XTCL-RATE-INDEX-PPGM-USED                CPLNKTCL
043700                                                 VALUE +1         CPLNKTCL
027800                                                THRU   +0125.     CPLNKTCL
027900                 20  XTCL-RATE-INDEX-PPGM-LAST                    CPLNKTCL
044000                                 PIC S9(0004)            COMP.    CPLNKTCL
044100             15  XTCL-RATE-GROUP                 OCCURS 125       CPLNKTCL
030000                                 INDEXED  BY XTCL-RATE-INDEX.     CPLNKTCL
030100                 20  XTCL-RATE.                                   CPLNKTCL
030200                     25  XTCL-RATE-RS-EFFECTIVE-DATE              CPLNKTCL
030300                                 PIC  X(0010).                    CPLNKTCL
034710                     25  XTCL-RATE-RS-EFFECT-END-DATE             CPLNKTCL
034720                                 PIC  X(0010).                    CPLNKTCL
030400                     25  XTCL-RATE-PAY-REP-CODE                   CPLNKTCL
030500                                 PIC  X(0003).                    CPLNKTCL
030600                     25  XTCL-RATE-RANGE-ADJ-ID                   CPLNKTCL
030700                                 PIC  X(0012).                    CPLNKTCL
031000                     25  FILLER  PIC  X(0065).                    CPLNKTCL
031100                 20  FILLER      REDEFINES XTCL-RATE.             CPLNKTCL
031200                     25  FILLER  PIC  X(0035).                    CPLNKTCL
031300                     25  XTCL-RATE-PAY-ID.                        CPLNKTCL
031400                         30  FILLER                               CPLNKTCL
031500                                 PIC  X(0001).                    CPLNKTCL
031600                         30  XTCL-RATE-TOC-TYPE                   CPLNKTCL
031700                                 PIC  X(0001).                    CPLNKTCL
031800                         30  FILLER                               CPLNKTCL
031900                                 PIC  X(0001).                    CPLNKTCL
032000                     25  XTCL-RATE-PAY-INTERVAL                   CPLNKTCL
032100                                 REDEFINES XTCL-RATE-PAY-ID       CPLNKTCL
032200                                 PIC S9(0003).                    CPLNKTCL
032300                     25  FILLER  PIC  X(0002).                    CPLNKTCL
046600                     25  XTCL-RATE-PAY-RATE                       CPLNKTCL
046700                                 PIC S9(0007)V9(06).              CPLNKTCL
046800                     25  XTCL-RATE-MO-RATE                        CPLNKTCL
046900                                 PIC S9(0007)V9(06).              CPLNKTCL
047000                     25  XTCL-RATE-HR-RATE                        CPLNKTCL
047100                                 PIC S9(0007)V9(06).              CPLNKTCL
047200                     25  XTCL-RATE-ALT-RATE                       CPLNKTCL
047300                                 PIC S9(0007)V9(06).              CPLNKTCL
047400                     25  FILLER  PIC  X(0008).                    CPLNKTCL
033000     05  FILLER                  PIC  X(0025).                    CPLNKTCL
047600         88  XTCL-PPTCTUTL-INTERFACE-ENDS        VALUE            CPLNKTCL
033200                                 '<PPTCTUTL-INTERFACE ENDS>'.     CPLNKTCL
