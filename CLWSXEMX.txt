000100     EJECT
000800*==========================================================%      DS1302
000200*=    COPYLIB: CLWSXEMX                                   =%      DS1302
000300*=    CHANGE   #DS1302      PROJ. REQUEST: PAYROLL RELEASE=%      DS1302
000400*=    NAME: R. MERRYMAN     MODIFICATION DATE: 11/09/2001 =%      DS1302
000500*=                                                        =%      DS1302
000600*=    DESCRIPTION:                                        =%      DS1302
000700*=    ADD WCE-VETERAN-STATUS CODE TO XEMX AND XAPX        =%      DS1302
000700*=    RECORDS.                                            =%      DS1302
000800*==========================================================%      DS1302
000200*    COPYID=CLWSXEMX
000300     SKIP1
000400*01  XEMX-EMPL-EXTRACT-RECORD      PICTURE  X(400).            ***
000500*
000600*
000700     05  XEMX-EMPLOYEE-ID              PICTURE  X(9).
000800*
000900*
001000     05  XEMX-RECORD-INFO.
001100*
001200         10  XEMX-RECORD-TYPE              PICTURE  X(1).
001300             88  XEMX-RECORD-EMPLOYEE          VALUE  'E'.
001400             88  XEMX-RECORD-EXTERNAL          VALUE  'X'.
001500         10  XEMX-RECORD-CHANGE-STATUS     PICTURE  X(1).
001600             88  XEMX-RECORD-CHG-NO-CHANGE     VALUE  ' '.
001700             88  XEMX-SOURCE-CHG-ADDED         VALUE  '+'.
001800             88  XEMX-SOURCE-CHG-DELETED       VALUE  '-'.
001900             88  XEMX-SOURCE-CHG-ALTERED       VALUE  '*'.
002000             88  XEMX-SOURCE-CHG-ID-CHANGE     VALUE  '#'.
002100         10  FILLER                        PICTURE  X(2).
002100         10  XEMX-EMPLOYEE-ID-PREVIOUS     PICTURE  X(9).
002300*
002400*
002500     05  XEMX-EMPLOYEE-INFO.
002600*
002700         10  XEMX-EMPL-STATUS-INFO.
002800             15  XEMX-EMP-NAME                 PICTURE  X(26).
002900             15  XEMX-EMP-STATUS               PICTURE  X(1).
003000                 88  XEMX-EMP-STATUS-ACTIVE        VALUE  'A'.
003100                 88  XEMX-EMP-STATUS-INACTIVE      VALUE  'I'.
003200                 88  XEMX-EMP-STATUS-SEPARATED     VALUE  'S'.
003300                 88  XEMX-EMP-STATUS-LWOP          VALUE  'N'.
003400                 88  XEMX-EMP-STATUS-LWP           VALUE  'P'.
003500             15  XEMX-LAST-ACTION-DATE-X.
003600                 20  XEMX-LAST-ACTION-CENTURY      PICTURE  9(2).
003700                 20  XEMX-LAST-ACTION-DATE.
003800                     25  XEMX-LAST-ACTION-YEAR     PICTURE  9(2).
003900                     25  XEMX-LAST-ACTION-MONTH    PICTURE  9(2).
004000                     25  XEMX-LAST-ACTION-DAY      PICTURE  9(2).
004100             15  XEMX-PAF-GEN-NUM              PICTURE  9(4).
004200             15  XEMX-HIRE-DATE-X.
004300                 20  XEMX-HIRE-CENTURY             PICTURE  9(2).
004400                 20  XEMX-HIRE-DATE.
004500                     25  XEMX-HIRE-YEAR            PICTURE  9(2).
004600                     25  XEMX-HIRE-MONTH           PICTURE  9(2).
004700                     25  XEMX-HIRE-DAY             PICTURE  9(2).
004100             15  XEMX-SEPARATION-REASON        PICTURE  X(2).
004200             15  XEMX-SEPARATION-DATE-X.
004300                 20  XEMX-SEPARATION-CENTURY       PICTURE  9(2).
004400                 20  XEMX-SEPARATION-DATE.
004500                     25  XEMX-SEPARATION-YEAR      PICTURE  9(2).
004600                     25  XEMX-SEPARATION-MONTH     PICTURE  9(2).
004700                     25  XEMX-SEPARATION-DAY       PICTURE  9(2).
005500             15  XEMX-LOA-TYPE-CODE            PICTURE  X(2).
005600             15  XEMX-LOA-STATUS-IND           PICTURE  X(1).
005700             15  XEMX-LOA-BEGIN-DATE-X.
005800                 20  XEMX-LOA-BEGIN-CENTURY        PICTURE  9(2).
005900                 20  XEMX-LOA-BEGIN-DATE.
006000                     25  XEMX-LOA-BEGIN-YEAR       PICTURE  9(2).
006100                     25  XEMX-LOA-BEGIN-MONTH      PICTURE  9(2).
006200                     25  XEMX-LOA-BEGIN-DAY        PICTURE  9(2).
006300             15  XEMX-LOA-RETURN-DATE-X.
006400                 20  XEMX-LOA-RETURN-CENTURY       PICTURE  9(2).
006500                 20  XEMX-LOA-RETURN-DATE.
006600                     25  XEMX-LOA-RETURN-YEAR      PICTURE  9(2).
006700                     25  XEMX-LOA-RETURN-MONTH     PICTURE  9(2).
006800                     25  XEMX-LOA-RETURN-DAY       PICTURE  9(2).
006900             15  XEMX-NEXT-SALARY-REV          PICTURE  X(1).
007000             15  XEMX-NEXT-SALREV-DATE-X.
007100                 20  XEMX-NEXT-SALREV-CENTURY      PICTURE  9(2).
007200                 20  XEMX-NEXT-SALREV-DATE.
007300                     25  XEMX-NEXT-SALREV-YEAR     PICTURE  9(2).
007400                     25  XEMX-NEXT-SALREV-MONTH    PICTURE  9(2).
007500                     25  XEMX-NEXT-SALREV-DAY      PICTURE  9(2).
007600             15  FILLER                        PICTURE  X(8).
005000*
005100         10  XEMX-EMPL-PERSONAL-INFO.
005200             15  XEMX-BIRTH-DATE-X.
005300                 20  XEMX-BIRTH-CENTURY            PICTURE  9(2).
005400                 20  XEMX-BIRTH-DATE.
005500                     25  XEMX-BIRTH-YEAR           PICTURE  9(2).
005600                     25  XEMX-BIRTH-MONTH          PICTURE  9(2).
005700                     25  XEMX-BIRTH-DAY            PICTURE  9(2).
005800             15  XEMX-SEX-CODE                 PICTURE  X(1).
005900             15  XEMX-ETHNIC-ID                PICTURE  X(1).
006000             15  XEMX-HANDICAP-STATUS          PICTURE  X(1).
006100             15  XEMX-DISABLED-STATUS          PICTURE  X(1).
006200             15  XEMX-VIET-VET-STATUS          PICTURE  X(1).
006200             15  XEMX-WCE-VET-STATUS           PICTURE  X(1).     DS1302
009000*            15  FILLER                        PICTURE  X(3).     DS1302
009000             15  FILLER                        PICTURE  X(2).     DS1302
006400*
006500         10  XEMX-EMPL-ORGANIZATIONAL-INFO.
006600             15  XEMX-HOME-DEPT                PICTURE  X(6).
006700             15  XEMX-VICE-CHAN-UNIT           PICTURE  X(2).
006800             15  XEMX-COLLEGE-CODE             PICTURE  X(3).
006900             15  XEMX-SURE-MAILDROP            PICTURE  X(5).
007000             15  XEMX-STUDENT-STATUS           PICTURE  X(1).
007100             15  XEMX-EMP-REL-CODE             PICTURE  X(1).
007200             15  XEMX-EMP-CBUC                 PICTURE  X(2).
007300             15  XEMX-EMP-ORGN-DISCLOSURE      PICTURE  X(1).
007400             15  XEMX-ACAD-SENATE-STATUS       PICTURE  X(1).
010200             15  FILLER                        PICTURE  X(4).
007600*
007700         10  XEMX-EMPL-PAYROLL-INFO.
007800             15  XEMX-SOC-SEC-NUM              PICTURE  X(9).
007900             15  XEMX-CITIZENSHIP-CODE         PICTURE  X(1).
008000             15  XEMX-VISA-TYPE                PICTURE  X(2).
008100             15  XEMX-RETIRE-ELIG-CODE         PICTURE  X(1).
008200             15  XEMX-FICA-ELIG-CODE           PICTURE  X(1).
008300             15  XEMX-BELI-IND-ASSIGNED        PICTURE  X(1).
008400             15  XEMX-BELI-IND-DERIVED         PICTURE  X(1).
008500             15  XEMX-PRIMARY-PAY-SCHED        PICTURE  X(2).
008600             15  XEMX-LEAVE-CODE               PICTURE  X(1).
008700             15  XEMX-YTD-GROSS-STATUS         PICTURE  X(1).
008800             15  XEMX-PCT-TIME-WORKED          PICTURE  9(1)V9(2).
008900             15  FILLER                        PICTURE  X(7).
009000*
009100*
009200     05  XEMX-APPOINTMENT-INFO.
009300*
009400         10  XEMX-APPT-COUNT               PICTURE  9(1).
009500*
009600         10  XEMX-APPT-ENTRY               OCCURS  2 .
012400             15  XEMX-APPT-ENTRY-KEY.
012500                 20  XEMX-TITLE-CODE               PICTURE  9(4).
012600                 20  XEMX-APPT-DEPT                PICTURE  X(6).
012700                 20  XEMX-APPT-TYPE                PICTURE  X(1).
012800             15  XEMX-APPT-MISC-GROUP.
012900                 20  XEMX-APPT-PRIORITY            PICTURE  X(1).
013000                     88  XEMX-APPT-PRI-REG             VALUE  '1'.
013100                     88  XEMX-APPT-PRI-NON-REG         VALUE  '2'.
013200                     88  XEMX-APPT-PRI-BYA             VALUE  '3'.
013300                     88  XEMX-APPT-PRI-WOS             VALUE  '4'.
013400                 20  XEMX-APPT-PERS-PROG           PICTURE  X(1).
013500                 20  XEMX-ACAD-BASIS               PICTURE  9(2).
013600                 20  XEMX-ACAD-PAID-OVER           PICTURE  9(2).
013700                 20  XEMX-STEP-GRADE               PICTURE  X(3).
013800                 20  XEMX-STEP-OVER-ABOVE          PICTURE  X(1).
013900                 20  XEMX-APPT-WORK-STUDY          PICTURE  X(1).
014000             15  XEMX-TITLE-INFO-GROUP.
014100                 20  XEMX-TITLE-UNIT-CODE          PICTURE  X(2).
014200                 20  XEMX-TITLE-ACAD-RANK          PICTURE  X(1).
014300                 20  XEMX-TITLE-CTO                PICTURE  X(3).
014400                 20  XEMX-TITLE-JOB-GROUP          PICTURE  X(3).
014500             15  XEMX-APPT-RATE-FTE-GROUP.
014600                 20  XEMX-APPT-RATE            PICTURE  9(5)V9(2).
014700                 20  XEMX-APPT-RATE-CODE           PICTURE  X(1).
014800                     88  XEMX-RATE-MONTHLY             VALUE  'M'.
014900                     88  XEMX-RATE-HOURLY              VALUE  'H'.
015000                     88  XEMX-RATE-BYA                 VALUE  'B'.
015100                 20  XEMX-APPT-ANNUAL-SALARY       PICTURE  9(6).
015200                 20  XEMX-APPT-FTE             PICTURE  9(1)V9(2).
015300                 20  XEMX-APPT-FTE-BASE            PICTURE  X(1).
015400                     88  XEMX-FTE-BASE-SALARY          VALUE  'S'.
015500                     88  XEMX-FTE-BASE-FIXED           VALUE  'F'.
015600                     88  XEMX-FTE-BASE-VAR             VALUE  'V'.
015700             15  XEMX-APPT-FUNDING-GROUP.
015800                 20  XEMX-APPT-FUNDING             PICTURE  X(6).
015900                 20  XEMX-APPT-FUNDING-TYPE        PICTURE  X(1).
016000                     88  XEMX-FUNDING-TYPE-STATE-1     VALUE  'S'.
016100                     88  XEMX-FUNDING-TYPE-STATE-2     VALUE  'T'.
016200                     88  XEMX-FUNDING-TYPE-HOSP        VALUE  'H'.
016300                     88  XEMX-FUNDING-TYPE-OTHER       VALUE  'O'.
016400                 20  XEMX-APPT-FUNDING-MULT        PICTURE  X(1).
016500                     88  XEMX-MULTIPLE-FUNDING         VALUE  '*'.
014600             15  FILLER                        PICTURE  X(3).
012000*
012100*
012200     05  XEMX-ADDRESS-INFO.
012300*
012400         10  XEMX-ADDRESS-LINE-1           PICTURE  X(30).
012500         10  XEMX-ADDRESS-LINE-2           PICTURE  X(30).
012600         10  XEMX-ADDRESS-CITY             PICTURE  X(21).
012700         10  XEMX-ADDRESS-STATE            PICTURE  X(2).
012800         10  XEMX-ADDRESS-ZIP-X.
012900             15  XEMX-ADDRESS-ZIP              PICTURE  X(5).
013000             15  XEMX-ADDRESS-ZIP-4            PICTURE  X(4).
