000100*                                                                 CLPDP795
000200*      CONTROL REPORT PPP795CR                                    CLPDP795
000300*                                                                 CLPDP795
000400*                                                                 CLPDP795
000500 PRINT-CONTROL-REPORT SECTION.                                    CLPDP795
000600                                                                  CLPDP795
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP795
000800                                                                  CLPDP795
000900     MOVE 'PPP795CR'             TO CR-HL1-RPT.                   CLPDP795
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP795
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP795
001200                                                                  CLPDP795
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP795
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP795
001500                                                                  CLPDP795
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP795
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP795
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP795
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP795
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP795
002100                                                                  CLPDP795
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP795
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP795
002400                                                                  CLPDP795
002500     MOVE SPEC-CARD              TO CR-DL5-CTL-CARD.              CLPDP795
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP795
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP795
002800                                                                  CLPDP795
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP795
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP795
003100                                                                  CLPDP795
004200*****ACCEPT TIME-WORK-AREA FROM TIME.                             DS1178
004300     COPY CPPDTIME.                                               DS1178
004400     MOVE PRE-EDIT-TIME          TO TIME-WORK-AREA.               DS1178
003400     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP795
003500     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP795
003600     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP795
003700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP795
003800                                                                  CLPDP795
003900     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP795
004000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP795
004100*                                                                 CLPDP795
004200*  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP795
004300*                                                                 CLPDP795
004400*  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP795
004500*  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP795
004600*                                                                 CLPDP795
004700     MOVE 'TXAFILE     '         TO CR-DL9-FILE.                  CLPDP795
004800     MOVE SO-CREATED             TO CR-DL9-ACTION.                CLPDP795
004900     MOVE WS-SRT-COUNT           TO CR-DL9-VALUE.                 CLPDP795
005000     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP795
005100     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP795
005200*                                                                 CLPDP795
005300*                                                                 CLPDP795
005400*                                                                 CLPDP795
005500     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP795
005600     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP795
005700                                                                  CLPDP795
005800     CLOSE CONTROLREPORT.                                         CLPDP795
