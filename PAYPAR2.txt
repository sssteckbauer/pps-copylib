      **************************************************************/    JAK0427
      *  COPYMEMBER: PAYPAR2                                       */    JAK0427
      *                                                            */    JAK0427
      *  RELEASE # ____0427____ SERVICE REQUEST NO(S)______________*/    JAK0427
      *  NAME ___KALBERER____   MODIFICATION DATE ____06/11/90_____*/    JAK0427
      *                                                            */    JAK0427
      *  DESCRIPTION -                                             */    JAK0427
      *                                                            */    JAK0427
      *   - ADDED BRSC TO THE DEDUCTION ARRAY INFORMATION          */    JAK0427
      *     WHICH CHANGED THE RECORD SIZE FROM 607 TO 717 BYTES    */    JAK0427
      *                                                            */    JAK0427
      **************************************************************/    JAK0427
      *    COPYID=PAYPAR2                                                PAYPAR2
      *                                                                * PAYPAR2
      ****************************************************************** PAYPAR2
      *    PAR SCREEN 2  RECORD                                        * PAYPAR2
      ****************************************************************** PAYPAR2
                                                                         PAYPAR2
        01  PAR-SCREEN-RECORD-2.                                         PAYPAR2
                                                                         PAYPAR2
           03  PS2R-KEY.                                                 PAYPAR2
                                                                         PAYPAR2
               05  PS2R-ID-NO              PICTURE X(9).                 PAYPAR2
               05  PS2R-EMP-NR REDEFINES PS2R-ID-NO.                     PAYPAR2
                   10  FILLER              PICTURE XX.                   PAYPAR2
                   10  PS2R-EMPLOYEE-NR    PICTURE X(7).                 PAYPAR2
                                                                         PAYPAR2
               05  PS2R-CYCLE.                                           PAYPAR2
                   07  PS2R-CYCLE-YR       PICTURE XX.                   PAYPAR2
                   07  PS2R-CYCLE-MO       PICTURE XX.                   PAYPAR2
                   07  PS2R-CYCLE-CYCLE    PICTURE XX.                   PAYPAR2
                                                                         PAYPAR2
                                                                         PAYPAR2
               05  PS2R-SEQ-NO             PICTURE 99.                   PAYPAR2
                                                                         PAYPAR2
               05  PS2R-RECORD-NO          PICTURE 99.                   PAYPAR2
                                                                         PAYPAR2
      ****************************************************************   PAYPAR2
      *              FIXED DATA PORTION FOLLOWS                      *   PAYPAR2
      ****************************************************************   PAYPAR2
                                                                         PAYPAR2
                                                                         PAYPAR2
      *                                                                * PAYPAR2
      ****************************************************************** PAYPAR2
      *            D E D U C T I O N S,   C O N T R I B U T I O N S    * PAYPAR2
      ****************************************************************** PAYPAR2
      *                                                                * PAYPAR2
                                                                         PAYPAR2
           03  PS2R-FILLER.                                              PAYPAR2
               05  PS2R-DEDUCTN-ARRAY.                                   PAYPAR2
                 06  PS2R-CUR-DED-REFS                                   PAYPAR2
                                       OCCURS 22  TIMES.                 PAYPAR2
                                                                         PAYPAR2
                                                                         PAYPAR2
                   07  PS2R-CUR-DED-CODE   PICTURE 9(3).                 PAYPAR2
                   07  PS2R-GTN-TYPE       PICTURE X.                    PAYPAR2
                       88  PS2R-GTN-DEDUCTION              VALUE 'D'.    PAYPAR2
                       88  PS2R-GTN-REDUCTION              VALUE 'R'.    PAYPAR2
                       88  PS2R-GTN-CONTRIBUTION           VALUE 'C'.    PAYPAR2
                                                                         PAYPAR2
                   07  PS2R-GTN-GROUP      PICTURE X.                    PAYPAR2
                       88  PS2R-GTN-GRP-TAX                VALUE 'T'.    PAYPAR2
                       88  PS2R-GTN-GRP-RETIREMENT         VALUE 'R'.    PAYPAR2
                       88  PS2R-GTN-GRP-INSURANCE          VALUE 'I'.    PAYPAR2
                       88  PS2R-GTN-GRP-MISC-DED           VALUE 'M'.    PAYPAR2
                                                                         PAYPAR2
                   07  PS2R-SPEC-TRANS     PICTURE X.                    PAYPAR2
                       88  PS2R-NORMAL-DED-CONTRIB         VALUE ' '.    PAYPAR2
                       88  PS2R-ONE-TIME-DED               VALUE 'O'.    PAYPAR2
                       88  PS2R-REFUND                     VALUE 'R'.    PAYPAR2
                                                                         PAYPAR2
                   07  PS2R-SOURCE-CODE    PICTURE X.                    PAYPAR2
                       88  PS2R-NORMAL-SOURCE              VALUE ' '.    PAYPAR2
                       88  PS2R-PREPAYMENT                 VALUE 'A'.    PAYPAR2
                       88  PS2R-DED-FRM-PREPAYMNT          VALUE 'P'.    PAYPAR2
                       88  PS2R-DED-SUSPENDED              VALUE 'S'.    PAYPAR2
                       88  PS2R-SUSP-RECVBLE               VALUE 'R'.    PAYPAR2
                       88  PS2R-DED-NOT-TAKEN              VALUE 'N'.    PAYPAR2
                       88  PS2R-DED-FRM-PREV-SUSP-BAL      VALUE 'D'.    PAYPAR2
                       88  PS2R-DED-FRM-SUSP-RECVBLE       VALUE 'V'.    PAYPAR2
                                                                         PAYPAR2
      *                                                                * PAYPAR2
      *****                                                        ***** PAYPAR2
      *        CODES:  BLANK   =   NORMAL                              * PAYPAR2
      *                    A   =   PREPAYMENT                          * PAYPAR2
      *                    P   =   DEDUCTION FROM A PREPAYMENT         * PAYPAR2
      *                    S   =   DEDUCTION WAS SUSPENDED             * PAYPAR2
      *                    R   =   DEDUCTION WAS SUSPENDED AS A        * PAYPAR2
      *                            RECEIVABLE                          * PAYPAR2
      *                    N   =   DEDUCTION NOT TAKEN                 * PAYPAR2
      *                    D   =   DEDUCTION WAS TAKEN FROM A          * PAYPAR2
      *                            PREVIOUSLY SUSPENDED BALANCE        * PAYPAR2
      *                    V   =   DEDUCTION WAS TAKEN FROM A          * PAYPAR2
      *                            PREVIOUSLY SUSPENDED RECEIVABLE     * PAYPAR2
      *****                                                        ***** PAYPAR2
      *                                                                * PAYPAR2
                                                                         PAYPAR2
                   07  PS2R-DED-QTR-CODE   PICTURE X.                    PAYPAR2
                   07  PS2R-DED-YR-CODE    PICTURE X.                    PAYPAR2
                   07  PS2R-DED-FILLER     PICTURE X.                    PAYPAR2
                   07  PS2R-CUR-AMT        PICTURE S9(5)V99    COMP-3.   PAYPAR2
                   07  PS2R-BRSC           PICTURE X(5).                 JAK0427
                                                                         PAYPAR2
                                                                         PAYPAR2
                                                                         PAYPAR2
      *                                                                  PAYPAR2
      ****************************************************************** PAYPAR2
      *            H O U R S   A D J U S T M E N T S                   * PAYPAR2
      ****************************************************************** PAYPAR2
      *                                                                * PAYPAR2
                                                                         PAYPAR2
               05  PS2R-HRS-ADJS-ARRAY.                                  PAYPAR2
                                                                         PAYPAR2
                 06  PS2R-HRS-ADJUSTMENTS                                PAYPAR2
                               OCCURS  10 TIMES.                         PAYPAR2
                                                                         PAYPAR2
                                                                         PAYPAR2
                   07  PS2R-HRS-CODE       PICTURE 9(4).                 PAYPAR2
                   07  PS2R-HRS-ADJ        PICTURE S9(5)V99    COMP-3.   PAYPAR2
                                                                         PAYPAR2
                                                                         PAYPAR2
                                                                         PAYPAR2
                                                                         PAYPAR2
      *                                                                  PAYPAR2
      ****************************************************************** PAYPAR2
      *            D O L L A R   A D J U S T M E N T S.                * PAYPAR2
      ****************************************************************** PAYPAR2
      *                                                                * PAYPAR2
                                                                         PAYPAR2
               05  PS2R-DLR-ADJS-ARRAY.                                  PAYPAR2
                                                                         PAYPAR2
                 06  PS2R-ADJ-FLDS      OCCURS 20 TIMES.                 PAYPAR2
                                                                         PAYPAR2
                                                                         PAYPAR2
                   07  PS2R-ADJ-CODE       PICTURE 9(4).                 PAYPAR2
                                                                         PAYPAR2
      *                                                                * PAYPAR2
      *****                                                        ***** PAYPAR2
      *        CODE:   55XX    FOR DOLLAR BALANCE SEGMENT              * PAYPAR2
      *                6XXX    FOR FLOATING SEGMENTS, XXX IS DED NO.   * PAYPAR2
      *****                                                        ***** PAYPAR2
      *                                                                * PAYPAR2
                                                                         PAYPAR2
                   07  PS2R-ADJ-TYPE       PICTURE X.                    PAYPAR2
                       88  PS2R-ADJ-NOT-GTN                VALUE ' '.    PAYPAR2
                       88  PS2R-ADJ-EMPLOYMNT-TO-DATE      VALUE 'E'.    PAYPAR2
                       88  PS2R-ADJ-FISCAL-YTD             VALUE 'F'.    PAYPAR2
                       88  PS2R-ADJ-QTD                    VALUE 'Q'.    PAYPAR2
                       88  PS2R-ADJ-SUSP-PREPAID           VALUE 'S'.    PAYPAR2
                       88  PS2R-ADJ-USER-DEF-BAL           VALUE 'U'.    PAYPAR2
                       88  PS2R-ADJ-CALENDAR-YTD           VALUE 'Y'.    PAYPAR2
                       88  PS2R-ADJ-DECLINING-BAL          VALUE 'D'.    PAYPAR2
                       88  PS2R-ADJ-GTN                    VALUE 'G'.    PAYPAR2
                                                                         PAYPAR2
      *                                                                * PAYPAR2
      ****************************************************************** PAYPAR2
      *        THIS FIELD CONTAINS THE GTN TABLE BALANCE INDICTOR      * PAYPAR2
      *        FOR THOSE ADJUSTMENTS TO THE GROSS-TO-NET SEGMENTS.     * PAYPAR2
      *        THEIR ASSOCIATED ADJUSTMENT CODE CONTAINS A HIGH-ORDER  * PAYPAR2
      *        BLANK.                                                  * PAYPAR2
      ****************************************************************** PAYPAR2
      *                                                                * PAYPAR2
                                                                         PAYPAR2
                   07  PS2R-ADJ-FILLER     PICTURE X.                    PAYPAR2
                   07  PS2R-ADJ-AMT        PICTURE S9(5)V99    COMP-3.   PAYPAR2
                                                                         PAYPAR2
      *                                                                * PAYPAR2
