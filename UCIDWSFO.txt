000100**************************************************************/   36430821
000200*  COPYMEMBER: UCIDWSFO                                      */   36430821
000300*  RELEASE: ___0821______ SERVICE REQUEST(S): _____3643____  */   36430821
000400*  NAME:_______TCM_______ MODIFICATION DATE:  __10/05/93____ */   36430821
000500*  DESCRIPTION:                                              */   36430821
000600*    ID SYSTEM FOOTER MAP DATA                               */   36430821
000700*                                                            */   36430821
000800**************************************************************/   36430821
000900*01  UCIDWSFO   EXTERNAL.                                         UCIDWSFO
001000     05  UCIDWSFO-EXT-AREA                 PIC X(1024).           UCIDWSFO
001100     05  UCIDWSFO-EXT-DATA REDEFINES UCIDWSFO-EXT-AREA.           UCIDWSFO
001200         10  UCIDWSFO-ENTERED-FUNC         PIC X(04).             UCIDWSFO
001300         10  UCIDWSFO-ENTERED-ID.                                 UCIDWSFO
001400             15 UCIDWSFO-ENTERED-ID-2      PIC X(02).             UCIDWSFO
001500             15 UCIDWSFO-ENTERED-ID-7      PIC X(07).             UCIDWSFO
001600         10  UCIDWSFO-ENTERED-SSN          PIC X(09).             UCIDWSFO
001700         10  UCIDWSFO-ENTERED-DOB.                                UCIDWSFO
001800             15 UCIDWSFO-ENTERED-MM        PIC X(02).             UCIDWSFO
001900             15 UCIDWSFO-FILLER1           PIC X(01).             UCIDWSFO
002000             15 UCIDWSFO-ENTERED-DD        PIC X(02).             UCIDWSFO
002100             15 UCIDWSFO-FILLER2           PIC X(01).             UCIDWSFO
002200             15 UCIDWSFO-ENTERED-YYYY      PIC X(04).             UCIDWSFO
002300         10  UCIDWSFO-ENTERED-LNAME        PIC X(15).             UCIDWSFO
002400         10  UCIDWSFO-FILLER               PIC X(977).            UCIDWSFO
