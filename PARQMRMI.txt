000100******************************************************************PARQMRMI
000200*                                                                *PARQMRMI
000300*                    P A R Q M R M I                             *PARQMRMI
000400*                                                                *PARQMRMI
000500*    THIS MEMBER DEFINES THE PAR MASTER RECORD AS IT EXISTS      *PARQMRMI
000600*    AFTER THE PAR DOCUMENTS HAVE BEEN DISTRIBUTED FOR           *PARQMRMI
000700*    COMPLETION.  THIS RECORD IS UPDATED WITH THE PERCENTAGES    *PARQMRMI
000800*    OF TIME WORKED.                                             *PARQMRMI
000900*                                                                *PARQMRMI
001000*        OTHER COPY MEMBERS: PARYMRMO - PAR MASTER OUT           *PARQMRMI
001100*                            PARYMRMW - PAR MASTER WORK          *PARQMRMI
001200*                            PARYMRAC - PAR NEW MASTERS          *PARQMRMI
001300******************************************************************PARQMRMI
001400                                                                  PARQMRMI
001500*01  MASTER-RECORD-MI.                                            PARQMRMI
001600                                                                  PARQMRMI
001700     05  PAR-NUMBER-MI.                                           PARQMRMI
001800         10   PAR-CAMPUS-MI            PIC 99.                    PARQMRMI
001900         10   PAR-DATE-MI              PIC 99.                    PARQMRMI
002000         10   PAR-SEQUENCE-MI          PIC 9(6).                  PARQMRMI
002100     05  FILLER                        PIC X(3).                  PARQMRMI
002200                                                                  PARQMRMI
002300     05  DISTRIBUTION-DATA-MI.                                    PARQMRMI
002400         10   DIST-ACCOUNT-MI          PIC 9(6).                  PARQMRMI
002500         10   DIST-FIELD-MI            PIC X(30).                 PARQMRMI
002600         10   DIST-HOME-DEPT-MI        PIC 9(6).                  PARQMRMI
002700                                                                  PARQMRMI
002800     05  DATE-OF-PAR-MI                PIC 9(4).                  PARQMRMI
002900     05  FILLER                        REDEFINES DATE-OF-PAR-MI.  PARQMRMI
003000         10   YEAR-OF-PAR-MI           PIC 99.                    PARQMRMI
003100         10   MONTH-OF-PAR-MI          PIC 99.                    PARQMRMI
003200                                                                  PARQMRMI
003300     05  EMPLOYEE-DATA-MI.                                        PARQMRMI
003400         10   EMP-SSN-MI               PIC 9(9).                  PARQMRMI
003500         10   EMP-NUMBER-MI            PIC 9(9).                  PARQMRMI
003600         10   EMP-NAME-MI              PIC X(30).                 PARQMRMI
003700         10   EMP-TITLE-CD-MI          PIC 9(4).                  PARQMRMI
003800                                                                  PARQMRMI
003900     05  RECORD-DATA-MI.                                          PARQMRMI
004000         10   RCD-TYPE-OF-PAR-MI       PIC X.                     PARQMRMI
004100*             88   MONTHLY-PAR-MI      VALUE 'M'.                 PARQMRMI
004200*             88   QUARTERLY-PAR-MI    VALUE 'Q'.                 PARQMRMI
004300              88   NON-PROF-MI         VALUE '2'.                 PARQMRMI
004400              88   PROF-MI             VALUE '1'.                 PARQMRMI
004500              88   NINE-MO-FAC-MI      VALUE 'N'.                 PARQMRMI
004600         10   RCD-LAST-UPDATE-MI       PIC XX.                    PARQMRMI
004700         10   FILLER                   REDEFINES                  PARQMRMI
004800                                       RCD-LAST-UPDATE-MI.        PARQMRMI
004900              15   RCD-TYPE-UPDATE-MI  PIC X.                     PARQMRMI
005000                   88   RCD-PRCT-MI    VALUE 'P'.                 PARQMRMI
005100                   88   RCD-ADD-MI     VALUE 'A'.                 PARQMRMI
005200                   88   RCD-CHNG-MI    VALUE 'C'.                 PARQMRMI
005300                   88   RCD-DLT-MI     VALUE 'D'.                 PARQMRMI
005400              15   RCD-NUM-UPDATE-MI   PIC 9.                     PARQMRMI
005500                   88   RCD-NO-UPDT-MI VALUE 0.                   PARQMRMI
005600         10   RCD-SELCTION-REASON-MI   PIC XX.                    PARQMRMI
005700              88   RCD-SEL-PI-MI       VALUE 'PI'.                PARQMRMI
005800              88   RCD-SEL-FF-MI       VALUE 'FF'.                PARQMRMI
005900                                                                  PARQMRMI
006000     05  BUDGETED-ACTIVITY-MI.                                    PARQMRMI
006100         10   BUDG-INST-DR-MI          PIC S999 COMP-3.           PARQMRMI
006200         10   BUDG-SPON-RES-MI         PIC S999 COMP-3.           PARQMRMI
006300         10   BUDG-OTHER-MI            PIC S999 COMP-3.           PARQMRMI
006400                                                                  PARQMRMI
006500     05  PERCENT-ACTIVITY-MI.                                     PARQMRMI
006600         10   PER-INST-DR-MI           PIC S999 COMP-3.           PARQMRMI
006700         10   PER-SPON-RES-MI          PIC S999 COMP-3.           PARQMRMI
006800         10   PER-PUBLIC-SER-MI        PIC S999 COMP-3.           PARQMRMI
006900         10   PER-DEPT-ADMIN-MI        PIC S999 COMP-3.           PARQMRMI
007000         10   PER-GENRL-ADMIN-MI       PIC S999 COMP-3.           PARQMRMI
007100         10   PER-OTHER-MI             PIC S999 COMP-3.           PARQMRMI
007200         10   PER-SPA-MI               PIC S999 COMP-3.           PARQMRMI
007300*    05  FILLER                        PIC X.                     PARQMRMI
007400*    05  SYSTEM-WIDE-FLAG-MI           PIC X.                     PARQMRMI
007500*        88  SYSTEM-WIDE-MI            VALUE '-'.                 PARQMRMI
