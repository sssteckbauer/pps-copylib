000010**************************************************************/   32021138
000020*  COPYMEMBER: CPWSXBAT                                      */   32021138
000030*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000040*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000050*  DESCRIPTION:                                              */   32021138
000060*  - MODIFIED FOR FULL ACCOUNTING UNIT                       */   32021138
000070**************************************************************/   32021138
000100**************************************************************/   51340550
000200*  COPYMEMBER: CPWSXBAT                                      */   51340550
000300*  RELEASE: ___0550______ SERVICE REQUEST(S): _____5134____  */   51340550
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___03/15/91__  */   51340550
000500*  DESCRIPTION:                                              */   51340550
000600*  - ADDITIONAL PAGE BATCHING CONTROLS                       */   51340550
000700*                                                            */   51340550
000800**************************************************************/   51340550
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXBAT                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   14520310
000200*  COPY MODULE:  CPWSXBAT                                    */   14520310
000300*  RELEASE # ___0310___   SERVICE REQUEST NO(S)____1452______*/   14520310
000400*  NAME __D. STORMAN___   MODIFICATION DATE ____08/18/87_____*/   14520310
000500*  DESCRIPTION                                               */   14520310
000600*  FOR CAMPUSES USING EXPANDED TIME REPORTING WORKSHEETS,    */   14520310
000700*  THE BATCH TRANSACTION COUNT MAY BE MODIFIED BY PPP350 TO  */   14520310
000800*  ACCOUNT FOR EXPANDED PAYROLL ACTIVITY INPUT TRANSACTIONS  */   14520310
000900*  THAT ARE SPLIT INTO TWO PIA TRANSACTIONS.  A FIELD WAS    */   14520310
001000*  ADDED TO HOLD THE ORIGINAL BATCH TRANSACTION COUNT IN     */   14520310
001100*  ORDER TO PASS THAT NUMBER TO PPP360 FOR PRINTING.         */   14520310
001200**************************************************************/   14520310
001300     SKIP2                                                        14520310
001400**************************************************************/   13190159
001500*  COPY MODULE:  CPWSXBAT                                    */   13190159
001600*  RELEASE # ___0159___   SERVICE REQUEST NO(S)____1319______*/   13190159
001700*  NAME ___JAL_________   MODIFICATION DATE ____09/20/85_____*/   13190159
001800*  DESCRIPTION                                               */   13190159
001900*  THE INPUT CARD RECORD LENGTH WAS INCREASED                */   13190159
002000*  FROM 80 TO 102 BYTES.                                     */   13190159
002100**************************************************************/   13190159
002200     SKIP2                                                        13190159
002300*    COPYID=CPWSXBAT                                              CPWSXBAT
002400*01  XBAT-BATCH-INPUT-TRAN.                                       30930413
002500     05  XBAT-ID.                                                 CPWSXBAT
002600         10  XBAT-ACTN-CD        PIC X.                           CPWSXBAT
002700         10  XBAT-SEQ-NO         PIC X(5).                        CPWSXBAT
002800         10  XBAT-ID-FILLER      PIC X(3).                        CPWSXBAT
002900     05  XBAT-TRAN-CD            PIC XX.                          CPWSXBAT
003000     05  XBAT-TRAN-IMAGE.                                         CPWSXBAT
003100         10  XBAT-BATCH-TYPE     PIC X.                           CPWSXBAT
003200         10  XBAT-BATCH-NOX      PIC XXX.                         CPWSXBAT
003300         10  XBAT-BATCH-NO9      REDEFINES                        CPWSXBAT
003400             XBAT-BATCH-NOX      PIC 999.                         CPWSXBAT
003500         10  XBAT-ORIGIN         PIC X(3).                        CPWSXBAT
003600         10  XBAT-TRAN-CTX       PIC XXX.                         CPWSXBAT
003700         10  XBAT-TRAN-CT9       REDEFINES                        CPWSXBAT
003800             XBAT-TRAN-CTX       PIC 999.                         CPWSXBAT
005400*****    10  XBAT-HASHX          PIC X(10).                       51340550
005500*****    10  XBAT-HASH9          REDEFINES                        51340550
005600*****        XBAT-HASHX          PIC 9(8)V99.                     51340550
005700         10  XBAT-HASH-TOTALS OCCURS 5 TIMES.                     51340550
005800             15  XBAT-HASHX          PIC X(10).                   51340550
005900             15  XBAT-HASH9          REDEFINES                    51340550
006000                 XBAT-HASHX          PIC 9(8)V99.                 51340550
004200*********10  FILLER              PIC X(49).                       14520310
006200*****    10  FILLER              PIC X(40).                       51340550
004400         10  XBAT-ORIG-TRAN-CTX  PIC XXX.                         14520310
004500         10  XBAT-ORIG-TRAN-CT9  REDEFINES                        14520310
004600             XBAT-ORIG-TRAN-CTX  PIC 999.                         14520310
006600*****    10  FILLER              PIC X(6).                        32021138
006610         10  FILLER              PIC X(24).                       32021138
004800         10  XBAT-FILLER-OP      PIC X(02).                       13190159
004900         10  XBAT-FILLER-COS     PIC X(20).                       13190159
