000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXBSR                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100*    COPYID=CPWSXBSR                                              CPWSXBSR
000200*01  XBSR-RECORD.                                                 30930413
000300     05  XBSR-I-D                PIC X(3)         VALUE 'BSR'.    CPWSXBSR
000400     05  XBSR-BEGIN-BATCH        PIC 9           VALUE ZERO.      CPWSXBSR
000500     05  XBSR-BATCH-STATUSES     PIC X(200)      VALUE ZEROS.     CPWSXBSR
000600     05  FILLER                  REDEFINES XBSR-BATCH-STATUSES.   CPWSXBSR
000700         10  XBSR-BATCH-STATUS   OCCURS 200                       CPWSXBSR
000800                                 PIC 9.                           CPWSXBSR
000900     05  XBSR-FILLER             PIC X(20)       VALUE SPACES.    CPWSXBSR
