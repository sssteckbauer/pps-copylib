000100**************************************************************/   36330704
000200*  COPYMEMBER: CPWSBUSH                                      */   36330704
000300*  RELEASE: ____0704____  SERVICE REQUEST(S): ____3633____   */   36330704
000400*  NAME:    ___B.I.T.___  CREATION DATE:      __10/02/92__   */   36330704
000500*  DESCRIPTION:                                              */   36330704
000600*    DCLGEN FOR THE PPPVBUSH VIEW                            */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900******************************************************************CPWSBUSH
001000* COBOL DECLARATION FOR TABLE PPPVBUSH_BUSH                      *CPWSBUSH
001100******************************************************************CPWSBUSH
001200*01  DCLPPPVBUSH-BUSH.                                            CPWSBUSH
001300     10 BUS-BUC              PIC X(2).                            CPWSBUSH
001400     10 BUS-SHC              PIC X(1).                            CPWSBUSH
001500     10 BUS-DISTRIBUTION     PIC X(1).                            CPWSBUSH
001600     10 SYSTEM-ENTRY-DATE    PIC X(10).                           CPWSBUSH
001700     10 SYSTEM-ENTRY-TIME    PIC X(8).                            CPWSBUSH
001800     10 BUS-SH-DUC-DESCRIP   PIC X(32).                           CPWSBUSH
001900     10 BUS-LAST-ACTION      PIC X(1).                            CPWSBUSH
002000     10 BUS-LAST-ACTION-DT   PIC X(10).                           CPWSBUSH
002100******************************************************************CPWSBUSH
002200* THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 8       *CPWSBUSH
002300******************************************************************CPWSBUSH
