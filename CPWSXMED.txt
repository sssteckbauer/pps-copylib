000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXMED                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100****************************************************************/ 14140251
000200*  COPY MEMBER:  CPWSXMED                                      */ 14140251
000300*  RELEASE:  0251                    SERVICE REQUEST:  1414    */ 14140251
000400*  NAME:     G. STEINITZ       MODIFICATION DATE:  10/18/86    */ 14140251
000500*  DESCRIPTION                                                 */ 14140251
000600*     THIS IS THE RECORD LAYOUT FOR THE MEDICARE ENROLLMENT    */ 14140251
000700*     TURNAROUND FILE.                                         */ 14140251
000800****************************************************************/ 14140251
000900*01  XMED-TURNS-RECORD.                                           30930413
001000     05  XMED-EMPL-ID                PIC  X(09).                  CPWSXMED
001100     05  FILLER                      PIC  X(71).                  CPWSXMED
