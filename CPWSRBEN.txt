000000**************************************************************/   04611523
000001*  COPYMEMBER: CPWSRBEN                                      */   04611523
000002*  RELEASE: ___1523______ SERVICE REQUEST(S): ____80461____  */   04611523
000003*  NAME:____J KENNEDY____ MODIFICATION DATE:  ___06/04/03__  */   04611523
000004*  DESCRIPTION:                                              */   04611523
000005*  ADDED DEPCARE TERMINATION DATE (DE 0315)                  */   04611523
000006*                                                            */   04611523
000007**************************************************************/   04611523
000000**************************************************************/   02891454
000001*  COPYMEMBER: CPWSRBEN                                      */   02891454
000002*  RELEASE: ___1454______ SERVICE REQUEST(S): ____80289____  */   02891454
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___11/26/02__  */   02891454
000004*  DESCRIPTION:                                              */   02891454
000005*  - ADDED  SPP-MGMT-EXCEPT-IND (DE 0412)                    */   02891454
000006*  - ADDED  SPP-MGMT-OVRD-RATE  (DE 0413)                    */   02891454
000009**************************************************************/   02891454
000000**************************************************************/   01911440
000001*  COPYMEMBER: CPWSRBEN                                      */   01911440
000002*  RELEASE: ___1440______ SERVICE REQUEST(S): 80238, 80191_  */   01911440
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___11/08/02__  */   01911440
000004*  DESCRIPTION:                                              */   01911440
000005*  - ADDED MEDICAL CONTRIBUTION BASE CURRENT YEAR (DE 0289)  */   01911440
000006*  - ADDED MEDICAL CONTRIBUTION BASE NEXT YEAR (DE 0290)     */   01911440
000006*  - ADDED HCRA TERMINATION DATE (DE 0314)                   */   01911440
000007**************************************************************/   01911440
000100**************************************************************/   48661418
000200*  COPYMEMBER: CPWSRBEN                                      */   48661418
000300*  RELEASE: ___1418______ SERVICE REQUEST(S): ____14866____  */   48661418
000400*  NAME:_______JLT_______ MODIFICATION DATE:  ___06/20/02__  */   48661418
000500*  DESCRIPTION:                                              */   48661418
000600*    ADDED STATE DECLARATION OF DPI (D.E. 0288)              */   48661418
000700**************************************************************/   48661418
000100**************************************************************/   71921294
000200*  COPYMEMBER: CPWSRBEN                                      */   71921294
000300*  RELEASE: ___1294______ SERVICE REQUEST(S): ____17192____  */   71921294
000400*  NAME:_______JLT_______ MODIFICATION DATE:  ___05/05/00__  */   71921294
000500*  DESCRIPTION:                                              */   71921294
000600*    ADDED AGENCY FEE MONTHLY MAINT FLAG (D.E. 0239)         */   71921294
000700*                                                            */   71921294
000800**************************************************************/   71921294
000000**************************************************************/   54541281
000001*  COPYMEMBER: CPWSRBEN                                      */   54541281
000002*  RELEASE: ___1281______ SERVICE REQUEST(S): ____15454____  */   54541281
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___06/22/99__  */   54541281
000004*  DESCRIPTION:                                              */   54541281
000005*  -  ADDED MEDICAL COVERAGE END DATE (EDB 0300),            */   54541281
000006*           DENTAL  COVERAGE END DATE (EDB 0271),            */   54541281
000007*           VISION  COVERAGE END DATE (EDB 0346),            */   54541281
000008*           LEGAL   COVERAGE END DATE (EDB 0380).            */   54541281
000009*  -  ADDED NEXT FUTURE BENEFITS COVERAGE DATE (EDB 0692)    */   54541281
000010**************************************************************/   54541281
000000**************************************************************/   49401219
000001*  COPYMEMBER: CPWSRBEN                                      */   49401219
000002*  RELEASE: ___1219______ SERVICE REQUEST(S): ____14940____  */   49401219
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___11/23/98__  */   49401219
000004*  DESCRIPTION:                                              */   49401219
000005*  - CONVERT PLN-403B-CHGD-DATE TO MAC-403B-CHGD-DATE.       */   49401219
000007**************************************************************/   49401219
000000**************************************************************/   49031194
000001*  COPYMEMBER: CPWSRBEN                                      */   49031194
000002*  RELEASE: ___1194______ SERVICE REQUEST(S): ____14903____  */   49031194
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___06/05/98__  */   49031194
000004*  DESCRIPTION:                                              */   49031194
000005*    ADDED ANTICIPATED RETIREMENT DATE (D.E. 0765)           */   49031194
000006*                                                            */   49031194
000007**************************************************************/   49031194
000001**************************************************************/   32181188
000002*  COPYMEMBER: CPWSRBEN                                      */   32181188
000004*  RELEASE: ___1188______ SERVICE REQUEST(S): ____13218____  */   32181188
000005*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___06/01/98__  */   32181188
000006*  DESCRIPTION:                                              */   32181188
000007*   ADDED COVERAGE CODES WITHOUT DOMESTIC PARTNER            */   32181188
000009**************************************************************/   32181188
000000**************************************************************/   25991140
000001*  COPYMEMBER: CPWSRBEN                                      */   25991140
000002*  RELEASE: ___1140______ SERVICE REQUEST(S): ____12599____  */   25991140
000003*  NAME:______M SANO_____ MODIFICATION DATE:  ___05/01/97__  */   25991140
000004*  DESCRIPTION:                                              */   25991140
000005*  ADDED SUSPENDED PREMIUM INDICATOR (SUS-PREMIUM-IND)       */   25991140
000006*   PERIOD OF INITIAL ELIGIBILITY END DATE (PIE-END-DATE)    */   25991140
000007*   PIN AUTHORIZATION SIGNATURE DATE (PIN-SIGN-DATE)         */   25991140
000008**************************************************************/   25991140
000000**************************************************************/   15460915
000001*  COPYMEMBER: CPWSRBEN                                      */   15460915
000002*  RELEASE: ___0915______ SERVICE REQUEST(S): ____11546____  */   15460915
000003*  NAME:__H. TRUONG _____ MODIFICATION DATE:  ___08/01/94__  */   15460915
000004*  DESCRIPTION:                                              */   15460915
000005*    ADDED COVERED COMPENSATION LIMIT CODE,                  */   15460915
000006*    COV-COMP-LIMIT-CD.                                      */   15460915
000007**************************************************************/   15460915
000000**************************************************************/   05550775
000001*  COPYMEMBER: CPWSRBEN                                      */   05550775
000002*  RELEASE: ___0775______ SERVICE REQUEST(S): _____0555____  */   05550775
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___06/01/93__  */   05550775
000004*  DESCRIPTION:                                              */   05550775
000005*    ADDED MED-PROVIDER-ID AND DEN-PROVIDER-ID               */   05550775
000007**************************************************************/   05550775
000100**************************************************************/   36260760
000200*  COPYMEMBER: CPWSRBEN                                      */   36260760
000300*  RELEASE: __0760_______ SERVICE REQUEST(S): _____3626____  */   36260760
000400*  NAME:_________PKD_____ MODIFICATION DATE:  ___04/30/93__  */   36260760
000500*  DESCRIPTION:                                              */   36260760
000600*    ADDED THE FOLLOWING:                                    */   36260760
000067*       EMP_HLTH_COVEFFDT                                    */   36260760
000068*       EMP_DENTL_COVEFFDT                                   */   36260760
000069*       EMP_VIS_COVEFFDT                                     */   36260760
000070*       EMP_LEGAL_COVEFFDT                                   */   36260760
000100**************************************************************/   36260760
000000**************************************************************/   05460730
000001*  COPYMEMBER: CPWSRBEN                                      */   05460730
000002*  RELEASE: ___0730______ SERVICE REQUEST(S): ____10546____  */   05460730
000003*  NAME:_____J. QUAN_____ MODIFICATION DATE:  ___12/21/92__  */   05460730
000004*  DESCRIPTION:                                              */   05460730
000005*  - ADDED PLN-403B-CHGD-DATE                                */   05460730
000006*                                                            */   05460730
000008*  - DELETED CODE PREVIOUSLY COMMENTED-OUT; MARKED 'CD'      */   05460730
000009**************************************************************/   05460730
000100**************************************************************/   36220651
000200*  COPYMEMBER: CPWSRBEN                                      */   36220651
000300*  RELEASE: __0651_______ SERVICE REQUEST(S): _____3622____  */   36220651
000400*  NAME:_________KXK_____ MODIFICATION DATE:  ___04/13/92__  */   36220651
000500*  DESCRIPTION:                                              */   36220651
000600*   - ADDED COVERAGE EFFECTIVE DATES TO REFLECT ADDITIONS    */   36220651
000700*     OF COVERAGE EFFECTIVE DATE COLUMNS TO BENEFITS TABLE.  */   36220651
000800**************************************************************/   36220651
000000**************************************************************/   02040634
000001*  COPYMEMBER: CPWSRBEN                                      */   02040634
000002*  RELEASE: ___0634______ SERVICE REQUEST(S): ____10204____  */   02040634
000003*  NAME:_____J. QUAN_____ MODIFICATION DATE:  ___01/03/91__  */   02040634
000004*  DESCRIPTION:                                              */   02040634
014600*  ADD EPD-SALARY-BASE                                       */   02040634
014700*      EPD-WAIT-PERIOD                                       */   02040634
014800*      EPD-COVRGE-DATE                                       */   02040634
014900*      EPD-DEENROLL                                          */   02040634
000007**************************************************************/   02040634
000100**************************************************************/   40280572
000200*  COPYMEMBER: CPWSRBEN                                      */   40280572
000300*  RELEASE: ___0572______ SERVICE REQUEST(S): _____4028____  */   40280572
000400*  NAME:_____J.L._LOE____ MODIFICATION DATE:  ___05/29/91__  */   40280572
000500*  DESCRIPTION:                                              */   40280572
000600*   - ADDED EXEC-SPP-FLAG AND EXEC-SPP-PCT FOR SEVERANCE PAY */   40280572
000700*   - THESE FIELDS ARE USED FOR ELIGIBILITY AND GRADE E %    */   40280572
000800**************************************************************/   40280572
000000**************************************************************/   45340564
000001*  COPYMEMBER: CPWSRBEN                                      */   45340564
000002*  RELEASE: ___0564______ SERVICE REQUEST(S): _____4534____  */   45340564
000003*  NAME:___M. BAPTISTA___ MODIFICATION DATE:  ___02/15/91__  */   45340564
000004*  DESCRIPTION:                                              */   45340564
000005*  - REMOVED DENTAL AND VISION ALTERNATIVE FIELDS.           */   45340564
000006*                                                            */   45340564
000007**************************************************************/   45340564
000100**************************************************************/  *36090513
000200*  COPYMEMBER: CPWSRBEN                                      */  *36090513
000300*  RELEASE: ____0513____  SERVICE REQUEST(S): ____3609____   */  *36090513
000400*  NAME: ______JAG______  CREATION DATE: ____11/05/90_____   */  *36090513
000500*  DESCRIPTION:                                              */  *36090513
000600*  - WORKING-STORAGE LAYOUT OF BEN TABLE ROW.                */  *36090513
000700**************************************************************/  *36090513
000800*    COPYID=CPWSRBEN                                              CPWSRBEN
000900*01  BEN-ROW.                                                     CPWSRBEN
001000     10 EMPLOYEE-ID          PIC X(9).                            CPWSRBEN
001100     10 AGE-JAN1             PIC S9(4) USAGE COMP.                CPWSRBEN
001200     10 RET-ELIG-CODE        PIC X(1).                            CPWSRBEN
001300     10 UCRS-ELIGDATE        PIC X(10).                           CPWSRBEN
001400     10 DENTAL-PLAN          PIC X(2).                            CPWSRBEN
001500     10 DENTAL-COVERAGE      PIC X(3).                            CPWSRBEN
001600     10 DENTAL-EFFDATE       PIC X(10).                           CPWSRBEN
001700     10 DENTAL-DEENROLL      PIC X(1).                            CPWSRBEN
001800     10 DENTAL-OPTOUT        PIC X(1).                            CPWSRBEN
002500     10 LIFEINS-PLAN         PIC X(1).                            CPWSRBEN
002600     10 LIFE-SALARY-BASE     PIC S9(4) USAGE COMP.                CPWSRBEN
002700     10 LIFE-UCPAIDAMT       PIC X(3).                            CPWSRBEN
002800     10 LIFE-EFFDATE         PIC X(10).                           CPWSRBEN
002900     10 LIFE-DEENROLL        PIC X(1).                            CPWSRBEN
003000     10 DEP-LIFE-IND         PIC X(1).                            CPWSRBEN
003100     10 DEP-LIFE-DEENROLL    PIC X(1).                            CPWSRBEN
003200     10 DEP-LIFE-EFFDATE     PIC X(10).                           CPWSRBEN
003300     10 ADD-PRINC-SUM        PIC X(3).                            CPWSRBEN
003400     10 ADD-COVERAGE         PIC X(1).                            CPWSRBEN
003500     10 ADD-EARNEFFDATE      PIC X(10).                           CPWSRBEN
003600     10 ADD-DEENROLL         PIC X(1).                            CPWSRBEN
003700     10 LTD-SALBASE          PIC S99999V USAGE COMP-3.            CPWSRBEN
003800     10 LTD-EARNEFFDATE      PIC X(10).                           CPWSRBEN
003900     10 LTD-DEENROLL         PIC X(1).                            CPWSRBEN
004000     10 STD-SALBASE          PIC S99999V USAGE COMP-3.            CPWSRBEN
004100     10 STD-WAITPERIOD       PIC X(3).                            CPWSRBEN
004200     10 STD-EARNEFFDATE      PIC X(10).                           CPWSRBEN
004300     10 STD-DEENROLL         PIC X(1).                            CPWSRBEN
004400     10 HLTH-PLAN            PIC X(2).                            CPWSRBEN
004500     10 HLTH-COVERCODE       PIC X(3).                            CPWSRBEN
004600     10 HLTH-EARNEFFDATE     PIC X(10).                           CPWSRBEN
004700     10 HLTH-DEENROLL        PIC X(1).                            CPWSRBEN
004800     10 HLTH-OPTOUT          PIC X(1).                            CPWSRBEN
004900     10 SEC-HLTH-PLAN        PIC X(2).                            CPWSRBEN
005000     10 SEC-HLTH-COVERAGE    PIC X(3).                            CPWSRBEN
005100     10 SEC-HLTH-DATE        PIC X(10).                           CPWSRBEN
005200     10 VIS-PLAN             PIC X(2).                            CPWSRBEN
005300     10 VIS-COVERAGE         PIC X(3).                            CPWSRBEN
005400     10 VIS-EARNEFFDATE      PIC X(10).                           CPWSRBEN
005500     10 VIS-DEENROLL         PIC X(1).                            CPWSRBEN
005600     10 VIS-OPTOUT           PIC X(1).                            CPWSRBEN
006300     10 LEGAL-PLAN           PIC X(2).                            CPWSRBEN
006400     10 LEGAL-COVERAGE       PIC X(3).                            CPWSRBEN
006500     10 LEGAL-EARNEFFDATE    PIC X(10).                           CPWSRBEN
006600     10 LEGAL-DEENROLL       PIC X(1).                            CPWSRBEN
006700     10 EXEC-BEN-ELIG        PIC X(1).                            CPWSRBEN
006800     10 INS-REDUCT-IND       PIC X(1).                            CPWSRBEN
006900     10 NDI-CODE             PIC X(1).                            CPWSRBEN
007000     10 CAL-CAS-DED-IND      PIC X(1).                            CPWSRBEN
007100     10 EXEC-LIFEFLAG        PIC X(1).                            CPWSRBEN
007200     10 EXEC-LIFESALARY      PIC S9(4) USAGE COMP.                CPWSRBEN
007300     10 EXEC-LIFECHANGE      PIC X(1).                            CPWSRBEN
007400     10 EXEC-LIFECHGDATE     PIC X(10).                           CPWSRBEN
007500     10 EXEC-LIFEINCOME      PIC S9999999V99 USAGE COMP-3.        CPWSRBEN
007600     10 DCP-PLAN-CODE        PIC X(1).                            CPWSRBEN
009300     10 EXEC-SPP-FLAG        PIC X(1).                            40280572
009400     10 EXEC-SPP-PCT         PIC S99V99 COMP-3.                   40280572
009500     10 EPD-SALARY-BASE      PIC S99999V USAGE COMP-3.            02040634
009600     10 EPD-WAIT-PERIOD      PIC X(3).                            02040634
009700     10 EPD-COVRGE-DATE      PIC X(10).                           02040634
009800     10 EPD-DEENROLL         PIC X(1).                            02040634
011700     10 DENTAL-COVEFFDATE    PIC X(10).                           36220651
011800     10 LIFE-UCPD-EFFDATE    PIC X(10).                           36220651
011900     10 LIFE-COVEFFDATE      PIC X(10).                           36220651
012000     10 DEPLIFE-COVEFFDATE   PIC X(10).                           36220651
012100     10 ADD-COVEFFDATE       PIC X(10).                           36220651
012400     10 HLTH-COVEFFDATE      PIC X(10).                           36220651
012500     10 VIS-COVEFFDATE       PIC X(10).                           36220651
012600     10 LEGAL-COVEFFDATE     PIC X(10).                           36220651
012700     10 NDI-COVEFFDATE       PIC X(10).                           36220651
012800     10 EXEC-LIFE-EFFDATE    PIC X(10).                           36220651
013000     10 MAC-403B-CHGD-DATE   PIC X(10).                           49401219
014810     10 EMP-HLTH-COVEFFDT    PIC X(10).                           36260760
014820     10 EMP-DENTL-COVEFFDT   PIC X(10).                           36260760
014830     10 EMP-VIS-COVEFFDT     PIC X(10).                           36260760
014840     10 EMP-LEGAL-COVEFFDT   PIC X(10).                           36260760
014850     10 MED-PROVIDER-ID      PIC X(26).                           05550775
014860     10 DEN-PROVIDER-ID      PIC X(26).                           05550775
014870     10 COV-COMP-LIMIT-CD    PIC X(01).                           15460915
014890     10 SUS-PREMIUM-IND      PIC X(01).                           25991140
014900     10 PIE-END-DATE         PIC X(10).                           25991140
015000     10 PIN-SIGN-DATE        PIC X(10).                           25991140
015000     10 DEN-COVERAGE-WODD    PIC X(03).                           32181188
015000     10 MED-COVERAGE-WODM    PIC X(03).                           32181188
015000     10 VIS-COVERAGE-WODV    PIC X(03).                           32181188
015004     10 ANTICIPATED-RET-DT   PIC X(10).                           49031194
015005     10 NXT-FUTBEN-ACT-DTE   PIC X(10).                           54541281
015006     10 HEALTH-COVEND-DATE   PIC X(10).                           54541281
015007     10 DENTAL-COVEND-DATE   PIC X(10).                           54541281
015008     10 VISION-COVEND-DATE   PIC X(10).                           54541281
015009     10 LEGAL-COVEND-DATE    PIC X(10).                           54541281
022100     10 AGENCY-FEE-MM-FLG    PIC X(01).                           71921294
022900     10 STATE-DECLAR-DPI     PIC X(01).                           48661418
022910     10 MED-CONT-BASE-CUR    PIC S9(4) USAGE COMP.                01911440
022920     10 MED-CONT-BASE-NEXT   PIC S9(4) USAGE COMP.                01911440
022700     10 HCRA-TERM-DATE       PIC X(10).                           02381440
022940     10 SPP-MGMT-EXCEP-IND   PIC X(01).                           02891454
022950     10 SPP-MGMT-OVRD-RATE   PIC X(01).                           02891454
022960     10 DEPCARE-TERM-DATE    PIC X(10).                           04611523
023000     10 RBEN-FILLER          PIC X(479).                          04611523
023010*****10 RBEN-FILLER          PIC X(489).                          04611523
023100*****                                                          CD 04611523
