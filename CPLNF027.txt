000800*==========================================================%      UCSD0102
000200*=    COPY MEMBER: CPLNK027                               =%      UCSD0102
000300*=    CHANGE #UCSD0102      PROJ. REQUEST: RELEASE 1138   =%      UCSD0102
000400*=    NAME: G. CHIU         MODIFICATION DATE: 08/27/97   =%      UCSD0102
000500*=                                                        =%      UCSD0102
000600*=    DESCRIPTION                                         =%      UCSD0102
000700*=    CHANGES MADE TO ACCOMMODATE THE LOCAL COA           =%      UCSD0102
000800*=    EXPANDED FUND FROM 5 CHARACTERS TO SIX CHARACTERS.  =%      UCSD0102
000900*=                                                        =%      UCSD0102
008000*==========================================================%      UCSD0102
000100**************************************************************/   32021138
000200*  COPYMEMBER: CPLNF027                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_______WJG_______ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000510*                                                            */   32021138
000600*  Initial release of linkage copymember between calling     */   32021138
000700*  program and module PPFAU027                               */   32021138
000710*                                                            */   32021138
000800**************************************************************/   32021138
000900*                                                            */   CPLNF027
001000*  This copymember is one of the Full Accounting Unit modules*/   CPLNF027
001100*  which each campus may need to modify to accommodate its   */   CPLNF027
001200*  Chart of Accounts structure.                              */   CPLNF027
001300*                                                            */   CPLNF027
002100**************************************************************/   CPLNF027
002300*01  PPFAU027-INTERFACE.                                          CPLNF027
002400    05  F027-INPUTS.                                              CPLNF027
002500        10  F027-FAU                     PIC X(30).               CPLNF027
002620    05  F027-OUTPUTS.                                             CPLNF027
002630        10  F027-FAU-COMPONENTS.                                  CPLNF027
002640            15  F027-C001-ACCOUNT        PIC X(06).               CPLNF027
002641*****       15  F027-C062-FUND           PIC X(05).               UCSD0102
002641            15  F027-C062-FUND           PIC X(06).               UCSD0102
002642            15  F027-C105-SUB            PIC X(02).               CPLNF027
002730        10  F027-FAILURE-CODE            PIC S9(04) COMP SYNC.    CPLNF027
002740        10  F027-FAILURE-TEXT            PIC X(35).               CPLNF027
