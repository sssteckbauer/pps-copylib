000000**************************************************************/   48801445
000001*  COPYMEMBER: CPPDXSSN                                      */   48801445
000002*  RELEASE: ___1445______ SERVICE REQUEST(S): ____14880____  */   48801445
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___11/18/02__  */   48801445
000004*  DESCRIPTION:                                              */   48801445
000005*  - Comments have been added to identify those specific     */   48801445
000006*    programs that use this copymember for social security   */   48801445
000007*    number edits.                                           */   48801445
000008**************************************************************/   48801445
000000**************************************************************/   48811441
000001*  COPYMEMBER: CPPDXSSN                                      */   48811441
000002*  RELEASE: ___1441______ SERVICE REQUEST(S): ____14881____  */   48811441
000003*  NAME:_______QUAN______ CREATION DATE:      ___11/14/02__  */   48811441
000004*  DESCRIPTION:                                              */   48811441
000005*  - INITIAL IMPLEMENTATION. USED IN CONJUNCTION WITH        */   48811441
000006*    WORKING STORAGE COPYMEMBER CPWSXSSN.                    */   48811441
000007*                                                            */   48811441
000008*    EDIT OF SOCIAL SECURITY NUMBER FOR W-2 PROCESS.         */   48811441
000009**************************************************************/   48811441
000019**************************************************************/   48801445
000060*--> As of 11/25/2002, these are the programs that currently */   48801445
000070*    use this copymember for social secutiy number edits. If */   48801445
000071*    changes are made to this copymember, the following      */   48801445
000072*    prgrams must be re-compiled:                            */   48801445
000080*                                                            */   48801445
000090*    W-4 Monthly Reporting: PPP775                           */   48801445
000091*    W-2 Reporting        : PPP610, PPTAXW2R, PPTAXW2T       */   48801445
000092*    1042-S Reporting     : PP1042SF, PP1042SR, PP1042ST     */   48801445
000100**************************************************************/   48801445
077850 XSSN-SOCIAL-SECURITY-NUM-EDIT.                                   CPPDXSSN
077850                                                                  CPPDXSSN
077850     SET XSSN-VALID TO TRUE.                                      CPPDXSSN
077850                                                                  CPPDXSSN
077803     IF (XSSN-NUMBER =  (    SPACES                               CPPDXSSN
077810                         OR '000000000'                           CPPDXSSN
077811                         OR '111111111'                           CPPDXSSN
077812                         OR '123456789'                           CPPDXSSN
077813                         OR '333333333' ) )                       CPPDXSSN
077840         OR                                                       CPPDXSSN
077850        (XSSN-3        =      0   )                               CPPDXSSN
077820         OR                                                       CPPDXSSN
077830        (XSSN-3        >=   734  AND XSSN-3 <= 749 )              CPPDXSSN
077820         OR                                                       CPPDXSSN
077830        (XSSN-3        >    772 )                                 CPPDXSSN
077850                                                                  CPPDXSSN
077850           SET XSSN-INVALID TO TRUE                               CPPDXSSN
077850     END-IF.                                                      CPPDXSSN
077850                                                                  CPPDXSSN
