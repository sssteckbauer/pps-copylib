000000**************************************************************/   36450775
000001*  COPYMEMBER: CPPDIPCM                                      */   36450775
000002*  RELEASE: ___0775______ SERVICE REQUEST(S): _____3645____  */   36450775
000003*  NAME:_______DDM_______ MODIFICATION DATE:  __06/01/93____ */   36450775
000004*  DESCRIPTION: INITIALIZE PCM ROW.                          */   36450775
000007**************************************************************/   36450775
052400     PERFORM                                                      CPPDIPCM
052600         INITIALIZE PCM-ROW                                       CPPDIPCM
052700         MOVE '0001-01-01' TO                                     CPPDIPCM
052800             LAST-PAY-DATE       OF PCM-ROW                       CPPDIPCM
054100         MOVE ZERO TO                                             CPPDIPCM
054200             CHK-DISP-CODE       OF PCM-ROW                       CPPDIPCM
054300             PRI-GROSS-CTL       OF PCM-ROW                       CPPDIPCM
054400         MOVE '1'  TO                                             CPPDIPCM
054500             MIN-RCD-FLAG        OF PCM-ROW                       CPPDIPCM
054810     END-PERFORM                                                  CPPDIPCM
