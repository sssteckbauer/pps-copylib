000100**************************************************************/   30930413
000200*  COPY MODULE:  CPWSXCST                                    */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000300*    COPYID=CPWSXCST                                              CPWSXCST
000200*01  CTL-SEGMENT-TABLE.                                           30930413
000300     03  CTL-SEG-TAB.                                             CPWSXCST
000400         05  CTL-SEG-TAB-DEL     PICTURE X.                       CPWSXCST
000500         05  CTL-SEG-TAB-KEY     PICTURE X(13).                   CPWSXCST
000600         05  CTL-SEG-TAB-DATA    PICTURE X(210).                  CPWSXCST
