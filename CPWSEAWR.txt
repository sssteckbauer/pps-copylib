000000**************************************************************/   32021138
000001*  COPYMEMBER: CPWSEAWR                                      */   32021138
000002*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000003*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000004*  DESCRIPTION:                                              */   32021138
000005*  - MODIFIED FOR FULL ACCOUNTING UNIT                       */   32021138
000007**************************************************************/   32021138
000000**************************************************************/   36450775
000001*  COPYMEMBER: CPWSEAWR                                      */   36450775
000002*  RELEASE: ___0775______ SERVICE REQUEST(S): _____3645____  */   36450775
000003*  NAME:_______DDM_______ MODIFICATION DATE:  __06/01/93____ */   36450775
000004*  DESCRIPTION:                                              */   36450775
000005*    -AWARD ARRAY               FOR AWR TABLE OF PPPEDB      */   36450775
000007**************************************************************/   36450775
000910*    COPYID=CPWSEAWR                                              CPWSEAWR
001000******************************************************************CPWSEAWR
001001*   COPY MEMBER USED TO STORE THE AWARD            ARRAY.        *CPWSEAWR
001002*   MAXIMUM ENTRIES OF TABLE IS STORED IN PAYROLL CONSTANT       *CPWSEAWR
001003*   (IDC) MEMBER.                                                *CPWSEAWR
001004******************************************************************CPWSEAWR
001100*01  AWARD-ARRAY.                                                 CPWSEAWR
001200     05  AWR-ARRAY                     OCCURS 100.                CPWSEAWR
001210         10  AWR-OCCURRENCE-KEY.                                  CPWSEAWR
001300             15  AWARD-ID              PIC S9(04)    COMP.        CPWSEAWR
001410         10  FILLER.                                              CPWSEAWR
001500             15  AWARD-TYPE            PIC  X(03).                CPWSEAWR
001510             15  AWARD-DATE            PIC  X(10).                CPWSEAWR
001520             15  AWARD-AMOUNT          PIC S9(06)V99 COMP-3.      CPWSEAWR
001530*****        15  AWARD-SOURCE          PIC  X(13).                32021138
001530             15  AWARD-SOURCE          PIC  X(30).                32021138
001600             15  AWARD-ADC-CD          PIC  X(01).                CPWSEAWR
002207******************************************************************CPWSEAWR
