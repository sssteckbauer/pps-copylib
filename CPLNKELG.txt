000000**************************************************************/   06061548
000001*  COPYMEMBER: CPLNKELG                                      */   06061548
000002*  RELEASE: ___1548______ SERVICE REQUEST(S): ____80606____  */   06061548
000003*  NAME:___S. B. ISAACS__ MODIFICATION DATE:  ___12/10/03__  */   06061548
000004*  DESCRIPTION:                                              */   06061548
000005*  -CODE MODIFIED TO TO IDENTIFY  2 NEW TYPES OF RECORDS FOR */   06061548
000006*   ENROLLMENT REPORTING AND THRESHOLD REPORTING FOR IX      */   06061548
000007*   COVERED EMPLOYEES.                                       */   06061548
000008**************************************************************/   06061548
000000**************************************************************/   52101313
000001*  COPYMEMBER: CPLNKELG                                      */   52101313
000002*  RELEASE: ___1313______ SERVICE REQUEST(S): ____15210____  */   52101313
000003*  NAME:__PHIL THOMPSON__ CREATION DATE:      ___11/15/00__  */   52101313
000004*  DESCRIPTION:                                              */   52101313
000005*     LINKAGE BETWEEN THE CALLING PROGRAM AND PPELGRPT WHICH */   52101313
000006*     HANDLES THE I/O FOR THE BENEFITS ELIGIBILITY REPORT.   */   52101313
000007**************************************************************/   52101313
001800*01  PPELGRPT-INTERFACE.                                          CPLNKELG
001900     05  PPELGRPT-RETURN-STATUS              PIC 9(01).           CPLNKELG
001910         88  PPELGRPT-ERROR                  VALUE 1 THROUGH 9.   CPLNKELG
001920         88  PPELGRPT-FILE-STATUS-ERROR      VALUE 9.             CPLNKELG
002000     05  PPELGRPT-ACTION                     PIC X(05).           CPLNKELG
002300         88  PPELGRPT-OPEN                   VALUE 'OPEN '.       CPLNKELG
002310         88  PPELGRPT-CLOSE                  VALUE 'CLOSE'.       CPLNKELG
002400         88  PPELGRPT-WRITE                  VALUE 'WRITE'.       CPLNKELG
002500     05  PPELGRPT-ELIGIBILITY-STATUS         PIC X(01).           CPLNKELG
002600         88  PPELGRPT-HRS-BEN-ELIG-ENROLLED  VALUE 'E'.           CPLNKELG
002610         88  PPELGRPT-HRS-BEN-ELIG-THRESHLD  VALUE 'T'.           CPLNKELG
002620         88  PPELGRPT-HRS-BEN-ELIG-ENRL-750  VALUE 'A'.           06061548
002630         88  PPELGRPT-HRS-BEN-ELIG-THRS-750  VALUE 'B'.           06061548
