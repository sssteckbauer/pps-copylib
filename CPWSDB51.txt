000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSDB51                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   30920381
000200*  COPYMEMBER: CPWSDB51                                      */   30920381
000300*  RELEASE: ____0381____  SERVICE REQUEST(S): ____3092____   */   30920381
000400*  NAME:    __J.WILCOX__  MODIFICATION DATE:  __11/13/88__   */   30920381
000500*  DESCRIPTION:                                              */   30920381
000600*                                                            */   30920381
000700*    - ADDED NEW DATA ELEMENTS REQUIRED FOR SECTION 89       */   30920381
000800*      BENEFITS PROCESSING                                   */   30920381
000900*      XDBS-5132-AVG-HOURS-WEEK (REPLACES FILLER 5132)       */   30920381
001000*      XDBS-5169-CUR-AVG-EXCLUDE                             */   30920381
001100*      XDBS-5170-JAN-AVG-EXCLUDE THROUGH                     */   30920381
001200*      XDBS-5181-DEC-AVG-EXCLUDE                             */   30920381
001300*    - SPACE FOR THE EXCLUSION FLAGS CAME FROM THE           */   30920381
001400*      ELIMINATION OF FILLER 5180 AND THE REDUCTION OF       */   30920381
001500*      FILLER 5170 (NOW FILLER 5168) TO 26 BYTES.            */   30920381
001600**************************************************************/   30920381
001700**************************************************************/   30860368
001800*  COPYMEMBER: CPWSDB51                                      */   30860368
001900*  RELEASE # ____0368____ SERVICE REQUEST NO(S)__3086________*/   30860368
002000*  NAME ______JLT______   MODIFICATION DATE ____08/26/88_____*/   30860368
002100*                                                            */   30860368
002200*  DESCRIPTION                                               */   30860368
002300*   - ADD NORMAL LEAVE HRS MAXIMUMS TO HOURS SEGMENT.        */   30860368
002400**************************************************************/   30860368
002500**************************************************************/   30860362
002600*  COPYMEMBER: CPWSDB51                                      */   30860362
002700*  RELEASE # ____0362____ SERVICE REQUEST NO(S)____3086______*/   30860362
002800*  NAME ______JLT______   MODIFICATION DATE ____05/26/88_____*/   30860362
002900*                                                            */   30860362
003000*  DESCRIPTION                                               */   30860362
003100*   - MODIFY HOURS SEGMENT (SEG 5100) DEFINITIONS FOR LEAVE  */   30860362
003200*     PROCESSING.                                            */   30860362
003300**************************************************************/   30860362
003400**************************************************************/   13190159
003500*  COPY MODULE:  CPWSDB51                                    */   13190159
003600*  RELEASE # ___0159___   SERVICE REQUEST NO(S)____1319______*/   13190159
003700*  NAME ___JAL_________   MODIFICATION DATE ____09/20/85_____*/   13190159
003800*  DESCRIPTION                                               */   13190159
003900*  FOR PHASE 2 OF THE PAYROLL FILE EXPANSION PROJECT,        */   13190159
004000*  USER-CODED TRANSACTION INPUT RECORD LENGTHS WILL BE       */   13190159
004100*  EXPANDED BY 22 BYTES, AND RECORDS AND WORK AREAS          */   13190159
004200*  RELATING TO THE EDB FILE WILL BE EXPANDED AS WELL.        */   13190159
004300*  THE EDB FILE 5100 SEGMENTS WILL BE EXPANDED               */   13190159
004400*  BY 66 BYTES FOR OFFICE OF THE PRESIDENT USE.              */   13190159
004500**************************************************************/   13190159
004600     SKIP2                                                        13190159
004700*    COPYID=CPWSDB51                                              CPWSDB51
004800*01  XDBS-HOURS-BALANCE-DATA51.                                   30930413
004900*                                                                *CPWSDB51
005000******************************************************************CPWSDB51
005100*        H O U R S   B A L A N C E   S E G M E N T   5 1 0 0     *CPWSDB51
005200******************************************************************CPWSDB51
005300*                                                                *CPWSDB51
005400     03  XDBS-DELETE-51              PICTURE X.                   CPWSDB51
005500         88  XDBS-SEQ-DELETED-51             VALUE HIGH-VALUES.   CPWSDB51
005600     03  XDBS-KEY-51.                                             CPWSDB51
005700         05  XDBS-ID-NUMBER-51       PICTURE X(9).                CPWSDB51
005800         05  XDBS-SEGMENT-51         PICTURE X(4).                CPWSDB51
005900     03  XDBS-HOURS-ELEMENTS.                                     CPWSDB51
006000         05  XDBS-5101-YTD-TOTAL-HRS PICTURE S9(7)V99    COMP-3.  CPWSDB51
006100         05  XDBS-5102-YTD-REG-HRS   PICTURE S9(7)V99    COMP-3.  CPWSDB51
006200         05  XDBS-5103-YTD-OTIME-HRS PICTURE S9(7)V99    COMP-3.  CPWSDB51
006300         05  XDBS-5104-YTD-NOWKD-HRS PICTURE S9(7)V99    COMP-3.  CPWSDB51
006400*********05  FILLER                  PICTURE S9(7)V99    COMP-3.  30860362
006500*********05  FILLER                  PICTURE S9(7)V99    COMP-3.  30860362
006600*********05  FILLER                  PICTURE S9(7)V99    COMP-3.  30860362
006700         05  XDBS-5105-SPEC-VAC-MAX  PICTURE S9(7)V99    COMP-3.  30860362
006800         05  XDBS-5106-SPEC-SKL-MAX  PICTURE S9(7)V99    COMP-3.  30860362
006900         05  XDBS-5107-SPEC-PTO-MAX  PICTURE S9(7)V99    COMP-3.  30860362
007000         05  FILLER-5108             PICTURE S9(7)V99    COMP-3.  30860362
007100         05  FILLER-5109             PICTURE S9(7)V99    COMP-3.  30860362
007200*********05  XDBS-5108-VAC-HRS-BAL   PICTURE S9(7)V99    COMP-3.  30860362
007300*********05  XDBS-5109-SK-LV-HRS-BAL PICTURE S9(7)V99    COMP-3.  30860362
007400         05  XDBS-5110-COMP-HRS-BAL  PICTURE S9(7)V99    COMP-3.  CPWSDB51
007500         05  FILLER                  PICTURE S9(7)V99    COMP-3.  CPWSDB51
007600         05  FILLER                  PICTURE S9(7)V99    COMP-3.  CPWSDB51
007700         05  FILLER-5113             PICTURE S9(7)V99    COMP-3.  30860362
007800         05  FILLER-5114             PICTURE S9(7)V99    COMP-3.  30860362
007900         05  FILLER-5115             PICTURE S9(7)V99    COMP-3.  30860362
008000         05  FILLER-5116             PICTURE S9(7)V99    COMP-3.  30860362
008100         05  FILLER-5117             PICTURE S9(7)V99    COMP-3.  30860362
008200         05  FILLER-5118             PICTURE S9(7)V99    COMP-3.  30860362
008300*********05  XDBS-5113-HRS-TWRD-LV-A PICTURE S9(7)V99    COMP-3.  30860362
008400*********05  XDBS-5114-HRS-TWRD-LV-B PICTURE S9(7)V99    COMP-3.  30860362
008500*********05  XDBS-5115-HRS-TWRD-LV-C PICTURE S9(7)V99    COMP-3.  30860362
008600*********05  XDBS-5116-HRS-TWRD-LV-D PICTURE S9(7)V99    COMP-3.  30860362
008700*********05  XDBS-5117-HRS-TWRD-LV-E PICTURE S9(7)V99    COMP-3.  30860362
008800*********05  XDBS-5118-HRS-TWRD-LV-F PICTURE S9(7)V99    COMP-3.  30860362
008900         05  XDBS-5119-CURR-MNTH-HRS PICTURE S9(7)V99    COMP-3.  CPWSDB51
009000         05  XDBS-5120-PAY-STAT-JAN  PICTURE S9(7)V99    COMP-3.  CPWSDB51
009100         05  XDBS-5121-PAY-STAT-FEB  PICTURE S9(7)V99    COMP-3.  CPWSDB51
009200         05  XDBS-5122-PAY-STAT-MAR  PICTURE S9(7)V99    COMP-3.  CPWSDB51
009300         05  XDBS-5123-PAY-STAT-APR  PICTURE S9(7)V99    COMP-3.  CPWSDB51
009400         05  XDBS-5124-PAY-STAT-MAY  PICTURE S9(7)V99    COMP-3.  CPWSDB51
009500         05  XDBS-5125-PAY-STAT-JUNE PICTURE S9(7)V99    COMP-3.  CPWSDB51
009600         05  XDBS-5126-PAY-STAT-JULY PICTURE S9(7)V99    COMP-3.  CPWSDB51
009700         05  XDBS-5127-PAY-STAT-AUG  PICTURE S9(7)V99    COMP-3.  CPWSDB51
009800         05  XDBS-5128-PAY-STAT-SEPT PICTURE S9(7)V99    COMP-3.  CPWSDB51
009900         05  XDBS-5129-PAY-STAT-OCT  PICTURE S9(7)V99    COMP-3.  CPWSDB51
010000         05  XDBS-5130-PAY-STAT-NOV  PICTURE S9(7)V99    COMP-3.  CPWSDB51
010100         05  XDBS-5131-PAY-STAT-DEC  PICTURE S9(7)V99    COMP-3.  CPWSDB51
010200         05  XDBS-5132-AVG-HRS-WEEK  PICTURE S9(7)V99    COMP-3.  30920381
010300*********05  FILLER-5132             PICTURE S9(7)V99    COMP-3.  30920381
010400         05  FILLER-5133             PICTURE S9(7)V99    COMP-3.  30860362
010500         05  FILLER-5134             PICTURE S9(7)V99    COMP-3.  30860362
010600         05  FILLER-5135             PICTURE S9(7)V99    COMP-3.  30860362
010700         05  XDBS-5136-NORM-VAC-MAX  PICTURE S9(7)V99    COMP-3.  30860368
010800         05  XDBS-5137-NORM-SKL-MAX  PICTURE S9(7)V99    COMP-3.  30860368
010900         05  XDBS-5138-NORM-PTO-MAX  PICTURE S9(7)V99    COMP-3.  30860368
011000*********05  FILLER-5136             PICTURE S9(7)V99    COMP-3.  30860368
011100*********05  FILLER-5137             PICTURE S9(7)V99    COMP-3.  30860368
011200*********05  FILLER-5138             PICTURE S9(7)V99    COMP-3.  30860368
011300         05  XDBS-5139-LAST-STUB-VAC-BAL                          30860362
011400                                     PICTURE S9(7)V99    COMP-3.  30860362
011500         05  XDBS-5140-LAST-STUB-SKL-BAL                          30860362
011600                                     PICTURE S9(7)V99    COMP-3.  30860362
011700         05  XDBS-5141-LAST-STUB-PTO-BAL                          30860362
011800                                     PICTURE S9(7)V99    COMP-3.  30860362
011900         05  FILLER-5142             PICTURE S9(7)V99    COMP-3.  30860362
012000         05  FILLER-OP1              PICTURE X(26).               30920381
012100         05  XDBS-5169-CUR-AVG-EXCLUDE      PICTURE X.            30920381
012200         05  XDBS-5170-5181-AVG-EXCLUDE.                          30920381
012300             07  XDBS-5170-JAN-AVG-EXCLUDE  PICTURE X.            30920381
012400             07  XDBS-5171-FEB-AVG-EXCLUDE  PICTURE X.            30920381
012500             07  XDBS-5172-MAR-AVG-EXCLUDE  PICTURE X.            30920381
012600             07  XDBS-5173-APR-AVG-EXCLUDE  PICTURE X.            30920381
012700             07  XDBS-5174-MAY-AVG-EXCLUDE  PICTURE X.            30920381
012800             07  XDBS-5175-JUN-AVG-EXCLUDE  PICTURE X.            30920381
012900             07  XDBS-5176-JUL-AVG-EXCLUDE  PICTURE X.            30920381
013000             07  XDBS-5177-AUG-AVG-EXCLUDE  PICTURE X.            30920381
013100             07  XDBS-5178-SEP-AVG-EXCLUDE  PICTURE X.            30920381
013200             07  XDBS-5179-OCT-AVG-EXCLUDE  PICTURE X.            30920381
013300             07  XDBS-5180-NOV-AVG-EXCLUDE  PICTURE X.            30920381
013400             07  XDBS-5181-DEC-AVG-EXCLUDE  PICTURE X.            30920381
013500         05  XDBS-MONTHLY-AVG-EXCLUDES      REDEFINES             30920381
013600             XDBS-5170-5181-AVG-EXCLUDE     OCCURS 12 TIMES.      30920381
013700             07  XDBS-MONTH-AVG-EXCLUDE     PICTURE X.            30920381
013800*********                                                      CD 30920381
013900*********05  XDBS-5170-FILLER-OP1    PICTURE X(30).               30920381
014000*********                                                      CD 30920381
014100*********05  XDBS-5170-FILLER-OP2    PICTURE X(09).               30920381
014200         05  XDBS-5182-LAST-STUB-VAC-IND                          30860362
014300                                     PICTURE X.                   30860362
014400         05  XDBS-5183-LAST-STUB-SKL-IND                          30860362
014500                                     PICTURE X.                   30860362
014600         05  XDBS-5184-LAST-STUB-PTO-IND                          30860362
014700                                     PICTURE X.                   30860362
014800         05  XDBS-5185-VAC-HRS-BAL   PICTURE S9(5)V9(6)  COMP-3.  30860362
014900         05  XDBS-5186-SKL-HRS-BAL   PICTURE S9(5)V9(6)  COMP-3.  30860362
015000         05  XDBS-5187-PTO-HRS-BAL   PICTURE S9(5)V9(6)  COMP-3.  30860362
015100         05  XDBS-5190-FILLER-OP3    PICTURE X(06).               13190159
015200*                                                                *CPWSDB51
015300******************************************************************CPWSDB51
015400*    H O U R   B A L A N C E   F O R   S U B S C R I P T I N G   *CPWSDB51
015500******************************************************************CPWSDB51
015600*                                                                *CPWSDB51
015700     03  XDBS-HRS-ELEMENTS   REDEFINES   XDBS-HOURS-ELEMENTS.     CPWSDB51
015800         05  XDBS-5100-HRS-ELEMNTS   OCCURS  42  TIMES.           CPWSDB51
015900             07  XDBS-5100-HRS-BAL   PICTURE S9(7)V99    COMP-3.  CPWSDB51
016000         05  XDBS-5100-FILLER-OP     PICTURE X(66).               13190159
