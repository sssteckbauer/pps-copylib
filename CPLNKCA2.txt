000000**************************************************************/   28521025
000001*  COPYMEMBER: CPLNKCA2                                      */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/01/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    INCLUDE THE CENTURY IN THE KCAL-YEAR FIELD.             */   28521025
000007**************************************************************/   28521025
000100**************************************************************/   36430911
000200*  COPYMEMBER: CPLNKCA2                                      */   36430911
000300*  RELEASE: ___0911______ SERVICE REQUEST(S): _____3643____  */   36430911
000400*  NAME:___SRS___________ MODIFICATION DATE:  ___06/30/94__  */   36430911
000500*  DESCRIPTION:                                              */   36430911
000600*  - NEW CPLNKCAL W/O OCCURS DEPENDING ON IN KCAL-PAY-CYCLE  */   36430911
000700**************************************************************/   36430911
000800*01 CALENDAR-INTERFACE.                                           CPLNKCA2
000900*-------------------------------------------------------------*   CPLNKCA2
001000*   INPUT FIELDS.                                                 CPLNKCA2
001100*-------------------------------------------------------------*   CPLNKCA2
001200****05  KCAL-YEAR                       PIC 9(02).                28521025
001210    05  KCAL-YEAR                       PIC 9(04).                28521025
001300    05  KCAL-MONTH                      PIC 9(02).                CPLNKCA2
001400*-------------------------------------------------------------*   CPLNKCA2
001500*   WHAT IS RETURNED TO THE CALLING PROGRAM.                      CPLNKCA2
001600*-------------------------------------------------------------*   CPLNKCA2
001700    05  KCAL-RETURN-CODE                PIC 9(01).                CPLNKCA2
001800        88  KCAL-NORMAL-RETURN            VALUE ZERO.             CPLNKCA2
001900        88  KCAL-NO-CYCLES-IN-MONTH       VALUE 1.                CPLNKCA2
002000        88  KCAL-MISMATCH-DATES           VALUE 3.                CPLNKCA2
002100        88  KCAL-CYCLE-TABLE-OVERFLOW     VALUE 5.                CPLNKCA2
002200        88  KCAL-PREV-MONTH-NOT-FOUND     VALUE 6.                CPLNKCA2
002300        88  KCAL-NEXT-MONTH-NOT-FOUND     VALUE 7.                CPLNKCA2
002400        88  KCAL-MONTH-NOT-FOUND          VALUE 8.                CPLNKCA2
002500        88  KCAL-CALENDAR-READ-ERROR      VALUE 9.                CPLNKCA2
002600    05  KCAL-DAYS-IN-MONTH              PIC 9(02).                CPLNKCA2
002700    05  KCAL-FISCAL-DAYS                PIC 9(02).                CPLNKCA2
002800    05  KCAL-NUMBER-OF-PAY-CYCLES       PIC S9(04) COMP.          CPLNKCA2
002900    05  KCAL-PAY-CYCLES.                                          CPLNKCA2
003000        10  KCAL-PAY-CYCLE              OCCURS 11 TIMES.          CPLNKCA2
003100            15  KCAL-CYCLE-TYPE         PIC X(02).                CPLNKCA2
003200            15  KCAL-CYCLE-BEGIN-DATE   PIC 9(06).                CPLNKCA2
003300            15  KCAL-CYCLE-END-DATE     PIC 9(06).                CPLNKCA2
003400            15  KCAL-CYCLE-CHECK-DATE   PIC 9(06).                CPLNKCA2
003500            15  KCAL-CYCLE-WORK-DAYS    PIC 9(02).                CPLNKCA2
003600*-----------    DATES ARE IN  YYMMDD  FORMAT      -------------*  CPLNKCA2
