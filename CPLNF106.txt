000100**************************************************************/   32021138
000200*  COPYMEMBER: CPLNF106                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_______JLT_______ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*    Initial release of linkage copymember between calling   */   32021138
000700*    program and module PPFAU106.                            */   32021138
000800**************************************************************/   32021138
000900*                                                             *   CPLNF106
001000*  This copymember is one of the Full Accounting Unit modules *   CPLNF106
001100*  which campuses may need to modify to accomodate their own  *   CPLNF106
001200*  Chart of Accounts structure.                               *   CPLNF106
001300*                                                             *   CPLNF106
001400***************************************************************   CPLNF106
001500*                                                                 CPLNF106
001600*01  PPFAU106-INTERFACE.                                          CPLNF106
001700     05  F106-INPUT.                                              CPLNF106
001800*                                                                 CPLNF106
001900         10  F106-IN-FAU                 PIC X(30).               CPLNF106
002000*                                                                 CPLNF106
002100      05  F106-OUTPUT.                                            CPLNF106
002200*                                                                 CPLNF106
002300         10  F106-OUT-FAU                PIC X(30).               CPLNF106
002400         10  F106-RETURN-IND             PIC X(01).               CPLNF106
002500             88  F106-RETURN-OK                  VALUE ' '.       CPLNF106
002600*                                                                 CPLNF106
002700*  *  *  *  *                                                     CPLNF106
