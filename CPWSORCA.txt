000100**************************************************************/   74611376
000200*  COPYMEMBER: CPWSORCA                                      */   74611376
000300*  RELEASE: ___1376______ SERVICE REQUEST(S): ____17461____  */   74611376
000400*  NAME:___STEINITZ______ MODIFICATION DATE:  ___10/31/01__  */   74611376
000500*  DESCRIPTION:                                              */   74611376
000600*  - ADDED LOGIC NEEDED FOR SUB-LOCATION PROCESSING.         */   74611376
000700**************************************************************/   74611376
000010**************************************************************/   32021138
000020*  COPYMEMBER: CPWSORCA                                      */   32021138
000030*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000040*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000050*  DESCRIPTION:                                              */   32021138
000060*  - MODIFIED FOR FULL ACCOUNTING UNIT                       */   32021138
000070**************************************************************/   32021138
000100**************************************************************/   36500900
000200*  COPYMEMBER: CPWSORCA                                      */   36500900
000300*  RELEASE: ___0900______ SERVICE REQUEST(S): ____3650_____  */   36500900
000400*  NAME: ___ RAB (BIT)___ MODIFICATION DATE:  __05/20/94____ */   36500900
000500*  DESCRIPTION:                                              */   36500900
000600*     MODIFIED FOR COST CENTER AND PROJECT CODE ENHANCEMENT  */   36500900
000700**************************************************************/   36500900
000100**************************************************************/   36100633
000200*  COPY MODULE:  CPWSORCA.                                   */   36100633
000300*  RELEASE __0633__           SERVICE REQUEST __3610__       */   36100633
000400*  NAME __SRS__               CREATION DATE ____09/04/91_____*/   36100633
000500*  DESCRIPTION                                               */   36100633
000600*   - CONTAINS THE WORK AREA FOR ORCA PROCESSING             */   36100633
000700**************************************************************/   36100633
000800                                                                  CPWSORCA
000900*01  ORCA-DATA.                                                   CPWSORCA
001000     05  ORCA-ID-NO                   PIC X(9).                   CPWSORCA
001200     05  ORCA-NAME                    PIC X(26).                  CPWSORCA
001300     05  ORCA-CHECK-DATE.                                         CPWSORCA
001400         10  ORCA-CHECK-YR            PIC 99.                     CPWSORCA
001500         10  ORCA-CHECK-MO            PIC 99.                     CPWSORCA
001600         10  ORCA-CHECK-DA            PIC 99.                     CPWSORCA
001700     05  ORCA-CHECK-NO                PIC 9(06).                  CPWSORCA
001800     05  ORCA-PAY-END-DATE.                                       CPWSORCA
001900         10  ORCA-PAY-END-YR          PIC 99.                     CPWSORCA
002000         10  ORCA-PAY-END-MO          PIC 99.                     CPWSORCA
002100         10  ORCA-PAY-END-DA          PIC 99.                     CPWSORCA
002200     05  ORCA-AFFECT-QTR              PIC X.                      CPWSORCA
002300     05  ORCA-AFFECT-YEAR             PIC X.                      CPWSORCA
002400     05  ORCA-GROSSES                 COMP-3.                     CPWSORCA
002500         10  ORCA-TOTAL-GROSS         PIC S9(7)V99.               CPWSORCA
002600         10  ORCA-FWT-GROSS           PIC S9(7)V99.               CPWSORCA
002700         10  ORCA-OASDI-GROSS         PIC S9(7)V99.               CPWSORCA
002800         10  ORCA-MEDICR-GROSS        PIC S9(7)V99.               CPWSORCA
002900         10  ORCA-STATE-GROSS         PIC S9(7)V99.               CPWSORCA
003000         10  ORCA-RETR-GROSS          PIC S9(7)V99.               CPWSORCA
003100         10  ORCA-NET-PAY             PIC S9(7)V99.               CPWSORCA
002400     05  ORCA-GROSSES-CORR            COMP-3.                     CPWSORCA
002500         10  ORCA-TOTAL-GROSS-CORR    PIC S9(7)V99.               CPWSORCA
002600         10  ORCA-FWT-GROSS-CORR      PIC S9(7)V99.               CPWSORCA
002700         10  ORCA-OASDI-GROSS-CORR    PIC S9(7)V99.               CPWSORCA
002800         10  ORCA-MEDICR-GROSS-CORR   PIC S9(7)V99.               CPWSORCA
002900         10  ORCA-STATE-GROSS-CORR    PIC S9(7)V99.               CPWSORCA
003000         10  ORCA-RETR-GROSS-CORR     PIC S9(7)V99.               CPWSORCA
003100         10  ORCA-NET-PAY-CORR        PIC S9(7)V99.               CPWSORCA
003200     05  ORCA-HGH-RETR-ELIG           PIC X.                      CPWSORCA
003300     05  ORCA-FICA-ELG                PIC X.                      CPWSORCA
003400     05  ORCA-STUDENT-STAT            PIC X.                      CPWSORCA
003500     05  ORCA-FED-TX-MARITAL-STATUS   PIC X.                      CPWSORCA
003600     05  ORCA-FED-TX-EXEMPT           PIC S9(3).                  CPWSORCA
003700     05  ORCA-ST-TX-MARITAL-STATUS    PIC X.                      CPWSORCA
003800     05  ORCA-ST-TX-PER-DED           PIC S9(3).                  CPWSORCA
003900     05  ORCA-ST-TX-ITM-DED           PIC S9(3).                  CPWSORCA
004000     05  ORCA-EMP-UNIT-CODE           PIC X(02).                  CPWSORCA
004100     05  ORCA-EMP-REL-CODE            PIC X(1).                   CPWSORCA
004200     05  ORCA-EMP-DIST-UNIT-CODE      PIC X(1).                   CPWSORCA
004300     05  ORCA-PRIMARY-PAY-SCHED       PIC X(2).                   CPWSORCA
007200*******                                                        CD 74611376
005810     05  ORCA-LIABILITY-FAU           PIC X(30).                  32021138
004500     05  ORCA-DCP-PLAN-CODE           PIC X(1).                   CPWSORCA
004600     05  ORCA-INS-REDUCT-IND          PIC X(1).                   CPWSORCA
004700     05  ORCA-RET-ELIG-CODE           PIC X(1).                   CPWSORCA
004800     05  ORCA-NO-ACCTS                PIC S9(4)     COMP.         CPWSORCA
004900     05  ORCA-NO-DED-REF              PIC S9(4)     COMP.         CPWSORCA
004901     05  ORCA-NO-ADDS                 PIC S9(4)     COMP.         CPWSORCA
005000     05  ORCA-ACCTS.                                              CPWSORCA
005100         10  ORCA-EARNING-DISTRIBUTION OCCURS 99 TIMES.           CPWSORCA
005200             15  ORCA-DIST-NO             PIC XX.                 CPWSORCA
007200*******                                                        CD 74611376
007500             15  ORCA-FAU                 PIC X(30).              32021138
005800             15  ORCA-TITLE-UNIT-CODE     PIC X(02).              CPWSORCA
005900             15  ORCA-EARN-SPCL-HNDLG-CODE PIC X.                 CPWSORCA
006000             15  ORCA-EARN-COVERAGE-IND   PIC X.                  CPWSORCA
006100             15  ORCA-EARN-DIST-UNIT-CODE PIC X.                  CPWSORCA
006200             15  ORCA-EARN-EMP-REL-CODE   PIC X.                  CPWSORCA
006300             15  ORCA-LV-ASSESSMNT-IND    PIC X.                  CPWSORCA
006400             15  ORCA-ACC-MGE-B           PIC X.                  CPWSORCA
006500             15  ORCA-PAYCY-CODE          PIC X.                  CPWSORCA
006600             15  ORCA-RETR-ELIG           PIC X.                  CPWSORCA
006700             15  ORCA-PAYRATE-HR          PIC S9(3)V9(4) COMP-3.  CPWSORCA
006800             15  ORCA-PAYRATE-SAL         REDEFINES               CPWSORCA
006900                      ORCA-PAYRATE-HR     PIC S9(5)V99   COMP-3.  CPWSORCA
007000             15  ORCA-RATE-TYPE           PIC X.                  CPWSORCA
007100             15  ORCA-EXPS-XFER           PIC X.                  CPWSORCA
007200             15  ORCA-LV-AC-CODE          PIC X.                  CPWSORCA
007300             15  ORCA-WK-STDY-CD          PIC X.                  CPWSORCA
007400             15  ORCA-INTRANS-CD          PIC XX.                 CPWSORCA
007500             15  ORCA-PERIOD-END-DATE.                            CPWSORCA
007600                 20  ORCA-PER-END-YR      PIC 99.                 CPWSORCA
007700                 20  ORCA-PER-END-MO      PIC 99.                 CPWSORCA
007800                 20  ORCA-PER-END-DA      PIC 99.                 CPWSORCA
007900             15  ORCA-TITLE-CODE.                                 CPWSORCA
008000                 20  ORCA-TTL-FILL        PIC 9.                  CPWSORCA
008100                 20  ORCA-TTL-CD          PIC 9(4).               CPWSORCA
008200             15  ORCA-DOS                 PIC X(3).               CPWSORCA
008300             15  ORCA-RATE-ADJ-IND        PIC X.                  CPWSORCA
008400             15  ORCA-TIME-HRS            PIC S9(3)V99   COMP-3.  CPWSORCA
008500             15  ORCA-EARN-AMT            PIC S9(5)V99   COMP-3.  CPWSORCA
008600             15  ORCA-EARN-PCT            PIC S9V9(4)    COMP-3.  CPWSORCA
008700             15  ORCA-APPT-TYPE           PIC X(1).               CPWSORCA
008701             15  ORCA-RATE-TYPE-CORR      PIC X.                  CPWSORCA
008710             15  ORCA-TIME-HRS-CORR       PIC S9(3)V99   COMP-3.  CPWSORCA
008720             15  ORCA-EARN-AMT-CORR       PIC S9(5)V99   COMP-3.  CPWSORCA
008730             15  ORCA-EARN-PCT-CORR       PIC S9V9(4)    COMP-3.  CPWSORCA
008720             15  ORCA-PAYRATE-HR-CORR     PIC S9(3)V9(4) COMP-3.  CPWSORCA
008730             15  ORCA-PAYRATE-SAL-CORR    PIC S9(5)V99   COMP-3.  CPWSORCA
012700             15  ORCA-SUB-LOCATION        PIC  X(02).             74611376
008800     05  ORCA-DEDUCTIONS.                                         CPWSORCA
008900             15  ORCA-DED-DATA            OCCURS 40 TIMES.        CPWSORCA
009000                 20  ORCA-GTN-NUMBER      PIC X(3).               CPWSORCA
009100                 20  ORCA-GTN-TYPE        PIC X.                  CPWSORCA
009200                 20  ORCA-GTN-GROUP       PIC X.                  CPWSORCA
009300                 20  ORCA-SPEC-TRANS      PIC X.                  CPWSORCA
009400                 20  ORCA-SOURCE-CODE     PIC X.                  CPWSORCA
009500                 20  ORCA-DED-QTR-CODE    PIC X.                  CPWSORCA
009600                 20  ORCA-DED-YR-CODE     PIC X.                  CPWSORCA
009700                 20  ORCA-DELETE          PIC X.                  CPWSORCA
009800                 20  ORCA-DED-AMT         PIC S9(5)V99   COMP-3.  CPWSORCA
009900                 20  ORCA-BRSC            PIC X(05).              CPWSORCA
010000                 20  ORCA-GTN-DESC        PIC X(15).              CPWSORCA
010100                 20  ORCA-OVRD-DED        PIC X(3).               CPWSORCA
010101                 20  ORCA-OVRD-DESC       PIC X(15).              CPWSORCA
010102                 20  ORCA-DED-AMT-CORR    PIC S9(5)V99   COMP-3.  CPWSORCA
010103     05  ORCA-ADDS.                                               CPWSORCA
010104             15  ORCA-GTN-ADDS            OCCURS 40 TIMES.        CPWSORCA
010105                 20  ORCA-ADD-GTN         PIC X(3).               CPWSORCA
010106                 20  ORCA-ADD-AMT         PIC S9(5)V99   COMP-3.  CPWSORCA
010107                 20  ORCA-ADD-DESC        PIC X(15).              CPWSORCA
010108                 20  ORCA-ADD-TYPE        PIC X(1).               CPWSORCA
010109                 20  ORCA-ADD-GROUP       PIC X(1).               CPWSORCA
