000100*==========================================================%      UCSD9999
000200*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD9999
000300*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD9999     =%      UCSD9999
000400*==========================================================%      UCSD9999
000500*==========================================================%      UCSD0035
000600*=   COPY MEMBER: CPWSDB05                                =%      UCSD0035
000700*=   CHANGE: ___0035____  SERVICE REQUEST(S): ___DT-035___=%      UCSD0035
000800*=   NAME:   JIM PIERCE   MODIFICATION DATE:  __03/14/91__=%      UCSD0035
000900*=   DESCRIPTION:                                         =%      UCSD0035
001000*=   DELETED DATA ELEMENTS FROM 0500 SEGMENT:             =%      UCSD0035
001100*=     XDBS-0530-CAMPUS-DATA                              =%      UCSD0035
001200*=     XDBS-0540-CAMPUS-DATA                              =%      UCSD0035
001300*=     XDBS-0590-CAMPUS-DATA ;                            =%      UCSD0035
001300*=   ADDED LOCAL DATA ELEMENTS TO 0500 SEGMENT:           =%      UCSD0035
001400*=     XDBS-0530-ACAD-DISCIPLINE                          =%      UCSD0035
001500*=     XDBS-0531-ASST-RANK-DATE                           =%      UCSD0035
001600*=     XDBS-0532-ASSO-RANK-DATE                           =%      UCSD0035
001700*=     XDBS-0533-FULL-RANK-DATE                           =%      UCSD0035
001800*=     XDBS-0534-STEP-DATE                                =%      UCSD0035
001900*=     XDBS-0535-EDU-INST                                 =%      UCSD0035
002000*=     XDBS-0536-EDU-LEVEL-2                              =%      UCSD0035
002100*=     XDBS-0537-EDU-LEVEL-2-YR                           =%      UCSD0035
002200*=     XDBS-0538-EDU-LEVEL-2-INST                         =%      UCSD0035
002300*=     XDBS-0539-PRIOR-SERVICE-INST                       =%      UCSD0035
002400*=     XDBS-0540-ACAD-SENATE-STATUS                       =%      UCSD0035
002500*=     XDBS-0541-CAMPUS-FILLER                            =%      UCSD0035
002700*=     XDBS-0590-CAMPUS-FILLER                            =%      UCSD0035
002800*=     XDBS-0591-LAST-UPD-OPERATOR                        =%      UCSD0035
002900*=     XDBS-0592-LAST-UPD-DATE                            =%      UCSD0035
003000*=     XDBS-0593-LAST-UPD-TIME                            =%      UCSD0035
003100*=     XDBS-0594-LAST-SCRN-FUNC                           =%      UCSD0035
002600*==========================================================%      UCSD0035
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSDB05                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
500600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   13190159
000200*  COPY MODULE:  CPWSDB05                                    */   13190159
000300*  RELEASE # ___0159___   SERVICE REQUEST NO(S)____1319______*/   13190159
000400*  NAME ___JAL_________   MODIFICATION DATE ____09/20/85_____*/   13190159
000500*  DESCRIPTION                                               */   13190159
000600*  FOR PHASE 2 OF THE PAYROLL FILE EXPANSION PROJECT,        */   13190159
000700*  USER-CODED TRANSACTION INPUT RECORD LENGTHS WILL BE       */   13190159
000800*  EXPANDED BY 22 BYTES, AND RECORDS AND WORK AREAS          */   13190159
000900*  RELATING TO THE EDB FILE WILL BE EXPANDED AS WELL.        */   13190159
001000*  THE EDB FILE ITSELF WILL BE EXPANDED AS FOLLOWS:          */   13190159
001100*   --  0500 SEGMENTS WILL BE EXPANDED                       */   13190159
001200*       BY 66 BYTES FOR INDIVIDUAL CAMPUS USE.               */   13190159
001300**************************************************************/   13190159
001400     SKIP2                                                        13190159
001500*    COPYID=CPWSDB05                                              CPWSDB05
001600*01  XDBS-CAMPUS-DATA5.                                           30930413
001700*                                                                *CPWSDB05
001800******************************************************************CPWSDB05
001900*        C A M P U S   D A T A   S E G M E N T   0 5 0 0         *CPWSDB05
002000******************************************************************CPWSDB05
002100*                                                                *CPWSDB05
002200     03  XDBS-DELETE-05              PICTURE X.                   CPWSDB05
002300         88  XDBS-SEG-DELETED-05             VALUE HIGH-VALUES.   CPWSDB05
002400     03  XDBS-KEY-05.                                             CPWSDB05
002500         05  XDBS-ID-NUMBER-05       PICTURE X(9).                CPWSDB05
002600         05  XDBS-SEGMENT-05         PICTURE X(4).                CPWSDB05
002700     03  XDBS-PERSONAL-DATA5-ELEMENTS.                            CPWSDB05
002800*                                                                *CPWSDB05
002900*****            ******************************              *****CPWSDB05
003000*                **  C A M P U S   D A T A   **                  *CPWSDB05
003100*****            ******************************              *****CPWSDB05
003200*                                                                *CPWSDB05
003300         05  XDBS-0510-CAMPUS-DATA   PICTURE X(30).               CPWSDB05
003400         05  XDBS-0520-CAMPUS-DATA   PICTURE X(30).               CPWSDB05
006800****     05  XDBS-0530-CAMPUS-DATA   PICTURE X(30).               CPWSDB05
006900****     05  XDBS-0540-CAMPUS-DATA   PICTURE X(30).               CPWSDB05
007000         05  XDBS-0530-ACAD-DISCIPLINE    PICTURE X(8).           UCSD0035
007100         05  XDBS-0531-ASST-RANK-DATE     PICTURE X(6).           UCSD0035
007200         05  XDBS-0532-ASSO-RANK-DATE     PICTURE X(6).           UCSD0035
007300         05  XDBS-0533-FULL-RANK-DATE     PICTURE X(6).           UCSD0035
007400         05  XDBS-0534-STEP-DATE          PICTURE X(6).           UCSD0035
007500         05  XDBS-0535-EDU-INST           PICTURE X(3).           UCSD0035
007600         05  XDBS-0536-EDU-LEVEL-2        PICTURE X(1).           UCSD0035
007700         05  XDBS-0537-EDU-LEVEL-2-YR     PICTURE X(2).           UCSD0035
007800         05  XDBS-0538-EDU-LEVEL-2-INST   PICTURE X(3).           UCSD0035
007900         05  XDBS-0539-PRIOR-SERVICE-INST PICTURE X(3).           UCSD0035
008000         05  XDBS-0540-ACAD-SENATE-STATUS PICTURE X(2).           UCSD0035
008100         05  XDBS-0541-CAMPUS-FILLER      PICTURE X(14).          UCSD0035
003700         05  XDBS-0550-CAMPUS-DATA   PICTURE X(30).               CPWSDB05
003800         05  XDBS-0560-CAMPUS-DATA   PICTURE X(30).               CPWSDB05
003900         05  XDBS-0570-CAMPUS-DATA   PICTURE X(30).               CPWSDB05
004000         05  XDBS-0580-CAMPUS-DATA   PICTURE X(30).               13190159
009200*        05  XDBS-0590-CAMPUS-DATA   PICTURE X(30).               UCSD0035
009300         05  XDBS-0590-CAMPUS-FILLER PICTURE X(09).               UCSD0035
009400         05  XDBS-0591-LAST-UPD-OPERATOR                          UCSD0035
009500                                     PICTURE X(03).               UCSD0035
009600         05  XDBS-0592-LAST-UPD-DATE PICTURE X(07).               UCSD0035
009700         05  XDBS-0593-LAST-UPD-TIME PICTURE X(07).               UCSD0035
009800         05  XDBS-0594-LAST-SCRN-FUNC                             UCSD0035
009900                                     PICTURE X(04).               UCSD0035
004200         05  XDBS-0595-CAMPUS-DATA   PICTURE X(06).               13190159
