000100*==========================================================%      UCSD9999
000200*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD9999
000300*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD9999     =%      UCSD9999
000400*==========================================================%      UCSD9999
000500*==========================================================%      UCSD0078
000600*=    COPY MEMBER: CLPDXDC3                               =%      UCSD0078
000700*=    CHANGE # UCSD0078     PROJ. REQUEST: REL988 TEMP FIX=%      UCSD0078
000800*=    NAME:JIM PIERCE       MODIFICATION DATE: 09/11/95   =%      UCSD0078
000900*=                                                        =%      UCSD0078
001000*=    DESCRIPTION: LOCAL VERSION OF CPPDXDC3 THAT         =%      UCSD0078
001100*=    REDIRECTS XDC3-STD-DATE FORMATTING TO CPPDXDC2      =%      UCSD0078
001200*=    WHEN XDC3-STD-DATE = 'YYMM99' TO PREVENT            =%      UCSD0078
001300*=    LE370 CEEDAYS FUNCTION FROM RETURNING               =%      UCSD0078
001400*=    ISO-FORMAT VALUE = '0001-01-01'                     =%      UCSD0078
001500*==========================================================%      UCSD0078
001600**************************************************************/   25330988
001700*  COPYMEMBER: CLPDXDC3                                      */   25330988
001800*  RELEASE: ___0988______ SERVICE REQUEST(S): ____12533____  */   25330988
001900*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___06/08/95__  */   25330988
002000*  DESCRIPTION:                                              */   25330988
002100*  - ADD LE370 CEELOCT FUNCTION TO GET CURRENT DATE/TIME.    */   25330988
002200*  - ADD FUNCTION TO SET LE/370 CENTURY WINDOW AFTER SAVING  */   25330988
002300*    THE CURRENT WINDOW VALUE.                               */   25330988
002400*  - ADD FUNCTION TO RESTORE LE/370 CENTURY WINDOW TO THE    */   25330988
002500*    VALUE PREVIOUSLY SAVED (DEFAULT SAVE VALUE IS 80).      */   25330988
002600*  - ADD FUNCTION TO VALIDATE ISO DATE INPUT.                */   25330988
002700*  - ADD FUNCTION TO CONVERT ISO DATE (REGARDLESS OF DAY)    */   25330988
002800*    TO THE LAST DAY OF THE GIVEN MONTH IN THE GIVEN YEAR.   */   25330988
002900**************************************************************/   25330988
003000**************************************************************/   36570961
003100*  COPYMEMBER: CLPDXDC3                                      */   36570961
003200*  RELEASE: ___0961______ SERVICE REQUEST(S): _____3657____  */   36570961
003300*  NAME:_______JAG_______ MODIFICATION DATE:  ___02/14/95__  */   36570961
003400*  DESCRIPTION:                                              */   36570961
003500*  CORRECTED SEPARATION FORMATTING IN CONVERT-USA-TO-FMT.    */   36570961
003600**************************************************************/   36570961
003700**************************************************************/   37930957
003800*  COPYMEMBER: CLPDXDC3                                      */   37930957
003900*  RELEASE: ___0957______ SERVICE REQUEST(S): ____13793____  */   37930957
004000*  NAME:_____J.WILCOX____ CREATION DATE:      ___01/19/95__  */   37930957
004100*  DESCRIPTION:                                              */   37930957
004200*  PROCEDURE DIVISION CODE FOR THIRD VERSION OF DATE         */   37930957
004300*  CONVERSION ROUTINES. USED IN CONJUNCTION WITH WORKING     */   37930957
004400*  STORAGE COPYMEMBER CPWSXDC3.                              */   37930957
004500**************************************************************/   37930957
004600 XDC3-CONVERT-STD-TO-ISO.                                         CLPDXDC3
004700     SET XDC3-VALID-DATE TO TRUE.                                 CLPDXDC3
004800     IF XDC3-STD-DATE = XDC3-HIGH-STD-DATE                        CLPDXDC3
004900         MOVE XDC3-HIGH-ISO-DATE TO XDC3-ISO-DATE                 CLPDXDC3
005000     ELSE                                                         CLPDXDC3
005100         IF XDC3-STD-DATE = XDC3-LOW-STD-DATE                     CLPDXDC3
005200             MOVE XDC3-LOW-ISO-DATE TO XDC3-ISO-DATE              CLPDXDC3
005300         ELSE                                                     CLPDXDC3
005400           IF XDC3-STD-DATE (5:2) = '99'                          UCSD0078
005500             MOVE XDC3-STD-DATE TO WSX-STD-DATE                   UCSD0078
005600             PERFORM CONVERT-STD-DATE                             UCSD0078
005700             MOVE DB2-DATE TO XDC3-ISO-DATE                       UCSD0078
005800           ELSE                                                   UCSD0078
005900             MOVE XDC3-STD-DATE TO XDC3-CHRDATE-STRING            CLPDXDC3
006000             CALL XDC3-PGM-CEEDAYS USING XDC3-CHRDATE,            CLPDXDC3
006100                                        XDC3-PICSTR-STD,          CLPDXDC3
006200                                        XDC3-LILIAN-DATE,         CLPDXDC3
006300                                        XDC3-FC                   CLPDXDC3
006400             IF XDC3-LILIAN-DATE = ZERO                           CLPDXDC3
006500                 SET XDC3-INVALID-DATE TO TRUE                    CLPDXDC3
006600                 MOVE XDC3-LOW-ISO-DATE TO XDC3-ISO-DATE          CLPDXDC3
006700             ELSE                                                 CLPDXDC3
006800                 CALL XDC3-PGM-CEEDATE USING XDC3-LILIAN-DATE,    CLPDXDC3
006900                                            XDC3-PICSTR-ISO,      CLPDXDC3
007000                                            XDC3-CHARDATE,        CLPDXDC3
007100                                            XDC3-FC               CLPDXDC3
007200                 MOVE XDC3-CHARDATE(1:10) TO XDC3-ISO-DATE        CLPDXDC3
007300             END-IF                                               CLPDXDC3
007400           END-IF                                                 UCSD0078
007500         END-IF                                                   CLPDXDC3
007600     END-IF.                                                      CLPDXDC3
007700*                                                                 CLPDXDC3
007800 XDC3-CONVERT-STD-LNG4-TO-ISO.                                    CLPDXDC3
007900     SET XDC3-VALID-DATE TO TRUE.                                 CLPDXDC3
008000     IF XDC3-STD-DATE-LNG4 = '9999'                               CLPDXDC3
008100         MOVE XDC3-HIGH-ISO-DATE TO XDC3-ISO-DATE                 CLPDXDC3
008200     ELSE                                                         CLPDXDC3
008300         IF XDC3-STD-DATE-LNG4 = '0000'                           CLPDXDC3
008400             MOVE XDC3-LOW-ISO-DATE TO XDC3-ISO-DATE              CLPDXDC3
008500         ELSE                                                     CLPDXDC3
008600             MOVE '01' TO XDC3-STD-DD                             CLPDXDC3
008700             PERFORM XDC3-CONVERT-STD-TO-ISO                      CLPDXDC3
008800         END-IF                                                   CLPDXDC3
008900     END-IF.                                                      CLPDXDC3
009000*                                                                 CLPDXDC3
009100 XDC3-CONVERT-STD-TO-USA.                                         CLPDXDC3
009200     SET XDC3-VALID-DATE TO TRUE.                                 CLPDXDC3
009300     IF XDC3-STD-DATE = XDC3-HIGH-STD-DATE                        CLPDXDC3
009400         MOVE XDC3-HIGH-USA-DATE TO XDC3-USA-DATE                 CLPDXDC3
009500     ELSE                                                         CLPDXDC3
009600         IF XDC3-STD-DATE = XDC3-LOW-STD-DATE                     CLPDXDC3
009700             MOVE XDC3-LOW-USA-DATE TO XDC3-USA-DATE              CLPDXDC3
009800         ELSE                                                     CLPDXDC3
009900             MOVE XDC3-STD-DATE TO XDC3-CHRDATE-STRING            CLPDXDC3
010000             CALL XDC3-PGM-CEEDAYS USING XDC3-CHRDATE,            CLPDXDC3
010100                                        XDC3-PICSTR-STD,          CLPDXDC3
010200                                        XDC3-LILIAN-DATE,         CLPDXDC3
010300                                        XDC3-FC                   CLPDXDC3
010400             IF XDC3-LILIAN-DATE = ZERO                           CLPDXDC3
010500                 SET XDC3-INVALID-DATE TO TRUE                    CLPDXDC3
010600                 MOVE XDC3-LOW-USA-DATE TO XDC3-USA-DATE          CLPDXDC3
010700             ELSE                                                 CLPDXDC3
010800                 CALL XDC3-PGM-CEEDATE USING XDC3-LILIAN-DATE,    CLPDXDC3
010900                                            XDC3-PICSTR-USA,      CLPDXDC3
011000                                            XDC3-CHARDATE,        CLPDXDC3
011100                                            XDC3-FC               CLPDXDC3
011200                 MOVE XDC3-CHARDATE(1:10) TO XDC3-USA-DATE        CLPDXDC3
011300             END-IF                                               CLPDXDC3
011400         END-IF                                                   CLPDXDC3
011500     END-IF.                                                      CLPDXDC3
011600*                                                                 CLPDXDC3
011700 XDC3-CONVERT-STD-TO-FMT.                                         CLPDXDC3
011800     MOVE XDC3-STD-YY TO XDC3-FMT-YY.                             CLPDXDC3
011900     MOVE XDC3-STD-MM TO XDC3-FMT-MM.                             CLPDXDC3
012000     MOVE XDC3-STD-DD TO XDC3-FMT-DD.                             CLPDXDC3
012100     MOVE '/' TO XDC3-FMT-SEP1,                                   CLPDXDC3
012200                 XDC3-FMT-SEP2.                                   CLPDXDC3
012300*                                                                 CLPDXDC3
012400 XDC3-CONVERT-ISO-TO-STD.                                         CLPDXDC3
012500     IF XDC3-ISO-DATE = XDC3-HIGH-ISO-DATE                        CLPDXDC3
012600         MOVE XDC3-HIGH-STD-DATE TO XDC3-STD-DATE                 CLPDXDC3
012700     ELSE                                                         CLPDXDC3
012800         IF XDC3-ISO-DATE = XDC3-LOW-ISO-DATE                     CLPDXDC3
012900             MOVE XDC3-LOW-STD-DATE TO XDC3-STD-DATE              CLPDXDC3
013000         ELSE                                                     CLPDXDC3
013100             MOVE XDC3-ISO-YY TO XDC3-STD-YY                      CLPDXDC3
013200             MOVE XDC3-ISO-MM TO XDC3-STD-MM                      CLPDXDC3
013300             MOVE XDC3-ISO-DD TO XDC3-STD-DD                      CLPDXDC3
013400         END-IF                                                   CLPDXDC3
013500     END-IF.                                                      CLPDXDC3
013600*                                                                 CLPDXDC3
013700 XDC3-CONVERT-ISO-TO-USA.                                         CLPDXDC3
013800     MOVE XDC3-ISO-CCYY TO XDC3-USA-CCYY.                         CLPDXDC3
013900     MOVE XDC3-ISO-MM TO XDC3-USA-MM.                             CLPDXDC3
014000     MOVE XDC3-ISO-DD TO XDC3-USA-DD.                             CLPDXDC3
014100     MOVE '/' TO XDC3-USA-SEP1,                                   CLPDXDC3
014200                 XDC3-USA-SEP2.                                   CLPDXDC3
014300*                                                                 CLPDXDC3
014400 XDC3-CONVERT-ISO-TO-FMT.                                         CLPDXDC3
014500     IF XDC3-ISO-DATE = XDC3-LOW-ISO-DATE                         CLPDXDC3
014600         MOVE XDC3-LOW-FMT-DATE TO XDC3-FMT-DATE                  CLPDXDC3
014700     ELSE                                                         CLPDXDC3
014800         IF XDC3-ISO-DATE = XDC3-HIGH-ISO-DATE                    CLPDXDC3
014900             MOVE XDC3-HIGH-FMT-DATE TO XDC3-FMT-DATE             CLPDXDC3
015000         ELSE                                                     CLPDXDC3
015100             MOVE XDC3-ISO-YY TO XDC3-FMT-YY                      CLPDXDC3
015200             MOVE XDC3-ISO-MM TO XDC3-FMT-MM                      CLPDXDC3
015300             MOVE XDC3-ISO-DD TO XDC3-FMT-DD                      CLPDXDC3
015400             MOVE '/' TO XDC3-FMT-SEP1,                           CLPDXDC3
015500                         XDC3-FMT-SEP2                            CLPDXDC3
015600         END-IF                                                   CLPDXDC3
015700     END-IF.                                                      CLPDXDC3
015800*                                                                 CLPDXDC3
015900 XDC3-CONVERT-ISO-TO-LILIAN.                                      CLPDXDC3
016000     MOVE XDC3-ISO-DATE TO XDC3-CHRDATE-STRING.                   CLPDXDC3
016100     CALL XDC3-PGM-CEEDAYS USING XDC3-CHRDATE,                    CLPDXDC3
016200                                 XDC3-PICSTR-ISO,                 CLPDXDC3
016300                                 XDC3-LILIAN-DATE,                CLPDXDC3
016400                                 XDC3-FC                          CLPDXDC3
016500     IF XDC3-LILIAN-DATE = ZERO                                   CLPDXDC3
016600         SET XDC3-INVALID-DATE TO TRUE                            CLPDXDC3
016700     ELSE                                                         CLPDXDC3
016800         SET XDC3-VALID-DATE TO TRUE                              CLPDXDC3
016900     END-IF.                                                      CLPDXDC3
017000*                                                                 CLPDXDC3
017100 XDC3-CONVERT-ISO-TO-INPUT6.                                      CLPDXDC3
017200     MOVE XDC3-ISO-MM TO XDC3-INPUT6-MM.                          CLPDXDC3
017300     MOVE XDC3-ISO-DD TO XDC3-INPUT6-DD.                          CLPDXDC3
017400     MOVE XDC3-ISO-YY TO XDC3-INPUT6-YY.                          CLPDXDC3
017500*                                                                 CLPDXDC3
017600 XDC3-CONVERT-USA-TO-STD.                                         CLPDXDC3
017700     IF XDC3-USA-DATE = XDC3-HIGH-USA-DATE                        CLPDXDC3
017800         MOVE XDC3-HIGH-STD-DATE TO XDC3-STD-DATE                 CLPDXDC3
017900     ELSE                                                         CLPDXDC3
018000         IF XDC3-USA-DATE = XDC3-LOW-USA-DATE                     CLPDXDC3
018100             MOVE XDC3-LOW-STD-DATE TO XDC3-STD-DATE              CLPDXDC3
018200         ELSE                                                     CLPDXDC3
018300             MOVE XDC3-USA-MM TO XDC3-STD-MM                      CLPDXDC3
018400             MOVE XDC3-USA-DD TO XDC3-STD-DD                      CLPDXDC3
018500             MOVE XDC3-USA-YY TO XDC3-STD-YY                      CLPDXDC3
018600         END-IF                                                   CLPDXDC3
018700     END-IF.                                                      CLPDXDC3
018800*                                                                 CLPDXDC3
018900 XDC3-CONVERT-USA-TO-ISO.                                         CLPDXDC3
019000     MOVE XDC3-USA-CCYY TO XDC3-ISO-CCYY.                         CLPDXDC3
019100     MOVE XDC3-USA-MM TO XDC3-ISO-MM.                             CLPDXDC3
019200     MOVE XDC3-USA-DD TO XDC3-ISO-DD.                             CLPDXDC3
019300     MOVE '-' TO XDC3-ISO-ISO1,                                   CLPDXDC3
019400                 XDC3-ISO-ISO2.                                   CLPDXDC3
019500*                                                                 CLPDXDC3
019600 XDC3-CONVERT-USA-TO-FMT.                                         CLPDXDC3
019700     IF XDC3-USA-DATE = XDC3-LOW-USA-DATE                         CLPDXDC3
019800         MOVE XDC3-LOW-FMT-DATE TO XDC3-FMT-DATE                  CLPDXDC3
019900     ELSE                                                         CLPDXDC3
020000         IF XDC3-USA-DATE = XDC3-HIGH-USA-DATE                    CLPDXDC3
020100             MOVE XDC3-HIGH-FMT-DATE TO XDC3-FMT-DATE             CLPDXDC3
020200         ELSE                                                     CLPDXDC3
020300             MOVE XDC3-USA-MM TO XDC3-FMT-MM                      CLPDXDC3
020400             MOVE XDC3-USA-DD TO XDC3-FMT-DD                      CLPDXDC3
020500             MOVE XDC3-USA-YY TO XDC3-FMT-YY                      CLPDXDC3
020600*************MOVE '/' TO XDC3-USA-SEP1,                           36570961
020700*************************XDC3-USA-SEP2                            36570961
020800             MOVE '/' TO XDC3-FMT-SEP1,                           36570961
020900                         XDC3-FMT-SEP2                            36570961
021000         END-IF                                                   CLPDXDC3
021100     END-IF.                                                      CLPDXDC3
021200*                                                                 CLPDXDC3
021300 XDC3-CONVERT-USA-TO-LILIAN.                                      CLPDXDC3
021400     MOVE XDC3-USA-DATE TO XDC3-CHRDATE-STRING.                   CLPDXDC3
021500     CALL XDC3-PGM-CEEDAYS USING XDC3-CHRDATE,                    CLPDXDC3
021600                                 XDC3-PICSTR-USA,                 CLPDXDC3
021700                                 XDC3-LILIAN-DATE,                CLPDXDC3
021800                                 XDC3-FC                          CLPDXDC3
021900     IF XDC3-LILIAN-DATE = ZERO                                   CLPDXDC3
022000         SET XDC3-INVALID-DATE TO TRUE                            CLPDXDC3
022100     ELSE                                                         CLPDXDC3
022200         SET XDC3-VALID-DATE TO TRUE                              CLPDXDC3
022300     END-IF.                                                      CLPDXDC3
022400*                                                                 CLPDXDC3
022500 XDC3-CONVERT-USA-TO-INPUT6.                                      CLPDXDC3
022600     MOVE XDC3-USA-MM TO XDC3-INPUT6-MM.                          CLPDXDC3
022700     MOVE XDC3-USA-DD TO XDC3-INPUT6-DD.                          CLPDXDC3
022800     MOVE XDC3-USA-YY TO XDC3-INPUT6-YY.                          CLPDXDC3
022900*                                                                 CLPDXDC3
023000 XDC3-CONVERT-LILIAN-TO-ISO.                                      CLPDXDC3
023100     CALL XDC3-PGM-CEEDATE USING XDC3-LILIAN-DATE,                CLPDXDC3
023200                                 XDC3-PICSTR-ISO,                 CLPDXDC3
023300                                 XDC3-CHARDATE,                   CLPDXDC3
023400                                 XDC3-FC.                         CLPDXDC3
023500     IF XDC3-CHARDATE = SPACES                                    CLPDXDC3
023600         SET XDC3-INVALID-DATE TO TRUE                            CLPDXDC3
023700         MOVE XDC3-LOW-ISO-DATE TO XDC3-ISO-DATE                  CLPDXDC3
023800     ELSE                                                         CLPDXDC3
023900         SET XDC3-VALID-DATE TO TRUE                              CLPDXDC3
024000         MOVE XDC3-CHARDATE(1:10) TO XDC3-ISO-DATE                CLPDXDC3
024100     END-IF.                                                      CLPDXDC3
024200*                                                                 CLPDXDC3
024300 XDC3-CONVERT-LILIAN-TO-USA.                                      CLPDXDC3
024400     CALL XDC3-PGM-CEEDATE USING XDC3-LILIAN-DATE,                CLPDXDC3
024500                                 XDC3-PICSTR-USA,                 CLPDXDC3
024600                                 XDC3-CHARDATE,                   CLPDXDC3
024700                                 XDC3-FC.                         CLPDXDC3
024800     IF XDC3-CHARDATE = SPACES                                    CLPDXDC3
024900         SET XDC3-INVALID-DATE TO TRUE                            CLPDXDC3
025000         MOVE XDC3-LOW-USA-DATE TO XDC3-USA-DATE                  CLPDXDC3
025100     ELSE                                                         CLPDXDC3
025200         SET XDC3-VALID-DATE TO TRUE                              CLPDXDC3
025300         MOVE XDC3-CHARDATE(1:10) TO XDC3-USA-DATE                CLPDXDC3
025400     END-IF.                                                      CLPDXDC3
025500*                                                                 CLPDXDC3
025600 XDC3-CONVERT-DB2-TO-STD.                                         CLPDXDC3
025700     IF XDC3-DB2-COL3 NUMERIC                                     CLPDXDC3
025800         MOVE XDC3-DB2-DATE TO XDC3-ISO-DATE                      CLPDXDC3
025900         PERFORM XDC3-CONVERT-ISO-TO-STD                          CLPDXDC3
026000     ELSE                                                         CLPDXDC3
026100         MOVE XDC3-DB2-DATE TO XDC3-USA-DATE                      CLPDXDC3
026200         PERFORM XDC3-CONVERT-USA-TO-STD                          CLPDXDC3
026300     END-IF.                                                      CLPDXDC3
026400*                                                                 CLPDXDC3
026500 XDC3-CONVERT-DB2-TO-ISO.                                         CLPDXDC3
026600     IF XDC3-DB2-COL3 NUMERIC                                     CLPDXDC3
026700         MOVE XDC3-DB2-DATE TO XDC3-ISO-DATE                      CLPDXDC3
026800     ELSE                                                         CLPDXDC3
026900         MOVE XDC3-DB2-DATE TO XDC3-USA-DATE                      CLPDXDC3
027000         PERFORM XDC3-CONVERT-USA-TO-ISO                          CLPDXDC3
027100     END-IF.                                                      CLPDXDC3
027200*                                                                 CLPDXDC3
027300 XDC3-CONVERT-DB2-TO-USA.                                         CLPDXDC3
027400     IF XDC3-DB2-COL3 NUMERIC                                     CLPDXDC3
027500         MOVE XDC3-DB2-DATE TO XDC3-ISO-DATE                      CLPDXDC3
027600         PERFORM XDC3-CONVERT-ISO-TO-USA                          CLPDXDC3
027700     ELSE                                                         CLPDXDC3
027800         MOVE XDC3-DB2-DATE TO XDC3-USA-DATE                      CLPDXDC3
027900     END-IF.                                                      CLPDXDC3
028000*                                                                 CLPDXDC3
028100 XDC3-CONVERT-DB2-TO-FMT.                                         CLPDXDC3
028200     IF XDC3-DB2-COL3 NUMERIC                                     CLPDXDC3
028300         MOVE XDC3-DB2-DATE TO XDC3-ISO-DATE                      CLPDXDC3
028400         PERFORM XDC3-CONVERT-ISO-TO-FMT                          CLPDXDC3
028500     ELSE                                                         CLPDXDC3
028600         MOVE XDC3-DB2-DATE TO XDC3-USA-DATE                      CLPDXDC3
028700         PERFORM XDC3-CONVERT-USA-TO-FMT                          CLPDXDC3
028800     END-IF.                                                      CLPDXDC3
028900*                                                                 CLPDXDC3
029000 XDC3-CONVERT-DB2-TO-LILIAN.                                      CLPDXDC3
029100     IF XDC3-DB2-COL3 NUMERIC                                     CLPDXDC3
029200         MOVE XDC3-DB2-DATE TO XDC3-ISO-DATE                      CLPDXDC3
029300         PERFORM XDC3-CONVERT-ISO-TO-LILIAN                       CLPDXDC3
029400     ELSE                                                         CLPDXDC3
029500         MOVE XDC3-DB2-DATE TO XDC3-USA-DATE                      CLPDXDC3
029600         PERFORM XDC3-CONVERT-USA-TO-LILIAN                       CLPDXDC3
029700     END-IF.                                                      CLPDXDC3
029800*                                                                 CLPDXDC3
029900 XDC3-CONVERT-DB2-TO-INPUT6.                                      CLPDXDC3
030000     IF XDC3-DB2-COL3 NUMERIC                                     CLPDXDC3
030100         MOVE XDC3-DB2-DATE TO XDC3-ISO-DATE                      CLPDXDC3
030200         PERFORM XDC3-CONVERT-ISO-TO-INPUT6                       CLPDXDC3
030300     ELSE                                                         CLPDXDC3
030400         MOVE XDC3-DB2-DATE TO XDC3-USA-DATE                      CLPDXDC3
030500         PERFORM XDC3-CONVERT-USA-TO-INPUT6                       CLPDXDC3
030600     END-IF.                                                      CLPDXDC3
030700*                                                                 CLPDXDC3
030800 XDC3-GET-INIT-DATE.                                              CLPDXDC3
030900     IF XDC3-DB2-COL3 NUMERIC                                     CLPDXDC3
031000         MOVE XDC3-LOW-ISO-DATE TO XDC3-INIT-DATE                 CLPDXDC3
031100     ELSE                                                         CLPDXDC3
031200         MOVE XDC3-LOW-USA-DATE TO XDC3-INIT-DATE                 CLPDXDC3
031300     END-IF.                                                      CLPDXDC3
031400*                                                                 CLPDXDC3
031500 XDC3-GET-HIGH-DATE.                                              CLPDXDC3
031600     IF XDC3-DB2-COL3 NUMERIC                                     CLPDXDC3
031700         MOVE XDC3-HIGH-ISO-DATE TO XDC3-HIGH-DATE                CLPDXDC3
031800     ELSE                                                         CLPDXDC3
031900         MOVE XDC3-HIGH-USA-DATE TO XDC3-HIGH-DATE                CLPDXDC3
032000     END-IF.                                                      CLPDXDC3
032100*                                                                 25330988
032200 XDC3-GET-CURRENT-DATE.                                           25330988
032300     CALL XDC3-PGM-CEELOCT USING XDC3-LILIAN-DATE,                25330988
032400                                 XDC3-LILIAN-TIMESTAMP,           25330988
032500                                 XDC3-GREGORIAN-CHAR,             25330988
032600                                 XDC3-FC.                         25330988
032700*                                                                 25330988
032800 XDC3-SET-CENTURY-WINDOW.                                         25330988
032900     CALL XDC3-PGM-CEEQCEN USING XDC3-SAVED-CENTURY-WINDOW,       25330988
033000                                 XDC3-FC.                         25330988
033100     CALL XDC3-PGM-CEESCEN USING XDC3-DESIRED-CENTURY-WINDOW,     25330988
033200                                 XDC3-FC.                         25330988
033300*                                                                 25330988
033400 XDC3-RESET-CENTURY-WINDOW.                                       25330988
033500     CALL XDC3-PGM-CEESCEN USING XDC3-SAVED-CENTURY-WINDOW,       25330988
033600                                 XDC3-FC.                         25330988
033700*                                                                 25330988
033800 XDC3-VALIDATE-ISO-DATE.                                          25330988
033900     PERFORM XDC3-CONVERT-ISO-TO-LILIAN.                          25330988
034000*                                                                 25330988
034100 XDC3-GET-END-OF-MONTH-DATE.                                      25330988
034200     PERFORM XDC3-VALIDATE-ISO-DATE.                              25330988
034300     IF XDC3-VALID-DATE                                           25330988
034400         MOVE 1 TO XDC3-ISO-DD                                    25330988
034500         IF XDC3-ISO-MM < 12                                      25330988
034600             ADD 1 TO XDC3-ISO-MM                                 25330988
034700         ELSE                                                     25330988
034800             ADD 1 TO XDC3-ISO-CCYY                               25330988
034900             MOVE 1 TO XDC3-ISO-MM                                25330988
035000         END-IF                                                   25330988
035100         PERFORM XDC3-CONVERT-ISO-TO-LILIAN                       25330988
035200         SUBTRACT 1 FROM XDC3-LILIAN-DATE                         25330988
035300         PERFORM XDC3-CONVERT-LILIAN-TO-ISO                       25330988
035400     END-IF.                                                      25330988
035500*                                                                 CLPDXDC3
035600 XDC3-VALIDATE-INPUT6-TO-ISO.                                     CLPDXDC3
035700     MOVE XDC3-INPUT6-DATE TO XDC3-CHRDATE-STRING.                CLPDXDC3
035800     CALL XDC3-PGM-CEEDAYS USING XDC3-CHRDATE,                    CLPDXDC3
035900                                XDC3-PICSTR-INPUT6,               CLPDXDC3
036000                                XDC3-LILIAN-DATE,                 CLPDXDC3
036100                                XDC3-FC.                          CLPDXDC3
036200     PERFORM XDC3-CONVERT-LILIAN-TO-ISO.                          CLPDXDC3
036300*                                                                 CLPDXDC3
036400 XDC3-VALIDATE-INPUT8-TO-ISO.                                     CLPDXDC3
036500     MOVE XDC3-INPUT8-DATE TO XDC3-CHRDATE-STRING.                CLPDXDC3
036600     CALL XDC3-PGM-CEEDAYS USING XDC3-CHRDATE,                    CLPDXDC3
036700                                XDC3-PICSTR-INPUT8,               CLPDXDC3
036800                                XDC3-LILIAN-DATE,                 CLPDXDC3
036900                                XDC3-FC.                          CLPDXDC3
037000     PERFORM XDC3-CONVERT-LILIAN-TO-ISO.                          CLPDXDC3
037100*                                                                 CLPDXDC3
