           05  GTN-CB-REC-WORK-AREA.

               10  GTN-CB-KEY-AREA.
                   15  GTN-CB-CONSTANT          PIC  X(04).
                   15  GTN-CB-GROUP             PIC  X(02).
                   15  GTN-CB-SEQ-NO            PIC  9(01).
                   15  FILLER                   PIC  X(01).

               10  GTN-CB-NUM-OF-OCCURS         PIC S9(04) COMP SYNC.

               10  GTN-CB-TABLE-ENTRIES     OCCURS 1 TO 300 TIMES
                     DEPENDING ON GTN-CB-NUM-OF-OCCURS
                       INDEXED BY GTN-CB-INDEX.

                   15  GTN-CB-DEDUCTION-NO       PIC X(03).
                   15  GTN-CB-BENEFIT-TYPE       PIC X(01).
                   15  GTN-CB-BENEFIT-PLAN       PIC X(02).
                   15  GTN-CB-CBELI              PIC X(01).
                   15  GTN-CB-CBBC               PIC X(01).

