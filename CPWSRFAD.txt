000100**************************************************************/  *36090553
000200*  COPYMEMBER: CPWSRFAD                                      */  *36090553
000300*  RELEASE: ____0553____  SERVICE REQUEST(S): ____3609____   */  *36090553
000400*  NAME: ______PXP______  CREATION DATE: ____04/08/91_____   */  *36090553
000500*  DESCRIPTION:                                              */  *36090553
000600*  - WORKING-STORAGE LAYOUT OF FAD TABLE ROW.                */  *36090553
000700**************************************************************/  *36090553
000800*    COPYID=CPWSRFAD                                              CPWSRFAD
000900*01  FAD-ROW.                                                     CPWSRFAD
001000     10 EMPLOYEE-ID          PIC X(9).                            CPWSRFAD
001100     10 FAD-COUNTRY          PIC X(2).                            CPWSRFAD
001200     10 FAD-PROVINCE         PIC X(15).                           CPWSRFAD
001300     10 FAD-POSTAL-CODE      PIC X(10).                           CPWSRFAD
001400******************************************************************CPWSRFAD
