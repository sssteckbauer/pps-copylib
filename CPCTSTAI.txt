000000**************************************************************/   30871460
000001*  COPYMEMBER: CPCTSTAI                                      */   30871460
000002*  RELEASE: ___1460______ SERVICE REQUEST(S): _____3087____  */   30871460
000003*  NAME:_____SRS_________ CREATION DATE:      ___01/22/03__  */   30871460
000004*  DESCRIPTION:                                              */   30871460
000005*  - NEW COPY MEMBER FOR STATE TAX TABLE INPUT (PPPSTA)      */   30871460
000007**************************************************************/   30871460
000008*    COPYID=CPCTSTAI                                              CPCTSTAI
000009*01  PPPSTA-TABLE-INPUT.                                          CPCTSTAI
000100     05  STAI-PERIOD-TYPE                PIC X.                   CPCTSTAI
000130     05  STAI-A-LI-DATA.                                          CPCTSTAI
000140         10 STAI-A-LI-SINGLE-X.                                   CPCTSTAI
000150            15 STAI-A-LI-SINGLE          PIC S9(5)V99.            CPCTSTAI
000160         10 STAI-A-LI-MARRIED1-X.                                 CPCTSTAI
000170            15 STAI-A-LI-MARRIED1        PIC S9(5)V99.            CPCTSTAI
000180         10 STAI-A-LI-MARRIED2-X.                                 CPCTSTAI
000190            15 STAI-A-LI-MARRIED2        PIC S9(5)V99.            CPCTSTAI
000200         10 STAI-A-LI-HEAD-HSE-X.                                 CPCTSTAI
000210            15 STAI-A-LI-HEAD-HSE        PIC S9(5)V99.            CPCTSTAI
000230     05  STAI-C-SD-DATA.                                          CPCTSTAI
000240         10 STAI-C-SD-SINGLE-X.                                   CPCTSTAI
000250            15 STAI-C-SD-SINGLE          PIC S9(5)V99.            CPCTSTAI
000260         10 STAI-C-SD-MARRIED1-X.                                 CPCTSTAI
000270            15 STAI-C-SD-MARRIED1        PIC S9(5)V99.            CPCTSTAI
000280         10 STAI-C-SD-MARRIED2-X.                                 CPCTSTAI
000290            15 STAI-C-SD-MARRIED2        PIC S9(5)V99.            CPCTSTAI
000300         10 STAI-C-SD-HEAD-HSE-X.                                 CPCTSTAI
000310            15 STAI-C-SD-HEAD-HSE        PIC S9(5)V99.            CPCTSTAI
