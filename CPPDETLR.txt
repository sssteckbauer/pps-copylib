000800*==========================================================%      UCSD0102
000200*=    COPY MEMBER: CPPDETLR                               =%      UCSD0102
000300*=    CHANGE #UCSD0102      PROJ. REQUEST: RELEASE 1138   =%      UCSD0102
000400*=    NAME: D. CHILCOAT     MODIFICATION DATE: 11/19/97   =%      UCSD0102
000500*=                                                        =%      UCSD0102
000600*=    DESCRIPTION                                         =%      UCSD0102
000700*=    CHANGES MADE TO ACCOMMODATE THE LOCAL COA           =%      UCSD0102
000800*=                                                        =%      UCSD0102
008000*==========================================================%      UCSD0102
000100**************************************************************/   32021138
000200*  COPYMEMBER: CPPDETLR                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_PHIL THOMPSON___ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*  BASE FAU RELATED PROCEDURE CODE FOR ONLINE FUNCTION ETLR. */   32021138
000700*  THIS CODE ONLY WORKS WITH THE BASE PROGRAM CPPDETLR AND   */   32021138
000800*  BASE MAP PPETLR0.                                         */   32021138
000900**************************************************************/   32021138
072700 CPPD-INITIALIZE-FIELDS SECTION.                                  CPPDETLR
072800******************************************************************CPPDETLR
072900*  INITIALIZE COLOR, HIGHLIGHTING AND PROTECTION ATTRIBUTES      *CPPDETLR
073000******************************************************************CPPDETLR
073300     PERFORM VARYING I FROM                                       CPPDETLR
073400         1 BY 1 UNTIL I > WS-NUMBER-SCRN-TRANS                    CPPDETLR
073500         MOVE UCCOMMON-CLR-ENTRY TO                               CPPDETLR
074500*****                           DE0230-XTAT-LOCC (I)              UCSD0102
074600*****                           DE0230-XTAT-ACCTC (I)             UCSD0102
074700*****                           DE0230-XTAT-COST-CENTERC (I)      UCSD0102
074800*****                           DE0230-XTAT-FNDC (I)              UCSD0102
074900*****                           DE0230-XTAT-PROJECT-CODEC (I)     UCSD0102
073110                                DE0230-XTAT-IFIS-INDEXC (I)       UCSD0102
075000                                DE0230-XTAT-SUBC (I)              CPPDETLR
076100     END-PERFORM.                                                 CPPDETLR
077700     PERFORM VARYING I FROM                                       CPPDETLR
077800         1 BY 1 UNTIL I > WS-NUMBER-SCRN-TRANS                    CPPDETLR
079800*****    MOVE WS-ATR TO         DE0230-XTAT-LOCA (I)              UCSD0102
080000*****    MOVE WS-ATR TO         DE0230-XTAT-ACCTA (I)             UCSD0102
080200*****    MOVE WS-ATR TO         DE0230-XTAT-COST-CENTERA (I)      UCSD0102
080400*****    MOVE WS-ATR TO         DE0230-XTAT-FNDA (I)              UCSD0102
080600*****    MOVE WS-ATR TO         DE0230-XTAT-PROJECT-CODEA (I)     UCSD0102
080800         MOVE WS-ATR TO         DE0230-XTAT-IFIS-INDEXA (I)       UCSD0102
080800         MOVE WS-ATR TO         DE0230-XTAT-SUBA (I)              CPPDETLR
083800     END-PERFORM.                                                 CPPDETLR
083900                                                                  CPPDETLR
084000     PERFORM VARYING I FROM                                       CPPDETLR
084100         1 BY 1 UNTIL I > WS-NUMBER-SCRN-TRANS                    CPPDETLR
084200         MOVE UCCOMMON-HLT-ENTRY TO                               CPPDETLR
085200*****                           DE0230-XTAT-LOCH (I)              UCSD0102
085300*****                           DE0230-XTAT-ACCTH (I)             UCSD0102
085400*****                           DE0230-XTAT-COST-CENTERH (I)      UCSD0102
085500*****                           DE0230-XTAT-FNDH (I)              UCSD0102
085600*****                           DE0230-XTAT-PROJECT-CODEH (I)     UCSD0102
073110                                DE0230-XTAT-IFIS-INDEXH (I)       UCSD0102
085700                                DE0230-XTAT-SUBH (I)              CPPDETLR
086800     END-PERFORM.                                                 CPPDETLR
087500                                                                  CPPDETLR
102400 CPPD-FORMAT-DATA           SECTION.                              CPPDETLR
102500******************************************************************CPPDETLR
102600*  FORMAT CPWSEDTH WITH DATA FROM THE TRANSACTION FOR PPVRTHFO   *CPPDETLR
102700******************************************************************CPPDETLR
109400                                                                  CPPDETLR
109510     ADD +1  TO CPWSEDTH-CNT.                                     CPPDETLR
109600     MOVE I TO CPWSEDTH-ELEM-SCREEN-OCCURNCE (CPWSEDTH-CNT).      CPPDETLR
109610     MOVE XTAT-FAU OF CCWSTHFT-ITEM (I)                           CPPDETLR
109620       TO CPWSEDTH-TRN-DATA(CPWSEDTH-CNT).                        CPPDETLR
109900     MOVE 0230 TO CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT).             CPPDETLR
110000     SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUE.     CPPDETLR
121400                                                                  CPPDETLR
121500 CPPD-FORMAT-SCREEN         SECTION.                              CPPDETLR
121600******************************************************************CPPDETLR
121700*  MOVE DATA FORMATTED BY PPVRTHFO TO THE MAP                    *CPPDETLR
121800******************************************************************CPPDETLR
121900                                                                  CPPDETLR
130300     MOVE CPWSEDTH-ELEM-SCREEN-OCCURNCE (EDIT-SUB) TO I.          CPPDETLR
130400     MOVE CPWSEDTH-SCREEN-DATA(EDIT-SUB) TO FAUR-FAU.             CPPDETLR
130510*****MOVE FAUR-LOCATION TO DE0230-XTAT-LOCO (I).                  UCSD0102
130530*****MOVE FAUR-ACCOUNT TO DE0230-XTAT-ACCTO (I).                  UCSD0102
130550*****MOVE FAUR-COST-CENTER TO DE0230-XTAT-COST-CENTERO (I).       UCSD0102
130560*****MOVE FAUR-FUND TO DE0230-XTAT-FNDO (I).                      UCSD0102
130570*****MOVE FAUR-PROJECT-CODE TO DE0230-XTAT-PROJECT-CODEO (I).     UCSD0102
130580     MOVE FAUR-INDEX TO DE0230-XTAT-IFIS-INDEXO (I)               UCSD0102
130580     MOVE FAUR-FUND TO DE0230-XTAT-IFIS-FUNDO (I)                 UCSD0102
130580     MOVE FAUR-SUB-ACCOUNT TO DE0230-XTAT-SUBO (I).               CPPDETLR
130600     IF CCWSTHFT-CHANGED-AT (I) > SPACE                           CPPDETLR
130700            AND UCCOMMON-FUNCTION(1:2) NOT = 'ET'                 CPPDETLR
130800        MOVE UCCOMMON-ATR-ENTRY-PRO TO                            CPPDETLR
130900*****                           DE0230-XTAT-LOCA (I)              UCSD0102
130910*****                           DE0230-XTAT-ACCTA (I)             UCSD0102
130920*****                           DE0230-XTAT-COST-CENTERA (I)      UCSD0102
130930*****                           DE0230-XTAT-FNDA (I)              UCSD0102
130940*****                           DE0230-XTAT-PROJECT-CODEA (I)     UCSD0102
130941                                DE0230-XTAT-IFIS-INDEXA (I)       UCSD0102
130950                                DE0230-XTAT-SUBA (I)              CPPDETLR
131000     END-IF.                                                      CPPDETLR
145300                                                                  CPPDETLR
145400 CPPD-EDIT-DATA   SECTION.                                        CPPDETLR
145500***************************************************************** CPPDETLR
145600*  ALL ALTERED MAP FIELDS WILL BE PASSED TO VREDIT FOR          * CPPDETLR
145700*  VALUE-RANGE EDITING.                                         * CPPDETLR
145900*  FOR MAP FIELDS NOT ALTERED, VALUES FROM THE TRN EXTERNALS    * CPPDETLR
146000*  WILL BE PASSED TO VREDIT FOR DATA FORMATTING.                * CPPDETLR
146200***************************************************************** CPPDETLR
175500                                                                  CPPDETLR
175501                                                                  CPPDETLR
175502     ADD +1 TO CPWSEDTH-CNT                                       CPPDETLR
175503     MOVE I TO  CPWSEDTH-ELEM-SCREEN-OCCURNCE (CPWSEDTH-CNT)      CPPDETLR
175504     MOVE 0230 TO CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT)              CPPDETLR
175507     MOVE XTAT-FAU OF CCWSTHFT-ITEM (I) TO FAUR-FAU               CPPDETLR
175508     MOVE XTAT-FAU OF CCWSTHFH-ITEM-HOLD (I) TO FAUR-FAU-HOLD     CPPDETLR
175509*****                                                             UCSD0102
175510*****MOVE UCCOMMON-CLR-ENTRY TO                                   UCSD0102
175511*****                 DE0230-XTAT-LOCC (I)                        UCSD0102
175512*****MOVE UCCOMMON-HLT-ENTRY TO                                   UCSD0102
175513*****                 DE0230-XTAT-LOCH (I)                        UCSD0102
175514*****IF DE0230-XTAT-LOCL (I) > ZERO    OR                         UCSD0102
175515*****   DE0230-XTAT-LOCF (I) = ERASE-EOF                          UCSD0102
175516*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      UCSD0102
175517*****   IF DE0230-XTAT-LOCF (I) = ERASE-EOF                       UCSD0102
175518*****      OR (CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) = 0180          UCSD0102
175519*****          AND DE0040-LINE-COMMANDI (I) = 'R')                UCSD0102
175520*****      OR (DE0040-LINE-COMMANDI (I) = 'B'                     UCSD0102
175521*****      AND CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) NOT = 0040)     UCSD0102
175522*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       UCSD0102
175523*****      MOVE FAUR-LOCATION-HOLD TO FAUR-LOCATION               UCSD0102
175524*****      MOVE UCCOMMON-ATR-ENTRY TO                             UCSD0102
175525*****                  DE0230-XTAT-LOCA (I)                       UCSD0102
175526*****   ELSE                                                      UCSD0102
175527*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         UCSD0102
175600*****                  DE0230-XTAT-LOCA (I)                       UCSD0102
175700*****      MOVE DE0230-XTAT-LOCI (I) TO FAUR-LOCATION             UCSD0102
175900*****   END-IF                                                    UCSD0102
176000*****ELSE                                                         UCSD0102
176100*****   IF NOT CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)           UCSD0102
176200*****      SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUEUCSD0102
176300*****   END-IF                                                    UCSD0102
176400*****END-IF.                                                      UCSD0102
176500*****                                                             UCSD0102
176900*****MOVE UCCOMMON-CLR-ENTRY TO                                   UCSD0102
177000*****                 DE0230-XTAT-ACCTC (I)                       UCSD0102
177100*****MOVE UCCOMMON-HLT-ENTRY TO                                   UCSD0102
177200*****                 DE0230-XTAT-ACCTH (I)                       UCSD0102
177300*****IF DE0230-XTAT-ACCTL (I) > ZERO    OR                        UCSD0102
177400*****   DE0230-XTAT-ACCTF (I) = ERASE-EOF                         UCSD0102
177500*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      UCSD0102
177600*****   IF DE0230-XTAT-ACCTF (I) = ERASE-EOF                      UCSD0102
177700*****      OR (CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) = 0180          UCSD0102
177800*****          AND DE0040-LINE-COMMANDI (I) = 'R')                UCSD0102
177900*****      OR (DE0040-LINE-COMMANDI (I) = 'B'                     UCSD0102
178000*****      AND CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) NOT = 0040)     UCSD0102
178100*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       UCSD0102
178200*****      MOVE FAUR-ACCOUNT-HOLD TO FAUR-ACCOUNT                 UCSD0102
178400*****      MOVE UCCOMMON-ATR-ENTRY TO                             UCSD0102
178500*****                  DE0230-XTAT-ACCTA (I)                      UCSD0102
178600*****   ELSE                                                      UCSD0102
178700*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         UCSD0102
178800*****                  DE0230-XTAT-ACCTA (I)                      UCSD0102
178900*****      MOVE DE0230-XTAT-ACCTI (I) TO FAUR-ACCOUNT             UCSD0102
179100*****   END-IF                                                    UCSD0102
179200*****ELSE                                                         UCSD0102
179210*****   IF NOT CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)           UCSD0102
179400*****      SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUEUCSD0102
179500*****   END-IF                                                    UCSD0102
179600*****END-IF.                                                      UCSD0102
179700*****                                                             UCSD0102
180100*****MOVE UCCOMMON-CLR-ENTRY TO                                   UCSD0102
180200*****                 DE0230-XTAT-COST-CENTERC (I)                UCSD0102
180300*****MOVE UCCOMMON-HLT-ENTRY TO                                   UCSD0102
180400*****                 DE0230-XTAT-COST-CENTERH (I)                UCSD0102
180500*****IF DE0230-XTAT-COST-CENTERL (I) > ZERO    OR                 UCSD0102
180600*****   DE0230-XTAT-COST-CENTERF (I) = ERASE-EOF                  UCSD0102
180700*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      UCSD0102
180800*****   IF DE0230-XTAT-COST-CENTERF (I) = ERASE-EOF               UCSD0102
180900*****      OR (CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) = 0180          UCSD0102
181000*****          AND DE0040-LINE-COMMANDI (I) = 'R')                UCSD0102
181100*****      OR (DE0040-LINE-COMMANDI (I) = 'B'                     UCSD0102
181200*****      AND CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) NOT = 0040)     UCSD0102
181300*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       UCSD0102
181400*****      MOVE FAUR-COST-CENTER-HOLD TO FAUR-COST-CENTER         UCSD0102
181600*****      MOVE UCCOMMON-ATR-ENTRY TO                             UCSD0102
181700*****                  DE0230-XTAT-COST-CENTERA (I)               UCSD0102
181800*****   ELSE                                                      UCSD0102
181900*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         UCSD0102
182000*****                  DE0230-XTAT-COST-CENTERA (I)               UCSD0102
182100*****      MOVE DE0230-XTAT-COST-CENTERI (I) TO FAUR-COST-CENTER  UCSD0102
182300*****   END-IF                                                    UCSD0102
182400*****ELSE                                                         UCSD0102
182410*****   IF NOT CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)           UCSD0102
182600*****      SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUEUCSD0102
182700*****   END-IF                                                    UCSD0102
182800*****END-IF.                                                      UCSD0102
182900*****                                                             UCSD0102
183300*****MOVE UCCOMMON-CLR-ENTRY TO                                   UCSD0102
183400*****                 DE0230-XTAT-FNDC (I)                        UCSD0102
183500*****MOVE UCCOMMON-HLT-ENTRY TO                                   UCSD0102
183600*****                 DE0230-XTAT-FNDH (I)                        UCSD0102
183700*****IF DE0230-XTAT-FNDL (I) > ZERO    OR                         UCSD0102
183800*****   DE0230-XTAT-FNDF (I) = ERASE-EOF                          UCSD0102
183900*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      UCSD0102
184000*****   IF DE0230-XTAT-FNDF (I) = ERASE-EOF                       UCSD0102
184100*****      OR (CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) = 0180          UCSD0102
184200*****          AND DE0040-LINE-COMMANDI (I) = 'R')                UCSD0102
184300*****      OR (DE0040-LINE-COMMANDI (I) = 'B'                     UCSD0102
184400*****      AND CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) NOT = 0040)     UCSD0102
184500*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       UCSD0102
184600*****      MOVE FAUR-FUND-HOLD TO FAUR-FUND                       UCSD0102
184800*****      MOVE UCCOMMON-ATR-ENTRY TO                             UCSD0102
184900*****                  DE0230-XTAT-FNDA (I)                       UCSD0102
185000*****   ELSE                                                      UCSD0102
185100*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         UCSD0102
185200*****                  DE0230-XTAT-FNDA (I)                       UCSD0102
185300*****      MOVE DE0230-XTAT-FNDI (I) TO FAUR-FUND                 UCSD0102
185500*****   END-IF                                                    UCSD0102
185600*****ELSE                                                         UCSD0102
185610*****   IF NOT CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)           UCSD0102
185800*****      SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUEUCSD0102
185900*****   END-IF                                                    UCSD0102
186000*****END-IF.                                                      UCSD0102
186100*****                                                             UCSD0102
186500*****MOVE UCCOMMON-CLR-ENTRY TO                                   UCSD0102
186600*****                 DE0230-XTAT-PROJECT-CODEC (I)               UCSD0102
186700*****MOVE UCCOMMON-HLT-ENTRY TO                                   UCSD0102
186800*****                 DE0230-XTAT-PROJECT-CODEH (I)               UCSD0102
186900*****IF DE0230-XTAT-PROJECT-CODEL (I) > ZERO    OR                UCSD0102
187000*****   DE0230-XTAT-PROJECT-CODEF (I) = ERASE-EOF                 UCSD0102
187100*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      UCSD0102
187200*****   IF DE0230-XTAT-PROJECT-CODEF (I) = ERASE-EOF              UCSD0102
187300*****      OR (CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) = 0180          UCSD0102
187400*****          AND DE0040-LINE-COMMANDI (I) = 'R')                UCSD0102
187500*****      OR (DE0040-LINE-COMMANDI (I) = 'B'                     UCSD0102
187600*****      AND CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) NOT = 0040)     UCSD0102
187700*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       UCSD0102
187800*****      MOVE FAUR-PROJECT-CODE-HOLD TO FAUR-PROJECT-CODE       UCSD0102
188000*****      MOVE UCCOMMON-ATR-ENTRY TO                             UCSD0102
188100*****                  DE0230-XTAT-PROJECT-CODEA (I)              UCSD0102
188200*****   ELSE                                                      UCSD0102
188300*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         UCSD0102
188400*****                  DE0230-XTAT-PROJECT-CODEA (I)              UCSD0102
188500*****      MOVE DE0230-XTAT-PROJECT-CODEI (I) TO FAUR-PROJECT-CODEUCSD0102
188700*****   END-IF                                                    UCSD0102
188800*****ELSE                                                         UCSD0102
188810*****   IF NOT CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)           UCSD0102
189000*****      SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUEUCSD0102
189100*****   END-IF                                                    UCSD0102
189200*****END-IF.                                                      UCSD0102
189300                                                                  UCSD0102
182550     MOVE UCCOMMON-CLR-ENTRY TO                                   UCSD0102
182560                      DE0230-XTAT-IFIS-INDEXC (I)                 UCSD0102
182570     MOVE UCCOMMON-HLT-ENTRY TO                                   UCSD0102
182580                      DE0230-XTAT-IFIS-INDEXH (I)                 UCSD0102
182590     IF DE0230-XTAT-IFIS-INDEXL (I) > ZERO      OR                UCSD0102
182591        DE0230-XTAT-IFIS-INDEXF (I) = ERASE-EOF                   UCSD0102
182592        SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      UCSD0102
182593        IF DE0230-XTAT-IFIS-INDEXF (I) = ERASE-EOF                UCSD0102
182594           OR (CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) = 0180          UCSD0102
182595               AND DE0040-LINE-COMMANDI (I) = 'R')                UCSD0102
182596           OR (DE0040-LINE-COMMANDI (I) = 'B'                     UCSD0102
182597           AND CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) NOT = 0040)     UCSD0102
182598           SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       UCSD0102
182599           MOVE FAUR-INDEX-HOLD TO FAUR-INDEX                     UCSD0102
182599           MOVE FAUR-ORGANIZATION-HOLD                            UCSD0102
182599                                    TO FAUR-ORGANIZATION          UCSD0102
182599           MOVE FAUR-PROGRAM-HOLD TO FAUR-PROGRAM                 UCSD0102
182599           MOVE FAUR-FUND-HOLD        TO FAUR-FUND                UCSD0102
182599           MOVE FAUR-LOCATION-HOLD    TO FAUR-LOCATION            UCSD0102
182601           MOVE UCCOMMON-ATR-ENTRY TO                             UCSD0102
182602                       DE0230-XTAT-IFIS-INDEXA (I)                UCSD0102
182603        ELSE                                                      UCSD0102
182604           MOVE UCCOMMON-ATR-ENTRY-MDT TO                         UCSD0102
182605                       DE0230-XTAT-IFIS-INDEXA (I)                UCSD0102
182606           MOVE DE0230-XTAT-IFIS-INDEXI (I)                       UCSD0102
566100                         TO FAUR-INDEX                            UCSD0102
566110           MOVE DE0230-XTAT-SUBI (I)                              UCSD0102
566120                         TO FAUR-SUB-ACCOUNT                      UCSD0102
566200           MOVE FAUR-FAU TO F001-FAU                              UCSD0102
271142           CALL PGM-PPFAU001 USING PPFAU001-INTERFACE             UCSD0102
330334           IF F001-FAU-VALID                                      UCSD0102
330335              MOVE F001-FAU        TO CPWSFAUR                    UCSD0102
330337           ELSE                                                   UCSD0102
330339              MOVE SPACES          TO FAUR-FUND                   UCSD0102
330339                                      FAUR-ORGANIZATION           UCSD0102
330339                                      FAUR-PROGRAM                UCSD0102
330339                                      FAUR-LOCATION               UCSD0102
330340           END-IF                                                 UCSD0102
330341        END-IF                                                    UCSD0102
182609     ELSE                                                         UCSD0102
330342        IF NOT CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)           UCSD0102
182610           SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUEUCSD0102
182620        END-IF                                                    UCSD0102
182613     END-IF                                                       UCSD0102
182624                                                                  CPPDETLR
189700     MOVE UCCOMMON-CLR-ENTRY TO                                   CPPDETLR
189800                      DE0230-XTAT-SUBC (I)                        CPPDETLR
189900     MOVE UCCOMMON-HLT-ENTRY TO                                   CPPDETLR
190000                      DE0230-XTAT-SUBH (I)                        CPPDETLR
190100     IF DE0230-XTAT-SUBL (I) > ZERO    OR                         CPPDETLR
190200        DE0230-XTAT-SUBF (I) = ERASE-EOF                          CPPDETLR
190300        SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDETLR
190400        IF DE0230-XTAT-SUBF (I) = ERASE-EOF                       CPPDETLR
190500           OR (CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) = 0180          CPPDETLR
190600               AND DE0040-LINE-COMMANDI (I) = 'R')                CPPDETLR
190700           OR (DE0040-LINE-COMMANDI (I) = 'B'                     CPPDETLR
190800           AND CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) NOT = 0040)     CPPDETLR
190900           SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDETLR
191000           MOVE FAUR-SUB-ACCOUNT-HOLD TO FAUR-SUB-ACCOUNT         CPPDETLR
191200           MOVE UCCOMMON-ATR-ENTRY TO                             CPPDETLR
191300                       DE0230-XTAT-SUBA (I)                       CPPDETLR
191400        ELSE                                                      CPPDETLR
191500           MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDETLR
191600                       DE0230-XTAT-SUBA (I)                       CPPDETLR
191700           MOVE DE0230-XTAT-SUBI (I) TO FAUR-SUB-ACCOUNT          CPPDETLR
191900        END-IF                                                    CPPDETLR
192400     ELSE                                                         CPPDETLR
192401        IF NOT CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)           CPPDETLR
192404           SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUECPPDETLR
192405        END-IF                                                    CPPDETLR
192406     END-IF.                                                      CPPDETLR
192407                                                                  CPPDETLR
192409     MOVE FAUR-FAU TO CPWSEDTH-ENTERED-DATA(CPWSEDTH-CNT).        CPPDETLR
192412     MOVE FAUR-FAU TO CPWSEDTH-TRN-DATA(CPWSEDTH-CNT).            CPPDETLR
228900                                                                  CPPDETLR
251800 CPPD-SET-VR-ERROR SECTION.                                       CPPDETLR
251900******************************************************************CPPDETLR
252000*  SET ERROR CODE AND HIGHLIGHTING FOR FIELDS WITH VALUE RANGE   *CPPDETLR
252100*  ERRORS RETURNED FROM PPEDTMGR                                 *CPPDETLR
252200******************************************************************CPPDETLR
252300                                                                  CPPDETLR
268400       SET CPWSEDTH-ELEM-ERRORS-FOUND(EDIT-SUB) TO TRUE.          CPPDETLR
268500       MOVE UCCOMMON-HLT-ERROR                                    CPPDETLR
268600*****            TO DE0230-XTAT-LOCH (I).                         UCSD0102
268601                 TO DE0230-XTAT-IFIS-INDEXH (I).                  UCSD0102
268700       MOVE UCCOMMON-ATR-ERROR-MDT                                CPPDETLR
268800*****            TO DE0230-XTAT-LOCA (I).                         UCSD0102
268601                 TO DE0230-XTAT-IFIS-INDEXA (I).                  UCSD0102
268900       MOVE UCCOMMON-CLR-ERROR                                    CPPDETLR
269000*****            TO DE0230-XTAT-LOCC (I).                         UCSD0102
268601                 TO DE0230-XTAT-IFIS-INDEXC (I).                  UCSD0102
269100       MOVE CPWSEDTH-SCREEN-DATA(EDIT-SUB)                        CPPDETLR
269200*****            TO DE0230-XTAT-LOCO (I).                         UCSD0102
572700                 TO FAUR-FAU                                      UCSD0102
572800       MOVE FAUR-INDEX TO DE0230-XTAT-IFIS-INDEXO (I).            UCSD0102
572900       MOVE FAUR-SUB-ACCOUNT TO DE0230-XTAT-SUBO (I).             UCSD0102
269300*****  MOVE  -1 TO DE0230-XTAT-LOCL (I)                           UCSD0102
573100       MOVE  -1 TO DE0230-XTAT-IFIS-INDEXL (I)                    UCSD0102
269400       SET ERRORS-FLAGGED TO TRUE.                                CPPDETLR
269500       SET UCCOMMON-EDIT-ERRORS-FOUND TO TRUE.                    CPPDETLR
269600                                                                  CPPDETLR
269700       MOVE CPWSSEVX-SEV-FATAL TO                                 CPPDETLR
269800                 HIGHEST-SEV OF CCWSTHFT-ITEM (I).                CPPDETLR
296100                                                                  CPPDETLR
299300 CPPD-EDIT-ERROR-ROUTINE    SECTION.                              CPPDETLR
299400******************************************************************CPPDETLR
299500*  SET ATTRIBUTES AND ERROR CODE FOR FIELDS IN ERROR             *CPPDETLR
299600******************************************************************CPPDETLR
315300       IF CPWSEDTH-ELEM-ERRORS-FOUND(EDIT-SUB)                    CPPDETLR
315400          MOVE CPWSEDTH-ELEM-SCREEN-OCCURNCE (EDIT-SUB) TO I      CPPDETLR
574400*****     MOVE UCCOMMON-HLT-ERROR                                 UCSD0102
574500*****            TO DE0230-XTAT-LOCH (I).                         UCSD0102
574600*****     MOVE UCCOMMON-ATR-ERROR-MDT                             UCSD0102
574700*****            TO DE0230-XTAT-LOCA (I).                         UCSD0102
574800*****     MOVE UCCOMMON-CLR-ERROR                                 UCSD0102
574900*****            TO DE0230-XTAT-LOCC (I).                         UCSD0102
575000          IF UCCOMMON-MSG-NUMBER = 'AU002'                        UCSD0102
575100             MOVE UCCOMMON-HLT-ERROR                              UCSD0102
315601                 TO DE0230-XTAT-IFIS-INDEXH (I)                   UCSD0102
575300             MOVE UCCOMMON-ATR-ERROR-MDT                          UCSD0102
315801                 TO DE0230-XTAT-IFIS-INDEXA (I)                   UCSD0102
575500             MOVE UCCOMMON-CLR-ERROR                              UCSD0102
316001                 TO DE0230-XTAT-IFIS-INDEXC (I)                   UCSD0102
575700             MOVE -1  TO DE0230-XTAT-IFIS-INDEXL (I)              UCSD0102
575800          ELSE                                                    UCSD0102
575900          IF UCCOMMON-MSG-NUMBER = 'AU001'                        UCSD0102
575910             MOVE UCCOMMON-HLT-ERROR                              UCSD0102
575920                 TO DE0230-XTAT-SUBH (I)                          UCSD0102
575930             MOVE UCCOMMON-ATR-ERROR-MDT                          UCSD0102
575940                 TO DE0230-XTAT-SUBA (I)                          UCSD0102
575950             MOVE UCCOMMON-CLR-ERROR                              UCSD0102
575960                 TO DE0230-XTAT-SUBC (I)                          UCSD0102
575970             MOVE -1  TO DE0230-XTAT-SUBL (I)                     UCSD0102
575980          END-IF                                                  UCSD0102
575990          END-IF                                                  UCSD0102
575991          MOVE CPWSEDTH-SCREEN-DATA(EDIT-SUB)                     CPPDETAP
316200*****            TO DE0230-XTAT-LOCO (I)                          UCSD0102
575993                 TO FAUR-FAU                                      UCSD0102
575994          MOVE FAUR-INDEX   TO DE0230-XTAT-IFIS-INDEXO (I)        UCSD0102
575995          MOVE FAUR-SUB-ACCOUNT TO DE0230-XTAT-SUBO (I)           UCSD0102
316300*****     MOVE  -1 TO DE0230-XTAT-LOCL (I)                        UCSD0102
316400               SET ERRORS-FLAGGED TO TRUE                         CPPDETLR
316500          MOVE CPWSSEVX-SEV-FATAL TO                              CPPDETLR
316600                 HIGHEST-SEV OF CCWSTHFT-ITEM (I)                 CPPDETLR
316700       END-IF.                                                    CPPDETLR
342900                                                                  CPPDETLR
343000 CPPD-UPDATE-TRANS         SECTION.                               CPPDETLR
343100******************************************************************CPPDETLR
343200*  UPDATE TRANSACTION WITH DATA IN CPWSEDTH ARRAY                *CPPDETLR
343300******************************************************************CPPDETLR
349800                                                                  CPPDETLR
350000       IF CPWSEDTH-ELEM-REQUEST-EDIT(EDIT-SUB)                    CPPDETLR
350100          MOVE CPWSEDTH-ELEM-SCREEN-OCCURNCE (EDIT-SUB) TO I      CPPDETLR
350200          MOVE CPWSEDTH-TRN-DATA(EDIT-SUB)                        CPPDETLR
350300                 TO XTAT-FAU OF CCWSTHFT-ITEM (I)                 CPPDETLR
350400       END-IF.                                                    CPPDETLR
361200                                                                  CPPDETLR
385000 CPPD-COMPLETE-LINE-COMMANDS SECTION.                             CPPDETLR
385100******************************************************************CPPDETLR
385200*  MOVE COPIED OR RETRIEVED TRANSACTIONS FROM THE HOLD AREA      *CPPDETLR
385300*  TO THE CURRENT TRANSACTION AREA                               *CPPDETLR
385400******************************************************************CPPDETLR
390500       MOVE UCCOMMON-ATR-ENTRY-MDT TO                             CPPDETLR
390600*****                  DE0230-XTAT-LOCA (J).                      UCSD0102
390600                       DE0230-XTAT-IFIS-INDEXA (J).               UCSD0102
390700*****  MOVE UCCOMMON-ATR-ENTRY-MDT TO                             CPPDETLR
390800*****                  DE0230-XTAT-ACCTA (J).                     CPPDETLR
390900*****  MOVE UCCOMMON-ATR-ENTRY-MDT TO                             CPPDETLR
391000*****                  DE0230-XTAT-COST-CENTERA (J).              CPPDETLR
391100*****  MOVE UCCOMMON-ATR-ENTRY-MDT TO                             CPPDETLR
391200*****                  DE0230-XTAT-FNDA (J).                      CPPDETLR
391300*****  MOVE UCCOMMON-ATR-ENTRY-MDT TO                             CPPDETLR
391400*****                  DE0230-XTAT-PROJECT-CODEA (J).             CPPDETLR
391500       MOVE UCCOMMON-ATR-ENTRY-MDT TO                             CPPDETLR
391600                       DE0230-XTAT-SUBA (J).                      CPPDETLR
401400                                                                  CPPDETLR
428800 CPPD-SET-ATTRIBUTES SECTION.                                     CPPDETLR
428900******************************************************************CPPDETLR
429000*  SET ATTRIBUTES FOR THE DATA ASSOCIATED WITH A MESSAGE         *CPPDETLR
429100******************************************************************CPPDETLR
429200                                                                  CPPDETLR
438700*****  MOVE -1   TO DE0230-XTAT-LOCL (WS-TRANS).                  UCSD0102
580100       MOVE -1   TO DE0230-XTAT-IFIS-INDEXL (WS-TRANS).           UCSD0102
438800       MOVE UCCOMMON-ATR-ERROR-MDT                                CPPDETLR
438900*****            TO DE0230-XTAT-LOCA (WS-TRANS).                  UCSD0102
438901                 TO DE0230-XTAT-IFIS-INDEXA (WS-TRANS).           UCSD0102
439000       MOVE UCCOMMON-CLR-ERROR                                    CPPDETLR
439100*****            TO DE0230-XTAT-LOCC (WS-TRANS).                  UCSD0102
438901                 TO DE0230-XTAT-IFIS-INDEXC (WS-TRANS).           UCSD0102
439200       MOVE UCCOMMON-HLT-ERROR                                    CPPDETLR
439300*****            TO DE0230-XTAT-LOCH (WS-TRANS).                  UCSD0102
438901                 TO DE0230-XTAT-IFIS-INDEXH (WS-TRANS).           UCSD0102
