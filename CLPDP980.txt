000010*==========================================================%      UCSD0006
000020*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD0006
000030*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD9999     =%      UCSD0006
000040*=                                                        =%      UCSD0006
000050*==========================================================%      UCSD0006
000600*==========================================================%      DS795
000700*=    COPY MEMBER: CLPDP980                               =%      DS795
000800*=    CHANGE # DS795        PROJ. REQUEST: LE/370         =%      DS795
000900*=                                         CONVERSION     =%      DS795
001000*=    NAME: G. CHIU         MODIFICATION DATE: 10/19/95   =%      DS795
001100*=                                                        =%      DS795
001200*=    DESCRIPTION:                                        =%      DS795
001300*=     REPLACED TIME 'ACCEPT' STATEMENT WITH              =%      DS795
001400*=     LE/370 VERSION OF CPPDTIME                         =%      DS795
001500*==========================================================%      DS795
000060*==========================================================%      UCSD0006
000070*=                                                        =%      UCSD0006
000080*=    COPY MEMBER: CLPDP980                               =%      UCSD0006
000090*=    CHANGE #0006          PROJ. REQUEST: CONTROL REPORT =%      UCSD0006
000091*=    NAME: MARI MCGEE      MODIFICATION DATE: 04/26/91   =%      UCSD0006
000092*=                                                        =%      UCSD0006
000093*=    DESCRIPTION                                         =%      UCSD0006
000094*=     ADD SECTION TO PRINT-CONTROL-REPORT.               =%      UCSD0006
000095*=                                                        =%      UCSD0006
000096*==========================================================%      UCSD0006
00001 *                                                                 CLPDP980
00003 *                                                                 CLPDP980
000300*      CONTROL REPORT PPP980CR                                    CLPDP980
00004 *                                                                 CLPDP980
000500*                                                                 CLPDP980
000510*PRINT-CONTROL-REPORT.                                            UCSD0006
000520 PRINT-CONTROL-REPORT SECTION.                                    UCSD0006
00006                                                                   CLPDP980
00007      OPEN OUTPUT CONTROLREPORT.                                   CLPDP980
00008                                                                   CLPDP980
001000     MOVE 'PPP980CR'             TO CR-HL1-RPT.                   CLPDP980
00010      MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP980
00011      WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP980
00012                                                                   CLPDP980
00013      MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP980
00014      WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP980
00015                                                                   CLPDP980
00016      MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP980
00017      MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP980
00018      MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP980
00019      MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP980
00020      WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP980
00021                                                                   CLPDP980
00022      MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP980
00023      WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP980
00024                                                                   CLPDP980
00027      MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP980
00028      WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP980
00029                                                                   CLPDP980
00030      MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP980
00031      WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP980
00032                                                                   CLPDP980
005800*****ACCEPT TIME-WORK-AREA FROM TIME.                             DS795
005900     COPY CPPDTIME.                                               DS795
006000     MOVE PRE-EDIT-TIME          TO TIME-WORK-AREA.               DS795
00034      MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP980
00035      MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP980
00036      MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP980
00037      WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP980
00038                                                                   CLPDP980
00039      MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP980
00040      WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP980
00041 *                                                                 CLPDP980
00042 *  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP980
00043 *                                                                 CLPDP980
00044 *  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP980
00045 *  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP980
00046 *                                                                 CLPDP980
004600*                                                                 CLPDP980
00047      MOVE 'TFL FILE'             TO CR-DL9-FILE.                  CLPDP980
004800     MOVE SO-CREATED             TO CR-DL9-ACTION.                CLPDP980
004900     MOVE BJE-RECS-COUNT         TO CR-DL9-VALUE.                 CLPDP980
00050      MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP980
00051      WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP980
00052 *                                                                 CLPDP980
00059      MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP980
00060      WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP980
00061                                                                   CLPDP980
00062      CLOSE CONTROLREPORT.                                         CLPDP980
