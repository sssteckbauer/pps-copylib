000000**************************************************************/   36430821
000001*  COPYMEMBER: CPWSXIID                                      */   36430821
000002*  RELEASE: ___0821______ SERVICE REQUEST(S): _____3643____  */   36430821
000003*  NAME:_______AAC_______ MODIFICATION DATE:  __10/05/93____ */   36430821
000004*  DESCRIPTION:                                              */   36430821
000005*       ENTRY/UPDATE INTERFACE TO THE IID SYSTEM             */   36430821
000007*       THIS COPYMEMBER CARRIES THE EMPLOYEE RECORD VALUES   */   36430821
000008*       USED BY THE IID SYSTEM FROM THE FRONT END (EEID OR   */   36430821
000009*       PPAPEUKS) TO THE BACK END (PPAPEUFE AND PPAPEUUP)    */   36430821
000010**************************************************************/   36430821
001000*01  CPWSXIID       EXTERNAL.                                     CPWSXIID
001100     03  CPWSXIID-EXT-AREA                     PIC X(512).        CPWSXIID
001200     03  CPWSXIID-EXT-DATA   REDEFINES   CPWSXIID-EXT-AREA.       CPWSXIID
003800         05  CPWSXIID-FETCHED-DATA.                               CPWSXIID
003801             10 XIID-ORIGINAL-ID               PIC X(09).         CPWSXIID
003802             10 XIID-ORIGINAL-NAME             PIC X(26).         CPWSXIID
003803             10 XIID-ORIGINAL-NAME-FIRST       PIC X(30).         CPWSXIID
003804             10 XIID-ORIGINAL-NAME-LAST        PIC X(30).         CPWSXIID
003805             10 XIID-ORIGINAL-NAME-MI          PIC X(30).         CPWSXIID
003806             10 XIID-ORIGINAL-SSN              PIC X(09).         CPWSXIID
003807             10 XIID-ORIGINAL-BIRTHDATE        PIC X(10).         CPWSXIID
003808             10 XIID-ORIGINAL-SUFFIX           PIC X(10).         CPWSXIID
003810         05  CPWSXIID-ENTERED-DATA.                               CPWSXIID
003811             10 XIID-ENTERED-NAME              PIC X(26).         CPWSXIID
003812             10 XIID-ENTERED-NAME-FIRST        PIC X(30).         CPWSXIID
003820             10 XIID-ENTERED-NAME-LAST         PIC X(30).         CPWSXIID
003830             10 XIID-ENTERED-NAME-MI           PIC X(30).         CPWSXIID
003850             10 XIID-ENTERED-SSN               PIC X(09).         CPWSXIID
003860             10 XIID-ENTERED-BIRTHDATE         PIC X(10).         CPWSXIID
003870             10 XIID-ENTERED-SUFFIX            PIC X(10).         CPWSXIID
003900         05  CPWSXIID-ASSIGNED-DATA.                              CPWSXIID
004100             10 XIID-ASSIGNED-ID               PIC X(09).         CPWSXIID
004200             10 XIID-ASSIGNED-TIMESTAMP        PIC X(26).         CPWSXIID
004300         05  CPWSXIID-CHANGE-FLAG              PIC X(01).         CPWSXIID
004310             88 XIID-NEW-HIRE                     VALUES SPACES.  CPWSXIID
004400             88 XIID-CHANGE-MADE                  VALUE '1'.      CPWSXIID
004500             88 XIID-ID-ASSIGNED                  VALUE '2'.      CPWSXIID
004900             88 XIID-DUP-SSN                      VALUE '3'.      CPWSXIID
005000             88 XIID-OTHER-ERROR                  VALUE '4'.      CPWSXIID
005100             88 XIID-PARTIAL-MATCH                VALUE '5'.      CPWSXIID
005200         05  CPWSXIID-INITIALIZED-IND          PIC X(10).         CPWSXIID
005300             88 CPWSXIID-INITIALIZED           VALUE '*CPWSXIID*'.CPWSXIID
006200         05  FILLER                            PIC X(167).        CPWSXIID
