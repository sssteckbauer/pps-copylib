000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXDCI                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100*    COPYID=CPWSXDCI                                              CPWSXDCI
000200*01  DATE-CALC-INTERFACE.                                         30930413
000300     03  XDCI-BEGIN-DATE.                                         CPWSXDCI
000400         05  XDCI-BEGIN-YY        PIC XX.                         CPWSXDCI
000500         05  XDCI-BEGIN-MM        PIC XX.                         CPWSXDCI
000600         05  XDCI-BEGIN-DD        PIC XX.                         CPWSXDCI
000700     03  XDCI-END-DATE.                                           CPWSXDCI
000800         05  XDCI-END-YY          PIC XX.                         CPWSXDCI
000900         05  XDCI-END-MM          PIC XX.                         CPWSXDCI
001000         05  XDCI-END-DD          PIC XX.                         CPWSXDCI
001100     03  XDCI-CALENDAR-DAYS       PIC 9(4).                       CPWSXDCI
001200     03  XDCI-FISCAL-DAYS         PIC 9(4).                       CPWSXDCI
001300     03  XDCI-HOLIDAYS            PIC 9(4).                       CPWSXDCI
001400     03  XDCI-RETURN-CODE         PIC 9.                          CPWSXDCI
001500**         VALUES ARE: 0-NO ERRORS                                CPWSXDCI
001600**                     1-INVALID BEGIN DATE                       CPWSXDCI
001700**                     2-INVALID END DATE                         CPWSXDCI
001800**                     3-BEGIN DATE OUT OF TABLE RANGE            CPWSXDCI
001900**                     4-END DATE OUT TABLE RANGE                 CPWSXDCI
002000**                     5-END DATE PRECEDES BEGIN DATE             CPWSXDCI
002100**                     6-FISCAL YEAR END NOT FOUND                CPWSXDCI
002200**                     7-CONTROL FILE ERROR                       CPWSXDCI
