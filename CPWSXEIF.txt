000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXEIF                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   13520171
000200*  COPYMEMBER: CPWSXEIF                                      */   13520171
000300*  RELEASE # ___0171___   SERVICE REQUEST NO(S)____1352______*/   13520171
000400*  NAME ___JAL ________   MODIFICATION DATE ____11/15/85_____*/   13520171
000500*  DESCRIPTION                                               */   13520171
000600*     MODIFIED TO HANDLE 51 OCCURS OF IO-EDB-SEGMENTS-REQ    */   13520171
000700*     WHEN EXPANDED NUMBER OF EDB SEGMENTS FROM 40 TO 50  .  */   13520171
001100**************************************************************/   13520171
001101*    COPYID=CPWSXEIF                                              CPWSXEIF
001102*01  EDB-INTERFACE.                                               30930413
001103     03  IO-EDB-NOM-KEY.                                          CPWSXEIF
001104         05 IO-EDB-NOM-SS-NUM    PICTURE X(9).                    CPWSXEIF
001105         05 IO-EDB-NOM-SEG       PICTURE X(4).                    CPWSXEIF
001106     03  IO-EDB-NEXT-SS-NUM      PICTURE X(9).                    CPWSXEIF
001107     03  IO-EDB-ACTION           PICTURE X(12).                   CPWSXEIF
001108     03  IO-EDB-ERROR-CODE       PICTURE 99.                      CPWSXEIF
001109     03  IO-EDB-SEGMENT-LIST               VALUE ZEROS.           CPWSXEIF
001110         05 IO-EDB-SEGMENTS-REQ  PICTURE XX OCCURS 51.            13520171
